﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using TusharInventory.Entity.Models.Basic;
using TusharInventory.Entity.Models.Settings;

namespace TusharInventory.Reporitory.Dbcontext
{
     public static class ModelBuilderExtension
    {
        public static void Seed(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Role>().HasData(
               new Role()
               {
                   Id = 1,
                   Name = "Main Admin"
               },
               new Role()
               {
                   Id = 2,
                   Name = "School Employee"
               },
                new Role()
                {
                    Id = 3,
                    Name = "Bookstore Employee"
                },
                new Role()
                {
                    Id = 4,
                    Name = "Bookstore Manager"
                },
                new Role()
                {
                    Id = 5,
                    Name = "Purchase Department"
                },
                new Role()
                {
                    Id = 6,
                    Name = "Subcity Manager"
                }
               );


            modelBuilder.Entity<User>().HasData(
               new User()
               {
                   Id = -1,
                   FirstName = "Main",
                   LastName="Admin",
                   SecondName=" ",
                   Email = "admin@gmail.com",
                   Password = "123456",
                   RoleId = 1,
                   Photograph = "",
                   EmployeeId = "-",
                   SchoolId = null,
                   PhoneNumber = "",
                   WoredaId = null,
                   RegionId = null,
                   SubcityId = null,
                   IsActive=true,
                   IsApproved=true,
                   

               }

               );

            modelBuilder.Entity<HeadOffice>().HasData(
               new HeadOffice()
               {
                   Id = -1,
                   Name = "Main Office"

               }
               
               );

            modelBuilder.Entity<BudgetType>().HasData(
               new HeadOffice()
               {
                   Id = 1,
                   Name = "Govt. Fund"

               },
               new HeadOffice()
               {
                   Id = 2,
                   Name = "Non-Govt. Fund"

               }

               );


        }
    }
}
