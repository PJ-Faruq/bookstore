
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TusharInventory.Entity.Models.Basic;
using TusharInventory.Entity.Models.inventory;
using TusharInventory.Entity.Models.Operation;
using TusharInventory.Entity.Models.Settings;

namespace TusharInventory.Reporitory.Dbcontext
{
    public class TusharDbContext : DbContext
    {
        public TusharDbContext(DbContextOptions<TusharDbContext> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Seed();
            foreach (var relationship in modelBuilder.Model.GetEntityTypes().SelectMany(e => e.GetForeignKeys()))
            {
                relationship.DeleteBehavior = DeleteBehavior.Restrict;
            }
        }


        //settings start
        public DbSet<Role> Roles { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<UserActivity> UserActivities { get; set; }



        //basics start
        public DbSet<GradeLavel> GradeLavels { get; set; }
        public DbSet<Language> Languages { get; set; }
        public DbSet<Ownership> Ownerships { get; set; }
        public DbSet<SchoolLavel> SchoolLavels { get; set; }

        public DbSet<Subcity> Subcities { get; set; }
        public DbSet<Subject> Subjects { get; set; }
        public DbSet<Woreda> Woredas { get; set; }
        public DbSet<Woreda1> Woreda1s { get; set; }
        public DbSet<Region> Region { get; set; }

        public DbSet<Book> Books { get; set; }
        public DbSet<School> Schools { get; set; }
        public DbSet<HeadOffice> HeadOffices { get; set; }
        public DbSet<Supplier> Suppliers { get; set; }



        //operation start
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderLine> OrderLines { get; set; }

        public DbSet<Receive> Receives { get; set; }
        public DbSet<ReceiveLine> ReceiveLines { get; set; }

        public DbSet<Sale> Sales { get; set; }
        public DbSet<SaleLine> SaleLines { get; set; }


        public DbSet<Purchase> Purchases { get; set; }
        public DbSet<PurchaseLine> PurchaseLines { get; set; }

        public DbSet<BudgetType> BudgetTypes { get; set; }
        public DbSet<PurchaseBook> PurchaseBooks { get; set; }
        public DbSet<PurchaseBookLine> PurchaseBookLines { get; set; }

        public DbSet<SubcityOrder> SubcityOrders { get; set; }
        public DbSet<SubcityOrderLine> SubcityOrderLines { get; set; }

        public DbSet<Budget> Budgets { get; set; }
        

        public DbSet<BankReceiptVoucher> BankReceiptVouchers { get; set; }



        //inventory start
        public DbSet<HoStock> HoStocks { get; set; }
        public DbSet<SubcityStock> SubcityStocks { get; set; }
        public DbSet<SchoolStock> SchoolStocks { get; set; }
    }
}
