using Tushar.Repository.Service.Base;
using System;
using System.Collections.Generic;
using System.Text;
using TusharInventory.Entity.Models.Settings;
using TusharInventory.Entitiey.ViewModels;
using System.Threading.Tasks;

namespace TusharInventory.Reporitory.Service.IServices.Settings
{
    public interface IUserService : IBaseService<User>
    {
        UserVm Authenticate(string username, string password);

        Task<IEnumerable<User>> GetBookstoreEmployee();
        
    }
}
