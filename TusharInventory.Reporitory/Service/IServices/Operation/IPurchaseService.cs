﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Tushar.Repository.Service.Base;
using TusharInventory.Entity.Models.Operation;
using TusharInventory.Entity.Models.Settings;

namespace TusharInventory.Reporitory.Service.IServices.Operation
{
    public interface IPurchaseService : IBaseService<Purchase>
    {


        Task<IEnumerable<User>> GetBookstoreManager();
        Task<IEnumerable<User>> GetPurchaseEmployee();

    }
}
