﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Tushar.Repository.Service.Base;
using TusharInventory.Entity.Models.Operation;

namespace TusharInventory.Reporitory.Service.IServices.Operation
{
    public interface IOrderService : IBaseService<Order>
    {
        bool ChangeStatus(Order entity);
        bool UpdateStatus(Order entity);
        Task<IEnumerable<Order>> GetPendingAll();
    }
}
