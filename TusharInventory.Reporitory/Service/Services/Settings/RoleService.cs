﻿using Tushar.Repository.Service.Base;
using System;
using System.Collections.Generic;
using System.Text;
using TusharInventory.Reporitory.Service.Base;
using TusharInventory.Reporitory.Dbcontext;
using TusharInventory.Entity.Models.Settings;
using TusharInventory.Reporitory.Service.IServices.Settings;

namespace TusharInventory.Reporitory.Service.Services.Settings
{
    public class RoleService : BaseService<Role>, IRoleService
    {
        private readonly TusharDbContext _context;
        public RoleService(TusharDbContext context) : base(context)
        {
            _context = context;
        }
    }
}
