using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using TusharInventory.Reporitory.Service.Base;
using TusharInventory.Reporitory.Dbcontext;
using TusharInventory.Entity.Models.Settings;
using TusharInventory.Reporitory.Service.IServices.Settings;
using TusharInventory.Reporitory.Helpers;
using TusharInventory.Entitiey.ViewModels;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;

namespace TusharInventory.Reporitory.Service.Services.Settings
{
    public class UserService : BaseService<User>, IUserService
    {
        private readonly TusharDbContext _context;
        private readonly AppSettings _appSettings;


        public UserService(IOptions<AppSettings> appSettings, TusharDbContext context) : base(context)
        {
            _appSettings = appSettings.Value;
            _context = context;
        }

        public UserVm Authenticate(string username, string password)
        {
            var user = _context.Users.Where(x => x.Email == username && x.Password == password && x.IsActive && x.IsApproved).Select(x =>
            new UserVm
            {
                Id = x.Id,
                Email = x.Email,
               // Password = x.Password,
                RoleId = x.RoleId,
                Role = x.Role.Name,
                SchoolId=x.SchoolId
               
            }).FirstOrDefault();

            // return null if user not found
            if (user == null)
                return null;

            // authentication successful so generate jwt token
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Email, user.Email.ToString()),
                    new Claim(ClaimTypes.Role, user.Role)
                }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            user.Token = tokenHandler.WriteToken(token);

            // remove password before returning
            user.Password = null;

            return user;
        }

        public override async Task<IEnumerable<User>> GetManager()
        {
            return await Set.Include(x => x.Role).ToListAsync();
        }

        public override async Task<User> GetByIdAsync(long id)
        {
            return await Set.Include(x => x.Role)
                .FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task<IEnumerable<User>> GetBookstoreEmployee()
        {
            return await Set.Where(m => m.RoleId == 3).ToListAsync();
        }
    }
}
