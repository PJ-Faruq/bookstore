﻿using System;
using System.Collections.Generic;
using System.Text;
using TusharInventory.Entity.Models.inventory;
using TusharInventory.Reporitory.Dbcontext;
using TusharInventory.Reporitory.Service.Base;
using TusharInventory.Reporitory.Service.IServices.Inventory;

namespace TusharInventory.Reporitory.Service.Services.Inventory
{
    public class SchoolStockService : BaseService<SchoolStock>, ISchoolStockService
    {
        private readonly TusharDbContext _context;
        public SchoolStockService(TusharDbContext context) : base(context)
        {
            _context = context;
        }
    }
}
