﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TusharInventory.Entity.Models.Basic;
using TusharInventory.Reporitory.Dbcontext;
using TusharInventory.Reporitory.Service.Base;
using TusharInventory.Reporitory.Service.IServices.Basic;

namespace TusharInventory.Reporitory.Service.Services.Basic
{
    public class SchoolService : BaseService<School>, ISchoolService
    {
        private readonly TusharDbContext _context;
        public SchoolService(TusharDbContext context) : base(context)
        {
            _context = context;
        }

        public override async Task<IEnumerable<School>> GetManager()
        {
            return await Set.Include(x => x.SchoolSubcity)
                .Include(x => x.SchoolWoreda)
                .Include(x => x.SchoolLavel)
                .Include(x => x.SchoolOwnership).ToListAsync();
        }

        public override async Task<School> GetByIdAsync(long id)
        {
            return await Set.Include(x => x.SchoolSubcity)
                .Include(x => x.SchoolWoreda)
                .Include(x => x.SchoolLavel)
                .Include(x=>x.Woreda1)
                .Include(x => x.SchoolOwnership)
                .FirstOrDefaultAsync(x => x.Id == id);
        }
    }
}
