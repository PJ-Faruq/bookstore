﻿using System;
using System.Collections.Generic;
using System.Text;
using TusharInventory.Entity.Models.Basic;
using TusharInventory.Reporitory.Dbcontext;
using TusharInventory.Reporitory.Service.Base;
using TusharInventory.Reporitory.Service.IServices.Basic;

namespace TusharInventory.Reporitory.Service.Services.Basic
{
    public class SubcityService : BaseService<Subcity>, ISubcityService
    {
        private readonly TusharDbContext _context;
        public SubcityService(TusharDbContext context) : base(context)
        {
            _context = context;
        }
    }
}
