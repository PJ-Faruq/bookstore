﻿using System;
using System.Collections.Generic;
using System.Text;
using TusharInventory.Entity.Models.Basic;
using TusharInventory.Reporitory.Dbcontext;
using TusharInventory.Reporitory.Service.Base;
using TusharInventory.Reporitory.Service.IServices.Basic;

namespace TusharInventory.Reporitory.Service.Services.Basic
{
    public class GradeLavelService : BaseService<GradeLavel>, IGradeLavelService
    {
        private readonly TusharDbContext _context;
        public GradeLavelService(TusharDbContext context) : base(context)
        {
            _context = context;
        }
    }
}
