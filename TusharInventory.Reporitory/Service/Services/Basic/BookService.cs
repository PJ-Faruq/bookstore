﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TusharInventory.Entity.Models.Basic;
using TusharInventory.Reporitory.Dbcontext;
using TusharInventory.Reporitory.Service.Base;
using TusharInventory.Reporitory.Service.IServices.Basic;

namespace TusharInventory.Reporitory.Service.Services.Basic
{
    public class BookService : BaseService<Book>, IBookService
    {
        private readonly TusharDbContext _context;
        public BookService(TusharDbContext context) : base(context)
        {
            _context = context;
        }

        public override async Task<IEnumerable<Book>> GetManager()
        {
            return await Set.Include(x => x.BookSubject).Include(x => x.BookLanguage).Include(x => x.BookGradeLavel).ToListAsync();
        }

        public override async Task<Book> GetByIdAsync(long id)
        {
            return await Set.Include(x => x.BookSubject).Include(x => x.BookLanguage).Include(x => x.BookGradeLavel)
                .FirstOrDefaultAsync(x => x.Id == id);
        }
    }
}
