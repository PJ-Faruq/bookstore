﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TusharInventory.Entity.Models.inventory;
using TusharInventory.Entity.Models.Operation;
using TusharInventory.Reporitory.Dbcontext;
using TusharInventory.Reporitory.Service.Base;
using TusharInventory.Reporitory.Service.IServices.Operation;

namespace TusharInventory.Reporitory.Service.Services.Operation
{
    public class OrderService : BaseService<Order>, IOrderService
    {
        private readonly TusharDbContext _context;
        public OrderService(TusharDbContext context) : base(context)
        {
            _context = context;
        }

        public override async Task<IEnumerable<Order>> GetManager()
        {
            return await Set.Include(x => x.School)
              .Include(x => x.Ho)
              .Include(x => x.Supplier).ToListAsync();
        }

        public async Task<IEnumerable<Order>> GetPendingAll()
        {
            return await Set.Include(x => x.School)
              .Include(x => x.Ho)
              .Include(x => x.Supplier).Where(x => x.Status == 2).ToListAsync();
        }

        public override async Task<Order> GetByIdAsync(long id)
        {
            var model = await Set.Include(x => x.School).ThenInclude(y=>y.SchoolOwnership)
              .Include(m=>m.SchoolEmployee)
              .Include(m=>m.BookstoreEmployee)
              .Include(m=>m.BookstoreManager)
              .Include(m=>m.Subcity)
              .Include(x => x.Ho)
              .Include(x => x.Supplier).FirstOrDefaultAsync(c => c.Id == id);

            await _context.Entry(model)
                 .Collection(c => c.OrderLines)
                 .Query()
                 .Include(x => x.BookSubject)
                 .Include(x => x.BookGradeLavel)
                 .Include(x => x.BookLanguage)
                 .LoadAsync();

            if (model != null && model.OrderLines != null && model.OrderLines.Any())
            {
                foreach (var orderLine in model.OrderLines)
                {
                    orderLine.AvailableQty = 0;
                    var hoStock = _context.HoStocks.Where( x => x.BookSubjectId == orderLine.BookSubjectId && x.BookGradeId == orderLine.BookGradeLavelId && x.BookLanguageId == orderLine.BookLanguageId && x.HoId == -1).FirstOrDefault();

                    if (hoStock != null)
                    {
                        orderLine.AvailableQty = hoStock.AvailableQty-hoStock.AllocatedQty;
                    }
                }
            }

            return model;
        }
        public override bool Update(Order entity)
        {
            var oldLines = _context.OrderLines.Where(x => x.OrderId == entity.Id).ToList();
            if (oldLines != null && oldLines.Any())
            {
                _context.RemoveRange(oldLines);
            }

            if (entity.OrderLines != null && entity.OrderLines.Any())
            {
                foreach (var tranLine in entity.OrderLines)
                {
                    tranLine.Order = null;
                    tranLine.BookSubject = null;
                    tranLine.BookGradeLavel = null;
                    tranLine.BookLanguage = null;
                }
                _context.AddRange(entity.OrderLines);
            }
            _context.Entry(entity).State = EntityState.Modified;

            return _context.SaveChanges() > 0;
        }
        public bool ChangeStatus(Order entity)
        {
            entity.OrderLines = null;
            _context.Entry(entity).State = EntityState.Modified;

            return _context.SaveChanges() > 0;
        }
        public override bool Remove(Order entity)
        {
            var oldLines = _context.OrderLines.Where(x => x.OrderId == entity.Id).ToList();
            if (oldLines != null && oldLines.Any())
            {
                _context.RemoveRange(oldLines);
            }
            _context.Remove(entity);
            return _context.SaveChanges() > 0;

        }

        public bool UpdateStatus(Order entity)
        {
            entity.OrderLines = null;
            _context.Entry(entity).State = EntityState.Modified;
            return _context.SaveChanges() > 0;
        }

        
    }
}
