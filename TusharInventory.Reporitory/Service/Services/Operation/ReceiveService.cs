﻿using System;
using System.Collections.Generic;
using System.Text;
using TusharInventory.Entity.Models.Operation;
using TusharInventory.Reporitory.Dbcontext;
using TusharInventory.Reporitory.Service.Base;
using TusharInventory.Reporitory.Service.IServices.Operation;

namespace TusharInventory.Reporitory.Service.Services.Operation
{
    public class ReceiveService : BaseService<Receive>, IReceiveService
    {
        private readonly TusharDbContext _context;
        public ReceiveService(TusharDbContext context) : base(context)
        {
            _context = context;
        }
    }
}
