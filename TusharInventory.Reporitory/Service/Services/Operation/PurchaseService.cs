﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TusharInventory.Entity.Models.Operation;
using TusharInventory.Entity.Models.Settings;
using TusharInventory.Reporitory.Dbcontext;
using TusharInventory.Reporitory.Service.Base;
using TusharInventory.Reporitory.Service.IServices.Operation;

namespace TusharInventory.Reporitory.Service.Services.Operation
{
     public class PurchaseService : BaseService<Purchase>, IPurchaseService
    {

        private readonly TusharDbContext _context;
        public PurchaseService(TusharDbContext context) : base(context)
        {
            _context = context;
        }

        public override async  Task<IEnumerable<Purchase>> GetManager()
        {
            return await _context.Purchases
                .Include(c => c.BookstoreEmployee)
                .Include(m => m.PurcahseDeptEmployee)
                .Include(m => m.BookstoreManager)
                .Include(m=>m.PurchaseLines)
                .ToListAsync();
        }

        public override async Task<Purchase> GetByIdAsync(long id)
        {
            return await _context.Purchases
                .Include(c => c.BookstoreEmployee)
                .Include(m => m.PurcahseDeptEmployee)
                .Include(m => m.BookstoreManager)
                .Include(m => m.PurchaseLines).ThenInclude(m=>m.BookSubject)
                .Include(m => m.PurchaseLines).ThenInclude(m => m.BookGradeLavel)
                .Include(m => m.PurchaseLines).ThenInclude(m => m.BookLanguage)
                .FirstOrDefaultAsync(c => c.Id == id);
        }


        public async Task<IEnumerable<User>> GetBookstoreManager()
        {
           return  await _context.Users.Where(m => m.RoleId == 4).ToListAsync();
        }

        public async Task<IEnumerable<User>> GetPurchaseEmployee()
        {
            return await _context.Users.Where(m => m.RoleId == 5).ToListAsync();
        }
    }
}
