
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using TusharInventory.Entity.IModels;
using Tushar.Repository.Service.Base;

namespace TusharInventory.Reporitory.Service.Base
{
  public abstract class BaseService<T> : IBaseService<T> where T : class, IEntity
  {

    #region Config

    protected BaseService(DbContext context)
    {
      Db = context;
    }

    protected DbContext Db { get; set; }

    protected DbSet<T> Set => Db.Set<T>();

    #endregion

    public virtual IEnumerable<T> GetAll()
    {
      return Set.ToList();
    }

    public virtual async Task<IEnumerable<T>> GetManager()
    {
      return await Set.ToListAsync();
    }


    public virtual T GetById(long id)
    {
      return Set.FirstOrDefault(c => c.Id == id);
    }

    public virtual async Task<T> GetByIdAsync(long id)
    {
      return await Set.FirstOrDefaultAsync(c => c.Id == id);
    }

    public virtual bool Add(T entity)
    {
      Set.Add(entity);
      return Db.SaveChanges() > 0;
    }

    public virtual async Task<bool> AddAsync(T entity)
    {
      Set.Add(entity);
      //If you doesn't use Unit Of Work, please un comment the bellow line
      return await Db.SaveChangesAsync() > 0;
    }




    public virtual bool AddRange(ICollection<T> entities)
    {
      Set.AddRange(entities);
      return true;
    }


    public virtual bool Update(T entity)
    {
      try
      {
        Set.Attach(entity);

        Db.Entry(entity).State = EntityState.Modified;
        return Db.SaveChanges() > 0;
      }
      catch (Exception ex)
      {
        return false;
      }
    }

    public virtual bool UpdateRange(ICollection<T> entities)
    {
      Set.UpdateRange(entities);
      return Db.SaveChanges() > 0;
    }

    public virtual bool Remove(T entity)
    {
      if (entity == null)
      {
        return false;
      }

      Set.Remove(entity);
      return Db.SaveChanges() > 0;
    }

    public virtual bool RemoveRange(ICollection<T> entities)
    {
      var isDeleted = false;
      if (entities != null && entities.Count > 0)
      {
        Set.RemoveRange(entities);
        isDeleted = Db.SaveChanges() > 0;
      }
      return isDeleted;
    }


    public virtual int GetCount()
    {
      return Set.Count();
    }

    public virtual async Task<int> GetCountAsync()
    {
      return await Set.CountAsync();
    }

    public virtual void Dispose()
    {
      Db?.Dispose();
    }


  }
}
