﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TusharInventory.Reporitory.Migrations
{
    public partial class SubcityStockQty_Added : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Region",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Region",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.AddColumn<double>(
                name: "SubcityStockQty",
                table: "SubcityOrderLines",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "SubcityStockQty",
                table: "SubcityOrderLines");

            migrationBuilder.InsertData(
                table: "Region",
                columns: new[] { "Id", "NameFile" },
                values: new object[] { 1, null });

            migrationBuilder.InsertData(
                table: "Region",
                columns: new[] { "Id", "NameFile" },
                values: new object[] { 2, null });
        }
    }
}
