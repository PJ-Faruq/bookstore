﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TusharInventory.Reporitory.Migrations
{
    public partial class Order_SubcityManager_Added : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "SubcityManagerId",
                table: "Orders",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Orders_SubcityManagerId",
                table: "Orders",
                column: "SubcityManagerId");

            migrationBuilder.AddForeignKey(
                name: "FK_Orders_Users_SubcityManagerId",
                table: "Orders",
                column: "SubcityManagerId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Orders_Users_SubcityManagerId",
                table: "Orders");

            migrationBuilder.DropIndex(
                name: "IX_Orders_SubcityManagerId",
                table: "Orders");

            migrationBuilder.DropColumn(
                name: "SubcityManagerId",
                table: "Orders");
        }
    }
}
