﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TusharInventory.Reporitory.Migrations
{
    public partial class PurchaseDateSetToNotNull : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<double>(
                name: "SubcityStockQty",
                table: "SubcityOrderLines",
                nullable: true,
                oldClrType: typeof(double),
                oldType: "float");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<double>(
                name: "SubcityStockQty",
                table: "SubcityOrderLines",
                type: "float",
                nullable: false,
                oldClrType: typeof(double),
                oldNullable: true);
        }
    }
}
