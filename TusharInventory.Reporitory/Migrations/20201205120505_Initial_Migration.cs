﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TusharInventory.Reporitory.Migrations
{
    public partial class Initial_Migration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "BudgetTypes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BudgetTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "GradeLavels",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GradeLavels", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "HeadOffices",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HeadOffices", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Languages",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Languages", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Ownerships",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    NameFile = table.Column<byte[]>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Ownerships", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Region",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    NameFile = table.Column<byte[]>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Region", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Roles",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Roles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "SchoolLavels",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    NameFile = table.Column<byte[]>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SchoolLavels", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Subcities",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    NameFile = table.Column<byte[]>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Subcities", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Subjects",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Subjects", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Suppliers",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: false),
                    Mobile = table.Column<string>(nullable: true),
                    Phone = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    Contact = table.Column<string>(nullable: true),
                    Fax = table.Column<string>(nullable: true),
                    City = table.Column<string>(nullable: true),
                    State = table.Column<string>(nullable: true),
                    ZipCode = table.Column<string>(nullable: true),
                    Country = table.Column<string>(nullable: true),
                    Address = table.Column<string>(nullable: true),
                    Address2 = table.Column<string>(nullable: true),
                    Details = table.Column<string>(nullable: true),
                    PreviousCreditBalance = table.Column<double>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Suppliers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "UserActivities",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserId = table.Column<int>(nullable: false),
                    Url = table.Column<string>(nullable: true),
                    UserName = table.Column<string>(nullable: true),
                    Data = table.Column<string>(nullable: true),
                    Date = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserActivities", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Woreda1s",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    NameFile = table.Column<byte[]>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Woreda1s", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Woredas",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    NameFile = table.Column<byte[]>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Woredas", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Budgets",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    BudgetTypeId = table.Column<int>(nullable: true),
                    Amount = table.Column<double>(nullable: false),
                    BudgetDate = table.Column<DateTime>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    CompanyName = table.Column<string>(nullable: true),
                    CompanyAddress = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Budgets", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Budgets_BudgetTypes_BudgetTypeId",
                        column: x => x.BudgetTypeId,
                        principalTable: "BudgetTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Books",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    BookSubjectId = table.Column<int>(nullable: false),
                    BookLanguageId = table.Column<int>(nullable: false),
                    BookGradeLavelId = table.Column<int>(nullable: false),
                    PurchasePrice = table.Column<double>(nullable: false),
                    SalePrice = table.Column<double>(nullable: false),
                    PageNo = table.Column<double>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Books", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Books_GradeLavels_BookGradeLavelId",
                        column: x => x.BookGradeLavelId,
                        principalTable: "GradeLavels",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Books_Languages_BookLanguageId",
                        column: x => x.BookLanguageId,
                        principalTable: "Languages",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Books_Subjects_BookSubjectId",
                        column: x => x.BookSubjectId,
                        principalTable: "Subjects",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "HoStocks",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedBy = table.Column<string>(nullable: true),
                    CreateDate = table.Column<DateTime>(nullable: false),
                    LastModifiedBy = table.Column<string>(nullable: true),
                    LastModifiedDate = table.Column<DateTime>(nullable: false),
                    HoId = table.Column<int>(nullable: false),
                    BookSubjectId = table.Column<int>(nullable: false),
                    BookGradeId = table.Column<int>(nullable: false),
                    BookLanguageId = table.Column<int>(nullable: false),
                    AvailableQty = table.Column<double>(nullable: false),
                    AllocatedQty = table.Column<double>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HoStocks", x => x.Id);
                    table.ForeignKey(
                        name: "FK_HoStocks_GradeLavels_BookGradeId",
                        column: x => x.BookGradeId,
                        principalTable: "GradeLavels",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_HoStocks_Languages_BookLanguageId",
                        column: x => x.BookLanguageId,
                        principalTable: "Languages",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_HoStocks_Subjects_BookSubjectId",
                        column: x => x.BookSubjectId,
                        principalTable: "Subjects",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_HoStocks_HeadOffices_HoId",
                        column: x => x.HoId,
                        principalTable: "HeadOffices",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "SubcityStocks",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedBy = table.Column<string>(nullable: true),
                    CreateDate = table.Column<DateTime>(nullable: false),
                    LastModifiedBy = table.Column<string>(nullable: true),
                    LastModifiedDate = table.Column<DateTime>(nullable: false),
                    SubcityId = table.Column<int>(nullable: false),
                    BookSubjectId = table.Column<int>(nullable: false),
                    BookGradeId = table.Column<int>(nullable: false),
                    BookLanguageId = table.Column<int>(nullable: false),
                    AvailableQty = table.Column<double>(nullable: false),
                    AllocatedQty = table.Column<double>(nullable: false),
                    IsRequested = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SubcityStocks", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SubcityStocks_GradeLavels_BookGradeId",
                        column: x => x.BookGradeId,
                        principalTable: "GradeLavels",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SubcityStocks_Languages_BookLanguageId",
                        column: x => x.BookLanguageId,
                        principalTable: "Languages",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SubcityStocks_Subjects_BookSubjectId",
                        column: x => x.BookSubjectId,
                        principalTable: "Subjects",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SubcityStocks_Subcities_SubcityId",
                        column: x => x.SubcityId,
                        principalTable: "Subcities",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Schools",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    NameFile = table.Column<byte[]>(nullable: true),
                    NumberOfStudent = table.Column<double>(nullable: true),
                    SchoolSubcityId = table.Column<int>(nullable: true),
                    SchoolWoredaId = table.Column<int>(nullable: true),
                    SchoolLavelId = table.Column<int>(nullable: true),
                    SchoolOwnershipId = table.Column<int>(nullable: true),
                    Woreda1Id = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Schools", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Schools_SchoolLavels_SchoolLavelId",
                        column: x => x.SchoolLavelId,
                        principalTable: "SchoolLavels",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Schools_Ownerships_SchoolOwnershipId",
                        column: x => x.SchoolOwnershipId,
                        principalTable: "Ownerships",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Schools_Subcities_SchoolSubcityId",
                        column: x => x.SchoolSubcityId,
                        principalTable: "Subcities",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Schools_Woredas_SchoolWoredaId",
                        column: x => x.SchoolWoredaId,
                        principalTable: "Woredas",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Schools_Woreda1s_Woreda1Id",
                        column: x => x.Woreda1Id,
                        principalTable: "Woreda1s",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "SchoolStocks",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedBy = table.Column<string>(nullable: true),
                    CreateDate = table.Column<DateTime>(nullable: false),
                    LastModifiedBy = table.Column<string>(nullable: true),
                    LastModifiedDate = table.Column<DateTime>(nullable: false),
                    SchoolId = table.Column<int>(nullable: false),
                    BookSubjectId = table.Column<int>(nullable: false),
                    BookGradeId = table.Column<int>(nullable: false),
                    BookLanguageId = table.Column<int>(nullable: false),
                    Quantity = table.Column<double>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SchoolStocks", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SchoolStocks_GradeLavels_BookGradeId",
                        column: x => x.BookGradeId,
                        principalTable: "GradeLavels",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SchoolStocks_Languages_BookLanguageId",
                        column: x => x.BookLanguageId,
                        principalTable: "Languages",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SchoolStocks_Subjects_BookSubjectId",
                        column: x => x.BookSubjectId,
                        principalTable: "Subjects",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SchoolStocks_Schools_SchoolId",
                        column: x => x.SchoolId,
                        principalTable: "Schools",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FirstName = table.Column<string>(nullable: true),
                    SecondName = table.Column<string>(nullable: true),
                    LastName = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: false),
                    Password = table.Column<string>(nullable: false),
                    RoleId = table.Column<int>(nullable: false),
                    Photograph = table.Column<string>(nullable: true),
                    EmployeeId = table.Column<string>(nullable: true),
                    SchoolId = table.Column<int>(nullable: true),
                    Address = table.Column<string>(nullable: true),
                    IsActive = table.Column<bool>(nullable: false),
                    IsApproved = table.Column<bool>(nullable: false),
                    PhoneNumber = table.Column<string>(nullable: true),
                    WoredaId = table.Column<int>(nullable: true),
                    RegionId = table.Column<int>(nullable: true),
                    SubcityId = table.Column<int>(nullable: true),
                    SchoolLavelId = table.Column<int>(nullable: true),
                    OwnershipId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Users_Ownerships_OwnershipId",
                        column: x => x.OwnershipId,
                        principalTable: "Ownerships",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Users_Region_RegionId",
                        column: x => x.RegionId,
                        principalTable: "Region",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Users_Roles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "Roles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Users_Schools_SchoolId",
                        column: x => x.SchoolId,
                        principalTable: "Schools",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Users_SchoolLavels_SchoolLavelId",
                        column: x => x.SchoolLavelId,
                        principalTable: "SchoolLavels",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Users_Subcities_SubcityId",
                        column: x => x.SubcityId,
                        principalTable: "Subcities",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Users_Woredas_WoredaId",
                        column: x => x.WoredaId,
                        principalTable: "Woredas",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Orders",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedBy = table.Column<string>(nullable: true),
                    CreateDate = table.Column<DateTime>(nullable: false),
                    LastModifiedBy = table.Column<string>(nullable: true),
                    LastModifiedDate = table.Column<DateTime>(nullable: false),
                    OrderId = table.Column<string>(nullable: true),
                    IsSchool = table.Column<bool>(nullable: false),
                    SchoolId = table.Column<int>(nullable: true),
                    SchoolEmployeeId = table.Column<int>(nullable: true),
                    BookstoreManagerId = table.Column<int>(nullable: true),
                    BookstoreEmployeeId = table.Column<int>(nullable: true),
                    HoId = table.Column<int>(nullable: true),
                    SupplierId = table.Column<int>(nullable: true),
                    SubcityId = table.Column<int>(nullable: true),
                    OrderDate = table.Column<DateTime>(nullable: false),
                    Amount = table.Column<double>(nullable: false),
                    Note = table.Column<string>(nullable: true),
                    Status = table.Column<int>(nullable: true),
                    BookstoreEmployeeStatus = table.Column<int>(nullable: true),
                    BookstoreManagerStatus = table.Column<int>(nullable: true),
                    IsRequestedToManger = table.Column<bool>(nullable: false),
                    IsRequestedToPurchase = table.Column<bool>(nullable: false),
                    IsCollected = table.Column<bool>(nullable: false),
                    TrackingNo = table.Column<string>(nullable: true),
                    ReasonForReject = table.Column<string>(nullable: true),
                    ApprovalInfo = table.Column<string>(nullable: true),
                    File = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Orders", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Orders_Users_BookstoreEmployeeId",
                        column: x => x.BookstoreEmployeeId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Orders_Users_BookstoreManagerId",
                        column: x => x.BookstoreManagerId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Orders_HeadOffices_HoId",
                        column: x => x.HoId,
                        principalTable: "HeadOffices",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Orders_Users_SchoolEmployeeId",
                        column: x => x.SchoolEmployeeId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Orders_Schools_SchoolId",
                        column: x => x.SchoolId,
                        principalTable: "Schools",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Orders_Subcities_SubcityId",
                        column: x => x.SubcityId,
                        principalTable: "Subcities",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Orders_Suppliers_SupplierId",
                        column: x => x.SupplierId,
                        principalTable: "Suppliers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Purchases",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedBy = table.Column<string>(nullable: true),
                    CreateDate = table.Column<DateTime>(nullable: false),
                    LastModifiedBy = table.Column<string>(nullable: true),
                    LastModifiedDate = table.Column<DateTime>(nullable: false),
                    BookstoreEmployeeId = table.Column<int>(nullable: false),
                    BookstoreManagerId = table.Column<int>(nullable: true),
                    PurcahseDeptEmployeeId = table.Column<int>(nullable: true),
                    Date = table.Column<DateTime>(nullable: true),
                    Status = table.Column<int>(nullable: false),
                    Amount = table.Column<double>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Purchases", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Purchases_Users_BookstoreEmployeeId",
                        column: x => x.BookstoreEmployeeId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Purchases_Users_BookstoreManagerId",
                        column: x => x.BookstoreManagerId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Purchases_Users_PurcahseDeptEmployeeId",
                        column: x => x.PurcahseDeptEmployeeId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "SubcityOrders",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    RequestId = table.Column<string>(nullable: true),
                    Date = table.Column<DateTime>(nullable: false),
                    BookstoreManagerId = table.Column<int>(nullable: false),
                    BookstoreEmployeeId = table.Column<int>(nullable: true),
                    SubcityManagerId = table.Column<int>(nullable: false),
                    SubcityId = table.Column<int>(nullable: true),
                    Status = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SubcityOrders", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SubcityOrders_Users_BookstoreEmployeeId",
                        column: x => x.BookstoreEmployeeId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SubcityOrders_Users_BookstoreManagerId",
                        column: x => x.BookstoreManagerId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SubcityOrders_Subcities_SubcityId",
                        column: x => x.SubcityId,
                        principalTable: "Subcities",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SubcityOrders_Users_SubcityManagerId",
                        column: x => x.SubcityManagerId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "OrderLines",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    OrderId = table.Column<int>(nullable: false),
                    BookSubjectId = table.Column<int>(nullable: false),
                    BookGradeLavelId = table.Column<int>(nullable: false),
                    BookLanguageId = table.Column<int>(nullable: false),
                    Quantity = table.Column<double>(nullable: false),
                    BookPrice = table.Column<double>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OrderLines", x => x.Id);
                    table.ForeignKey(
                        name: "FK_OrderLines_GradeLavels_BookGradeLavelId",
                        column: x => x.BookGradeLavelId,
                        principalTable: "GradeLavels",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_OrderLines_Languages_BookLanguageId",
                        column: x => x.BookLanguageId,
                        principalTable: "Languages",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_OrderLines_Subjects_BookSubjectId",
                        column: x => x.BookSubjectId,
                        principalTable: "Subjects",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_OrderLines_Orders_OrderId",
                        column: x => x.OrderId,
                        principalTable: "Orders",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Sales",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedBy = table.Column<string>(nullable: true),
                    CreateDate = table.Column<DateTime>(nullable: false),
                    LastModifiedBy = table.Column<string>(nullable: true),
                    LastModifiedDate = table.Column<DateTime>(nullable: false),
                    HoId = table.Column<int>(nullable: false),
                    OrderId = table.Column<int>(nullable: false),
                    SaleDate = table.Column<DateTime>(nullable: false),
                    Amount = table.Column<double>(nullable: false),
                    Note = table.Column<string>(nullable: true),
                    Status = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Sales", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Sales_HeadOffices_HoId",
                        column: x => x.HoId,
                        principalTable: "HeadOffices",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Sales_Orders_OrderId",
                        column: x => x.OrderId,
                        principalTable: "Orders",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "PurchaseBooks",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedBy = table.Column<string>(nullable: true),
                    CreateDate = table.Column<DateTime>(nullable: false),
                    LastModifiedBy = table.Column<string>(nullable: true),
                    LastModifiedDate = table.Column<DateTime>(nullable: false),
                    PurchaseRequestId = table.Column<int>(nullable: true),
                    BudgetTypeId = table.Column<int>(nullable: true),
                    SupplierId = table.Column<int>(nullable: false),
                    PurchaseId = table.Column<string>(nullable: true),
                    BookstoreEmployeeId = table.Column<int>(nullable: true),
                    BookstoreManagerId = table.Column<int>(nullable: true),
                    PurcahseDeptEmployeeId = table.Column<int>(nullable: false),
                    PurchaseDate = table.Column<DateTime>(nullable: true),
                    Amount = table.Column<double>(nullable: false),
                    TransactionTrackingNumber = table.Column<string>(nullable: true),
                    ReferenceNumber = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PurchaseBooks", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PurchaseBooks_Users_BookstoreEmployeeId",
                        column: x => x.BookstoreEmployeeId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PurchaseBooks_Users_BookstoreManagerId",
                        column: x => x.BookstoreManagerId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PurchaseBooks_BudgetTypes_BudgetTypeId",
                        column: x => x.BudgetTypeId,
                        principalTable: "BudgetTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PurchaseBooks_Users_PurcahseDeptEmployeeId",
                        column: x => x.PurcahseDeptEmployeeId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PurchaseBooks_Purchases_PurchaseRequestId",
                        column: x => x.PurchaseRequestId,
                        principalTable: "Purchases",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PurchaseBooks_Suppliers_SupplierId",
                        column: x => x.SupplierId,
                        principalTable: "Suppliers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "PurchaseLines",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    PurchaseId = table.Column<int>(nullable: false),
                    BookSubjectId = table.Column<int>(nullable: false),
                    BookGradeLavelId = table.Column<int>(nullable: false),
                    BookLanguageId = table.Column<int>(nullable: false),
                    Quantity = table.Column<double>(nullable: false),
                    BookPrice = table.Column<double>(nullable: false),
                    AvailableQty = table.Column<double>(nullable: false),
                    PurchaseQuantity = table.Column<double>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PurchaseLines", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PurchaseLines_GradeLavels_BookGradeLavelId",
                        column: x => x.BookGradeLavelId,
                        principalTable: "GradeLavels",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PurchaseLines_Languages_BookLanguageId",
                        column: x => x.BookLanguageId,
                        principalTable: "Languages",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PurchaseLines_Subjects_BookSubjectId",
                        column: x => x.BookSubjectId,
                        principalTable: "Subjects",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PurchaseLines_Purchases_PurchaseId",
                        column: x => x.PurchaseId,
                        principalTable: "Purchases",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "SubcityOrderLines",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    SubcityOrderId = table.Column<int>(nullable: false),
                    BookSubjectId = table.Column<int>(nullable: false),
                    BookGradeLavelId = table.Column<int>(nullable: false),
                    BookLanguageId = table.Column<int>(nullable: false),
                    Quantity = table.Column<double>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SubcityOrderLines", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SubcityOrderLines_GradeLavels_BookGradeLavelId",
                        column: x => x.BookGradeLavelId,
                        principalTable: "GradeLavels",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SubcityOrderLines_Languages_BookLanguageId",
                        column: x => x.BookLanguageId,
                        principalTable: "Languages",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SubcityOrderLines_Subjects_BookSubjectId",
                        column: x => x.BookSubjectId,
                        principalTable: "Subjects",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SubcityOrderLines_SubcityOrders_SubcityOrderId",
                        column: x => x.SubcityOrderId,
                        principalTable: "SubcityOrders",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Receives",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedBy = table.Column<string>(nullable: true),
                    CreateDate = table.Column<DateTime>(nullable: false),
                    LastModifiedBy = table.Column<string>(nullable: true),
                    LastModifiedDate = table.Column<DateTime>(nullable: false),
                    IsSchool = table.Column<bool>(nullable: false),
                    OrderId = table.Column<int>(nullable: false),
                    SaleId = table.Column<int>(nullable: false),
                    SchoolId = table.Column<int>(nullable: true),
                    HoId = table.Column<int>(nullable: true),
                    ReceiveDate = table.Column<DateTime>(nullable: false),
                    Amount = table.Column<double>(nullable: false),
                    Note = table.Column<string>(nullable: true),
                    Status = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Receives", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Receives_HeadOffices_HoId",
                        column: x => x.HoId,
                        principalTable: "HeadOffices",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Receives_Orders_OrderId",
                        column: x => x.OrderId,
                        principalTable: "Orders",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Receives_Sales_SaleId",
                        column: x => x.SaleId,
                        principalTable: "Sales",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Receives_Schools_SchoolId",
                        column: x => x.SchoolId,
                        principalTable: "Schools",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "SaleLines",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    SaleId = table.Column<int>(nullable: false),
                    BookSubjectId = table.Column<int>(nullable: false),
                    BookGradeId = table.Column<int>(nullable: false),
                    BookLanguageId = table.Column<int>(nullable: false),
                    Quantity = table.Column<double>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SaleLines", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SaleLines_GradeLavels_BookGradeId",
                        column: x => x.BookGradeId,
                        principalTable: "GradeLavels",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SaleLines_Languages_BookLanguageId",
                        column: x => x.BookLanguageId,
                        principalTable: "Languages",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SaleLines_Subjects_BookSubjectId",
                        column: x => x.BookSubjectId,
                        principalTable: "Subjects",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SaleLines_Sales_SaleId",
                        column: x => x.SaleId,
                        principalTable: "Sales",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "PurchaseBookLines",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    PurchaseBookId = table.Column<int>(nullable: false),
                    BookSubjectId = table.Column<int>(nullable: false),
                    BookGradeLavelId = table.Column<int>(nullable: false),
                    BookLanguageId = table.Column<int>(nullable: false),
                    BookPrice = table.Column<double>(nullable: false),
                    Quantity = table.Column<double>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PurchaseBookLines", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PurchaseBookLines_GradeLavels_BookGradeLavelId",
                        column: x => x.BookGradeLavelId,
                        principalTable: "GradeLavels",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PurchaseBookLines_Languages_BookLanguageId",
                        column: x => x.BookLanguageId,
                        principalTable: "Languages",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PurchaseBookLines_Subjects_BookSubjectId",
                        column: x => x.BookSubjectId,
                        principalTable: "Subjects",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PurchaseBookLines_PurchaseBooks_PurchaseBookId",
                        column: x => x.PurchaseBookId,
                        principalTable: "PurchaseBooks",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ReceiveLines",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ReceiveId = table.Column<int>(nullable: false),
                    BookSubjectId = table.Column<int>(nullable: false),
                    BookGradeId = table.Column<int>(nullable: false),
                    BookLanguageId = table.Column<int>(nullable: false),
                    OrderedQuantity = table.Column<double>(nullable: false),
                    ReceivedQuantity = table.Column<double>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ReceiveLines", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ReceiveLines_GradeLavels_BookGradeId",
                        column: x => x.BookGradeId,
                        principalTable: "GradeLavels",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ReceiveLines_Languages_BookLanguageId",
                        column: x => x.BookLanguageId,
                        principalTable: "Languages",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ReceiveLines_Subjects_BookSubjectId",
                        column: x => x.BookSubjectId,
                        principalTable: "Subjects",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ReceiveLines_Receives_ReceiveId",
                        column: x => x.ReceiveId,
                        principalTable: "Receives",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.InsertData(
                table: "BudgetTypes",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { 1, "Govt. Fund" },
                    { 2, "Non-Govt. Fund" }
                });

            migrationBuilder.InsertData(
                table: "HeadOffices",
                columns: new[] { "Id", "Name" },
                values: new object[] { -1, "Main Office" });

            migrationBuilder.InsertData(
                table: "Region",
                columns: new[] { "Id", "NameFile" },
                values: new object[,]
                {
                    { 1, null },
                    { 2, null }
                });

            migrationBuilder.InsertData(
                table: "Roles",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { 1, "Main Admin" },
                    { 2, "School Employee" },
                    { 3, "Bookstore Employee" },
                    { 4, "Bookstore Manager" },
                    { 5, "Purchase Department" },
                    { 6, "Subcity Manager" }
                });

            migrationBuilder.InsertData(
                table: "Suppliers",
                columns: new[] { "Id", "Address", "Address2", "City", "Contact", "Country", "Details", "Email", "Fax", "Mobile", "Name", "Phone", "PreviousCreditBalance", "State", "ZipCode" },
                values: new object[,]
                {
                    { 1, null, null, null, null, null, null, null, null, null, "Rahim", null, null, null, null },
                    { 2, null, null, null, null, null, null, null, null, null, "Rubel", null, null, null, null }
                });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "Address", "Email", "EmployeeId", "FirstName", "IsActive", "IsApproved", "LastName", "OwnershipId", "Password", "PhoneNumber", "Photograph", "RegionId", "RoleId", "SchoolId", "SchoolLavelId", "SecondName", "SubcityId", "WoredaId" },
                values: new object[] { -1, null, "admin@gmail.com", "-", "Main", true, true, "Admin", null, "123456", "", "", null, 1, null, null, " ", null, null });

            migrationBuilder.CreateIndex(
                name: "IX_Books_BookGradeLavelId",
                table: "Books",
                column: "BookGradeLavelId");

            migrationBuilder.CreateIndex(
                name: "IX_Books_BookLanguageId",
                table: "Books",
                column: "BookLanguageId");

            migrationBuilder.CreateIndex(
                name: "IX_Books_BookSubjectId",
                table: "Books",
                column: "BookSubjectId");

            migrationBuilder.CreateIndex(
                name: "IX_Budgets_BudgetTypeId",
                table: "Budgets",
                column: "BudgetTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_HoStocks_BookGradeId",
                table: "HoStocks",
                column: "BookGradeId");

            migrationBuilder.CreateIndex(
                name: "IX_HoStocks_BookLanguageId",
                table: "HoStocks",
                column: "BookLanguageId");

            migrationBuilder.CreateIndex(
                name: "IX_HoStocks_BookSubjectId",
                table: "HoStocks",
                column: "BookSubjectId");

            migrationBuilder.CreateIndex(
                name: "IX_HoStocks_HoId",
                table: "HoStocks",
                column: "HoId");

            migrationBuilder.CreateIndex(
                name: "IX_OrderLines_BookGradeLavelId",
                table: "OrderLines",
                column: "BookGradeLavelId");

            migrationBuilder.CreateIndex(
                name: "IX_OrderLines_BookLanguageId",
                table: "OrderLines",
                column: "BookLanguageId");

            migrationBuilder.CreateIndex(
                name: "IX_OrderLines_BookSubjectId",
                table: "OrderLines",
                column: "BookSubjectId");

            migrationBuilder.CreateIndex(
                name: "IX_OrderLines_OrderId",
                table: "OrderLines",
                column: "OrderId");

            migrationBuilder.CreateIndex(
                name: "IX_Orders_BookstoreEmployeeId",
                table: "Orders",
                column: "BookstoreEmployeeId");

            migrationBuilder.CreateIndex(
                name: "IX_Orders_BookstoreManagerId",
                table: "Orders",
                column: "BookstoreManagerId");

            migrationBuilder.CreateIndex(
                name: "IX_Orders_HoId",
                table: "Orders",
                column: "HoId");

            migrationBuilder.CreateIndex(
                name: "IX_Orders_SchoolEmployeeId",
                table: "Orders",
                column: "SchoolEmployeeId");

            migrationBuilder.CreateIndex(
                name: "IX_Orders_SchoolId",
                table: "Orders",
                column: "SchoolId");

            migrationBuilder.CreateIndex(
                name: "IX_Orders_SubcityId",
                table: "Orders",
                column: "SubcityId");

            migrationBuilder.CreateIndex(
                name: "IX_Orders_SupplierId",
                table: "Orders",
                column: "SupplierId");

            migrationBuilder.CreateIndex(
                name: "IX_PurchaseBookLines_BookGradeLavelId",
                table: "PurchaseBookLines",
                column: "BookGradeLavelId");

            migrationBuilder.CreateIndex(
                name: "IX_PurchaseBookLines_BookLanguageId",
                table: "PurchaseBookLines",
                column: "BookLanguageId");

            migrationBuilder.CreateIndex(
                name: "IX_PurchaseBookLines_BookSubjectId",
                table: "PurchaseBookLines",
                column: "BookSubjectId");

            migrationBuilder.CreateIndex(
                name: "IX_PurchaseBookLines_PurchaseBookId",
                table: "PurchaseBookLines",
                column: "PurchaseBookId");

            migrationBuilder.CreateIndex(
                name: "IX_PurchaseBooks_BookstoreEmployeeId",
                table: "PurchaseBooks",
                column: "BookstoreEmployeeId");

            migrationBuilder.CreateIndex(
                name: "IX_PurchaseBooks_BookstoreManagerId",
                table: "PurchaseBooks",
                column: "BookstoreManagerId");

            migrationBuilder.CreateIndex(
                name: "IX_PurchaseBooks_BudgetTypeId",
                table: "PurchaseBooks",
                column: "BudgetTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_PurchaseBooks_PurcahseDeptEmployeeId",
                table: "PurchaseBooks",
                column: "PurcahseDeptEmployeeId");

            migrationBuilder.CreateIndex(
                name: "IX_PurchaseBooks_PurchaseRequestId",
                table: "PurchaseBooks",
                column: "PurchaseRequestId");

            migrationBuilder.CreateIndex(
                name: "IX_PurchaseBooks_SupplierId",
                table: "PurchaseBooks",
                column: "SupplierId");

            migrationBuilder.CreateIndex(
                name: "IX_PurchaseLines_BookGradeLavelId",
                table: "PurchaseLines",
                column: "BookGradeLavelId");

            migrationBuilder.CreateIndex(
                name: "IX_PurchaseLines_BookLanguageId",
                table: "PurchaseLines",
                column: "BookLanguageId");

            migrationBuilder.CreateIndex(
                name: "IX_PurchaseLines_BookSubjectId",
                table: "PurchaseLines",
                column: "BookSubjectId");

            migrationBuilder.CreateIndex(
                name: "IX_PurchaseLines_PurchaseId",
                table: "PurchaseLines",
                column: "PurchaseId");

            migrationBuilder.CreateIndex(
                name: "IX_Purchases_BookstoreEmployeeId",
                table: "Purchases",
                column: "BookstoreEmployeeId");

            migrationBuilder.CreateIndex(
                name: "IX_Purchases_BookstoreManagerId",
                table: "Purchases",
                column: "BookstoreManagerId");

            migrationBuilder.CreateIndex(
                name: "IX_Purchases_PurcahseDeptEmployeeId",
                table: "Purchases",
                column: "PurcahseDeptEmployeeId");

            migrationBuilder.CreateIndex(
                name: "IX_ReceiveLines_BookGradeId",
                table: "ReceiveLines",
                column: "BookGradeId");

            migrationBuilder.CreateIndex(
                name: "IX_ReceiveLines_BookLanguageId",
                table: "ReceiveLines",
                column: "BookLanguageId");

            migrationBuilder.CreateIndex(
                name: "IX_ReceiveLines_BookSubjectId",
                table: "ReceiveLines",
                column: "BookSubjectId");

            migrationBuilder.CreateIndex(
                name: "IX_ReceiveLines_ReceiveId",
                table: "ReceiveLines",
                column: "ReceiveId");

            migrationBuilder.CreateIndex(
                name: "IX_Receives_HoId",
                table: "Receives",
                column: "HoId");

            migrationBuilder.CreateIndex(
                name: "IX_Receives_OrderId",
                table: "Receives",
                column: "OrderId");

            migrationBuilder.CreateIndex(
                name: "IX_Receives_SaleId",
                table: "Receives",
                column: "SaleId");

            migrationBuilder.CreateIndex(
                name: "IX_Receives_SchoolId",
                table: "Receives",
                column: "SchoolId");

            migrationBuilder.CreateIndex(
                name: "IX_SaleLines_BookGradeId",
                table: "SaleLines",
                column: "BookGradeId");

            migrationBuilder.CreateIndex(
                name: "IX_SaleLines_BookLanguageId",
                table: "SaleLines",
                column: "BookLanguageId");

            migrationBuilder.CreateIndex(
                name: "IX_SaleLines_BookSubjectId",
                table: "SaleLines",
                column: "BookSubjectId");

            migrationBuilder.CreateIndex(
                name: "IX_SaleLines_SaleId",
                table: "SaleLines",
                column: "SaleId");

            migrationBuilder.CreateIndex(
                name: "IX_Sales_HoId",
                table: "Sales",
                column: "HoId");

            migrationBuilder.CreateIndex(
                name: "IX_Sales_OrderId",
                table: "Sales",
                column: "OrderId");

            migrationBuilder.CreateIndex(
                name: "IX_Schools_SchoolLavelId",
                table: "Schools",
                column: "SchoolLavelId");

            migrationBuilder.CreateIndex(
                name: "IX_Schools_SchoolOwnershipId",
                table: "Schools",
                column: "SchoolOwnershipId");

            migrationBuilder.CreateIndex(
                name: "IX_Schools_SchoolSubcityId",
                table: "Schools",
                column: "SchoolSubcityId");

            migrationBuilder.CreateIndex(
                name: "IX_Schools_SchoolWoredaId",
                table: "Schools",
                column: "SchoolWoredaId");

            migrationBuilder.CreateIndex(
                name: "IX_Schools_Woreda1Id",
                table: "Schools",
                column: "Woreda1Id");

            migrationBuilder.CreateIndex(
                name: "IX_SchoolStocks_BookGradeId",
                table: "SchoolStocks",
                column: "BookGradeId");

            migrationBuilder.CreateIndex(
                name: "IX_SchoolStocks_BookLanguageId",
                table: "SchoolStocks",
                column: "BookLanguageId");

            migrationBuilder.CreateIndex(
                name: "IX_SchoolStocks_BookSubjectId",
                table: "SchoolStocks",
                column: "BookSubjectId");

            migrationBuilder.CreateIndex(
                name: "IX_SchoolStocks_SchoolId",
                table: "SchoolStocks",
                column: "SchoolId");

            migrationBuilder.CreateIndex(
                name: "IX_SubcityOrderLines_BookGradeLavelId",
                table: "SubcityOrderLines",
                column: "BookGradeLavelId");

            migrationBuilder.CreateIndex(
                name: "IX_SubcityOrderLines_BookLanguageId",
                table: "SubcityOrderLines",
                column: "BookLanguageId");

            migrationBuilder.CreateIndex(
                name: "IX_SubcityOrderLines_BookSubjectId",
                table: "SubcityOrderLines",
                column: "BookSubjectId");

            migrationBuilder.CreateIndex(
                name: "IX_SubcityOrderLines_SubcityOrderId",
                table: "SubcityOrderLines",
                column: "SubcityOrderId");

            migrationBuilder.CreateIndex(
                name: "IX_SubcityOrders_BookstoreEmployeeId",
                table: "SubcityOrders",
                column: "BookstoreEmployeeId");

            migrationBuilder.CreateIndex(
                name: "IX_SubcityOrders_BookstoreManagerId",
                table: "SubcityOrders",
                column: "BookstoreManagerId");

            migrationBuilder.CreateIndex(
                name: "IX_SubcityOrders_SubcityId",
                table: "SubcityOrders",
                column: "SubcityId");

            migrationBuilder.CreateIndex(
                name: "IX_SubcityOrders_SubcityManagerId",
                table: "SubcityOrders",
                column: "SubcityManagerId");

            migrationBuilder.CreateIndex(
                name: "IX_SubcityStocks_BookGradeId",
                table: "SubcityStocks",
                column: "BookGradeId");

            migrationBuilder.CreateIndex(
                name: "IX_SubcityStocks_BookLanguageId",
                table: "SubcityStocks",
                column: "BookLanguageId");

            migrationBuilder.CreateIndex(
                name: "IX_SubcityStocks_BookSubjectId",
                table: "SubcityStocks",
                column: "BookSubjectId");

            migrationBuilder.CreateIndex(
                name: "IX_SubcityStocks_SubcityId",
                table: "SubcityStocks",
                column: "SubcityId");

            migrationBuilder.CreateIndex(
                name: "IX_Users_OwnershipId",
                table: "Users",
                column: "OwnershipId");

            migrationBuilder.CreateIndex(
                name: "IX_Users_RegionId",
                table: "Users",
                column: "RegionId");

            migrationBuilder.CreateIndex(
                name: "IX_Users_RoleId",
                table: "Users",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "IX_Users_SchoolId",
                table: "Users",
                column: "SchoolId");

            migrationBuilder.CreateIndex(
                name: "IX_Users_SchoolLavelId",
                table: "Users",
                column: "SchoolLavelId");

            migrationBuilder.CreateIndex(
                name: "IX_Users_SubcityId",
                table: "Users",
                column: "SubcityId");

            migrationBuilder.CreateIndex(
                name: "IX_Users_WoredaId",
                table: "Users",
                column: "WoredaId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Books");

            migrationBuilder.DropTable(
                name: "Budgets");

            migrationBuilder.DropTable(
                name: "HoStocks");

            migrationBuilder.DropTable(
                name: "OrderLines");

            migrationBuilder.DropTable(
                name: "PurchaseBookLines");

            migrationBuilder.DropTable(
                name: "PurchaseLines");

            migrationBuilder.DropTable(
                name: "ReceiveLines");

            migrationBuilder.DropTable(
                name: "SaleLines");

            migrationBuilder.DropTable(
                name: "SchoolStocks");

            migrationBuilder.DropTable(
                name: "SubcityOrderLines");

            migrationBuilder.DropTable(
                name: "SubcityStocks");

            migrationBuilder.DropTable(
                name: "UserActivities");

            migrationBuilder.DropTable(
                name: "PurchaseBooks");

            migrationBuilder.DropTable(
                name: "Receives");

            migrationBuilder.DropTable(
                name: "SubcityOrders");

            migrationBuilder.DropTable(
                name: "GradeLavels");

            migrationBuilder.DropTable(
                name: "Languages");

            migrationBuilder.DropTable(
                name: "Subjects");

            migrationBuilder.DropTable(
                name: "BudgetTypes");

            migrationBuilder.DropTable(
                name: "Purchases");

            migrationBuilder.DropTable(
                name: "Sales");

            migrationBuilder.DropTable(
                name: "Orders");

            migrationBuilder.DropTable(
                name: "Users");

            migrationBuilder.DropTable(
                name: "HeadOffices");

            migrationBuilder.DropTable(
                name: "Suppliers");

            migrationBuilder.DropTable(
                name: "Region");

            migrationBuilder.DropTable(
                name: "Roles");

            migrationBuilder.DropTable(
                name: "Schools");

            migrationBuilder.DropTable(
                name: "SchoolLavels");

            migrationBuilder.DropTable(
                name: "Ownerships");

            migrationBuilder.DropTable(
                name: "Subcities");

            migrationBuilder.DropTable(
                name: "Woredas");

            migrationBuilder.DropTable(
                name: "Woreda1s");
        }
    }
}
