﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TusharInventory.Reporitory.Migrations
{
    public partial class BankReceipt_Added : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "BankReceiptVouchers",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    OrderId = table.Column<int>(nullable: true),
                    Date = table.Column<DateTime>(nullable: false),
                    SubcityId = table.Column<int>(nullable: false),
                    Kebele = table.Column<string>(nullable: true),
                    SchoolId = table.Column<int>(nullable: false),
                    BookstoreEmployeeId = table.Column<int>(nullable: false),
                    City = table.Column<string>(nullable: true),
                    HouseNumber = table.Column<string>(nullable: true),
                    Cash = table.Column<bool>(nullable: false),
                    Check = table.Column<bool>(nullable: false),
                    CheckNo = table.Column<string>(nullable: true),
                    DepositeSlip = table.Column<bool>(nullable: false),
                    ReferenceNo = table.Column<string>(nullable: true),
                    DepositeDate = table.Column<DateTime>(nullable: true),
                    BankTransfer = table.Column<bool>(nullable: false),
                    Amount = table.Column<double>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BankReceiptVouchers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BankReceiptVouchers_Users_BookstoreEmployeeId",
                        column: x => x.BookstoreEmployeeId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_BankReceiptVouchers_Orders_OrderId",
                        column: x => x.OrderId,
                        principalTable: "Orders",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_BankReceiptVouchers_Schools_SchoolId",
                        column: x => x.SchoolId,
                        principalTable: "Schools",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_BankReceiptVouchers_Subcities_SubcityId",
                        column: x => x.SubcityId,
                        principalTable: "Subcities",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_BankReceiptVouchers_BookstoreEmployeeId",
                table: "BankReceiptVouchers",
                column: "BookstoreEmployeeId");

            migrationBuilder.CreateIndex(
                name: "IX_BankReceiptVouchers_OrderId",
                table: "BankReceiptVouchers",
                column: "OrderId");

            migrationBuilder.CreateIndex(
                name: "IX_BankReceiptVouchers_SchoolId",
                table: "BankReceiptVouchers",
                column: "SchoolId");

            migrationBuilder.CreateIndex(
                name: "IX_BankReceiptVouchers_SubcityId",
                table: "BankReceiptVouchers",
                column: "SubcityId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "BankReceiptVouchers");
        }
    }
}
