import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './auth/auth.guard';
import { Subcity } from './models/basic/subcity';
import { AdminPanelComponent } from './regulars/components/admin-panel/admin-panel.component';
import { BookCreateComponent } from './regulars/components/admin-panel/book/book-create/book-create.component';
import { BookListComponent } from './regulars/components/admin-panel/book/book-list/book-list.component';
import { BookComponent } from './regulars/components/admin-panel/book/book.component';
import { BudgetCreateComponent } from './regulars/components/admin-panel/budget/budget-create/budget-create.component';
import { BudgetEditModalComponent } from './regulars/components/admin-panel/budget/budget-edit-modal/budget-edit-modal.component';
import { BudgetListComponent } from './regulars/components/admin-panel/budget/budget-list/budget-list.component';
import { BudgetComponent } from './regulars/components/admin-panel/budget/budget.component';
import { AdminDashboardComponent } from './regulars/components/admin-panel/dashboard/admin-dashboard/admin-dashboard.component';
import { BookstoreEmployeeDashboardComponent } from './regulars/components/admin-panel/dashboard/bookstore-employee-dashboard/bookstore-employee-dashboard.component';
import { DashboardComponent } from './regulars/components/admin-panel/dashboard/dashboard.component';
import { ManagerDashboardComponent } from './regulars/components/admin-panel/dashboard/manager-dashboard/manager-dashboard.component';
import { PurchaseDashboardComponent } from './regulars/components/admin-panel/dashboard/purchase-dashboard/purchase-dashboard.component';
import { SchoolDashboardComponent } from './regulars/components/admin-panel/dashboard/school-dashboard/school-dashboard.component';
import { SubcityStoreDashboardComponent } from './regulars/components/admin-panel/dashboard/subcity-store-dashboard/subcity-store-dashboard.component';
import { GradeLevelCreateComponent } from './regulars/components/admin-panel/grade-level/grade-level-create/grade-level-create.component';
import { GradeLevelEditModalComponent } from './regulars/components/admin-panel/grade-level/grade-level-edit-modal/grade-level-edit-modal.component';
import { GradeLevelListComponent } from './regulars/components/admin-panel/grade-level/grade-level-list/grade-level-list.component';
import { GradeLevelComponent } from './regulars/components/admin-panel/grade-level/grade-level.component';
import { LanguageCreateComponent } from './regulars/components/admin-panel/language/language-create/language-create.component';
import { LanguageEditModalComponent } from './regulars/components/admin-panel/language/language-edit-modal/language-edit-modal.component';
import { LanguageListComponent } from './regulars/components/admin-panel/language/language-list/language-list.component';
import { LanguageComponent } from './regulars/components/admin-panel/language/language.component';
import { OrderCreateComponent } from './regulars/components/admin-panel/order/order-create/order-create.component';
import { OrderListComponent } from './regulars/components/admin-panel/order/order-list/order-list.component';
import { OrderComponent } from './regulars/components/admin-panel/order/order.component';
import { PendingOrderListComponent } from './regulars/components/admin-panel/order/pending-order-list/pending-order-list.component';
import { PurchaseBookCreateComponent } from './regulars/components/admin-panel/purchase-book/purchase-book-create/purchase-book-create.component';
import { PurchaseBookListComponent } from './regulars/components/admin-panel/purchase-book/purchase-book-list/purchase-book-list.component';
import { PurchaseListComponent } from './regulars/components/admin-panel/purchase/purchase-list/purchase-list.component';
import { ReceiveCreateComponent } from './regulars/components/admin-panel/receive/receive-create/receive-create.component';
import { ReceiveListComponent } from './regulars/components/admin-panel/receive/receive-list/receive-list.component';
import { ReceiveComponent } from './regulars/components/admin-panel/receive/receive.component';
import { RegionCreateComponent } from './regulars/components/admin-panel/region/region-create/region-create.component';
import { RegionEditModalComponent } from './regulars/components/admin-panel/region/region-edit-modal/region-edit-modal.component';
import { RegionListComponent } from './regulars/components/admin-panel/region/region-list/region-list.component';
import { RegionComponent } from './regulars/components/admin-panel/region/region.component';
import { BankReceiptVoucherCreateComponent } from './regulars/components/admin-panel/report/bank-receipt-voucher/bank-receipt-voucher-create/bank-receipt-voucher-create.component';
import { BankReceiptVoucherDetailComponent } from './regulars/components/admin-panel/report/bank-receipt-voucher/bank-receipt-voucher-detail/bank-receipt-voucher-detail.component';
import { BankReceiptVoucherComponent } from './regulars/components/admin-panel/report/bank-receipt-voucher/bank-receipt-voucher.component';
import { BankSlipComponent } from './regulars/components/admin-panel/report/bank-slip/bank-slip.component';
import { BookRequestReportComponent } from './regulars/components/admin-panel/report/book-request-report/book-request-report.component';
import { BudgetReportComponent } from './regulars/components/admin-panel/report/budget-report/budget-report.component';
import { LogAuditListComponent } from './regulars/components/admin-panel/report/log-audit/log-audit-list/log-audit-list.component';
import { PurchaseBookReportComponent } from './regulars/components/admin-panel/report/purchase-book-report/purchase-book-report.component';
import { PurchaseReportComponent } from './regulars/components/admin-panel/report/purchase-report/purchase-report.component';
import { ReportComponent } from './regulars/components/admin-panel/report/report.component';
import { SalesReportComponent } from './regulars/components/admin-panel/report/sales-report/sales-report.component';

import { RoleCreateComponent } from './regulars/components/admin-panel/role/role-create/role-create.component';
import { RoleListComponent } from './regulars/components/admin-panel/role/role-list/role-list.component';
import { RoleComponent } from './regulars/components/admin-panel/role/role.component';
import { SaleCreateComponent } from './regulars/components/admin-panel/sale/sale-create/sale-create.component';
import { SaleListComponent } from './regulars/components/admin-panel/sale/sale-list/sale-list.component';
import { SaleComponent } from './regulars/components/admin-panel/sale/sale.component';
import { SchoolLevelCreateComponent } from './regulars/components/admin-panel/school-level/school-level-create/school-level-create.component';
import { SchoolLevelEditModalComponent } from './regulars/components/admin-panel/school-level/school-level-edit-modal/school-level-edit-modal.component';
import { SchoolLevelListComponent } from './regulars/components/admin-panel/school-level/school-level-list/school-level-list.component';
import { SchoolLevelComponent } from './regulars/components/admin-panel/school-level/school-level.component';
import { SchoolOwnershipCreateComponent } from './regulars/components/admin-panel/school-ownership/school-ownership-create/school-ownership-create.component';
import { SchoolOwnershipEditModalComponent } from './regulars/components/admin-panel/school-ownership/school-ownership-edit-modal/school-ownership-edit-modal.component';
import { SchoolOwnershipListComponent } from './regulars/components/admin-panel/school-ownership/school-ownership-list/school-ownership-list.component';
import { SchoolOwnershipComponent } from './regulars/components/admin-panel/school-ownership/school-ownership.component';
import { SchoolBulkImportComponent } from './regulars/components/admin-panel/school/school-bulk-import/school-bulk-import.component';
import { SchoolCreateComponent } from './regulars/components/admin-panel/school/school-create/school-create.component';
import { SchoolListComponent } from './regulars/components/admin-panel/school/school-list/school-list.component';
import { SchoolComponent } from './regulars/components/admin-panel/school/school.component';
import { BookstoreStockComponent } from './regulars/components/admin-panel/stock/bookstore-stock/bookstore-stock.component';
import { LowStockComponent } from './regulars/components/admin-panel/stock/low-stock/low-stock.component';
import { SchoolStockComponent } from './regulars/components/admin-panel/stock/school-stock/school-stock.component';
import { StockCreateComponent } from './regulars/components/admin-panel/stock/stock-create/stock-create.component';
import { StockComponent } from './regulars/components/admin-panel/stock/stock.component';
import { SubcityStoreListComponent } from './regulars/components/admin-panel/subcity-store/subcity-store-list/subcity-store-list.component';
import { SubcityStoreRequestListComponent } from './regulars/components/admin-panel/subcity-store/subcity-store-request-list/subcity-store-request-list.component';
import { SubcityStoreComponent } from './regulars/components/admin-panel/subcity-store/subcity-store.component';
import { SubcityCreateComponent } from './regulars/components/admin-panel/subcity/subcity-create/subcity-create.component';
import { SubcityEditModalComponent } from './regulars/components/admin-panel/subcity/subcity-edit-modal/subcity-edit-modal.component';
import { SubcityListComponent } from './regulars/components/admin-panel/subcity/subcity-list/subcity-list.component';
import { SubcityComponent } from './regulars/components/admin-panel/subcity/subcity.component';
import { SubjectCreateComponent } from './regulars/components/admin-panel/subject/subject-create/subject-create.component';
import { SubjectEditModalComponent } from './regulars/components/admin-panel/subject/subject-edit-modal/subject-edit-modal.component';
import { SubjectListComponent } from './regulars/components/admin-panel/subject/subject-list/subject-list.component';
import { SubjectComponent } from './regulars/components/admin-panel/subject/subject.component';
import { SupplierCreateComponent } from './regulars/components/admin-panel/supplier/supplier-create/supplier-create.component';
import { SupplierEditModalComponent } from './regulars/components/admin-panel/supplier/supplier-edit-modal/supplier-edit-modal.component';
import { SupplierListComponent } from './regulars/components/admin-panel/supplier/supplier-list/supplier-list.component';
import { SupplierComponent } from './regulars/components/admin-panel/supplier/supplier.component';
import { UserCreateComponent } from './regulars/components/admin-panel/user/user-create/user-create.component';
import { UserListComponent } from './regulars/components/admin-panel/user/user-list/user-list.component';
import { UserComponent } from './regulars/components/admin-panel/user/user.component';
import { WoredaCreateComponent } from './regulars/components/admin-panel/woreda/woreda-create/woreda-create.component';
import { WoredaEditModalComponent } from './regulars/components/admin-panel/woreda/woreda-edit-modal/woreda-edit-modal.component';
import { WoredaListComponent } from './regulars/components/admin-panel/woreda/woreda-list/woreda-list.component';
import { WoredaComponent } from './regulars/components/admin-panel/woreda/woreda.component';
import { Woreda1CreateComponent } from './regulars/components/admin-panel/woreda1/woreda1-create/woreda1-create.component';
import { Woreda1EditModalComponent } from './regulars/components/admin-panel/woreda1/woreda1-edit-modal/woreda1-edit-modal.component';
import { Woreda1ListComponent } from './regulars/components/admin-panel/woreda1/woreda1-list/woreda1-list.component';
import { Woreda1Component } from './regulars/components/admin-panel/woreda1/woreda1.component';
import { ForbiddenComponent } from './regulars/components/forbidden/forbidden.component';
import { PageNotFoundComponent } from './regulars/components/page-not-found/page-not-found.component';
import { RequestExceedPageComponent } from './regulars/components/request-exceed-page/request-exceed-page.component';
import { LoginComponent } from './regulars/components/user-auth/login/login.component';
import { SignUpComponent } from './regulars/components/user-auth/sign-up/sign-up.component';
import { UserAuthComponent } from './regulars/components/user-auth/user-auth.component';


const routes: Routes = [
  {
    path: "",
    component: AdminPanelComponent,
    canActivate: [AuthGuard],
    children: [
      { path: "dashboard", component: DashboardComponent },
      {
        path: "",
        component: DashboardComponent,
        children: [
          { path: "admin-dashboard", component: AdminDashboardComponent },
          {
            path: "bookstore-employee-dashboard",
            component: BookstoreEmployeeDashboardComponent,
          },
          {
            path: "school-dashboard",
            component: SchoolDashboardComponent,
            canActivate: [AuthGuard],
            data: { roles: ["School Employee"] },
          },
          { path: "manager-dashboard", component: ManagerDashboardComponent },
          { path: "purchase-dashboard", component: PurchaseDashboardComponent },
          {
            path: "subcity-store-dashboard",
            component: SubcityStoreDashboardComponent,
          },
        ],
      },
      {
        path: "book",
        canActivate: [AuthGuard],
        data: { roles: ["Main Admin"] },
        component: BookComponent,
        children: [
          { path: "create", component: BookCreateComponent },
          { path: "edit/:id", component: BookCreateComponent },
          { path: "list", component: BookListComponent },
        ],
      },
      {
        path: "subject",
        canActivate: [AuthGuard],
        data: { roles: ["Main Admin"] },
        component: SubjectComponent,
        children: [
          { path: "create", component: SubjectCreateComponent },
          { path: "edit/:id", component: SubjectEditModalComponent },
          { path: "list", component: SubjectListComponent },
        ],
      },
      {
        path: "language",
        canActivate: [AuthGuard],
        data: { roles: ["Main Admin"] },
        component: LanguageComponent,
        children: [
          { path: "create", component: LanguageCreateComponent },
          { path: "edit/:id", component: LanguageEditModalComponent },
          { path: "list", component: LanguageListComponent },
        ],
      },
      {
        path: "grade-level",
        canActivate: [AuthGuard],
        data: { roles: ["Main Admin"] },
        component: GradeLevelComponent,
        children: [
          { path: "create", component: GradeLevelCreateComponent },
          { path: "edit/:id", component: GradeLevelEditModalComponent },
          { path: "list", component: GradeLevelListComponent },
        ],
      },
      {
        path: "subcity",
        canActivate: [AuthGuard],
        data: { roles: ["Main Admin"] },
        component: SubcityComponent,
        children: [
          { path: "create", component: SubcityCreateComponent },
          { path: "edit/:id", component: SubcityEditModalComponent },
          { path: "list", component: SubcityListComponent },
        ],
      },

      {
        path: "woreda",
        canActivate: [AuthGuard],
        data: { roles: ["Main Admin"] },
        component: WoredaComponent,
        children: [
          { path: "create", component: WoredaCreateComponent },
          { path: "edit/:id", component: WoredaEditModalComponent },
          { path: "list", component: WoredaListComponent },
        ],
      },

      {
        path: "woreda1",
        canActivate: [AuthGuard],
        data: { roles: ["Main Admin"] },
        component: Woreda1Component,
        children: [
          { path: "create", component: Woreda1CreateComponent },
          { path: "edit/:id", component: Woreda1EditModalComponent },
          { path: "list", component: Woreda1ListComponent },
        ],
      },

      {
        path: "region",
        canActivate: [AuthGuard],
        data: { roles: ["Main Admin"] },
        component: RegionComponent,
        children: [
          { path: "create", component: RegionCreateComponent },
          { path: "edit/:id", component: RegionEditModalComponent },
          { path: "list", component: RegionListComponent },
        ],
      },

      {
        path: "supplier",
        component: SupplierComponent,
        children: [
          {
            path: "create",
            component: SupplierCreateComponent,
            canActivate: [AuthGuard],
            data: { roles: ["Main Admin"] },
          },
          {
            path: "edit/:id",
            component: SupplierEditModalComponent,
            canActivate: [AuthGuard],
            data: { roles: ["Main Admin"] },
          },
          {
            path: "list",
            component: SupplierListComponent,
            canActivate: [AuthGuard],
            data: { roles: ["Main Admin", "Purchase Department"] },
          },
        ],
      },

      {
        path: "school-level",
        canActivate: [AuthGuard],
        data: { roles: ["Main Admin"] },
        component: SchoolLevelComponent,
        children: [
          { path: "create", component: SchoolLevelCreateComponent },
          { path: "edit/:id", component: SchoolLevelEditModalComponent },
          { path: "list", component: SchoolLevelListComponent },
        ],
      },

      {
        path: "school-ownership",
        canActivate: [AuthGuard],
        data: { roles: ["Main Admin"] },
        component: SchoolOwnershipComponent,
        children: [
          { path: "create", component: SchoolOwnershipCreateComponent },
          { path: "edit/:id", component: SchoolOwnershipEditModalComponent },
          { path: "list", component: SchoolOwnershipListComponent },
        ],
      },

      {
        path: "stock",
        component: StockComponent,
        children: [
          {
            path: "bookstore",
            component: BookstoreStockComponent,
            canActivate: [AuthGuard],
            data: {
              roles: ["Bookstore Employee", "Bookstore Manager", "Main Admin"],
            },
          },
          {
            path: "school",
            component: SchoolStockComponent,
            canActivate: [AuthGuard],
            data: { roles: ["School Employee", "Bookstore Manager"] },
          },
          {
            path: "low",
            component: LowStockComponent,
            data: {
              roles: ["Bookstore Employee", "Bookstore Manager", "Main Admin"],
            },
          },
          {
            path: "new",
            component: StockCreateComponent,
            canActivate: [AuthGuard],
            data: { roles: ["Main Admin", "Bookstore Manager"] },
          },
        ],
      },

      {
        path: "report",
        component: ReportComponent,
        children: [
          {
            path: "bank-slip",
            component: BankSlipComponent,
            canActivate: [AuthGuard],
            data: { roles: ["Bookstore Employee"] },
          },
          { path: "sales-report/:id", component: SalesReportComponent },
          { path: "purchase-report/:id", component: PurchaseReportComponent },
          {
            path: "bank-receipt-voucher/:id",
            component: BankReceiptVoucherDetailComponent,
          },
          {
            path: "purchase-book-report",
            component: PurchaseBookReportComponent,
            canActivate: [AuthGuard],
            data: { roles: ["Bookstore Manager", "Main Admin"] },
          },
          {
            path: "budget-report",
            component: BudgetReportComponent,
            canActivate: [AuthGuard],
            data: { roles: ["Bookstore Manager", "Main Admin"] },
          },
          {
            path: "bank-receipt-voucher-create",
            component: BankReceiptVoucherCreateComponent,
          },
          {
            path: "book-request-report",
            component: BookRequestReportComponent,
            canActivate: [AuthGuard],
            data: {
              roles: ["School Employee", "Main Admin", "Bookstore Manager"],
            },
          },
          {
            path: "log-audit",
            component: ReportComponent,
            children: [
              {
                path: "list",
                component: LogAuditListComponent,
                canActivate: [AuthGuard],
                data: { roles: ["Main Admin"] },
              },
            ],
          },
        ],
      },

      {
        path: "school",
        component: SchoolComponent,
        children: [
          {
            path: "create",
            component: SchoolCreateComponent,
            canActivate: [AuthGuard],
            data: { roles: ["Main Admin"] },
          },
          {
            path: "edit/:id",
            component: SchoolCreateComponent,
            canActivate: [AuthGuard],
            data: { roles: ["Main Admin"] },
          },
          {
            path: "list",
            component: SchoolListComponent,
            canActivate: [AuthGuard],
            data: { roles: ["Bookstore Employee", "Main Admin"] },
          },
          {
            path: "bulk-import",
            component: SchoolBulkImportComponent,
            canActivate: [AuthGuard],
            data: { roles: ["Main Admin"] },
          },
        ],
      },

      {
        path: "order",
        component: OrderComponent,
        children: [
          {
            path: "create",
            component: OrderCreateComponent,
            canActivate: [AuthGuard],
            data: { roles: ["School Employee"] },
          },
          { path: "edit/:id", component: OrderCreateComponent },
          {
            path: "list",
            component: OrderListComponent,
            canActivate: [AuthGuard],
            data: {
              roles: [
                "School Employee",
                "Bookstore Employee",
                "Bookstore Manager",
                "Main Admin",
                "Subcity Manager",
              ],
            },
          },
          { path: "pending-list", component: PendingOrderListComponent },
        ],
      },
      {
        path: "purchase",
        component: OrderComponent,
        children: [
          {
            path: "purchase-list",
            component: PurchaseListComponent,
            canActivate: [AuthGuard],
            data: { roles: ["Bookstore Manager", "Purchase Department"] },
          },
        ],
      },

      {
        path: "purchase-book",
        component: OrderComponent,
        children: [
          {
            path: "create",
            component: PurchaseBookCreateComponent,
            data: {
              roles: ["Purchase Department"],
            },
          },
          {
            path: "list",
            component: PurchaseBookListComponent,
            canActivate: [AuthGuard],
            data: {
              roles: [
                "Bookstore Employee",
                "Bookstore Manager",
                "Purchase Department",
              ],
            },
          },
        ],
      },

      {
        path: "sale",
        component: SaleComponent,
        children: [
          { path: "create", component: SaleCreateComponent },
          { path: "edit/:id", component: SaleCreateComponent },
          { path: "list", component: SaleListComponent },
        ],
      },

      {
        path: "receive",
        component: ReceiveComponent,
        children: [
          { path: "create", component: ReceiveCreateComponent },
          { path: "edit/:id", component: ReceiveCreateComponent },
          { path: "list", component: ReceiveListComponent },
        ],
      },
      {
        path: "budget",
        component: BudgetComponent,
        canActivate: [AuthGuard],
        data: { roles: ["Bookstore Employee", "Bookstore Manager"] },
        children: [
          { path: "create", component: BudgetCreateComponent },
          { path: "edit/:id", component: BudgetEditModalComponent },
          { path: "list", component: BudgetListComponent },
        ],
      },
      {
        path: "budget",
        component: BudgetComponent,
        children: [
          { path: "create", component: BudgetCreateComponent },
          { path: "edit/:id", component: BudgetEditModalComponent },
          { path: "list", component: BudgetListComponent },
        ],
      },

      {
        path: "subcity-store-stock",
        component: SubcityStoreComponent,
        children: [
          {
            path: "list",
            component: SubcityStoreListComponent,
            data: {
              roles: [
                "Bookstore Employee",
                "Bookstore Manager",
                "Subcity Manager",
              ],
            },
          },
        ],
      },

      {
        path: "subcity-store",
        component: SubcityStoreComponent,
        children: [
          {
            path: "request-list",
            component: SubcityStoreRequestListComponent,
            data: {
              roles: [
                "Bookstore Employee",
                "Bookstore Manager",
                "Subcity Manager",
              ],
            },
          },
        ],
      },

      {
        path: "user",
        component: UserComponent,
        children: [
          { path: "create", component: UserCreateComponent },
          { path: "edit/:id", component: UserCreateComponent },
          {
            path: "list",
            component: UserListComponent,
            canActivate: [AuthGuard],
            data: { roles: ["Main Admin"] },
          },
        ],
      },

      { path: "request-limit", component: RequestExceedPageComponent },
    ],
  },
  {
    path: "user-auth",
    component: UserAuthComponent,
    children: [
      { path: "login", component: LoginComponent },
      { path: "registration", component: SignUpComponent },
    ],
  },

  { path: "forbidden", component: ForbiddenComponent },
  { path: "", redirectTo: "", pathMatch: "full" },
  { path: "**", component: PageNotFoundComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
