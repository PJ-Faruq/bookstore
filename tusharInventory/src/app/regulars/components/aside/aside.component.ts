import { Component, OnInit } from '@angular/core';
import { UserVm } from 'src/app/models/view-models/userVm.model';
import { AutehnticationService } from '../../services/autehntication.service';

@Component({
  selector: 'app-aside',
  templateUrl: './aside.component.html',
  styleUrls: ['./aside.component.css']
})
export class AsideComponent implements OnInit {

  constructor(private authenticationService: AutehnticationService) {
    
  }

  currentUser: UserVm;
  ngOnInit() {
    this.currentUser = this.authenticationService.currentUserValue;
  }


}
