import { Component, OnInit } from "@angular/core";
import { AppSettings } from "src/app/shared/app-settings";
import { Router, ActivatedRoute } from "@angular/router";
import { NgForm } from "@angular/forms";
import { HttpErrorResponse } from "@angular/common/http";
import { first } from "rxjs/operators";
import { ToastrService } from "ngx-toastr";
import { AutehnticationService } from "src/app/regulars/services/autehntication.service";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.css"],
})
export class LoginComponent implements OnInit {
  returnUrl: string;
  hide = true;
  isLoginError: boolean = false;
  app: AppSettings = new AppSettings();

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private authenticationService: AutehnticationService,
    private tostr: ToastrService
  ) {
    // redirect to home if already logged in
    if (this.authenticationService.currentUserValue) {
      this.router.navigate(["/"]);
    }
  }

  ngOnInit() {
    // get return url from route parameters or default to '/'
    this.returnUrl = this.route.snapshot.queryParams["returnUrl"] || "/";
  }

  onSubmit(form?: NgForm) {
    var data = form.value;
    this.authenticationService
      .login(data.Email, data.Password)
      .pipe(first())
      .subscribe(
        (data) => {
          if (data.Role == "Bookstore Employee") {
            this.router.navigate(["/bookstore-employee-dashboard"]);
          } else if (data.Role == "Main Admin") {
            this.router.navigate(["/admin-dashboard"]);
          } else if (data.Role == "School Employee") {
            this.router.navigate(["/school-dashboard"]);
          } else if (data.Role == "Purchase Department") {
            this.router.navigate(["/purchase-dashboard"]);
          } else if (data.Role == "Bookstore Manager") {
            this.router.navigate(["/manager-dashboard"]);
          } else if (data.Role == "Subcity Manager") {
            this.router.navigate(["/subcity-store-dashboard"]);
          } else {
            this.router.navigate([this.returnUrl]);
          }
        },
        (err: HttpErrorResponse) => {
          this.isLoginError = true;
          this.tostr.error("Incorrect username or password", "Error");
        }
      );
  }

  onSingUp(signUpType){
     this.router.navigate(["/user-auth/registration", { signUpType: signUpType }]);
  }
}
