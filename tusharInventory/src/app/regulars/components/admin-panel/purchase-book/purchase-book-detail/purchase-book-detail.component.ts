import { analyzeAndValidateNgModules } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { DetailComponent } from 'src/app/generics/components/detail/detail.component';
import { GenericService } from 'src/app/generics/services/generic.service';
import { PurchaseBook } from 'src/app/models/operation/purchase-book';
import { UserVm } from 'src/app/models/view-models/userVm.model';
import { AutehnticationService } from 'src/app/regulars/services/autehntication.service';
import { OrderService } from 'src/app/regulars/services/order.service';

@Component({
  selector: "app-purchase-book-detail",
  templateUrl: "./purchase-book-detail.component.html",
  styleUrls: ["./purchase-book-detail.component.css"],
})
export class PurchaseBookDetailComponent
  extends DetailComponent
  implements OnInit {
  currentUser: UserVm;
  modalReff: BsModalRef;

  constructor(
    public modalRef: BsModalRef,
    public modalService: BsModalService,
    public service: GenericService,
    public _service: OrderService,
    public spinner: NgxSpinnerService,
    private toastr: ToastrService,
    private authenticationService: AutehnticationService
  ) {
    super(modalRef, service, spinner);
    this.apiUrl = "PurchaseBooks";
  }

  ngOnInit(): void {

        this.setDataByParams(this.data.id);     
        this.currentUser = this.authenticationService.currentUserValue;
  }
}
