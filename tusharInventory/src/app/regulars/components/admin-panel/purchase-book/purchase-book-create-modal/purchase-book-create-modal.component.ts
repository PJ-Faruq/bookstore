import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { GenericService } from 'src/app/generics/services/generic.service';
import { Book } from 'src/app/models/basic/book';
import { BudgetType } from 'src/app/models/basic/BudgetType';
import { SubjectModel } from 'src/app/models/basic/subject';
import { Supplier } from 'src/app/models/basic/supplier';
import { PurchaseBook } from 'src/app/models/operation/purchase-book';
import { PurchaseBookLine } from 'src/app/models/operation/purchase-book-line';
import { User } from 'src/app/models/user';
import { OrderShowVm } from 'src/app/models/view-models/OrderShowVm';
import { UserVm } from 'src/app/models/view-models/userVm.model';
import { AutehnticationService } from 'src/app/regulars/services/autehntication.service';
import { PurchaseBookService } from 'src/app/regulars/services/purchase-book.service';
import { PurchaseService } from 'src/app/regulars/services/purchase.service';

@Component({
  selector: "app-purchase-book-create-modal",
  templateUrl: "./purchase-book-create-modal.component.html",
  styleUrls: ["./purchase-book-create-modal.component.css"],
})
export class PurchaseBookCreateModalComponent implements OnInit {
  model: any;
  data: any;
  apiUrl: any;
  currentUser: UserVm;
  supplierList: Supplier[] = [];
  bookstoreEmployeeList: User[] = [];

  bookList: Book[] = [];
  subjectList: SubjectModel[] = [];
  gradeList: SubjectModel[] = [];
  languageList: SubjectModel[] = [];

  orderVmList: OrderShowVm[] = [];
  budgetTypeList: BudgetType[] = [];

  bookModel: any;
  BookPrice: any;
  budgetAmount: any;
  purchaseBook: PurchaseBook;

  datePickerConfig: Partial<BsDatepickerConfig>;
  constructor(
    public service: GenericService,
    public modalRef: BsModalRef,
    public _service: PurchaseBookService,
    public purchaseService:PurchaseService,
    public router: Router,
    public activatedRoute: ActivatedRoute,
    public spinner: NgxSpinnerService,
    public toastr: ToastrService,
    private authenticationService: AutehnticationService
  ) {
    this.currentUser = this.authenticationService.currentUserValue;

    this.model = new PurchaseBook();
    this.purchaseBook = new PurchaseBook();
    this.apiUrl = "Orders";

    this.datePickerConfig = Object.assign(
      {},
      {
        containerClass: "theme-dark-blue",
        showWeekNumbers: true,
        // dateInputFormat: 'DD/MM/YYYY'
      }
    );
  }

  ngOnInit(): void {
    this.model = Object.assign({}, this.data.model);

    this.setPurchaseBook();

    this.service.getAll("Books").subscribe(
      (data) => {
        this.bookList = data;
      },
      (err) => {
        this.bookList = [];
      }
    );

    this.service.getAll("Suppliers").subscribe(
      (data) => {
        this.supplierList = data;
      },
      (err) => {
        this.supplierList = [];
      }
    );
    this._service.GetBudgetType().subscribe(
      (data) => {
        this.budgetTypeList = data;
      },
      (err) => {
        this.budgetTypeList = [];
      }
    );
  }

  setPurchaseBook() {

    //Clone a object without referencing

    this.purchaseBook = Object.assign({}, this.data.model);
    this.purchaseBook.Id = 0;
    this.purchaseBook.PurchaseRequestId=this.model.Id;
    this.purchaseBook.BookstoreEmployeeId = this.model.BookstoreEmployeeId;
    this.purchaseBook.BookstoreEmployee=null;
    this.purchaseBook.BookstoreManagerId = this.model.BookstoreManagerId;
    this.purchaseBook.BookstoreManager=null;
    this.purchaseBook.PurcahseDeptEmployeeId = this.currentUser.Id;
    this.purchaseBook.PurcahseDeptEmployee=null;
    this.purchaseBook.PurchaseDate = new Date();
    this.purchaseBook.Amount = this.model.Amount;

    //Clone an array without referencing
    this.purchaseBook.PurchaseBookLines = this.model.PurchaseLines.map((x) =>
      Object.assign({}, x)
    );

    for (let item of this.purchaseBook.PurchaseBookLines) {
      item.Id = 0;
      item.Quantity = item["PurchaseQuantity"];
      item.BookSubject = null;
      item.BookGradeLavel = null;
      item.BookLanguage = null;
      
    }

    //For showing data in the table
    for (let item of this.model.PurchaseLines) {
      let orderShowVm = new OrderShowVm();
      orderShowVm.BookGradeName = item.BookGradeLavel.Name;
      orderShowVm.BookLanguageName = item.BookLanguage.Name;
      orderShowVm.BookSubjectName = item.BookSubject.Name;
      orderShowVm.Quantity = item.PurchaseQuantity;
      orderShowVm.BookPrice = item.BookPrice;

      this.orderVmList.push(orderShowVm);
    }
  }

  getBudgetAmount(budgetTypeId: number) {
    this._service.getBudget(budgetTypeId).subscribe((response) => {
      this.budgetAmount = response;
      
    });
  }

  onDeleteLine(i) {
    this.model.PurchaseBookLines.splice(i, 1);
    this.orderVmList.splice(i, 1);
    this.getTotalAmount();
  }

  onSubmit(form) {
    if(this.budgetAmount<this.purchaseBook.Amount){
      this.toastr.error("Budget is not Enough");
      return;
    }
    
    this.purchaseBook.PurcahseDeptEmployeeId = this.currentUser.Id;

    this.service.add(this.purchaseBook, "PurchaseBooks").subscribe(
      (response) => {
        if (response) {
          //this.router.navigate(["/order/list"]);

          //Change The status of Purchse Request Table;
          let updateModel = Object.assign({}, this.data.model);
          updateModel.Status = 3;
          updateModel.PurcahseDeptEmployeeId=this.currentUser.Id;
          this.purchaseService
            .changeStatus(updateModel)
            .subscribe((response) => {
              if (response) {
                this.modalRef.hide();
                this.purchaseService.refreshList("refresh");
                this.toastr.success("Purchase Book Successful");
              } else {
                this.modalRef.hide();
                this.toastr.error(
                  "Purchase Request Rejected Failed !",
                  "Eroor"
                );
              }
            });

        }
      },
      (error) => {
        this.toastr.error("Request Submission Failed !");
      }
    );
  }

  getTotalAmount() {
    let totalAmount = 0;
    for (let item of this.model.PurchaseBookLines) {
      totalAmount = totalAmount + item.BookPrice * item.Quantity;
    }

    this.model.Amount = totalAmount;
  }
}
