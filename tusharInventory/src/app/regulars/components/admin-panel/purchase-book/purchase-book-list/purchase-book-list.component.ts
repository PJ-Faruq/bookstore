import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DataTableDirective } from 'angular-datatables';
import { BsModalService } from 'ngx-bootstrap/modal';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { Subject } from 'rxjs';
import { ListComponent } from 'src/app/generics/components/list/list.component';
import { GenericService } from 'src/app/generics/services/generic.service';
import { UserVm } from 'src/app/models/view-models/userVm.model';
import { AutehnticationService } from 'src/app/regulars/services/autehntication.service';
import { ConfirmService } from 'src/app/regulars/services/confirm.service';
import { PurchaseBookService } from 'src/app/regulars/services/purchase-book.service';
import { OrderDetailModalComponent } from '../../order/order-detail-modal/order-detail-modal.component';
import { PurchaseBookDetailComponent } from '../purchase-book-detail/purchase-book-detail.component';

@Component({
  selector: "app-purchase-book-list",
  templateUrl: "./purchase-book-list.component.html",
  styleUrls: ["./purchase-book-list.component.css"],
})
export class PurchaseBookListComponent extends ListComponent implements OnInit {
  currentUser: UserVm;
  apiUrl = "PurchaseBooks";

  //Datatable Field
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject<any>();
  dtElement: DataTableDirective;

  constructor(
    public service: GenericService,
    public _service: PurchaseBookService,
    public modalService: BsModalService,
    public confirmService: ConfirmService,
    public toastr: ToastrService,
    public spinner: NgxSpinnerService,
    private router: Router,
    private authenticationService: AutehnticationService
  ) {
    super(modalService, confirmService, toastr, spinner);
  }

  ngOnInit(): void {
    this.currentUser = this.authenticationService.currentUserValue;
    this.spinner.show();

    this.listChangedSubscribe = this.service.listChanged.subscribe(
      (data) => {
        this.modelList = data.reverse();

        this.spinner.hide();
      },
      (error) => {
        this.modelList = [];
        this.spinner.hide();
      }
    );
    this.confirmSubscription = this.confirmService.deleted.subscribe((data) => {
      if (data) {
        this.service.setAll(this.apiUrl);
        this.toastr.warning("Deleted Successfully", "Success");
      } else {
        this.toastr.warning("Can't Delete this item", "Warning");
      }
    });
    this.service.setAll(this.apiUrl);
  }

  openDetailModal(id: number) {
    this.modalRef = this.modalService.show(PurchaseBookDetailComponent, {
      initialState: {
        title: "Purchase Detail",
        data: {
          id: id,
        },
      },
      class: "modal-lg",
      ignoreBackdropClick: false,
    });
  }

  onClickReceipt(id: number) {
    const url = this.router.createUrlTree(["/report/purchase-report", id]);
    window.open(url.toString(), "_blank");
  }

  //#region Datatable Settings
  ngAfterViewInit(): void {
    setTimeout(() => {
      this.dtTrigger.next();
    }, 300);
  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }

  rerender(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      // Destroy the table first
      dtInstance.destroy();
      // Call the dtTrigger to rerender again
      this.dtTrigger.next();
    });
  }

  //#endregion
}
