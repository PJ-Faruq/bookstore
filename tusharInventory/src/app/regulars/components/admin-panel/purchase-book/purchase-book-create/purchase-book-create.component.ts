import { HttpClientJsonpModule } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { CreateComponent } from 'src/app/generics/components/create/create.component';
import { GenericService } from 'src/app/generics/services/generic.service';
import { Book } from 'src/app/models/basic/book';
import { Budget } from 'src/app/models/basic/budget';
import { BudgetType } from 'src/app/models/basic/BudgetType';
import { SubjectModel } from 'src/app/models/basic/subject';
import { Supplier } from 'src/app/models/basic/supplier';
import { Order } from 'src/app/models/operation/order';
import { OrderLine } from 'src/app/models/operation/order-line';
import { PurchaseBook } from 'src/app/models/operation/purchase-book';
import { PurchaseBookLine } from 'src/app/models/operation/purchase-book-line';
import { User } from 'src/app/models/user';
import { OrderShowVm } from 'src/app/models/view-models/OrderShowVm';
import { UserVm } from 'src/app/models/view-models/userVm.model';
import { AutehnticationService } from 'src/app/regulars/services/autehntication.service';
import { OrderService } from 'src/app/regulars/services/order.service';
import { PurchaseBookService } from 'src/app/regulars/services/purchase-book.service';
import { PurchaseService } from 'src/app/regulars/services/purchase.service';

@Component({
  selector: "app-purchase-book-create",
  templateUrl: "./purchase-book-create.component.html",
  styleUrls: ["./purchase-book-create.component.css"],
})
export class PurchaseBookCreateComponent
  extends CreateComponent
  implements OnInit {
    data:any
  currentUser: UserVm;
  supplierList: Supplier[] = [];
  bookstoreEmployeeList: User[] = [];

  bookList: Book[] = [];
  subjectList: SubjectModel[] = [];
  gradeList: SubjectModel[] = [];
  languageList: SubjectModel[] = [];

  orderVmList: OrderShowVm[] = [];
  budgetTypeList: BudgetType[] = [];

  bookModel: any;
  BookPrice: any;
  budgetAmount: any;

  datePickerConfig: Partial<BsDatepickerConfig>;
  constructor(
    public service: GenericService,
    public _service: PurchaseBookService,
    public router: Router,
    public activatedRoute: ActivatedRoute,
    public spinner: NgxSpinnerService,
    public toastr: ToastrService,
    private authenticationService: AutehnticationService
  ) {
    super(service, router, activatedRoute, spinner, toastr);
    this.currentUser = this.authenticationService.currentUserValue;

    this.model = new PurchaseBook();
    this.apiUrl = "Orders";

    this.datePickerConfig = Object.assign(
      {},
      {
        containerClass: "theme-dark-blue",
        showWeekNumbers: true,
        // dateInputFormat: 'DD/MM/YYYY'
      }
    );
  }

  ngOnInit(): void {

    this.service.getAll("Books").subscribe(
      (data) => {
        this.bookList = data;
      },
      (err) => {
        this.bookList = [];
      }
    );

    this.service.getAll("Suppliers").subscribe(
      (data) => {
        this.supplierList = data;
      },
      (err) => {
        this.supplierList = [];
      }
    );
    this._service.GetBudgetType().subscribe(
      (data) => {
        this.budgetTypeList = data;
      },
      (err) => {
        this.budgetTypeList = [];
      }
    );

    this.service.getAll("Subjects").subscribe(
      (data) => {
        this.subjectList = data;
      },
      (err) => {
        this.subjectList = [];
      }
    );

    this.service.getAll("GradeLavels").subscribe(
      (data) => {
        this.gradeList = data;
      },
      (err) => {
        this.gradeList = [];
      }
    );

    this.service.getAll("Languages").subscribe(
      (data) => {
        this.languageList = data;
      },
      (err) => {
        this.languageList = [];
      }
    );

    this.setDataByParams();
  }

  getBudgetAmount(budgetTypeId:number){
    this._service.getBudget(budgetTypeId).subscribe(
      response=>{
        
        this.budgetAmount=response;
        console.log(this.budgetAmount);
      }
    )
  }

  onNewLine(form: NgForm) {
    const value = form.value;


      //For Binding value for the backend
      var purchaseLine = new PurchaseBookLine();
    purchaseLine = value;
    purchaseLine.BookPrice = this.BookPrice;
    this.model.PurchaseBookLines.push(purchaseLine);

    //Checking if there is enough budget
    let totalAmount = 0;
    for (let item of this.model.PurchaseBookLines) {
      totalAmount = totalAmount + item.BookPrice * item.Quantity;
    }

    if (this.budgetAmount < totalAmount) {
      this.toastr.error("Budget is Not Enough");
      this.model.PurchaseBookLines.pop(purchaseLine);
      return;
    }


    this.getTotalAmount();

    //For showing order custom view in the Table
    let orderVm = new OrderShowVm();

    orderVm.BookSubjectName = this.subjectList.find(
      (m) => m.Id == value.BookSubjectId
    ).Name;
    orderVm.BookLanguageName = this.languageList.find(
      (m) => m.Id == value.BookLanguageId
    ).Name;
    orderVm.BookGradeName = this.gradeList.find(
      (m) => m.Id == value.BookGradeLavelId
    ).Name;

    orderVm.Quantity = value.Quantity;
    orderVm.BookPrice = this.BookPrice;

    this.orderVmList.push(orderVm);
    form.reset();
  }

  getPrice(form: NgForm) {
    console.log(form);
    let value = form.value;
    if (
      value.BookLanguageId == "" ||
      value.BookLanguageId == null ||
      value.BookGradeLavelId == "" ||
      value.BookGradeLavelId == null ||
      value.BookSubjectId == "" ||
      value.BookSubjectId == null
    ) {
      return;
    }

    this._service.getPurchasePrice(value).subscribe(
      (response) => {
        if (response == null) {
          this.toastr.error("Please Add this Book First");
        }

        this.BookPrice = response;
      },
      (error) => {
        this.toastr.error("Some Errors Occur");
      }
    );
  }

  onDeleteLine(i) {
    this.model.PurchaseBookLines.splice(i, 1);
    this.orderVmList.splice(i, 1);
    this.getTotalAmount();
  }

  onSubmit(form) {
    this.model.PurcahseDeptEmployeeId = this.currentUser.Id;
    this.model.PurchaseDate=new Date();
    this.service.add(this.model, "PurchaseBooks").subscribe(
      (response) => {
        if (response) {
          this.toastr.success("Purchasing Books Successful");
          // this.orderForm.control.reset();
          // this.model = new Order();
          // this.fileInput.value = null;
          this.router.navigate(["/purchase-book/list"]);
        }
      },
      (error) => {
        this.toastr.error("Request Submission Failed !");
      }
    );
  }

  getTotalAmount() {
    let totalAmount = 0;
    for (let item of this.model.PurchaseBookLines) {
      totalAmount = totalAmount + item.BookPrice * item.Quantity;
    }

    this.model.Amount = totalAmount;
  }
}
