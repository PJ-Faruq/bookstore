import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { NgxSpinnerService } from "ngx-spinner";
import { ToastrService } from "ngx-toastr";
import { CreateComponent } from "src/app/generics/components/create/create.component";
import { GenericService } from "src/app/generics/services/generic.service";
import { SchoolLavel } from 'src/app/models/basic/school-lavel';

import { CheckNameService } from "src/app/regulars/services/check-name.service";

@Component({
  selector: "app-schoolLavel-create",
  templateUrl: "./school-level-create.component.html",
  styleUrls: ["./school-level-create.component.css"],
})
export class SchoolLevelCreateComponent extends CreateComponent implements OnInit {
  title = "SchoolLavel";

  constructor(
    public service: GenericService,
    public checkNameService: CheckNameService,

    public router: Router,
    public activatedRoute: ActivatedRoute,
    public spinner: NgxSpinnerService,
    public toastr: ToastrService
  ) {
    super(service, router, activatedRoute, spinner, toastr);
    this.model = new SchoolLavel();
    this.apiUrl = "SchoolLavels";
  }

  ngOnInit() {
    this.setDataByParams();
  }

  reset() {
    this.editMode = false;
    this.model = new SchoolLavel();
  }

  onSubmit() {
    this.checkNameService.checkName("SchoolLavels", this.model).subscribe(
      (response) => {
        if (response == null) {
          this.AddItem();
        } else {
          this.toastr.error("This Name is Already Exist !");
        }
      },
      (error) => {
        this.toastr.error("Internal Server Error");
      }
    );
  }

  AddItem() {
    this.service.add(this.model, "SchoolLavels").subscribe(
      (resp) => {
        if (resp) {
          this.toastr.success("School Level Successfully Added");
          this.router.navigate(["/school-level/list"]);
        } else {
          this.toastr.error("School Level Added Failed !");
        }
      },
      (err) => {
        this.toastr.error("Internal Server Error");
      }
    );
  }
}

