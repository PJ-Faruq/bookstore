import { Component, OnInit } from '@angular/core';
import { EditComponent } from 'src/app/generics/components/edit/edit.component';
import { Router } from "@angular/router";
import { BsModalRef } from "ngx-bootstrap/modal";
import { NgxSpinnerService } from "ngx-spinner";
import { ToastrService } from "ngx-toastr";
import { GenericService } from "src/app/generics/services/generic.service";
import { CheckNameService } from "src/app/regulars/services/check-name.service";;
import { SchoolLevelService } from 'src/app/regulars/services/school-level.service';
import { SchoolLavel } from 'src/app/models/basic/school-lavel';

@Component({
  selector: 'app-school-level-edit-modal',
  templateUrl: './school-level-edit-modal.component.html',
  styleUrls: ['./school-level-edit-modal.component.css']
})
export class SchoolLevelEditModalComponent extends EditComponent implements OnInit {

 editMode: boolean = true;
  existingName: string = null;

  constructor(
    public modalRef: BsModalRef,
    public service: GenericService,
    public checkNameService: CheckNameService,
    public _service: SchoolLevelService,
    public router: Router,
    public toastr: ToastrService,
    public spinner: NgxSpinnerService
  ) {
    super(modalRef, service, router, toastr, spinner);
    this.model = new SchoolLavel();
    this.apiUrl = "SchoolLavels";
  }

  ngOnInit() {
    this.setDataByParams();

    setTimeout(() => {
      this.existingName = this.model.Name;
    }, 1000);
  }

  onUpdate() {
    this.checkNameService
      .checkName("SchoolLavels", this.model)
      .subscribe((response) => {
        if (response != null) {
          if (response["Name"] != this.existingName) {
            this.toastr.error("This SchoolLavel name is Already Exist");
          } else {
            this.updateSchoolLavel();
          }
        }

        if (response == null) {
          this.updateSchoolLavel();
        }
      });
  }

  updateSchoolLavel() {
    this.service.update(this.model.Id, this.model, "SchoolLavels").subscribe(
      (response) => {
        if (response) {
          this.modalRef.hide();
          this.toastr.success("SchoolLavel Updated Successfully");
          this._service.refreshList("refresh");
        }
      },
      (error) => {
        this.toastr.error("Internal Server Error");
      }
    );
  }

}
