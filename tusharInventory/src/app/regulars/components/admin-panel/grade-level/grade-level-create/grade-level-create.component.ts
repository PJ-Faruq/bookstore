import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { NgxSpinnerService } from "ngx-spinner";
import { ToastrService } from "ngx-toastr";
import { CreateComponent } from "src/app/generics/components/create/create.component";
import { GenericService } from "src/app/generics/services/generic.service";
import { GradeLavel } from 'src/app/models/basic/grade-lavel';
import { CheckNameService } from "src/app/regulars/services/check-name.service";

@Component({
  selector: "app-gradeLavel-create",
  templateUrl: "./grade-level-create.component.html",
  styleUrls: ["./grade-level-create.component.css"],
})
export class GradeLevelCreateComponent
  extends CreateComponent
  implements OnInit {
  title = "GradeLavel";

  constructor(
    public service: GenericService,
    public checkNameService: CheckNameService,

    public router: Router,
    public activatedRoute: ActivatedRoute,
    public spinner: NgxSpinnerService,
    public toastr: ToastrService
  ) {
    super(service, router, activatedRoute, spinner, toastr);
    this.model = new GradeLavel();
    this.apiUrl = "GradeLavels";
  }

  ngOnInit() {
    this.setDataByParams();
  }

  reset() {
    this.editMode = false;
    this.model = new GradeLavel();
  }

  onSubmit() {
    this.checkNameService.checkName("GradeLavels", this.model).subscribe(
      (response) => {
        if (response == null) {
          this.AddItem();
        } else {
          this.toastr.error("This Name is Already Exist !");
        }
      },
      (error) => {
        this.toastr.error("Internal Server Error");
      }
    );
  }

  AddItem() {
    this.service.add(this.model, "GradeLavels").subscribe(
      (resp) => {
        if (resp) {
          this.toastr.success("GradeLavel Successfully Added");
          this.router.navigate(["/grade-level/list"]);
        } else {
          this.toastr.error("GradeLavel Added Failed !");
        }
      },
      (err) => {
        this.toastr.error("Internal Server Error");
      }
    );
  }
}
