import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { BsModalRef } from "ngx-bootstrap/modal";
import { NgxSpinnerService } from "ngx-spinner";
import { ToastrService } from "ngx-toastr";
import { EditComponent } from "src/app/generics/components/edit/edit.component";
import { GenericService } from "src/app/generics/services/generic.service";
import { GradeLavel } from "src/app/models/basic/grade-lavel";
import { CheckNameService } from "src/app/regulars/services/check-name.service";
import { GradeLavelService } from 'src/app/regulars/services/grade-lavel.service';


@Component({
  selector: "app-gradeLavel-edit-modal",
  templateUrl: "./grade-level-edit-modal.component.html",
  styleUrls: ["./grade-level-edit-modal.component.css"],
})
export class GradeLevelEditModalComponent
  extends EditComponent
  implements OnInit {
  editMode: boolean = true;
  existingName: string = null;

  constructor(
    public modalRef: BsModalRef,
    public service: GenericService,
    public checkNameService: CheckNameService,
    public _service: GradeLavelService,
    public router: Router,
    public toastr: ToastrService,
    public spinner: NgxSpinnerService
  ) {
    super(modalRef, service, router, toastr, spinner);
    this.model = new GradeLavel();
    this.apiUrl = "GradeLavels";
  }

  ngOnInit() {
    this.setDataByParams();

    setTimeout(() => {
      this.existingName = this.model.Name;
    }, 1000);
  }

  onUpdate() {
    this.checkNameService
      .checkName("GradeLavels", this.model)
      .subscribe((response) => {
        if (response != null) {
          if (response["Name"] != this.existingName) {
            this.toastr.error("This GradeLavel name is Already Exist");
          } else {
            this.updateGradeLavel();
          }
        }

        if (response == null) {
          this.updateGradeLavel();
        }
      });
  }

  updateGradeLavel() {
    this.service.update(this.model.Id, this.model, "GradeLavels").subscribe(
      (response) => {
        if (response) {
          this.modalRef.hide();
          this.toastr.success("GradeLavel Updated Successfully");
          this._service.refreshList("refresh");
        }
      },
      (error) => {
        this.toastr.error("Internal Server Error");
      }
    );
  }
}


