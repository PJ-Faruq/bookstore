import { Component, OnInit } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { SubjectModel } from "src/app/models/basic/subject";
import { ListComponent } from 'src/app/generics/components/list/list.component';
import { GenericService } from 'src/app/generics/services/generic.service';
import { UserVm } from 'src/app/models/view-models/userVm.model';
import { ConfirmService } from 'src/app/regulars/services/confirm.service';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { AutehnticationService } from 'src/app/regulars/services/autehntication.service';
import { SubjectEditModalComponent } from '../subject-edit-modal/subject-edit-modal.component';
import { SubjectService } from 'src/app/regulars/services/subject.service';
import { Subject } from 'rxjs';
import { DataTableDirective } from 'angular-datatables';

@Component({
  selector: "app-subject-list",
  templateUrl: "./subject-list.component.html",
  styleUrls: ["./subject-list.component.css"],
})
export class SubjectListComponent extends ListComponent implements OnInit {
  currentUser: UserVm;
  title = "Subject";
  apiUrl = "Subjects";
  createLink = "/" + this.title.toLowerCase() + "/create";

  model: SubjectModel;
  subjectList: any = [];

  //Datatable Field
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject<any>();
  dtElement: DataTableDirective;

  constructor(
    public service: GenericService,
    public _service: SubjectService,
    public modalService: BsModalService,
    public confirmService: ConfirmService,
    public toastr: ToastrService,
    public spinner: NgxSpinnerService,
    private authenticationService: AutehnticationService
  ) {
    super(modalService, confirmService, toastr, spinner);
    this.model = new SubjectModel();
    this._service.listen().subscribe((response) => {
      this.service.setAll(this.apiUrl);
    });

    this.confirmService.listen().subscribe(
      response=>{
        this.service.setAll(this.apiUrl);
      }
    )
  }

  ngOnInit(): void {
    this.currentUser = this.authenticationService.currentUserValue;
    this.listChangedSubscribe = this.service.listChanged.subscribe(
      (data) => {
        this.modelList = data.reverse();
        this.subjectList = this.modelList.map(function (a) {
          return { Name: a.Name, Id: a.Id };
        });
        console.log(this.subjectList);

        this.spinner.hide();
      },
      (error) => {
        this.modelList = [];
        this.spinner.hide();
      }
    );
    this.service.setAll(this.apiUrl);
  }

  openEditModal(id: number) {
    this.modalRef = this.modalService.show(SubjectEditModalComponent, {
      initialState: {
        title: this.title,
        data: {
          id: id,
        },
      },
      class: "modal-lg",
    });
  }

  //#region Datatable Settings
  ngAfterViewInit(): void {
    setTimeout(() => {
      this.dtTrigger.next();
    }, 300);
  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }

  rerender(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      // Destroy the table first
      dtInstance.destroy();
      // Call the dtTrigger to rerender again
      this.dtTrigger.next();
    });
  }

  //#endregion

}
