import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { EditComponent } from 'src/app/generics/components/edit/edit.component';
import { GenericService } from 'src/app/generics/services/generic.service';
import { SubjectModel } from 'src/app/models/basic/subject';
import { CheckNameService } from 'src/app/regulars/services/check-name.service';
import { SubjectService } from 'src/app/regulars/services/subject.service';

@Component({
  selector: "app-subject-edit-modal",
  templateUrl: "./subject-edit-modal.component.html",
  styleUrls: ["./subject-edit-modal.component.css"],
})
export class SubjectEditModalComponent extends EditComponent implements OnInit {
  editMode: boolean = true;
  existingName:string=null;

  constructor(
    public modalRef: BsModalRef,
    public service: GenericService,
    public checkNameService: CheckNameService,
    public _service: SubjectService,
    public router: Router,
    public toastr: ToastrService,
    public spinner: NgxSpinnerService
  ) {
    super(modalRef, service, router, toastr, spinner);
    this.model = new SubjectModel();
    this.apiUrl = "Subjects";
  }

  ngOnInit() {
    this.setDataByParams();

    setTimeout(() => {
      this.existingName = this.model.Name;
    }, 1000);
    
  }

  onUpdate() {
    this.checkNameService
      .checkName("Subjects", this.model)
      .subscribe((response) => {
        if (response != null) {
          if (response["Name"] != this.existingName) {
            this.toastr.error("This Subject name is Already Exist");
          } else {
            this.updateSubject();
          }
        }

        if (response == null) {
          this.updateSubject();
        }
      });
  }

  updateSubject() {
    this.service.update(this.model.Id, this.model, "Subjects").subscribe(
      (response) => {
        if (response) {
          this.modalRef.hide();
          this.toastr.success("Subject Updated Successfully");
          this._service.refreshList("refresh");
        }
      },
      (error) => {
        this.toastr.error("Internal Server Error");
      }
    );
  }
}
