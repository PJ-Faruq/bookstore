import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { NgxSpinnerService } from "ngx-spinner";
import { ToastrService } from "ngx-toastr";
import { SubjectModel } from "src/app/models/basic/subject";
import { CreateComponent } from "src/app/generics/components/create/create.component";
import { GenericService } from "src/app/generics/services/generic.service";
import { CheckNameService } from "src/app/regulars/services/check-name.service";

@Component({
  selector: "app-subject-create",
  templateUrl: "./subject-create.component.html",
  styleUrls: ["./subject-create.component.css"],
})
export class SubjectCreateComponent extends CreateComponent implements OnInit {
  title = "Subject";

  constructor(
    public service: GenericService,
    public checkNameService: CheckNameService,

    public router: Router,
    public activatedRoute: ActivatedRoute,
    public spinner: NgxSpinnerService,
    public toastr: ToastrService
  ) {
    super(service, router, activatedRoute, spinner, toastr);
    this.model = new SubjectModel();
    this.apiUrl = "Subjects";
  }

  ngOnInit() {
    this.setDataByParams();
  }

  reset() {
    this.editMode = false;
    this.model = new SubjectModel();
  }

  onSubmit() {
    this.checkNameService.checkName("Subjects", this.model).subscribe(
      (response) => {
        if (response == null) {
          this.AddItem();
        }

        else{
          this.toastr.error("This Name is Already Exist !")
        }
      },
      (error) => {
        this.toastr.error("Internal Server Error");
      }
    );
  }

  AddItem() {
    this.service.add(this.model, "Subjects").subscribe(
      (resp) => {
        if (resp) {
          this.toastr.success("Subject Successfully Added");
          this.router.navigate(["/subject/list"]);
        } else {
          this.toastr.error("Subject Added Failed !");
        }
      },
      (err) => {
        this.toastr.error("Internal Server Error");
      }
    );
  }
}
