import { Component, OnInit } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { ListComponent } from 'src/app/generics/components/list/list.component';
import { GenericService } from 'src/app/generics/services/generic.service';
import { ConfirmService } from 'src/app/regulars/services/confirm.service';
import { RoleDetailModalComponent } from '../role-detail-modal/role-detail-modal.component';
import { RoleEditModalComponent } from '../role-edit-modal/role-edit-modal.component';

@Component({
  selector: 'app-role-list',
  templateUrl: './role-list.component.html',
  styleUrls: ['./role-list.component.css']
})
export class RoleListComponent extends ListComponent implements OnInit {


  title = "Role";
  apiUrl = "Roles";
  createLink = "/" + this.title.toLowerCase() + "/create";


  constructor(public service: GenericService,
    public modalService: BsModalService,
    public confirmService: ConfirmService,
    public toastr: ToastrService,
    public spinner: NgxSpinnerService
  ) {
    super(modalService, confirmService, toastr, spinner);
  }

  ngOnInit(): void {
    this.spinner.show();
    this.listChangedSubscribe = this.service.listChanged.subscribe(
      (data) => {
        this.modelList = data;
        this.spinner.hide();
      },
      (error) => {
        this.modelList = [];
        this.spinner.hide();
      }
    );
    this.confirmSubscription = this.confirmService.deleted.subscribe((data) => {
      if (data) {
        this.service.setAll(this.apiUrl);
        this.toastr.warning("Deleted Successfully", "Success");
      } else {
        this.toastr.warning("Can't Delete this item", "Warning");
      }
    });
    this.service.setAll(this.apiUrl);
  }

  openDetailModal(id: number) {
    this.modalRef = this.modalService.show(RoleDetailModalComponent, {
      initialState: {
        title: this.title,
        data: {
          id: id
        },
      },
      class: "modal-lg",
    });
  }

  openEditModal(id: number) {
    this.modalRef = this.modalService.show(RoleEditModalComponent, {
      initialState: {
        title: this.title,
        data: {
          id: id,
        },
      },
      class: "modal-lg",
    });
  }
}
