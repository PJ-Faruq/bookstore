import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { EditComponent } from 'src/app/generics/components/edit/edit.component';
import { GenericService } from 'src/app/generics/services/generic.service';
import { Role } from 'src/app/models/role';

@Component({
  selector: 'app-role-edit-modal',
  templateUrl: './role-edit-modal.component.html',
  styleUrls: ['./role-edit-modal.component.css']
})
export class RoleEditModalComponent extends EditComponent implements OnInit {

  editMode: boolean = true;

  constructor(
    public modalRef: BsModalRef,
    public service: GenericService,
    public router: Router,
    public toastr: ToastrService,
    public spinner: NgxSpinnerService) {

    super(modalRef, service, router, toastr, spinner);
    this.model = new Role();
    this.apiUrl = "Roles";

  }

  ngOnInit() {
    this.setDataByParams();
  }

}
