import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { NgxSpinnerService } from 'ngx-spinner';
import { DetailComponent } from 'src/app/generics/components/detail/detail.component';
import { GenericService } from 'src/app/generics/services/generic.service';
import { Role } from 'src/app/models/role';

@Component({
  selector: 'app-role-detail-modal',
  templateUrl: './role-detail-modal.component.html',
  styleUrls: ['./role-detail-modal.component.css']
})
export class RoleDetailModalComponent extends DetailComponent implements OnInit {

  constructor(public modalRef: BsModalRef,
    public service: GenericService,
    public spinner: NgxSpinnerService) {

    super(modalRef, service, spinner);
    this.model = new Role();
    this.apiUrl = "Roles"

  }

}
