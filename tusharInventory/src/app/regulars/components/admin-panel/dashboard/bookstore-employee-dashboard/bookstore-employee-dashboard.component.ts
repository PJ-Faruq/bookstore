import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserVm } from 'src/app/models/view-models/userVm.model';
import { DashboardService as DashboardService } from 'src/app/regulars/services/dashboard.service';
import { AutehnticationService } from 'src/app/regulars/services/autehntication.service';

@Component({
  selector: "app-bookstore-employee-dashboard",
  templateUrl: "./bookstore-employee-dashboard.component.html",
  styleUrls: ["./bookstore-employee-dashboard.component.css"],
})
export class BookstoreEmployeeDashboardComponent implements OnInit {
  currentUser: UserVm;
  model: any;
  constructor(
    private authenticationService: AutehnticationService,
    public service: DashboardService,
    public router: Router
  ) {}

  ngOnInit(): void {
    this.currentUser = this.authenticationService.currentUserValue;

    this.service
      .getBookstoreEmployeeDashboardData(this.currentUser.Id)
      .subscribe((response) => {
        this.model = response;
      });
  }

  onBookStock() {
    this.router.navigate(["/stock/bookstore"]);
  }

  onTotalSchool() {
    this.router.navigate(["/school/list"]);
  }

  onRequestClick(status: string) {
    this.router.navigate(["/order/list", { empStatus: status }]);
  }

  onTotalPurchase() {
    this.router.navigate(["/purchase-book/list"]);
  }

  onTotalBankSlip() {
    this.router.navigate(["/report/bank-slip"]);
  }

  onSubcityStockRequestClick(status: string) {
    this.router.navigate(["/subcity-store/request-list", { status: status }]);
  }
}
