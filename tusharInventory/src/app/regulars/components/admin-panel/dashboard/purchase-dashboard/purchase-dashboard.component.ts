import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserVm } from 'src/app/models/view-models/userVm.model';
import { DashboardService } from 'src/app/regulars/services/dashboard.service';
import { AutehnticationService } from 'src/app/regulars/services/autehntication.service';

@Component({
  selector: "app-purchase-dashboard",
  templateUrl: "./purchase-dashboard.component.html",
  styleUrls: ["./purchase-dashboard.component.css"],
})
export class PurchaseDashboardComponent implements OnInit {
  currentUser: UserVm;
  model: any;

  //For Chart
  single: any[];
  pieData: any[];
  multi: any[];

  viewForSub: any[] = [700, 400];
  viewForGrade: any[] = [700, 400];
  viewForLang: any[] = [700, 400];
  viewForPie: any[] = [700, 400];
  viewForUserInfo: any[] = [700, 400];

  // options
  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showLegend = true;
  showXAxisLabel = true;
  xAxisLabel = "Subject";
  showYAxisLabel = true;
  yAxisLabel = "Available Qty";

  colorScheme = {
    domain: ["#5AA454", "#A10A28", "#C7B42C", "#AAAAAA"],
  };

  colorSchemeForBudget = {
    domain: ["cornflowerblue", "sandybrown"],
  };

  constructor(
    private authenticationService: AutehnticationService,
    public service: DashboardService,
    public router: Router
  ) {}

  ngOnInit(): void {
    this.currentUser = this.authenticationService.currentUserValue;

    this.service
      .getPurchaseDashboardData(this.currentUser.Id)
      .subscribe((response) => {
        this.model = response;

        //Pie Chart Setting
        this.pieData = [{}];
      });
  }

  onBookStock() {
    this.router.navigate(["/stock/bookstore"]);
  }

  onTotalSchool() {
    this.router.navigate(["/school/list"]);
  }

  onTotalSupplier() {
    this.router.navigate(["/supplier/list"]);
  }

  onTotalPurchase() {
    this.router.navigate(["/purchase-book/list"]);
  }

  onLowStock(){
    this.router.navigate(["/stock/low"]);
  }

  onRequestClick(status: string) {
    this.router.navigate([
      "/purchase/purchase-list",
      { purchaseStatus: status },
    ]);
  }

  //For Budget Chart Data

  onSelect(data): void {
    console.log("Item clicked", JSON.parse(JSON.stringify(data)));
  }
  onActivate(data): void {
    console.log("Activate", JSON.parse(JSON.stringify(data)));
  }

  onDeactivate(data): void {
    console.log("Deactivate", JSON.parse(JSON.stringify(data)));
  }
}
