import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserVm } from 'src/app/models/view-models/userVm.model';
import { DashboardService } from 'src/app/regulars/services/dashboard.service';
import { AutehnticationService } from 'src/app/regulars/services/autehntication.service';

@Component({
  selector: "app-school-dashboard",
  templateUrl: "./school-dashboard.component.html",
  styleUrls: ["./school-dashboard.component.css"],
})
export class SchoolDashboardComponent implements OnInit {
  currentUser: UserVm;
  model: any;
  constructor(
    private authenticationService: AutehnticationService,
    public service: DashboardService,
    public router: Router
  ) {}

  ngOnInit(): void {
    this.currentUser = this.authenticationService.currentUserValue;

    this.service
      .getSchoolDashboardData(this.currentUser.SchoolId)
      .subscribe((response) => {
        this.model = response;
        console.log(this.model);
      });
  }

  onBookStock() {
    this.router.navigate(["/stock/school"]);
  }

  onRequestClick(status: string) {
    this.router.navigate(["/order/list", { schoolStatus: status }]);
  }
}
