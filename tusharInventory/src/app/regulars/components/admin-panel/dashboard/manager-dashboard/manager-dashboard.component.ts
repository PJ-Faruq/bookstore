import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserVm } from 'src/app/models/view-models/userVm.model';
import { DashboardService } from 'src/app/regulars/services/dashboard.service';
import { AutehnticationService } from 'src/app/regulars/services/autehntication.service';

@Component({
  selector: "app-manager-dashboard",
  templateUrl: "./manager-dashboard.component.html",
  styleUrls: ["./manager-dashboard.component.css"],
})
export class ManagerDashboardComponent implements OnInit {
  currentUser: UserVm;
  model: any;

  //For Chart
  single: any[];
  pieData: any[];
  multi: any[];

  viewForSub: any[] = [700, 400];
  viewForGrade: any[] = [700, 400];
  viewForLang: any[] = [700, 400];
  viewForPie: any[] = [700, 400];
  viewForUserInfo: any[] = [700, 400];

  // options
  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showLegend = true;
  showXAxisLabel = true;
  xAxisLabel = "Subject";
  showYAxisLabel = true;
  yAxisLabel = "Available Qty";

  colorScheme = {
    domain: ["#5AA454", "#A10A28", "#C7B42C", "#AAAAAA"],
  };

  colorSchemeForBudget = {
    domain: ["cornflowerblue", "sandybrown"],
  };

  constructor(
    private authenticationService: AutehnticationService,
    public service: DashboardService,
    public router: Router
  ) {}

  ngOnInit(): void {
    this.currentUser = this.authenticationService.currentUserValue;

    this.service
      .getManagerDashboardData(this.currentUser.Id)
      .subscribe((response) => {
        this.model = response;

        //Chart Setting
        this.viewForSub = [this.model.SubjectChartdata.length * 50, 400];
        this.viewForGrade = [this.model.GradeChartdata.length * 50, 400];
        this.viewForLang = [this.model.LanguageChartdata.length * 50, 400];

        //Pie Chart Setting
        this.pieData = [{}];
      });
  }

  onBookStock() {
    this.router.navigate(["/stock/bookstore"]);
  }

  onTotalPurchase() {
    this.router.navigate(["/purchase-book/list"]);
  }

  onRequestClick(status: string) {
    this.router.navigate(["/order/list", { managerStatus: status }]);
  }

  onPurchaseRequestClick(status: string) {
    this.router.navigate([
      "/purchase/purchase-list",
      { managerStatus: status },
    ]);
  }

  onSubcityStockRequestClick(status: string) {
    this.router.navigate(["/subcity-store/request-list", { status: status }]);
  }

  //Chart Setting

  onSelect(event) {
    console.log(event);
  }

  //For Pie Chart
  // onSelect(data): void {
  //   console.log("Item clicked", JSON.parse(JSON.stringify(data)));
  // }

  onActivate(data): void {
    console.log("Activate", JSON.parse(JSON.stringify(data)));
  }

  onDeactivate(data): void {
    console.log("Deactivate", JSON.parse(JSON.stringify(data)));
  }
}
