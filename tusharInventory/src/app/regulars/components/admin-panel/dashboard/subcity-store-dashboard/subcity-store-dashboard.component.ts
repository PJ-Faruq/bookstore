import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { UserVm } from "src/app/models/view-models/userVm.model";
import { DashboardService as DashboardService } from "src/app/regulars/services/dashboard.service";
import { AutehnticationService } from "src/app/regulars/services/autehntication.service";


@Component({
  selector: "app-subcity-store-dashboard",
  templateUrl: "./subcity-store-dashboard.component.html",
  styleUrls: ["./subcity-store-dashboard.component.css"],
})
export class SubcityStoreDashboardComponent implements OnInit {
  currentUser: UserVm;
  model: any;
  constructor(
    private authenticationService: AutehnticationService,
    public service: DashboardService,
    public router: Router
  ) {}

  ngOnInit(): void {
    this.currentUser = this.authenticationService.currentUserValue;

    this.service
      .getSubcityDashboardData(this.currentUser.Id)
      .subscribe((response) => {
        this.model = response;
      });
  }

  onSubcityStockRequestClick(status: string) {
    this.router.navigate(["/subcity-store/request-list", { status: status }]);
  }

  onRequestClick(status: string) {
    this.router.navigate(["/order/list", { subcityStatus: status }]);
  }
}
