import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from 'src/app/models/user';
import { AdminDashboardVm } from 'src/app/models/view-models/adminDashboardVm';
import { UserVm } from 'src/app/models/view-models/userVm.model';
import { DashboardService } from 'src/app/regulars/services/dashboard.service';
import { AutehnticationService } from 'src/app/regulars/services/autehntication.service';

const data=[
  {
    name: "Germany",
    value: 8940000,
  },
  {
    name: "USA",
    value: 5000000,
  },
  {
    name: "France",
    value: 7200000,
  },
  {
    name: "UK",
    value: 6200000,
  },
];

@Component({
  selector: "app-admin-dashboard",
  templateUrl: "./admin-dashboard.component.html",
  styleUrls: ["./admin-dashboard.component.css"],
})
export class AdminDashboardComponent implements OnInit {
  currentUser: UserVm;
  model: any;

  //For Chart
  single: any[];
  pieData: any[];
  multi: any[];

  viewForSub: any[] = [700, 400];
  viewForGrade: any[] = [700, 400];
  viewForLang: any[] = [700, 400];
  viewForPie: any[] = [700, 400];
  viewForUserInfo: any[] = [700, 400];

  // options
  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showLegend = true;
  showXAxisLabel = true;
  xAxisLabel = "Subject";
  showYAxisLabel = true;
  yAxisLabel = "Available Qty";

  colorScheme = {
    domain: ["#5AA454", "#A10A28", "#C7B42C", "#AAAAAA"],
  };

  constructor(
    private authenticationService: AutehnticationService,
    public service: DashboardService,
    public router: Router
  ) {}

  ngOnInit(): void {
    this.currentUser = this.authenticationService.currentUserValue;

    this.service.getAdminDashboardData().subscribe((response) => {
      this.model = response;

      //Chart Setting
      this.viewForSub = [this.model.SubjectChartdata.length * 50, 400];
      this.viewForGrade = [this.model.GradeChartdata.length * 50, 400];
      this.viewForLang = [this.model.LanguageChartdata.length * 50, 400];

      //Pie Chart Setting
      this.pieData = [{}];
    });
  }

  onBookStock() {
    this.router.navigate(["/stock/bookstore"]);
  }

  onTotalSchool() {
    this.router.navigate(["/school/list"]);
  }

  onRequestClick(status: string) {
    this.router.navigate(["/order/list", { status: status }]);
  }

  onActiveUser(status: string) {
    this.router.navigate(["/user/list", { status: status }]);
  }

  onUserPendingRequest(status:string){
      this.router.navigate(["/user/list", { status: status }]);
  }
  //Chart Setting

  onSelect(event) {
    console.log(event);
  }

  //For Pie Chart
  // onSelect(data): void {
  //   console.log("Item clicked", JSON.parse(JSON.stringify(data)));
  // }

  onActivate(data): void {
    console.log("Activate", JSON.parse(JSON.stringify(data)));
  }

  onDeactivate(data): void {
    console.log("Deactivate", JSON.parse(JSON.stringify(data)));
  }
}
