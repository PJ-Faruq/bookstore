import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserVm } from 'src/app/models/view-models/userVm.model';
import { AutehnticationService } from 'src/app/regulars/services/autehntication.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  constructor(private authenticationService: AutehnticationService,private router: Router) {

  }

  currentUser: UserVm;
  ngOnInit() {
    this.currentUser = this.authenticationService.currentUserValue;

    if (this.currentUser.Role == "Bookstore Employee") {
      this.router.navigate(["/bookstore-employee-dashboard"]);
    } else if (this.currentUser.Role == "Main Admin") {
      this.router.navigate(["/admin-dashboard"]);
    } else if (this.currentUser.Role == "School Employee") {
      this.router.navigate(["/school-dashboard"]);
    } else if (this.currentUser.Role == "Purchase Department") {
      this.router.navigate(["/purchase-dashboard"]);
    } else if (this.currentUser.Role == "Bookstore Manager") {
      this.router.navigate(["/manager-dashboard"]);
    } else if (this.currentUser.Role == "Subcity Manager") {
      this.router.navigate(["/subcity-store-dashboard"]);
    }
    else{
      this.router.navigate(["/user-auth/login"]);
    }

    
  }


}
