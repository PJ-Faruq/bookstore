import {
  Component,
  OnInit,
  ViewChild
} from "@angular/core";
import {
  NgForm
} from "@angular/forms";
import {
  Router,
  ActivatedRoute
} from "@angular/router";
import {
  BsDatepickerConfig
} from "ngx-bootstrap/datepicker";
import {
  NgxSpinnerService
} from "ngx-spinner";
import {
  ToastrService
} from "ngx-toastr";
import {
  CreateComponent
} from "src/app/generics/components/create/create.component";
import {
  GenericService
} from "src/app/generics/services/generic.service";
import {
  Book
} from "src/app/models/basic/book";
import {
  HeadOffice
} from "src/app/models/basic/head-office";
import {
  School
} from "src/app/models/basic/school";
import {
  SubjectModel
} from "src/app/models/basic/subject";
import {
  Supplier
} from "src/app/models/basic/supplier";
import {
  Order
} from "src/app/models/operation/order";
import {
  OrderLine
} from "src/app/models/operation/order-line";
import {
  User
} from "src/app/models/user";
import {
  OrderShowVm
} from "src/app/models/view-models/OrderShowVm";
import {
  UserVm
} from "src/app/models/view-models/userVm.model";
import {
  AutehnticationService
} from "src/app/regulars/services/autehntication.service";
import {
  OrderService
} from "src/app/regulars/services/order.service";

@Component({
  selector: "app-order-create",
  templateUrl: "./order-create.component.html",
  styleUrls: ["./order-create.component.css"],
})
export class OrderCreateComponent extends CreateComponent implements OnInit {
  currentUser: UserVm;
  title = "Order";
  @ViewChild("form") orderForm: NgForm;
  @ViewChild("formRow") rowForm: NgForm;
  @ViewChild("fileInput") fileInput: HTMLInputElement;

  schoolList: School[] = [];
  hoList: HeadOffice[] = [];
  supplierList: Supplier[] = [];
  bookstoreEmployeeList: User[] = [];

  bookList: Book[] = [];
  subjectList: SubjectModel[] = [];
  gradeList: SubjectModel[] = [];
  languageList: SubjectModel[] = [];

  orderVmList: OrderShowVm[] = [];

  bookModel: any;
  orderCount: number;

  datePickerConfig: Partial<BsDatepickerConfig>;
  BookPrice: any;
  constructor(
    public service: GenericService,
    public _service: OrderService,
    public router: Router,
    public activatedRoute: ActivatedRoute,
    public spinner: NgxSpinnerService,
    public toastr: ToastrService,
    private authenticationService: AutehnticationService
  ) {
    super(service, router, activatedRoute, spinner, toastr);
    this.currentUser = this.authenticationService.currentUserValue;

    this.model = new Order();
    this.apiUrl = "Orders";
    this.modelId = parseInt(this.activatedRoute.snapshot.paramMap.get("id"));

    this.datePickerConfig = Object.assign(
      {},
      {
        containerClass: "theme-dark-blue",
        showWeekNumbers: true,
        // dateInputFormat: 'DD/MM/YYYY'
      }
    );
  }

  ngOnInit() {
    if (!this.modelId) {
      this._service.getOrderCount().subscribe((response) => {
        this.orderCount = response;

        if (this.orderCount >= 1000) {
          this.router.navigate(["/request-limit"]);
        }
      });
    }

    this._service.getBookstoreEmployee().subscribe((response) => {
      this.bookstoreEmployeeList = response;
    });

    this.service.getAll("Schools").subscribe(
      (data) => {
        this.schoolList = data;
        this.model.SchoolId = this.currentUser.SchoolId;
      },
      (err) => {
        this.schoolList = [];
      }
    );

    this.service.getAll("HeadOffices").subscribe(
      (data) => {
        this.hoList = data;
      },
      (err) => {
        this.hoList = [];
      }
    );

    this.service.getAll("Books").subscribe(
      (data) => {
        this.bookList = data;
      },
      (err) => {
        this.bookList = [];
      }
    );

    this.service.getAll("Suppliers").subscribe(
      (data) => {
        this.supplierList = data;
      },
      (err) => {
        this.supplierList = [];
      }
    );

    this.service.getAll("Subjects").subscribe(
      (data) => {
        this.subjectList = data;
      },
      (err) => {
        this.subjectList = [];
      }
    );

    this.service.getAll("GradeLavels").subscribe(
      (data) => {
        this.gradeList = data;
      },
      (err) => {
        this.gradeList = [];
      }
    );

    this.service.getAll("Languages").subscribe(
      (data) => {
        this.languageList = data;
      },
      (err) => {
        this.languageList = [];
      }
    );

    this.setDataByParams();

    setTimeout(() => {
      if (this.modelId) {
        this.bindEditValue();
      }
    }, 1000);
  }

  bindEditValue() {
    for (let value of this.model.OrderLines) {
      //For showing order custom view in the Table
      let orderVm = new OrderShowVm();

      orderVm.BookSubjectName = this.subjectList.find(
        (m) => m.Id == value["BookSubjectId"]
      ).Name;
      orderVm.BookLanguageName = this.languageList.find(
        (m) => m.Id == value["BookLanguageId"]
      ).Name;
      orderVm.BookGradeName = this.gradeList.find(
        (m) => m.Id == value["BookGradeLavelId"]
      ).Name;

      orderVm.Quantity = value["Quantity"];
      orderVm.BookPrice = value["BookPrice"];

      this.orderVmList.push(orderVm);
    }
  }

  reset() {
    this.editMode = false;
    this.model = new Order();
  }

  onChangeFile(file: FileList) {
    this.model.File = file.item(0);
  }

  onNewLine(form: NgForm) {
    const value = form.value;

    //For Binding value for the backend
    var orderLine = new OrderLine();
    orderLine = value;
    orderLine.BookPrice = this.BookPrice;
    this.model.OrderLines.push(orderLine); 

    this.getTotalAmount();

    //For showing order custom view in the Table
    let orderVm = new OrderShowVm();

    orderVm.BookSubjectName = this.subjectList.find(
      (m) => m.Id == value.BookSubjectId
    ).Name;
    orderVm.BookLanguageName = this.languageList.find(
      (m) => m.Id == value.BookLanguageId
    ).Name;
    orderVm.BookGradeName = this.gradeList.find(
      (m) => m.Id == value.BookGradeLavelId
    ).Name;

    orderVm.Quantity = value.Quantity;
    orderVm.BookPrice = this.BookPrice;

    this.orderVmList.push(orderVm);
    form.reset();
  }

  getPrice(form: NgForm) {
    console.log(form);
    let value = form.value;
    if (
      value.BookLanguageId == "" ||
      value.BookLanguageId == null ||
      value.BookGradeLavelId == "" ||
      value.BookGradeLavelId == null ||
      value.BookSubjectId == "" ||
      value.BookSubjectId == null
    ) {
      return;
    }

    this._service.getPrice(value).subscribe(
      (response) => {
        if (response == null) {
          this.toastr.error("Sorry, No Book Available !!");
        }

        this.BookPrice = response;
      },
      (error) => {
        this.toastr.error("Some Errors Occur");
      }
    );
  }

  onDeleteLine(i) {
    this.model.OrderLines.splice(i, 1);
    this.orderVmList.splice(i, 1);
    this.getTotalAmount();
  }

  onSubmit(form) {
    if (this.modelId) {
      this.service.update(this.modelId, this.model, "Orders").subscribe(
        (response) => {
          if (response) {
            this.toastr.success("Request Successfully Updated");
             this.router.navigate(["/order/list", { schoolStatus: "pending" }]);
          }
        },
        (error) => {
          this.toastr.error("Request Submission Failed !");
        }
      );
    } else {
      const data = new FormData();
      //data.append("IsSchool", this.model.IsSchool);
      //data.append("SchoolId", this.model.SchoolId);
     // data.append("SchoolEmployeeId", this.currentUser.Id.toString());
      //data.append("BookstoreEmployeeId", this.model.BookstoreEmployeeId);
      //data.append("OrderDate", new Date().toDateString());
      //data.append("Amount", this.model.Amount);
      //data.append("Note", this.model.Note);
      //data.append("File", this.model.File);
      //data.append("Status", "1");
      //data.append("BookstoreEmployeeStatus", "1");
      //data.append("OrderLine", JSON.stringify(this.model.OrderLines));

      this.model.SchoolEmployeeId = this.currentUser.Id.toString();
      this.model.OrderDate = new Date().toDateString();
      this.model.Status=1;
      this.model.BookstoreEmployeeStatus=1;

      this.service.add(this.model, "Orders").subscribe(
        (response) => {
          if (response) {
            this.toastr.success("Request Successfully Submitted");
            this.router.navigate(["/order/list", { schoolStatus: "pending" }]);
          }
        },
        (error) => {
          this.toastr.error("Request Submission Failed !");
        }
      );
    }
  }

  getTotalAmount() {
    let totalAmount = 0;
    for (let item of this.model.OrderLines) {
      totalAmount = totalAmount + item.BookPrice * item.Quantity;
    }

    this.model.Amount = totalAmount;
  }

  getTotalAmountForVm() {
    let totalAmount = 0;
    for (let item of this.orderVmList) {
      totalAmount = totalAmount + item.BookPrice * item.Quantity;
    }

    for(let i=0;i<this.orderVmList.length;i++){
      this.model.OrderLines[i].Quantity = this.orderVmList[i].Quantity;
    }

    this.model.Amount = totalAmount;
    console.log(this.model);
    
  }

  // getTotal() {
  //   var amount = 0;
  //   this.model.OrderLines.forEach((line) => {
  //     var book = this.bookList.find(
  //       (x) =>
  //         x.BookSubjectId === line.BookSubjectId &&
  //         x.BookGradeLavelId === line.BookGradeId &&
  //         x.BookLanguageId === line.BookLanguageId
  //     );
  //     if (book != null && book != undefined) {
  //       var linePrice = book.SalePrice * line.Quantity;
  //       amount = amount + linePrice;
  //     }
  //   });
  //   this.model.Amount = amount;
  // }
}
