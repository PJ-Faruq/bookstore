import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { DetailComponent } from 'src/app/generics/components/detail/detail.component';
import { GenericService } from 'src/app/generics/services/generic.service';
import { Order } from 'src/app/models/operation/order';
import { UserVm } from 'src/app/models/view-models/userVm.model';
import { AutehnticationService } from 'src/app/regulars/services/autehntication.service';
import { OrderService } from 'src/app/regulars/services/order.service';
import { ReadFile } from 'src/app/shared/read-file';

@Component({
  selector: 'app-order-file-upload-modal',
  templateUrl: './order-file-upload-modal.component.html',
  styleUrls: ['./order-file-upload-modal.component.css']
})
export class OrderFileUploadModalComponent extends DetailComponent implements OnInit {

  currentUser: UserVm;
  file: string;

  constructor(public modalRef: BsModalRef,
    public service: GenericService,
    public _service:OrderService,
    public spinner: NgxSpinnerService,
    private toastr: ToastrService,
    private authenticationService: AutehnticationService,
    private _readFile: ReadFile,) {

    super(modalRef, service, spinner);
    this.model = new Order();
    this.apiUrl = "Orders"

  }

  ngOnInit(): void {
    this.currentUser = this.authenticationService.currentUserValue;
    this.setDataByParams(this.data.id);
  }


  update() {
    this.model.File = this.file;


    const data=new FormData();
    data.append("Id",this.model.Id);
    data.append("File",this.model.File);

    this._service.updateFile(data).subscribe((response) => {

      if(response){
        this.toastr.success('Bank Slip Successfully Uploaded', 'Success');
        this._service.refreshList("Update");
        this.service.setAll(this.apiUrl);
        
        this.modalRef.hide();
      }
      
    },
      (error) => {
        this.toastr.error('Ops! network or server related error occured', 'Error');
      })
  }

  //for Image --- start
  fileChange(event: any) {
    if (event.target.files && event.target.files[0]) {
      this.file = event.target.files[0];
    }
  }

  fileRemovedImage() {
    this.file = '';
  }
  //for Image --- end
}
