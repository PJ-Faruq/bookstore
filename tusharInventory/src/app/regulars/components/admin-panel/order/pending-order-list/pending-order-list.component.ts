import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BsModalService } from 'ngx-bootstrap/modal';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { ListComponent } from 'src/app/generics/components/list/list.component';
import { GenericService } from 'src/app/generics/services/generic.service';
import { UserVm } from 'src/app/models/view-models/userVm.model';
import { AutehnticationService } from 'src/app/regulars/services/autehntication.service';
import { ConfirmService } from 'src/app/regulars/services/confirm.service';
import { OrderDetailModalComponent } from '../order-detail-modal/order-detail-modal.component';

@Component({
  selector: 'app-pending-order-list',
  templateUrl: './pending-order-list.component.html',
  styleUrls: ['./pending-order-list.component.css']
})
export class PendingOrderListComponent extends ListComponent implements OnInit {

  title = "Order";
  apiUrl = "Orders";
  createLink = "/" + this.title.toLowerCase() + "/create";
  currentUser: UserVm;

  constructor(public service: GenericService,
    public modalService: BsModalService,
    public confirmService: ConfirmService,
    public toastr: ToastrService,
    public spinner: NgxSpinnerService,
    private router: Router,
    private authenticationService: AutehnticationService
  ) {
    super(modalService, confirmService, toastr, spinner);
  }

  ngOnInit() {
    this.currentUser = this.authenticationService.currentUserValue;
    this.spinner.show();
    this.listChangedSubscribe = this.service.listChanged.subscribe(
      (data) => {
        this.modelList = data;
        this.spinner.hide();
      },
      (error) => {
        this.modelList = [];
        this.spinner.hide();
      }
    );
    this.service.setAllByParam(this.apiUrl, 0, "Pending");
  }

  openDetailModal(id: number) {
    this.modalRef = this.modalService.show(OrderDetailModalComponent, {
      initialState: {
        title: this.title,
        data: {
          id: id
        },
      },
      class: "modal-lg",
    });
  }
}
