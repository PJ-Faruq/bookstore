import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DataTableDirective } from 'angular-datatables';
import { BsModalService } from 'ngx-bootstrap/modal';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { Subject } from 'rxjs';
import { ListComponent } from 'src/app/generics/components/list/list.component';
import { GenericService } from 'src/app/generics/services/generic.service';
import { Order } from 'src/app/models/operation/order';
import { OrderSearchVm } from 'src/app/models/view-models/OrderSearchVm';
import { UserVm } from 'src/app/models/view-models/userVm.model';
import { AutehnticationService } from 'src/app/regulars/services/autehntication.service';
import { ConfirmService } from 'src/app/regulars/services/confirm.service';
import { OrderService } from 'src/app/regulars/services/order.service';
import { OrderDetailModalComponent } from '../order-detail-modal/order-detail-modal.component';
import { OrderEditModalComponent } from '../order-edit-modal/order-edit-modal.component';
import { OrderFileUploadModalComponent } from '../order-file-upload-modal/order-file-upload-modal.component';

@Component({
  selector: "app-order-list",
  templateUrl: "./order-list.component.html",
  styleUrls: ["./order-list.component.css"],
})
export class OrderListComponent extends ListComponent implements OnInit {
  title = "Order";
  apiUrl = "Orders";
  searchVm: OrderSearchVm;
  createLink = "/" + this.title.toLowerCase() + "/create";
  currentUser: UserVm;
  p: number = 1;

  //Datatable Field
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject<any>();
  dtElement: DataTableDirective;

  constructor(
    public service: GenericService,
    public _service: OrderService,
    public modalService: BsModalService,
    public confirmService: ConfirmService,
    public toastr: ToastrService,
    public spinner: NgxSpinnerService,
    private router: Router,
    private route: ActivatedRoute,
    private authenticationService: AutehnticationService
  ) {
    super(modalService, confirmService, toastr, spinner);

    this._service.listen().subscribe((response) => {
      this.getBySearch();
    });

    this.confirmService.listen().subscribe((response) => {
      this.service.setAll(this.apiUrl);
    });

    this.searchVm = new OrderSearchVm();

    this.searchVm.AdminStatus = this.route.snapshot.paramMap.get("status"); //this comes from Main admin dashboard for filtering
    this.searchVm.EmpStatus = this.route.snapshot.paramMap.get("empStatus"); //this comes from Bookstore Employee dashboard for filtering
    this.searchVm.SchoolStatus = this.route.snapshot.paramMap.get(
      "schoolStatus"
    ); //this comes from School dashboard for filtering
    this.searchVm.ManagerStatus = this.route.snapshot.paramMap.get(
      "managerStatus"
    ); //this comes from Manager dashboard for filtering
    this.searchVm.SubcityStatus = this.route.snapshot.paramMap.get(
      "subcityStatus"
    ); //this comes from Subcity dashboard for filtering
  }

  ngOnInit() {
    this.currentUser = this.authenticationService.currentUserValue;
    this.getBySearch();

    this.listChangedSubscribe = this.service.listChanged.subscribe(
      (data) => {
        //this.modelList = data;

        this.spinner.hide();
      },
      (error) => {
        this.modelList = [];
        this.spinner.hide();
      }
    );
    this.confirmSubscription = this.confirmService.deleted.subscribe((data) => {
      if (data) {
        this.service.setAll(this.apiUrl);
        this._service.refreshList("Update");
        this.toastr.success("Deleted Successfully", "Success");
      } else {
        this.toastr.warning("Can't Delete this item", "Warning");
      }
    });
    //this.service.setAll(this.apiUrl);
  }

  getBySearch() {
    this.searchVm.CurrentUserId = this.currentUser.Id;
    this._service.search(this.searchVm).subscribe((response) => {
      if (this.currentUser.Role == "School Employee") {
        this.modelList = response.reverse();
      } else {
        this.modelList = response;
      }
    });
  }

  onEdit(Id: number) {
    this.router.navigate(["/order/edit", Id]);
  }

  openDetailModal(id: number) {
    this.modalRef = this.modalService.show(OrderDetailModalComponent, {
      initialState: {
        title: this.title,
        data: {
          id: id,
        },
      },
      class: "modal-lg",
      ignoreBackdropClick: false,
    });
  }

  openFileUploadModal(id: number) {
    this.modalRef = this.modalService.show(OrderFileUploadModalComponent, {
      initialState: {
        title: "Upload Bank Slip",
        data: {
          id: id,
        },
      },
      class: "modal-lg",
      ignoreBackdropClick: true,
    });
  }

  onBookEmployeeCompleted(id: number) {
    //this.router.navigate(['/report/sales-report',id]);
    this.service.getById(id, "Orders").subscribe((response) => {
      let model = new Order();
      model = response;
      model.IsCollected = true;
      model.BookstoreManagerStatus = 4;
      model.BookstoreEmployeeStatus = 7;
      model.Status = 7;

      this._service.updateMainStock(model).subscribe(
        (response) => {
          if (response) {
            this.toastr.success("Order Successfully Completed");
            this._service.refreshList("Update");
            this.service.setAll(this.apiUrl);
          }
        },
        (error) => {
          this.toastr.error("Internal Server Error");
        }
      );
    });
  }

  onBookSubcityCompleted(id: number) {
    this.service.getById(id, "Orders").subscribe((response) => {
      let model = new Order();
      model = response;
      model.IsCollected = true;
      model.BookstoreManagerStatus = 4;
      model.BookstoreEmployeeStatus = 7;
      model.Status = 7;

      this._service.updateSubcityStock(model).subscribe(
        (response) => {
          if (response) {
            this.toastr.success("Order Successfully Completed");
            this._service.refreshList("Update");
            this.service.setAll(this.apiUrl);
          }
        },
        (error) => {
          this.toastr.error("Internal Server Error");
        }
      );
    });
  }
  onClickReceipt(id: number) {
    const url = this.router.createUrlTree(["/report/sales-report", id]);
    window.open(url.toString(), "_blank");
  }

  //#region Datatable Settings
      ngAfterViewInit(): void {
        setTimeout(() => {
          this.dtTrigger.next();
        }, 300);
      }

      ngOnDestroy(): void {
        // Do not forget to unsubscribe the event
        this.dtTrigger.unsubscribe();
      }

      rerender(): void {
        this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
          // Destroy the table first
          dtInstance.destroy();
          // Call the dtTrigger to rerender again
          this.dtTrigger.next();
        });
      }

  //#endregion

}
