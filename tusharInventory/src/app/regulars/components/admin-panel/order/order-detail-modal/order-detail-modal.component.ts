import { ToastrService } from 'ngx-toastr';
import { Component, OnInit } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { NgxSpinnerService } from 'ngx-spinner';
import { DetailComponent } from 'src/app/generics/components/detail/detail.component';
import { GenericService } from 'src/app/generics/services/generic.service';
import { Order } from 'src/app/models/operation/order';
import { UserVm } from 'src/app/models/view-models/userVm.model';
import { AutehnticationService } from 'src/app/regulars/services/autehntication.service';
import { OrderService } from 'src/app/regulars/services/order.service';
import { AppSettings } from 'src/app/shared/app-settings';
import { PurchaseCreateComponent } from '../../purchase/purchase-create/purchase-create.component';
import { ThrowStmt } from '@angular/compiler';
import { PurchaseService } from 'src/app/regulars/services/purchase.service';

@Component({
  selector: "app-order-detail-modal",
  templateUrl: "./order-detail-modal.component.html",
  styleUrls: ["./order-detail-modal.component.css"],
})
export class OrderDetailModalComponent
  extends DetailComponent
  implements OnInit {
  currentUser: UserVm;
  outOfStock = false;
  isAvailable = false;
  modalReff: BsModalRef;
  managerList: any;
  pendingModel:Order;

  constructor(
    public modalRef: BsModalRef,
    public modalService: BsModalService,
    public purchaseService: PurchaseService,
    public service: GenericService,
    public _service: OrderService,
    public spinner: NgxSpinnerService,
    private toastr: ToastrService,
    private authenticationService: AutehnticationService
  ) {
    super(modalRef, service, spinner);
    this.model = new Order();
    this.pendingModel=new Order;
    this.apiUrl = "Orders";
  }

  ngOnInit(): void {
    this.setDataByParams(this.data.id);

    this.currentUser = this.authenticationService.currentUserValue;

    this.purchaseService.getManagerList().subscribe((response) => {
      this.managerList = response;
    });

    setTimeout(() => {
      //for disabling the purchase button
      for (let item of this.model.OrderLines) {
        if (item.AvailableQty < item.Quantity) {
          this.outOfStock = true;
        } else {
          this.isAvailable = true;
        }
      }
    }, 300);
  }

  //Manager Click Events
  onManagerApprove() {
    this.model.BookstoreManagerStatus = 2;
    this.model.BookstoreManagerId = this.currentUser.Id;

    if (this.model.School.SchoolOwnership.Name == "የመንግስት") {
      this.checkSubcityStock();
    } else {
      this.model.BookstoreEmployeeStatus = 4;
      this.model.Status = 2;

      const info = {
        message: "Order Approved Successfull",
        type: "success",
      };
      this.updateStatus(info);
    }

    

    
  }

  onManagerReject() {
    this.model.BookstoreManagerStatus = 3; //Manager status 3 == reject
    this.model.BookstoreEmployeeStatus = 6; //BookstoreEmployeeStatus 6 == reject
    this.model.Status = 5; // Status 5 == reject

    const info = { message: "Order is Rejected !! ", type: "error" };
    this.updateStatus(info);

    //Update Allocated Stock
    this._service.updateAllocatedStock(this.model).subscribe(
      (response) => {},
      (error) => {
        this.toastr.error("Server Error");
      }
    );
  }

  //Bookstore Employee Click Events

  onOutOfStock() {
    this.model.BookstoreEmployeeStatus = 8;
    this.model.Status = 6;

    const info = {
      message: "Order Pending due to Out of Stock",
      type: "error",
    };
    this.updateStatus(info);
  }

  onRequestToManagerForAvailable(){

    //Create a new order with outOfStock Books
    this.pendingModel = Object.assign({}, this.model); //Clone a object without referencing
    this.pendingModel.OrderLines = this.pendingModel.OrderLines.filter(
      (m) => m.AvailableQty < m.Quantity
    );
    this.pendingModel.Id = 0;

    this.pendingModel.BookstoreEmployeeStatus = 8;
    this.pendingModel.Status = 6;

    //For avoiding Attaching problem while creating a new order
    this.pendingModel.BookstoreEmployee=null;
    this.pendingModel.Ho=null;
    this.pendingModel.School=null;
    this.pendingModel.SchoolEmployee=null;
    this.pendingModel.BookstoreManager=null;

    for(let item of this.pendingModel.OrderLines){
      item.BookGradeLavel= null;
      item.BookLanguage=null;
      item.BookSubject=null;
      item.Id=0;
      item.OrderId=0;
      item.Order=null;
    }

    this.service.add(this.pendingModel, "Orders").subscribe(
      (response) => {
        if (response) {
          //this.toastr.success("Request Successfully Submitted");
        }
      },
      (error) => {
        this.toastr.error("Some Error Occured");
      }
    );

    //Update Order list with only the Available Books
    
    this.model.BookstoreManagerStatus = 1;
    this.model.BookstoreEmployeeStatus = 2;
    this.model.IsRequestedToManger = true;
    this.model.BookstoreEmployeeId = this.currentUser.Id;

    this.model.OrderLines = this.model.OrderLines.filter(
      (m) => m.AvailableQty >= m.Quantity
    );
    

    const total =this.pendingModel.OrderLines.length + this.model.OrderLines.length;
    this.model.ApprovalInfo =
      this.model.OrderLines.length +
      " of " +
      total +
      " Books Approved " +
      this.pendingModel.OrderLines.length +
      " Book in Pending for out of Stock";
    
    this.service.update(this.model.Id, this.model, "Orders").subscribe(
      (response) => {
        if (response) {
          this.toastr.success("Request Successfully sent to Manager");
          this.modalRef.hide();
          this._service.refreshList("update");

          //Update Allocated Stock
          this._service.updateAllocatedStock(this.model).subscribe(
            (response) => {},
            (error) => {
              this.toastr.error("Server Error");
            }
          );
        }
      },
      (error) => {
        this.toastr.error("Some Error Occured");
      }
    );

  }

  requestTOManager() {
    if (this.currentUser.Role == "Bookstore Employee") {
      this.model.BookstoreManagerStatus = 1;
      this.model.BookstoreEmployeeStatus = 2;
      this.model.IsRequestedToManger = true;
      this.model.BookstoreEmployeeId = this.currentUser.Id;

      //Update Status
      const info = {
        message: "Request Successfully Sent to Manager",
        type: "success",
      };
      this.updateStatus(info);

      //Update Allocated Stock
      this._service.updateAllocatedStock(this.model).subscribe(
        (response) => {},
        (error) => {
          this.toastr.error("Server Error");
        }
      );
    }
  }

  onBookstoreEmployeeApprove() {
    this.checkSubcityStock();
  }

  onBookstoreEmployeeReject() {
    this.model.BookstoreEmployeeStatus = 6;
    this.model.BookstoreManagerStatus = 3;
    this.model.Status = 5;

    //Update Allocated Stock
    this._service.updateAllocatedStock(this.model).subscribe(
      (response) => {},
      (error) => {
        this.toastr.error("Server Error");
      }
    );

    const info = { message: "Order Approve Failed", type: "error" };
    this.updateStatus(info);
  }

  //Click Events End

  //Open Modal for create a purchase

  onRequestForPurchase() {
    this.modalRef.hide();
    this.modalReff = this.modalService.show(PurchaseCreateComponent, {
      initialState: {
        title: this.title,
        data: this.model,
      },
      class: "modal-lg",
      ignoreBackdropClick: true,
    });
  }

  updateStatus(info: any) {
    this._service
      .updateStatus(this.model.Id, this.model)
      .subscribe((response) => {
        if (response) {
          this.modalRef.hide();
          this._service.refreshList("Update");

          if (info.type == "success") {
            this.toastr.success(info.message);
          } else {
            this.toastr.error(info.message);
          }

          this._service.refreshList("Refresh");
        }
      });
  }

  downloadFile(fileName) {
    window.open(AppSettings.ROOT_IMAGE_PATH + fileName, "_blank");
  }

  checkSubcityStock(){
    this._service.checkSubcityStock(this.model).subscribe((response) => {
      if (response != null) {

        this.model.Status = 8;
        this.model.SubcityId = response;
      } else {
        this.model.Status = 4;
      }

      this.model.BookstoreEmployeeStatus = 5;

      const info = {
        message: "Order Approved Successfull",
        type: "success",
      };
      this.updateStatus(info);
    });
  }
}