import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { BsModalRef } from "ngx-bootstrap/modal";
import { NgxSpinnerService } from "ngx-spinner";
import { ToastrService } from "ngx-toastr";
import { EditComponent } from "src/app/generics/components/edit/edit.component";
import { GenericService } from "src/app/generics/services/generic.service";
import { Region } from "src/app/models/basic/region";
import { CheckNameService } from "src/app/regulars/services/check-name.service";
import { RegionService } from 'src/app/regulars/services/region.service';

@Component({
  selector: "app-region-edit-modal",
  templateUrl: "./region-edit-modal.component.html",
  styleUrls: ["./region-edit-modal.component.css"],
})
export class RegionEditModalComponent extends EditComponent implements OnInit {
  editMode: boolean = true;
  existingName: string = null;

  constructor(
    public modalRef: BsModalRef,
    public service: GenericService,
    public checkNameService: CheckNameService,
    public _service: RegionService,
    public router: Router,
    public toastr: ToastrService,
    public spinner: NgxSpinnerService
  ) {
    super(modalRef, service, router, toastr, spinner);
    this.model = new Region();
    this.apiUrl = "Regions";
  }

  ngOnInit() {
    this.setDataByParams();

    setTimeout(() => {
      this.existingName = this.model.Name;
    }, 1000);
  }

  onUpdate() {
    this.checkNameService
      .checkName("Regions", this.model)
      .subscribe((response) => {
        if (response != null) {
          if (response["Name"] != this.existingName) {
            this.toastr.error("This Region name is Already Exist");
          } else {
            this.updateRegion();
          }
        }

        if (response == null) {
          this.updateRegion();
        }
      });
  }

  updateRegion() {
    this.service.update(this.model.Id, this.model, "Regions").subscribe(
      (response) => {
        if (response) {
          this.modalRef.hide();
          this.toastr.success("Region Updated Successfully");
          this._service.refreshList("refresh");
        }
      },
      (error) => {
        this.toastr.error("Internal Server Error");
      }
    );
  }
}

