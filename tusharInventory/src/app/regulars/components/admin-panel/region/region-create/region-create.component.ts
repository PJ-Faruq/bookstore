import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { CreateComponent } from 'src/app/generics/components/create/create.component';
import { GenericService } from 'src/app/generics/services/generic.service';
import { Region } from 'src/app/models/basic/region';
import { CheckNameService } from 'src/app/regulars/services/check-name.service';

@Component({
  selector: "app-region-create",
  templateUrl: "./region-create.component.html",
  styleUrls: ["./region-create.component.css"],
})
export class RegionCreateComponent extends CreateComponent implements OnInit {
  title = "Region";

  constructor(
    public service: GenericService,
    public checkNameService: CheckNameService,

    public router: Router,
    public activatedRoute: ActivatedRoute,
    public spinner: NgxSpinnerService,
    public toastr: ToastrService
  ) {
    super(service, router, activatedRoute, spinner, toastr);
    this.model = new Region();
    this.apiUrl = "Regions";
  }

  ngOnInit() {
    this.setDataByParams();
  }

  reset() {
    this.editMode = false;
    this.model = new Region();
  }

  onSubmit() {
    this.checkNameService.checkName("Regions", this.model).subscribe(
      (response) => {
        if (response == null) {
          this.AddItem();
        } else {
          this.toastr.error("This Name is Already Exist !");
        }
      },
      (error) => {
        this.toastr.error("Internal Server Error");
      }
    );
  }

  AddItem() {
    this.service.add(this.model, "Regions").subscribe(
      (resp) => {
        if (resp) {
          this.toastr.success("Region Successfully Added");
          this.router.navigate(["/region/list"]);
        } else {
          this.toastr.error("Region Added Failed !");
        }
      },
      (err) => {
        this.toastr.error("Internal Server Error");
      }
    );
  }
}
