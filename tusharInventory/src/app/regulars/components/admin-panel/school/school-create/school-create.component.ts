import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { CreateComponent } from 'src/app/generics/components/create/create.component';
import { GenericService } from 'src/app/generics/services/generic.service';
import { Ownership } from 'src/app/models/basic/ownership';
import { School } from 'src/app/models/basic/school';
import { SchoolLavel } from 'src/app/models/basic/school-lavel';
import { Subcity } from 'src/app/models/basic/subcity';
import { Woreda } from 'src/app/models/basic/woreda';
import { SchoolService } from 'src/app/regulars/services/school.service';

@Component({
  selector: "app-school-create",
  templateUrl: "./school-create.component.html",
  styleUrls: ["./school-create.component.css"],
})
export class SchoolCreateComponent  implements OnInit {
  title = "School";
  model:School;
  editMode:boolean=false;

  schoolSubcityList: Subcity[] = [];
  schoolWoredaList: Woreda[] = [];
  schoolWoreda1List: Woreda[] = [];
  schoolLavelList: SchoolLavel[] = [];
  schoolOwnershipList: Ownership[] = [];

  constructor(
    public service: GenericService,
    public _service:SchoolService,
    public router: Router,
    public activatedRoute: ActivatedRoute,
    public spinner: NgxSpinnerService,
    public toastr: ToastrService
  ) {
    this.model=new School();
  }

  ngOnInit() {
    this.service.getAll("Subcities").subscribe(
      (data) => {
        this.schoolSubcityList = data;
      },
      (err) => {
        this.schoolSubcityList = [];
      }
    );

    this.service.getAll("Woredas").subscribe(
      (data) => {
        this.schoolWoredaList = data;
      },
      (err) => {
        this.schoolWoredaList = [];
      }
    );

    this.service.getAll("Woreda1s").subscribe(
          (data) => {
            this.schoolWoreda1List = data;
          },
          (err) => {
            this.schoolWoreda1List = [];
          }
        );

    this.service.getAll("SchoolLavels").subscribe(
      (data) => {
        this.schoolLavelList = data;
      },
      (err) => {
        this.schoolLavelList = [];
      }
    );

    this.service.getAll("Ownerships").subscribe(
      (data) => {
        this.schoolOwnershipList = data;
      },
      (err) => {
        this.schoolOwnershipList = [];
      }
    );

  }

  onSubmit(){
    this._service.checkSchoolName(this.model).subscribe(
      response=>{
        if(response!=null){
          if(response['Name']==this.model.Name){
            this.toastr.error("This School name is Already Exist");
          }

          else{
            this.addSchool();
          }
          
        }

        if (response == null) {
          this.addSchool();
        }
      }
    )
    
  }

  addSchool(){
    this.service.add(this.model,"Schools").subscribe(
      response=>{
        if(response){
          this.toastr.success("School Added Successfully");
          this.router.navigate(['/school/list']);
        }
      },
      error=>{
        this.toastr.error("Internal Server Error");
      }
    )
  }

  reset() {
    this.editMode = false;
    this.model = new School();
  }
}
