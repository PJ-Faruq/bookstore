import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { EditComponent } from 'src/app/generics/components/edit/edit.component';
import { GenericService } from 'src/app/generics/services/generic.service';
import { Ownership } from 'src/app/models/basic/ownership';
import { School } from 'src/app/models/basic/school';
import { SchoolLavel } from 'src/app/models/basic/school-lavel';
import { Subcity } from 'src/app/models/basic/subcity';
import { Woreda } from 'src/app/models/basic/woreda';
import { SchoolService } from 'src/app/regulars/services/school.service';

@Component({
  selector: "app-school-edit-modal",
  templateUrl: "./school-edit-modal.component.html",
  styleUrls: ["./school-edit-modal.component.css"],
})
export class SchoolEditModalComponent extends EditComponent implements OnInit {
  editMode: boolean = true;

  schoolSubcityList: Subcity[] = [];
  schoolWoredaList: Woreda[] = [];
  schoolWoreda1List: Woreda[] = [];
  schoolLavelList: SchoolLavel[] = [];
  schoolOwnershipList: Ownership[] = [];

  constructor(
    public modalRef: BsModalRef,
    public service: GenericService,
    public _service: SchoolService,
    public router: Router,
    public toastr: ToastrService,
    public spinner: NgxSpinnerService
  ) {
    super(modalRef, service, router, toastr, spinner);
    this.model = new School();
    this.apiUrl = "Schools";
  }

  ngOnInit() {
    this.service.getAll("Subcities").subscribe(
      (data) => {
        this.schoolSubcityList = data;
      },
      (err) => {
        this.schoolSubcityList = [];
      }
    );

    this.service.getAll("Woredas").subscribe(
      (data) => {
        this.schoolWoredaList = data;
      },
      (err) => {
        this.schoolWoredaList = [];
      }
    );

    this.service.getAll("Woreda1s").subscribe(
      (data) => {
        this.schoolWoreda1List = data;
      },
      (err) => {
        this.schoolWoreda1List = [];
      }
    );

    this.service.getAll("SchoolLavels").subscribe(
      (data) => {
        this.schoolLavelList = data;
      },
      (err) => {
        this.schoolLavelList = [];
      }
    );

    this.service.getAll("Ownerships").subscribe(
      (data) => {
        this.schoolOwnershipList = data;
      },
      (err) => {
        this.schoolOwnershipList = [];
      }
    );

    this.setDataByParams();
  }

  onUpdate() {
    this._service.checkSchoolName(this.model).subscribe((response) => {
      if (response != null) {
        if (response["Name"] != this.model.Name) {
          this.toastr.error("This School name is Already Exist");
        } else {
          this.updateSchool();
        }
      }

      if (response == null) {
        this.updateSchool();
      }
    });
  }

  updateSchool() {
    this.service.update(this.model.Id,this.model, "Schools").subscribe(
      (response) => {
        if (response) {
          this.modalRef.hide();
          this.toastr.success("School Updated Successfully");
          this._service.refreshList("refresh");  
        }
      },
      (error) => {
        this.toastr.error("Internal Server Error");
      }
    );
  }
}