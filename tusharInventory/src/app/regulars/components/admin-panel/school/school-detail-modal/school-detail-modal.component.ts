import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { NgxSpinnerService } from 'ngx-spinner';
import { DetailComponent } from 'src/app/generics/components/detail/detail.component';
import { GenericService } from 'src/app/generics/services/generic.service';
import { School } from 'src/app/models/basic/school';

@Component({
  selector: 'app-school-detail-modal',
  templateUrl: './school-detail-modal.component.html',
  styleUrls: ['./school-detail-modal.component.css']
})
export class SchoolDetailModalComponent extends DetailComponent implements OnInit {

  constructor(public modalRef: BsModalRef,
    public service: GenericService,
    public spinner: NgxSpinnerService) {

    super(modalRef, service, spinner);
    this.model = new School();
    this.apiUrl = "Schools"

  }
}
