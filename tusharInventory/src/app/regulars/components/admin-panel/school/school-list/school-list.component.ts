import { Component, OnInit } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { ListComponent } from 'src/app/generics/components/list/list.component';
import { GenericService } from 'src/app/generics/services/generic.service';
import { Ownership } from 'src/app/models/basic/ownership';
import { School } from 'src/app/models/basic/school';
import { SchoolLavel } from 'src/app/models/basic/school-lavel';
import { Subcity } from 'src/app/models/basic/subcity';
import { Woreda } from 'src/app/models/basic/woreda';
import { UserVm } from 'src/app/models/view-models/userVm.model';
import { AutehnticationService } from 'src/app/regulars/services/autehntication.service';
import { ConfirmService } from 'src/app/regulars/services/confirm.service';
import { SchoolService } from 'src/app/regulars/services/school.service';
import { SchoolDetailModalComponent } from '../school-detail-modal/school-detail-modal.component';
import { SchoolEditModalComponent } from '../school-edit-modal/school-edit-modal.component';

@Component({
  selector: "app-school-list",
  templateUrl: "./school-list.component.html",
  styleUrls: ["./school-list.component.css"],
})
export class SchoolListComponent extends ListComponent implements OnInit {
  currentUser: UserVm;
  title = "School";
  apiUrl = "Schools";
  createLink = "/" + this.title.toLowerCase() + "/create";

  p: number = 1;
  model: School;
  schoolList: any = [];
  schoolSubcityList: Subcity[] = [];
  schoolWoredaList: Woreda[] = [];
  schoolWoreda1List: Woreda[] = [];
  schoolLavelList: SchoolLavel[] = [];
  schoolOwnershipList: Ownership[] = [];

  constructor(
    public service: GenericService,
    public _service: SchoolService,
    public modalService: BsModalService,
    public confirmService: ConfirmService,
    public toastr: ToastrService,
    public spinner: NgxSpinnerService,
    private authenticationService: AutehnticationService
  ) {
    super(modalService, confirmService, toastr, spinner);
    this.model = new School();

    // For Refreshing List from outside
    this._service.listen().subscribe((m) => {
      this.service.setAll(this.apiUrl);
    });
    this.confirmService.listen().subscribe((response) => {
      this.service.setAll(this.apiUrl);
    });
  }

  ngOnInit(): void {
    this.currentUser = this.authenticationService.currentUserValue;

    this.service.getAll("Subcities").subscribe(
      (data) => {
        this.schoolSubcityList = data;
      },
      (err) => {
        this.schoolSubcityList = [];
      }
    );

    this.service.getAll("Woredas").subscribe(
      (data) => {
        this.schoolWoredaList = data;
      },
      (err) => {
        this.schoolWoredaList = [];
      }
    );

    this.service.getAll("Woreda1s").subscribe(
      (data) => {
        this.schoolWoreda1List = data;
      },
      (err) => {
        this.schoolWoreda1List = [];
      }
    );

    this.service.getAll("SchoolLavels").subscribe(
      (data) => {
        this.schoolLavelList = data;
      },
      (err) => {
        this.schoolLavelList = [];
      }
    );

    this.service.getAll("Ownerships").subscribe(
      (data) => {
        this.schoolOwnershipList = data;
      },
      (err) => {
        this.schoolOwnershipList = [];
      }
    );

    this.spinner.show();
    this.listChangedSubscribe = this.service.listChanged.subscribe(
      (data) => {
        this.modelList = data;
        this.modelList=this.modelList.reverse();
        this.schoolList =this.modelList.map(function(a) {return {"Name":a.Name,Id:a.Id}});
        
        this.spinner.hide();
      },
      (error) => {
        this.modelList = [];
        this.spinner.hide();
      }
    );
    this.confirmSubscription = this.confirmService.deleted.subscribe((data) => {
      if (data) {
        this.service.setAll(this.apiUrl);
        this.toastr.warning("Deleted Successfully", "Success");
      } else {
        this.toastr.warning("Can't Delete this item", "Warning");
      }
    });
    this.service.setAll(this.apiUrl);
  }

  openDetailModal(id: number) {
    this.modalRef = this.modalService.show(SchoolDetailModalComponent, {
      initialState: {
        title: this.title,
        data: {
          id: id,
        },
      },
      class: "modal-lg",
    });
  }

  openEditModal(id: number) {
    this.modalRef = this.modalService.show(SchoolEditModalComponent, {
      initialState: {
        title: this.title,
        data: {
          id: id,
        },
      },
      class: "modal-lg",
    });
  }

  onSearch() {
    this._service.search(this.model).subscribe((response) => {
      this.p=1;
      this.modelList = response;

    });
  }
}
