import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { SchoolBulkVm } from 'src/app/models/view-models/schoolBulkVm';
import { SchoolService } from 'src/app/regulars/services/school.service';
import * as XLSX from 'xlsx';

@Component({
  selector: "app-school-bulk-import",
  templateUrl: "./school-bulk-import.component.html",
  styleUrls: ["./school-bulk-import.component.css"],
})
export class SchoolBulkImportComponent implements OnInit {
  data: [][];
  schoolList: SchoolBulkVm[] = [];
  status:string="";
  isSubmittalbe:boolean=false;

  constructor(
    public service: SchoolService,
    public toastr: ToastrService,
    private spinner: NgxSpinnerService
  ) {}

  ngOnInit(): void {
    this.spinner.show("Loading");
  }

  onChangeFile(file: FileList) {
    //this.model.PhotographFile = file.item(0);
    var fileReader = new FileReader();
    fileReader.readAsBinaryString(file.item(0));
    fileReader.onload = (event: any) => {
      const bstr: string = event.target.result;
      const workBook: XLSX.WorkBook = XLSX.read(bstr, { type: "binary" });
      const workSheetName: string = workBook.SheetNames[0];
      const workSheet: XLSX.WorkSheet = workBook.Sheets[workSheetName];
      // console.log(workSheet);
      //convert sheet data to json object
      this.data = XLSX.utils.sheet_to_json(workSheet, { header: 1 });

      //Data Bind to the Model
      //let schoolList: SchoolBulkVm[] = [];

      for (let i = 0; i < this.data.length; i++) {
        let school: SchoolBulkVm;
        for (let j = 0; j < 1; j++) {
          school = new SchoolBulkVm();
          school.Subcity = this.data[i][j];
          school.Woreda1 = this.data[i][j + 1];
          school.Woreda = this.data[i][j + 2];
          school.SchoolName = this.data[i][j + 3];
          school.SchoolLevel = this.data[i][j + 4];
          school.SchoolOwnership = this.data[i][j + 5];
        }
        this.schoolList.push(school);
      }
    };
  }

  onSubmit() {
    this.isSubmittalbe=true;
    this.status="Importing......"
    this.service.bulkImport(this.schoolList).subscribe(
      (response) => {
        if (response) {
          this.toastr.success("Import Bulk Data Successfull");
          this.status="Successfully Imported";
          this.isSubmittalbe=false;
        }
      },
      (error) => {
        this.toastr.error("Internal Server Error");
      }
    );
  }
}
