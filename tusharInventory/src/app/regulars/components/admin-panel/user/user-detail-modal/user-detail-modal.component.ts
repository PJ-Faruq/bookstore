import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { DetailComponent } from 'src/app/generics/components/detail/detail.component';
import { GenericService } from 'src/app/generics/services/generic.service';
import { User } from 'src/app/models/user';
import { UserService } from 'src/app/regulars/services/user.service';
import { AppSettings } from 'src/app/shared/app-settings';


@Component({
  selector: "app-user-detail-modal",
  templateUrl: "./user-detail-modal.component.html",
  styleUrls: ["./user-detail-modal.component.css"],
})
export class UserDetailModalComponent implements OnInit {
  data: any;
  imgSrc: string;
  model: User;
  constructor(
    public modalRef: BsModalRef,
    public service: GenericService,
    public _service: UserService,
    public spinner: NgxSpinnerService,
    public toastr: ToastrService
  ) {
    this.model = new User();
  }

  ngOnInit() {
    this._service.getUserForAdmin(this.data.id).subscribe((response) => {
      this.model = response;
      this.imgSrc = AppSettings.ROOT_IMAGE_PATH + this.model.Photograph;
    });
  }

  onApproved() {
    const data = new FormData();
    data.append("Id", this.model.Id.toString());
    data.append("FirstName", this.model.FirstName);
    data.append("Photograph", this.model.Photograph);
    data.append("EmployeeId", this.model.EmployeeId);
    data.append("LastName", this.model.LastName);
    data.append("SecondName", this.model.SecondName);
    data.append("Address", this.model.Address);
    data.append("IsActive", JSON.stringify(this.model.IsActive));
    data.append("IsApproved", JSON.stringify(true));

    data.append("Email", this.model.Email);
    data.append("Password", this.model.Password);
    data.append("RoleId", this.model.RoleId.toString());
    data.append("PhotographFile", this.model.PhotographFile);
    // data.append("EmployeeId", this.model.); //Bind in the backend

    if (this.model.SchoolId != null) {
      data.append("SchoolId", this.model.SchoolId.toString());
      data.append("SchoolLavelId", this.model.SchoolLavelId.toString());
      data.append("OwnershipId", this.model.OwnershipId.toString());
    } else {
      data.append("SchoolId", null);
    }

    data.append("PhoneNumber", this.model.PhoneNumber);
    data.append("WoredaId", this.model.WoredaId.toString());
    data.append("RegionId", this.model.RegionId.toString());
    data.append("SubcityId", this.model.SubcityId.toString());

    this._service.update(data).subscribe(
      (response) => {
        if (response) {
          this.modalRef.hide();
          this.toastr.success("User Request Approved");
          this._service.refreshList("Update");
        }
      },

      (error) => {
        this.toastr.error("Internal Server Error");
      }
    );
  }
}
