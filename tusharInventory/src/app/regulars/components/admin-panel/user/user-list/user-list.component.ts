import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DataTableDirective } from 'angular-datatables';
import { BsModalService } from 'ngx-bootstrap/modal';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { Subject } from 'rxjs';
import { ListComponent } from 'src/app/generics/components/list/list.component';
import { GenericService } from 'src/app/generics/services/generic.service';
import { ConfirmService } from 'src/app/regulars/services/confirm.service';
import { UserService } from 'src/app/regulars/services/user.service';
import { UserDetailModalComponent } from '../user-detail-modal/user-detail-modal.component';
import { UserEditModalComponent } from '../user-edit-modal/user-edit-modal.component';

@Component({
  selector: "app-user-list",
  templateUrl: "./user-list.component.html",
  styleUrls: ["./user-list.component.css"],
})
export class UserListComponent extends ListComponent implements OnInit {
  title = "User";
  apiUrl = "Users";
  createLink = "/" + this.title.toLowerCase() + "/create";
  userStatus: string = null;

  //Datatable Field
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject<any>();
  dtElement: DataTableDirective;

  constructor(
    public service: GenericService,
    public _service: UserService,
    public modalService: BsModalService,
    public confirmService: ConfirmService,
    public toastr: ToastrService,
    public spinner: NgxSpinnerService,
    private route: ActivatedRoute
  ) {
    super(modalService, confirmService, toastr, spinner);
    this.userStatus = this.route.snapshot.paramMap.get("status");
    this._service.listen().subscribe((response) => {
      this.service.setAll(this.apiUrl);
    });
        this.confirmService.listen().subscribe((response) => {
          this.service.setAll(this.apiUrl);
        });
  }

  ngOnInit(): void {
    this.spinner.show();
    this.listChangedSubscribe = this.service.listChanged.subscribe(
      (data) => {
        this.modelList = data;

        if (this.userStatus == "Active") {
          this.modelList = this.modelList.filter((m) => m.IsActive == true);
        }

        if (this.userStatus == "pending") {
          this.modelList = this.modelList.filter((m) => m.IsApproved == false);
        }

        this.spinner.hide();
      },
      (error) => {
        this.modelList = [];
        this.spinner.hide();
      }
    );
    this.confirmSubscription = this.confirmService.deleted.subscribe((data) => {
      if (data) {
        this.service.setAll(this.apiUrl);
        this.toastr.error("Deleted Successfully", "Success");
      } else {
        this.toastr.warning("Can't Delete this item", "Warning");
      }
    });
    this.service.setAll(this.apiUrl);
  }

  openDetailModal(id: number) {
    this.modalRef = this.modalService.show(UserDetailModalComponent, {
      initialState: {
        title: this.title,
        data: {
          id: id,
        },
      },
      class: "modal-lg",
    });
  }

  openEditModal(id: number) {
    this.modalRef = this.modalService.show(UserEditModalComponent, {
      initialState: {
        title: this.title,
        data: {
          id: id,
        },
      },
      class: "modal-lg",
    });
  }

  //#region Datatable Settings
  ngAfterViewInit(): void {
    setTimeout(() => {
      this.dtTrigger.next();
    }, 300);
  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }

  rerender(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      // Destroy the table first
      dtInstance.destroy();
      // Call the dtTrigger to rerender again
      this.dtTrigger.next();
    });
  }

  //#endregion
}
