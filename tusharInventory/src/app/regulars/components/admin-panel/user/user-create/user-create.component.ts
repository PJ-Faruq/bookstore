import {
  Component,
  OnInit
}

from '@angular/core';

import {Router,ActivatedRoute} from '@angular/router';

import {
  NgxSpinnerService
}

from 'ngx-spinner';

import {
  ToastrService
}

from 'ngx-toastr';

import {
  CreateComponent
}

from 'src/app/generics/components/create/create.component';

import {
  GenericService
}

from 'src/app/generics/services/generic.service';

import {
  School
}

from 'src/app/models/basic/school';

import {
  FormBuilder,
  FormGroup,
  NgForm,
  Validators
}

from "@angular/forms";

import {
  Role
}

from 'src/app/models/role';

import {
  User
}

from 'src/app/models/user';

import {
  Woreda
}

from 'src/app/models/basic/woreda';

import {
  Region
}

from 'src/app/models/basic/Region';

import {
  Subcity
}

from 'src/app/models/basic/subcity';

import {
  UserService
}

from 'src/app/regulars/services/user.service';
import { ThrowStmt } from '@angular/compiler';

@Component({
    selector: "app-user-create",
    templateUrl: "./user-create.component.html",
    styleUrls: ["./user-create.component.css"],
  }

) export class UserCreateComponent implements OnInit {

  userTypeList=[{Id:1,Name:"Bookstore User"},{Id:2,Name:"School User"}];
  IsActive:boolean=true;
  title = "User";
  isSchool: boolean = false;
  model: User;
  editMode: boolean = false;
  isSubmittable: boolean = false;
  UserTypeId: number ;
  signUpType:string;

  roleList: Role[] = [];
  schoolList: School[] = [];
  woredaList: Woreda[] = [];
  regionList: Region[] = [];
  subcityList: Subcity[] = [];
  imageUrl: string = "";
  schoolLavelList: any[];
  ownershipList: any[];

  constructor(public service: GenericService,
    public _service: UserService,
    public router: Router,
    public activatedRoute: ActivatedRoute,
    public spinner: NgxSpinnerService,
    public toastr: ToastrService) {
    this.model = new User();
    this.signUpType = this.activatedRoute.snapshot.paramMap.get("signUpType");
  }

  ngOnInit() {

    this.service.getAll("Roles").subscribe((data) => {
        this.roleList = data;
      }

      ,
      (err) => {
        this.roleList = [];
      }

    );

    this.service.getAll("Schools/GetSchoolName").subscribe((response) => {
        this.schoolList = response;
        console.log(this.schoolList);
        
      }

    );

    this.service.getAll("Subcities").subscribe((response) => {
        this.subcityList = response;
      }

    );

    this.service.getAll("Woredas").subscribe((response) => {
        this.woredaList = response;
      }

    );

    this.service.getAll("SchoolLavels").subscribe((response) => {
      this.schoolLavelList = response;
    });
    this.service.getAll("Ownerships").subscribe((response) => {
      this.ownershipList = response;
    });

    this.service.getAll("Regions").subscribe((response) => {
        this.regionList = response;
        console.log(this.regionList);
      }

    );
  }

  onUserTypeChange(value:any) {
    this.UserTypeId=value;
    if (this.UserTypeId == 2) {
      this.isSchool = true;
      this.model.RoleId=2;
    }

    if (this.UserTypeId == 1) {
      this.isSchool = false;
      this.model.RoleId=null;
    }
  }

  onChangeFile(file: FileList) {
    this.model.PhotographFile = file.item(0);
    var fileReader = new FileReader();
    fileReader.readAsDataURL(file.item(0));

    fileReader.onload = (event: any) => {
      this.imageUrl = event.target.result;
    }

    ;
  }

  onSchoolChange(id: number) {

    this.service.getById(id, "Schools").subscribe((response) => {
        this.model.SubcityId = response.SchoolSubcityId;
        this.model.WoredaId = response.SchoolWoredaId;
        this.model.SchoolLavelId = response.SchoolLavelId;
        this.model.OwnershipId = response.SchoolOwnershipId;

        
      }
    );
  }


  onRadioBtnChange(status:string){
    if(status=="Active"){
      this.IsActive=true;
    }
    else{
      this.IsActive=false;
    }
  }

  onSubmit() {


    

    const data = new FormData();
    data.append("FirstName",this.model.FirstName);
    data.append("LastName",this.model.LastName);
    data.append("SecondName",this.model.SecondName);
    data.append("Address",this.model.Address);
    data.append("IsActive",JSON.stringify(this.IsActive));
    data.append("IsApproved", JSON.stringify(true));



    data.append("Email", this.model.Email);
    data.append("Password", this.model.Password);
    data.append("RoleId", this.model.RoleId.toString());
    data.append("PhotographFile", this.model.PhotographFile);
    // data.append("EmployeeId", this.model.); //Bind in the backend

    if (this.model.SchoolId != null) {
      data.append("SchoolId", this.model.SchoolId.toString());
      data.append("SchoolLavelId", this.model.SchoolLavelId.toString());
      data.append("OwnershipId", this.model.OwnershipId.toString());

    } else {
      data.append("SchoolId", null);
    }

    data.append("PhoneNumber", this.model.PhoneNumber);
    data.append("WoredaId", this.model.WoredaId.toString());
    data.append("RegionId", this.model.RegionId.toString());
    data.append("SubcityId", this.model.SubcityId.toString());

    this.service.add(data, "Users").subscribe((response) => {
        if (response) {
          this.router.navigate(["/user/list"]);
        }
      }

      ,
      (error) => {
        this.toastr.error("Internal Server Error");
      }

    );
  }

  checkEmail(f: any) {
    const email = f.value;

    this._service.checkEmail(email).subscribe((response) => {
        if (response == null) {
          this.isSubmittable = true;
        } else {
          if (response["Email"] == email) {
            this.isSubmittable = false;
            this.toastr.error("The Email is Already Exist");
          } else {
            this.isSubmittable = true;
          }
        }
      }

    );
  }

  reset() {
    this.model = new User();
  }
}
