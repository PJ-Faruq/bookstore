import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { EditComponent } from 'src/app/generics/components/edit/edit.component';
import { GenericService } from 'src/app/generics/services/generic.service';
import { Region } from 'src/app/models/basic/Region';
import { School } from 'src/app/models/basic/school';
import { Subcity } from 'src/app/models/basic/subcity';
import { Woreda } from 'src/app/models/basic/woreda';
import { Role } from 'src/app/models/role';
import { User } from 'src/app/models/user';
import { UserService } from 'src/app/regulars/services/user.service';
import { AppSettings } from 'src/app/shared/app-settings';

@Component({
  selector: "app-user-edit-modal",
  templateUrl: "./user-edit-modal.component.html",
  styleUrls: ["./user-edit-modal.component.css"],
})
export class UserEditModalComponent extends EditComponent implements OnInit {
  userTypeList = [
    { Id: 1, Name: "Bookstore User" },
    { Id: 2, Name: "School User" },
  ];
  IsActive: boolean = true;
  title = "User";
  isSchool: boolean = false;
  model: User;
  editMode: boolean = false;
  isSubmittable: boolean = false;
  UserTypeId: number;
  existingEmail:string;

  roleList: Role[] = [];
  schoolList: School[] = [];
  woredaList: Woreda[] = [];
  regionList: Region[] = [];
  subcityList: Subcity[] = [];
  imageUrl: string = "";
  imgSrc: string;
  schoolLavelList: any[];
  ownershipList: any[];

  constructor(
    public service: GenericService,
    public modalRef: BsModalRef,
    public _service: UserService,
    public router: Router,
    public activatedRoute: ActivatedRoute,
    public spinner: NgxSpinnerService,
    public toastr: ToastrService
  ) {
    super();
    this.model = new User();
  }

  ngOnInit() {
    this.service.getAll("Roles").subscribe(
      (data) => {
        this.roleList = data;
      },

      (err) => {
        this.roleList = [];
      }
    );

    this.service.getAll("Schools/GetSchoolName").subscribe((response) => {
      this.schoolList = response;
    });

    this.service.getAll("Subcities").subscribe((response) => {
      this.subcityList = response;
    });

    this.service.getAll("Woredas").subscribe((response) => {
      this.woredaList = response;
    });

        this.service.getAll("SchoolLavels").subscribe((response) => {
          this.schoolLavelList = response;
        });
        this.service.getAll("Ownerships").subscribe((response) => {
          this.ownershipList = response;
        });

    this.service.getAll("Regions").subscribe((response) => {
      this.regionList = response;
    });

    this._service.getUserForAdmin(this.data.id).subscribe((response) => {
      this.model = response;
      this.imgSrc = AppSettings.ROOT_IMAGE_PATH + this.model.Photograph;
      if(this.model.SchoolId){
        this.isSchool=true;
      }
    });

    setTimeout(() => {
      this.checkMail(this.model.Email);
      this.existingEmail = this.model.Email;
      this.IsActive=this.model.IsActive;
    }, 1000);

    
  }

  onUserTypeChange(value: any) {
    this.UserTypeId = value;
    if (this.UserTypeId == 2) {
      this.isSchool = true;
      this.model.RoleId = 2;
    }

    if (this.UserTypeId == 1) {
      this.isSchool = false;
      this.model.RoleId = null;
    }
  }

  onChangeFile(file: FileList) {
    this.model.PhotographFile = file.item(0);
    var fileReader = new FileReader();
    fileReader.readAsDataURL(file.item(0));

    fileReader.onload = (event: any) => {
      this.imageUrl = event.target.result;
    };
  }

  onSchoolChange(id: number) {
    this.service.getById(id, "Schools").subscribe((response) => {
      this.model.SubcityId = response.SchoolSubcityId;
      this.model.WoredaId = response.SchoolWoredaId;
    });
  }

  onRadioBtnChange(status: string) {
    if (status == "Active") {
      this.IsActive = true;
    } else {
      this.IsActive = false;
    }
  }

  onSubmit() {
    const data = new FormData();
    data.append("Id", this.model.Id.toString());
    data.append("FirstName", this.model.FirstName);
    data.append("Photograph", this.model.Photograph);
    data.append("EmployeeId", this.model.EmployeeId);
    data.append("LastName", this.model.LastName);
    data.append("SecondName", this.model.SecondName);
    data.append("Address", this.model.Address);
    data.append("IsActive", JSON.stringify(this.IsActive));
    data.append("IsApproved", JSON.stringify(this.model.IsApproved));

    data.append("Email", this.model.Email);
    data.append("Password", this.model.Password);
    data.append("RoleId", this.model.RoleId.toString());
    data.append("PhotographFile", this.model.PhotographFile);
    // data.append("EmployeeId", this.model.); //Bind in the backend

    if (this.model.SchoolId != null) {
      data.append("SchoolId", this.model.SchoolId.toString());
      data.append("SchoolLavelId", this.model.SchoolLavelId.toString());
      data.append("OwnershipId", this.model.OwnershipId.toString());
    } else {
      data.append("SchoolId", null);
    }

    data.append("PhoneNumber", this.model.PhoneNumber);
    data.append("WoredaId", this.model.WoredaId.toString());
    data.append("RegionId", this.model.RegionId.toString());
    data.append("SubcityId", this.model.SubcityId.toString());

    this._service.update(data).subscribe(
      (response) => {
        if (response) {
          this.modalRef.hide();
          this.toastr.success("Updated Successfully");
          this._service.refreshList("Update");
          this.router.navigate(["/user/list"]);
        }
      },

      (error) => {
        this.toastr.error("Internal Server Error");
      }
    );
  }

  onCheckEmail(f: any) {
    
    const email = f.value;
    this.checkMail(email);
  }

  checkMail(email:any){
        this._service.checkEmail(email).subscribe((response) => {
          if (response == null) {
            this.isSubmittable = true;
          } else {
            if (response["Email"] != this.existingEmail) {
              this.isSubmittable = false;
              this.toastr.error("The Email is Already Exist");
            } else {
              this.isSubmittable = true;
            }
          }
        });
  }

  reset() {
    this.modalRef.hide();
  }
}
