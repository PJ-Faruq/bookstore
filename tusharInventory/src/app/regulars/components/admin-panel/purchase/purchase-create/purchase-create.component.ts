import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { ToastrService } from 'ngx-toastr';
import { GenericService } from 'src/app/generics/services/generic.service';
import { Order } from 'src/app/models/operation/order';
import { Purchase } from 'src/app/models/operation/purchase';
import { UserVm } from 'src/app/models/view-models/userVm.model';
import { AutehnticationService } from 'src/app/regulars/services/autehntication.service';
import { OrderService } from 'src/app/regulars/services/order.service';
import { PurchaseService } from 'src/app/regulars/services/purchase.service';

@Component({
  selector: "app-purchase-create",
  templateUrl: "./purchase-create.component.html",
  styleUrls: ["./purchase-create.component.css"],
})
export class PurchaseCreateComponent implements OnInit {
  currentUser: UserVm;
  managerList: any;
  purchaseEmployeeList: any;
  data: any; //values come from modal
  model: Purchase;

  constructor(
    public modalReff: BsModalRef,
    public purchaseService: PurchaseService,
    public orderService: OrderService,
    private authenticationService: AutehnticationService,
    private service: GenericService,
    public toastr: ToastrService
  ) {
    this.model = new Purchase();
  }

  ngOnInit(): void {
    this.currentUser = this.authenticationService.currentUserValue;

    this.purchaseService.getManagerList().subscribe(
      response=>{
        this.managerList=response;

      }
    )

    this.purchaseService.getPurchaseEmployeeList().subscribe(
      response=>{
        this.purchaseEmployeeList=response;

      }
    )

    //Filet list for only those items which are out of stock
    this.model.PurchaseLines = this.data.OrderLines.filter(
      (m) => m.Quantity > m.AvailableQty
    );

    //For avoiding id conflict while inset in database
    for (let i = 0; i < this.model.PurchaseLines.length; i++) {
      // this.model.PurchaseLines[i].PurchaseQuantity=this.data.OrderLines[i].Quantity - this.data.OrderLines[i].AvailableQty;
      this.model.PurchaseLines[i].Id = 0;
    }
  }

  onSubmit(form: any) {
    this.model.BookstoreEmployeeId = this.currentUser.Id;
    this.model.Status = 1;

    for (let item of this.model.PurchaseLines) {
      item.BookGradeLavel = null;
      item.BookLanguage = null;
      item.BookSubject = null;
    }

    this.service.add(this.model, "Purchases").subscribe((response) => {
      if (response) {

        //Update IsRequestToPurchase Property in Order Table
        this.data.IsRequestedToPurchase = true;
        this.orderService.updateStatus(this.data.Id,this.data).subscribe(
          response=>{
            
          }
        );
        this.modalReff.hide();

        this.toastr.success(
          "Purchase Request Successfully sent to the Manager"
        );
      }
    });
  }
}
