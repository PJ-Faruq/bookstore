import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DataTableDirective } from 'angular-datatables';
import { BsModalService } from 'ngx-bootstrap/modal';
import { ToastrService } from 'ngx-toastr';
import { Subject } from 'rxjs';
import { GenericService } from 'src/app/generics/services/generic.service';
import { UserVm } from 'src/app/models/view-models/userVm.model';
import { AutehnticationService } from 'src/app/regulars/services/autehntication.service';
import { PurchaseService } from 'src/app/regulars/services/purchase.service';
import { PurchaseDetailComponent } from '../purchase-detail/purchase-detail.component';

@Component({
  selector: "app-purchase-list",
  templateUrl: "./purchase-list.component.html",
  styleUrls: ["./purchase-list.component.css"],
})
export class PurchaseListComponent implements OnInit {
  currentUser: UserVm;
  purchaseList: any;
  modalRef: any;
  managerStatus: string; //this comes from Manager dashboard for filtering
  purchaseStatus: string; //this comes from Manager dashboard for filtering

  //Datatable Field
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject<any>();
  dtElement: DataTableDirective;

  constructor(
    public purchaseService: PurchaseService,
    private authenticationService: AutehnticationService,
    private service: GenericService,
    public toastr: ToastrService,
    private route: ActivatedRoute,
    public modalService: BsModalService,
    private router: Router
  ) {
    this.managerStatus = this.route.snapshot.paramMap.get("managerStatus");
    this.purchaseStatus = this.route.snapshot.paramMap.get("purchaseStatus");

    // For Refreshing List from outside
    this.purchaseService.listen().subscribe((m) => {
      this.getPurchaseList();
    });
  }

  ngOnInit(): void {
    this.currentUser = this.authenticationService.currentUserValue;

    this.getPurchaseList();
  }

  getPurchaseList() {
    this.service.getAll("Purchases").subscribe((response) => {
      this.purchaseList = response.reverse();
      // if (this.currentUser.Role == "Purchase Department") {
      //   this.purchaseList = response.filter((m) => m.Status == 2);
      // } else {
      //   this.purchaseList = response;
      // }

      this.filterForManagerDashboard();
      this.filterForPurchaseDashboard();
    });
  }
  filterForPurchaseDashboard() {
    if (this.purchaseStatus == "pending") {
      this.purchaseList = this.purchaseList.filter((m) => m.Status == 2);
    }

    if (this.purchaseStatus == "completed") {
      this.purchaseList = this.purchaseList.filter((m) => m.Status == 3);
    }

    // if (this.purchaseStatus == "total") {
    //   this.purchaseList = this.purchaseList.filter((m) => m.Status == 4);
    // }
  }

  filterForManagerDashboard() {
    if (this.managerStatus == "pending") {
      this.purchaseList = this.purchaseList.filter((m) => m.Status == 1);
    }

    if (this.managerStatus == "approved") {
      this.purchaseList = this.purchaseList.filter(
        (m) => m.Status == 2 || m.Status == 3
      );
    }

    if (this.managerStatus == "rejected") {
      this.purchaseList = this.purchaseList.filter((m) => m.Status == 4);
    }

    if (this.managerStatus == "total") {
      this.purchaseList = this.purchaseList;
    }
  }

  openDetailModal(id: number) {
    this.modalRef = this.modalService.show(PurchaseDetailComponent, {
      initialState: {
        title: "Purchase Detail",
        data: {
          id: id,
        },
      },
      class: "modal-lg",
      ignoreBackdropClick: false,
    });
  }

  onClickReceipt(purchaseRequestId: number) {
    this.purchaseService
      .getPurchaseBookByPurchaseId(purchaseRequestId)
      .subscribe((response) => {
        if (response != null) {
          const url = this.router.createUrlTree([
            "/report/purchase-report",
            response,
          ]);
          let urll = url.toString();
          window.open(url.toString(), "_blank");
        }
      });
  }

  //#region Datatable Settings
  ngAfterViewInit(): void {
    setTimeout(() => {
      this.dtTrigger.next();
    }, 300);
  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }

  rerender(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      // Destroy the table first
      dtInstance.destroy();
      // Call the dtTrigger to rerender again
      this.dtTrigger.next();
    });
  }

  //#endregion
}
