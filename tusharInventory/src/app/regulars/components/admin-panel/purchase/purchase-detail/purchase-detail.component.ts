import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { DetailComponent } from 'src/app/generics/components/detail/detail.component';
import { GenericService } from 'src/app/generics/services/generic.service';
import { PurchaseBook } from 'src/app/models/operation/purchase-book';
import { PurchaseBookLine } from 'src/app/models/operation/purchase-book-line';
import { UserVm } from 'src/app/models/view-models/userVm.model';
import { AutehnticationService } from 'src/app/regulars/services/autehntication.service';
import { PurchaseService } from 'src/app/regulars/services/purchase.service';
import { PurchaseBookCreateModalComponent } from '../../purchase-book/purchase-book-create-modal/purchase-book-create-modal.component';
import { PurchaseBookCreateComponent } from '../../purchase-book/purchase-book-create/purchase-book-create.component';

@Component({
  selector: "app-purchase-detail",
  templateUrl: "./purchase-detail.component.html",
  styleUrls: ["./purchase-detail.component.css"],
})
export class PurchaseDetailComponent extends DetailComponent implements OnInit {
  data: any;
  model: any;
  currentUser: UserVm;

  constructor(
    public service: GenericService,
    public modalService: BsModalService,
    public modalRef: BsModalRef,
    public toastr: ToastrService,
    public spinner: NgxSpinnerService,
    private router: Router,
    public _service: PurchaseService,
    public authenticationService: AutehnticationService
  ) {
    super(modalRef, service, spinner);
  }

  ngOnInit(): void {
    this.currentUser = this.authenticationService.currentUserValue;

    this.service.getById(this.data.id, "Purchases").subscribe((response) => {
      this.model = response;
    });
  }

  onApproved() {
    this.model.Status = 2;
    this.model.BookstoreManagerId = this.currentUser.Id;
    this._service.changeStatus(this.model).subscribe((response) => {
      if (response) {
        this.modalRef.hide();
        this._service.refreshList("Update");
        this.toastr.success(
          "Purchase Request Approved Successfully",
          "Success"
        );
      } else {
        this.modalRef.hide();
        this.toastr.error("Purchase Request Approved Failed !", "Eroor");
      }
    });
  }

  onRejected() {
    this.model.Status = 4;
    this._service.changeStatus(this.model).subscribe((response) => {
      if (response) {
        this.modalRef.hide();
        this._service.refreshList("refresh");
        this.toastr.error("Purchase Request Rejected");
      } else {
        this.modalRef.hide();
        this.toastr.error("Purchase Request Rejected Failed !", "Eroor");
      }
    });
  }

  onPurchase() {
    let book = new PurchaseBook();

    book.Id = 0;
    book.PurchaseRequestId=this.model.Id;
    book.BookstoreEmployeeId = this.model.BookstoreEmployeeId;
    book.BookstoreManagerId = this.model.BookstoreManagerId;
    book.PurcahseDeptEmployeeId = this.model.PurcahseDeptEmployeeId;
    book.PurchaseDate = new Date();
    book.Amount = 0;

    for (let item of this.model.PurchaseLines) {
      let bookLine = new PurchaseBookLine();

      bookLine.Id = 0;
      bookLine.PurchaseBookId = item.PurchaseId;
      bookLine.BookSubjectId = item.BookSubjectId;
      bookLine.BookGradeLavelId = item.BookGradeLavelId;
      bookLine.BookLanguageId = item.BookLanguageId;
      bookLine.Quantity = item.PurchaseQuantity;

      book.PurchaseBookLines.push(bookLine);
    }

    this.modalRef.hide();
    this.openPurchaseModal(this.model);

    // this._service.AddBook(book).subscribe((response) => {
    //   if (response) {
    //     this.modalRef.hide();

    //     this.toastr.success("Purchase Successfull", "Success");
    //   } else {
    //     this.modalRef.hide();
    //     this.toastr.error("Purchase Failed !", "Eroor");
    //   }
    // });

    // this.model.Status = 3;
    // this._service.changeStatus(this.model).subscribe((response) => {
    //   if (response) {
    //     this.setDataByParams(this.model.id);
    //   }
    // });
  }

  openPurchaseModal(model: any) {
    this.modalRef = this.modalService.show(PurchaseBookCreateModalComponent, {
      initialState: {
        title: "Purchase Detail",
        data: {
          model: model,
        },
      },
      class: "modal-lg",
      ignoreBackdropClick: false,
    });
  }
}
