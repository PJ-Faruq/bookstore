import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { BsModalRef } from "ngx-bootstrap/modal";
import { NgxSpinnerService } from "ngx-spinner";
import { ToastrService } from "ngx-toastr";
import { EditComponent } from "src/app/generics/components/edit/edit.component";
import { GenericService } from "src/app/generics/services/generic.service";
import { Woreda1 } from "src/app/models/basic/woreda1";
import { CheckNameService } from "src/app/regulars/services/check-name.service";
import { Woreda1Service } from "src/app/regulars/services/woreda1.service";

@Component({
  selector: "app-woreda1-edit-modal",
  templateUrl: "./woreda1-edit-modal.component.html",
  styleUrls: ["./woreda1-edit-modal.component.css"],
})
export class Woreda1EditModalComponent
  extends EditComponent
  implements OnInit {
  editMode: boolean = true;
  existingName: string = null;

  constructor(
    public modalRef: BsModalRef,
    public service: GenericService,
    public checkNameService: CheckNameService,
    public _service: Woreda1Service,
    public router: Router,
    public toastr: ToastrService,
    public spinner: NgxSpinnerService
  ) {
    super(modalRef, service, router, toastr, spinner);
    this.model = new Woreda1();
    this.apiUrl = "Woreda1s";
  }

  ngOnInit() {
    this.setDataByParams();

    setTimeout(() => {
      this.existingName = this.model.Name;
    }, 1000);
  }

  onUpdate() {
    this.checkNameService
      .checkName("Woreda1s", this.model)
      .subscribe((response) => {
        if (response != null) {
          if (response["Name"] != this.existingName) {
            this.toastr.error("This Woreda1 name is Already Exist");
          } else {
            this.updateWoreda1();
          }
        }

        if (response == null) {
          this.updateWoreda1();
        }
      });
  }

  updateWoreda1() {
    this.service.update(this.model.Id, this.model, "Woreda1s").subscribe(
      (response) => {
        if (response) {
          this.modalRef.hide();
          this.toastr.success("Woreda1 Updated Successfully");
          this._service.refreshList("refresh");
        }
      },
      (error) => {
        this.toastr.error("Internal Server Error");
      }
    );
  }
}

