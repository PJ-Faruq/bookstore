import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { NgxSpinnerService } from "ngx-spinner";
import { ToastrService } from "ngx-toastr";
import { CreateComponent } from "src/app/generics/components/create/create.component";
import { GenericService } from "src/app/generics/services/generic.service";
import { Woreda1 } from "src/app/models/basic/woreda1";
import { CheckNameService } from "src/app/regulars/services/check-name.service";

@Component({
  selector: "app-woreda1-create",
  templateUrl: "./woreda1-create.component.html",
  styleUrls: ["./woreda1-create.component.css"],
})
export class Woreda1CreateComponent extends CreateComponent implements OnInit {
  title = "Woreda1";

  constructor(
    public service: GenericService,
    public checkNameService: CheckNameService,

    public router: Router,
    public activatedRoute: ActivatedRoute,
    public spinner: NgxSpinnerService,
    public toastr: ToastrService
  ) {
    super(service, router, activatedRoute, spinner, toastr);
    this.model = new Woreda1();
    this.apiUrl = "Woreda1s";
  }

  ngOnInit() {
    this.setDataByParams();
  }

  reset() {
    this.editMode = false;
    this.model = new Woreda1();
  }

  onSubmit() {
    this.checkNameService.checkName("Woreda1s", this.model).subscribe(
      (response) => {
        if (response == null) {
          this.AddItem();
        } else {
          this.toastr.error("This Name is Already Exist !");
        }
      },
      (error) => {
        this.toastr.error("Internal Server Error");
      }
    );
  }

  AddItem() {
    this.service.add(this.model, "Woreda1s").subscribe(
      (resp) => {
        if (resp) {
          this.toastr.success("Woreda1 Successfully Added");
          this.router.navigate(["/woreda1/list"]);
        } else {
          this.toastr.error("Woreda1 Added Failed !");
        }
      },
      (err) => {
        this.toastr.error("Internal Server Error");
      }
    );
  }
}

