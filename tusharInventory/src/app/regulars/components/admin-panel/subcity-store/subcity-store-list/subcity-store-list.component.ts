import { Component, OnInit } from "@angular/core";
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { flatMap } from 'rxjs/operators';
import { GenericService } from "src/app/generics/services/generic.service";
import { Book } from "src/app/models/basic/book";
import { GradeLavel } from "src/app/models/basic/grade-lavel";
import { Language } from "src/app/models/basic/language";
import { SubjectModel } from "src/app/models/basic/subject";
import { StockSearchVm } from 'src/app/models/view-models/stockSearchVm';
import { SubcityStockSearchVm } from 'src/app/models/view-models/SubcityStockSearchVm';
import { UserVm } from "src/app/models/view-models/userVm.model";
import { AutehnticationService } from "src/app/regulars/services/autehntication.service";
import { StockService } from "src/app/regulars/services/stock.service";
import { SubcityStoreService } from 'src/app/regulars/services/subcity-store.service';
import { SubcityStoreDetailComponent } from '../subcity-store-detail/subcity-store-detail.component';

@Component({
  selector: "app-subcity-store-list",
  templateUrl: "./subcity-store-list.component.html",
  styleUrls: ["./subcity-store-list.component.css"],
})
export class SubcityStoreListComponent implements OnInit {
  stockStatusList = [
    {
      Id: 1,
      Name: "ALL",
    },
    {
      Id: 2,
      Name: "Low Stock & Requested",
    },
    {
      Id: 3,
      Name: "Low Stock & Not Requested",
    },
  ];
  subjectList: SubjectModel[] = [];
  languageList: Language[] = [];
  gradeLavelList: GradeLavel[] = [];
  isAllSelected: boolean = false;
  currentUser: UserVm;
  model: SubcityStockSearchVm;
  stockList: StockSearchVm[] = [];
  requestedStockList: StockSearchVm[] = [];
  modalRef: BsModalRef;
  subcityList: any[];
  constructor(
    public service: GenericService,
    public _service: SubcityStoreService,
    public modalService: BsModalService,
    private authenticationService: AutehnticationService
  ) {
    this.model = new SubcityStockSearchVm();
  }

  ngOnInit(): void {
    this.currentUser = this.authenticationService.currentUserValue;

    this.service.getAll("Subcities").subscribe(
      (data) => {
        this.subcityList = data;
      },
      (err) => {
        this.subcityList = [];
      }
    );
    this.service.getAll("Subjects").subscribe(
      (data) => {
        this.subjectList = data;
      },
      (err) => {
        this.subjectList = [];
      }
    );

    this.service.getAll("Languages").subscribe(
      (data) => {
        this.languageList = data;
      },
      (err) => {
        this.languageList = [];
      }
    );

    this.service.getAll("GradeLavels").subscribe(
      (data) => {
        this.gradeLavelList = data;
      },
      (err) => {
        this.gradeLavelList = [];
      }
    );

    if(this.currentUser.Role=="Subcity Manager"){
      this._service.getAllByUserId(this.currentUser.Id).subscribe((response) => {
        this.stockList = response;
        this.model.UserId = this.currentUser.Id;
        console.log(this.stockList);
      });
    }
  }

  onSelectAll(isChecked: boolean) {
    this.isAllSelected = !this.isAllSelected;
    if (this.isAllSelected) {
      for (let item of this.stockList) {
        item.isChecked = true;
        item.isDisabled = true;
      }
    } else {
      for (let item of this.stockList) {
        item.isChecked = false;
        item.isDisabled = false;
      }
    }

    if (isChecked) {
      this.requestedStockList = this.stockList.filter(
        (m) => m.IsRequested == false
      );
    }
    if (!isChecked) {
      this.requestedStockList = [];
    }
  }

  onSingleItemSelect(id: number, isChecked: boolean) {
    if (isChecked) {
      var item = this.stockList.find((m) => m.Id == id);
      this.requestedStockList.push(item);
    }
    if (!isChecked) {
      var item = this.stockList.find((m) => m.Id == id);
      if (item != null) {
        this.requestedStockList = this.requestedStockList.filter(
          (m) => m.Id != id
        );
      }
    }
  }

  openDetailModal() {
    this.modalRef = this.modalService.show(SubcityStoreDetailComponent, {
      initialState: {
        data: {
          subcityId:this.model.SubcityId,
          list: this.requestedStockList,
        },
      },
      class: "modal-lg",
      ignoreBackdropClick: false,
    });
  }

  onSearch() {
    if(this.model.SubcityId==null && this.currentUser.Role !="Subcity Manager"){
      return;
    }
    this.requestedStockList=[];
    this._service.getAllBySearch(this.model).subscribe((response) => {
      this.stockList = response;
    });
  }
}
