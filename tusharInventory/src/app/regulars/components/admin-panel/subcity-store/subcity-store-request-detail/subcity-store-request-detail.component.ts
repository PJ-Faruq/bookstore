import { Component, OnInit } from "@angular/core";
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { ToastrService } from 'ngx-toastr';
import { SubcityOrder } from 'src/app/models/operation/SubcityOrder';
import { UserVm } from "src/app/models/view-models/userVm.model";
import { AutehnticationService } from "src/app/regulars/services/autehntication.service";
import { SubcityStoreService } from "src/app/regulars/services/subcity-store.service";

@Component({
  selector: "app-subcity-store-request-detail",
  templateUrl: "./subcity-store-request-detail.component.html",
  styleUrls: ["./subcity-store-request-detail.component.css"],
})
export class SubcityStoreRequestDetailComponent implements OnInit {
  currentUser: UserVm;
  data: any;
  model: SubcityOrder;
  bookstoreEmployeeList: any;
  isAcceptable:boolean=false;
  constructor(
    public _service: SubcityStoreService,
    private authenticationService: AutehnticationService,
    public modalRef: BsModalRef,
    public modalService: BsModalService,
    public toastr: ToastrService
  ) {
    this.currentUser = this.authenticationService.currentUserValue;
    this.model = new SubcityOrder();
  }

  ngOnInit(): void {
    this._service.getOrderById(this.data.id).subscribe((response) => {
      this.model = response;
    });

    this._service.getBookstoreEmployee().subscribe((response) => {
      this.bookstoreEmployeeList = response;
    });
  }

  onApproved() {
    this.model.Status = 2;
    this.model.BookstoreManagerId = this.currentUser.Id;

    this._service.updateRequest(this.model).subscribe((response) => {
      if (response) {
        this.modalRef.hide();
        this.toastr.success("The Request Approved Successfully");
        this._service.refreshList("update");
      }
    },
    error=>{
      this.toastr.error("Request Approved Failed. Internal Server Error");
    });
  }

  onRejected() {
        this.model.Status = 4;
        this.model.BookstoreManagerId = this.currentUser.Id;

        this._service.updateRequest(this.model).subscribe(
          (response) => {
            if (response) {
              this.modalRef.hide();
              this.toastr.error("The Request is Rejected");
              this._service.refreshList("update");
            }
          },
          (error) => {
            this.toastr.error("Request Rejection Failed. Internal Server Error");
          }
        );
  }

  onCompleted(){
        this.model.Status = 3;
        this.model.BookstoreEmployeeId = this.currentUser.Id;

        this._service.updateRequest(this.model).subscribe(
          (response) => {
            if (response) {
              this.modalRef.hide();
              this.toastr.success("The Request is Completed");
              this._service.refreshList("update");
            }
          },
          (error) => {
            this.toastr.error("Request Completion Failed. Internal Server Error");
          }
        );
  }

  checkQuantity(mainStockQty,transferQuantity){
    if(transferQuantity>mainStockQty || transferQuantity==0){
      this.isAcceptable=false;
    }
    else{
      this.isAcceptable=true;
    }
  }
}
