import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { ToastrService } from "ngx-toastr";
import { BsModalService } from "ngx-bootstrap/modal";
import { NgxSpinnerService } from "ngx-spinner";
import { GenericService } from "src/app/generics/services/generic.service";
import { UserVm } from "src/app/models/view-models/userVm.model";
import { AutehnticationService } from "src/app/regulars/services/autehntication.service";
import { AppSettings } from "src/app/shared/app-settings";
import { PurchaseCreateComponent } from "../../purchase/purchase-create/purchase-create.component";
import { PurchaseService } from "src/app/regulars/services/purchase.service";
import { SubcityService } from 'src/app/regulars/services/subcity.service';
import { SubcityOrder } from 'src/app/models/operation/SubcityOrder';
import { SubcityOrderLine } from 'src/app/models/operation/SubcityOrderLine';
import { SubcityStoreService } from 'src/app/regulars/services/subcity-store.service';
import { Router } from '@angular/router';

@Component({
  selector: "app-subcity-store-detail",
  templateUrl: "./subcity-store-detail.component.html",
  styleUrls: ["./subcity-store-detail.component.css"],
})
export class SubcityStoreDetailComponent implements OnInit {
  data: any;
  currentUser: UserVm;
  outOfStock = false;
  isAvailable = false;
  modalReff: BsModalRef;
  managerList: any;
  subcityManagerList: any;
  model: SubcityOrder;
  subcityList: any[];
  bookstoreEmployeeList: any;

  constructor(
    public modalRef: BsModalRef,
    public modalService: BsModalService,
    public purchaseService: PurchaseService,
    public service: GenericService,
    public _service: SubcityStoreService,
    public spinner: NgxSpinnerService,
    public router: Router,
    private toastr: ToastrService,
    private authenticationService: AutehnticationService
  ) {
    this.model = new SubcityOrder();
  }

  ngOnInit(): void {
    this.service.getAll("Subcities").subscribe(
      (data) => {
        this.subcityList = data;

        setTimeout(() => {
          this.model.SubcityId = this.data.subcityId;
        }, 100);

        setTimeout(() => {
          this._service
            .getSubcityManagerListBySubcityId(this.model.SubcityId)
            .subscribe((response) => {
              this.subcityManagerList = response;
            });
        }, 300);
      },
      (err) => {
        this.subcityList = [];
      }
    );

    this._service.getBookstoreEmployee().subscribe((response) => {
      this.bookstoreEmployeeList = response;
    });
    this._service.getManagerList().subscribe((response) => {
      this.managerList = response;
    });
    this.currentUser = this.authenticationService.currentUserValue;

    if (this.currentUser.Role == "Bookstore Manager") {
      this._service.getOrderForManager(this.data.list).subscribe((response) => {
        this.model = response;
      });
    }

    if (this.currentUser.Role == "Subcity Manager") {
      for (let item of this.data.list) {
        let orderLine = new SubcityOrderLine();
        orderLine.BookGradeLavelId = item.BookGradeId;
        orderLine.BookLanguageId = item.BookLanguageId;
        orderLine.BookSubjectId = item.BookSubjectId;
        orderLine.SubcityStockQty = item.AvailableQty;
        this.model.OrderLines.push(orderLine);
      }
    }
  }

  //Manager Click Events
  onManagerApprove() {}

  onRequest() {
    this.model.Date = new Date();
    this.model.Status = 1;
    this.model.SubcityManagerId = this.currentUser.Id;

    this._service.createRequest(this.model).subscribe((response) => {
      if (response) {
        this.modalRef.hide();
        this.router.navigate(["subcity-store/request-list"]);
        this.toastr.success("Request Succesfully Sent to The Manager");
      }
    });
  }

  onTransfer() {
    this.model.BookstoreManagerId = this.currentUser.Id;
    this.model.Date = new Date();
    this.model.Status = 2;

    

    this._service.createRequest(this.model).subscribe((response) => {
      if (response) {
        this.modalRef.hide();
        this.router.navigate(["subcity-store/request-list"]);
        this.toastr.success("Transfer Request Successful");
      }
    });
  }
}
