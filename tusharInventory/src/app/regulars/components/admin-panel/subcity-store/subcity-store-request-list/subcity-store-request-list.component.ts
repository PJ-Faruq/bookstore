import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from '@angular/router';
import { DataTableDirective } from "angular-datatables";
import { BsModalRef, BsModalService } from "ngx-bootstrap/modal";
import { Subject } from "rxjs";
import { GenericService } from "src/app/generics/services/generic.service";
import { SubcityOrder } from "src/app/models/operation/SubcityOrder";
import { SubcityOrderLine } from "src/app/models/operation/SubcityOrderLine";
import { SubcityOrderSearchVm } from 'src/app/models/view-models/SubcityOrderSearchVm';
import { UserVm } from "src/app/models/view-models/userVm.model";
import { AutehnticationService } from "src/app/regulars/services/autehntication.service";
import { SubcityStoreService } from "src/app/regulars/services/subcity-store.service";
import { SubcityStoreDetailComponent } from "../subcity-store-detail/subcity-store-detail.component";
import { SubcityStoreRequestDetailComponent } from "../subcity-store-request-detail/subcity-store-request-detail.component";

@Component({
  selector: "app-subcity-store-request-list",
  templateUrl: "./subcity-store-request-list.component.html",
  styleUrls: ["./subcity-store-request-list.component.css"],
})
export class SubcityStoreRequestListComponent implements OnInit {
  modelList: SubcityOrder[] = [];
  currentUser: UserVm;
  modalRef: BsModalRef;
  managerStatus: string;
  searchVm: SubcityOrderSearchVm;
  p: number = 1;

  //Datatable Field
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject<any>();
  dtElement: DataTableDirective;
  constructor(
    public service: GenericService,
    public _service: SubcityStoreService,
    public modalService: BsModalService,
    private route: ActivatedRoute,
    private authenticationService: AutehnticationService
  ) {
    // For Refreshing List from outside
    this._service.listen().subscribe((m) => {
      this.getAll();
    });
    this.searchVm = new SubcityOrderSearchVm();
    this.searchVm.Status = this.route.snapshot.paramMap.get("status");
  }

  ngOnInit(): void {
    this.currentUser = this.authenticationService.currentUserValue;
    this.getAll();
  }

  getAll() {
    this.searchVm.UserId = this.currentUser.Id;
    this._service.getSubcityOrder(this.searchVm).subscribe((response) => {
      if (
        this.currentUser.Role == "Subcity Manager" ||
        this.currentUser.Role == "Bookstore Manager"
      ) {
        this.modelList = response.reverse();
      } else {
        this.modelList = response;
      }
    });
  }

  openDetailModal(id: number) {
    this.modalRef = this.modalService.show(SubcityStoreRequestDetailComponent, {
      initialState: {
        data: {
          id: id,
        },
      },
      class: "modal-lg",
      ignoreBackdropClick: false,
    });
  }

  //#region Datatable Settings
  ngAfterViewInit(): void {
    setTimeout(() => {
      this.dtTrigger.next();
    }, 300);
  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }

  rerender(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      // Destroy the table first
      dtInstance.destroy();
      // Call the dtTrigger to rerender again
      this.dtTrigger.next();
    });
  }

  //#endregion
}
