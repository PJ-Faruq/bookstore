import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { BsModalRef } from "ngx-bootstrap/modal";
import { NgxSpinnerService } from "ngx-spinner";
import { ToastrService } from "ngx-toastr";
import { EditComponent } from "src/app/generics/components/edit/edit.component";
import { GenericService } from "src/app/generics/services/generic.service";
import { Language } from "src/app/models/basic/language";
import { CheckNameService } from "src/app/regulars/services/check-name.service";
import { LanguageService } from "src/app/regulars/services/language.service";

@Component({
  selector: "app-language-edit-modal",
  templateUrl: "./language-edit-modal.component.html",
  styleUrls: ["./language-edit-modal.component.css"],
})
export class LanguageEditModalComponent extends EditComponent implements OnInit {
  editMode: boolean = true;
  existingName: string = null;

  constructor(
    public modalRef: BsModalRef,
    public service: GenericService,
    public checkNameService: CheckNameService,
    public _service: LanguageService,
    public router: Router,
    public toastr: ToastrService,
    public spinner: NgxSpinnerService
  ) {
    super(modalRef, service, router, toastr, spinner);
    this.model = new Language();
    this.apiUrl = "Languages";
  }

  ngOnInit() {
    this.setDataByParams();

    setTimeout(() => {
      this.existingName = this.model.Name;
    }, 1000);
  }

  onUpdate() {
    this.checkNameService
      .checkName("Languages", this.model)
      .subscribe((response) => {
        if (response != null) {
          if (response["Name"] != this.existingName) {
            this.toastr.error("This Language name is Already Exist");
          } else {
            this.updateLanguage();
          }
        }

        if (response == null) {
          this.updateLanguage();
        }
      });
  }

  updateLanguage() {
    this.service.update(this.model.Id, this.model, "Languages").subscribe(
      (response) => {
        if (response) {
          this.modalRef.hide();
          this.toastr.success("Language Updated Successfully");
          this._service.refreshList("refresh");
        }
      },
      (error) => {
        this.toastr.error("Internal Server Error");
      }
    );
  }
}

