import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { CreateComponent } from 'src/app/generics/components/create/create.component';
import { GenericService } from 'src/app/generics/services/generic.service';
import { Language } from 'src/app/models/basic/language';
import { CheckNameService } from 'src/app/regulars/services/check-name.service';

@Component({
  selector: "app-language-create",
  templateUrl: "./language-create.component.html",
  styleUrls: ["./language-create.component.css"],
})
export class LanguageCreateComponent extends CreateComponent implements OnInit {
  title = "Language";

  constructor(
    public service: GenericService,
    public checkNameService: CheckNameService,

    public router: Router,
    public activatedRoute: ActivatedRoute,
    public spinner: NgxSpinnerService,
    public toastr: ToastrService
  ) {
    super(service, router, activatedRoute, spinner, toastr);
    this.model = new Language();
    this.apiUrl = "Languages";
  }

  ngOnInit() {
    this.setDataByParams();
  }

  reset() {
    this.editMode = false;
    this.model = new Language();
  }

  onSubmit() {
    this.checkNameService.checkName("Languages", this.model).subscribe(
      (response) => {
        if (response == null) {
          this.AddItem();
        } else {
          this.toastr.error("This Name is Already Exist !");
        }
      },
      (error) => {
        this.toastr.error("Internal Server Error");
      }
    );
  }

  AddItem() {
    this.service.add(this.model, "Languages").subscribe(
      (resp) => {
        if (resp) {
          this.toastr.success("Language Successfully Added");
          this.router.navigate(["/language/list"]);
        } else {
          this.toastr.error("Language Added Failed !");
        }
      },
      (err) => {
        this.toastr.error("Internal Server Error");
      }
    );
  }
}
