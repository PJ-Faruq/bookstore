import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { NgxSpinnerService } from "ngx-spinner";
import { ToastrService } from "ngx-toastr";
import { CreateComponent } from "src/app/generics/components/create/create.component";
import { GenericService } from "src/app/generics/services/generic.service";
import { Ownership } from "src/app/models/basic/ownership";
import { CheckNameService } from "src/app/regulars/services/check-name.service";

@Component({
  selector: "app-ownership-create",
  templateUrl: "./school-ownership-create.component.html",
  styleUrls: ["./school-ownership-create.component.css"],
})
export class SchoolOwnershipCreateComponent extends CreateComponent implements OnInit {
  title = "Ownership";

  constructor(
    public service: GenericService,
    public checkNameService: CheckNameService,

    public router: Router,
    public activatedRoute: ActivatedRoute,
    public spinner: NgxSpinnerService,
    public toastr: ToastrService
  ) {
    super(service, router, activatedRoute, spinner, toastr);
    this.model = new Ownership();
    this.apiUrl = "Ownerships";
  }

  ngOnInit() {
    this.setDataByParams();
  }

  reset() {
    this.editMode = false;
    this.model = new Ownership();
  }

  onSubmit() {
    this.checkNameService.checkName("Ownerships", this.model).subscribe(
      (response) => {
        if (response == null) {
          this.AddItem();
        } else {
          this.toastr.error("This Name is Already Exist !");
        }
      },
      (error) => {
        this.toastr.error("Internal Server Error");
      }
    );
  }

  AddItem() {
    this.service.add(this.model, "Ownerships").subscribe(
      (resp) => {
        if (resp) {
          this.toastr.success("Ownership Successfully Added");
          this.router.navigate(["/school-ownership/list"]);
        } else {
          this.toastr.error("Ownership Added Failed !");
        }
      },
      (err) => {
        this.toastr.error("Internal Server Error");
      }
    );
  }
}
