import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { BsModalRef } from "ngx-bootstrap/modal";
import { NgxSpinnerService } from "ngx-spinner";
import { ToastrService } from "ngx-toastr";
import { EditComponent } from "src/app/generics/components/edit/edit.component";
import { GenericService } from "src/app/generics/services/generic.service";
import { Ownership } from "src/app/models/basic/ownership";
import { CheckNameService } from "src/app/regulars/services/check-name.service";
import { SchoolOwnershipService } from 'src/app/regulars/services/school-ownership.service';

@Component({
  selector: "app-ownership-edit-modal",
  templateUrl: "./school-ownership-edit-modal.component.html",
  styleUrls: ["./school-ownership-edit-modal.component.css"],
})
export class SchoolOwnershipEditModalComponent
  extends EditComponent
  implements OnInit {
  editMode: boolean = true;
  existingName: string = null;

  constructor(
    public modalRef: BsModalRef,
    public service: GenericService,
    public checkNameService: CheckNameService,
    public _service: SchoolOwnershipService,
    public router: Router,
    public toastr: ToastrService,
    public spinner: NgxSpinnerService
  ) {
    super(modalRef, service, router, toastr, spinner);
    this.model = new Ownership();
    this.apiUrl = "Ownerships";
  }

  ngOnInit() {
    this.setDataByParams();

    setTimeout(() => {
      this.existingName = this.model.Name;
    }, 1000);
  }

  onUpdate() {
    this.checkNameService
      .checkName("Ownerships", this.model)
      .subscribe((response) => {
        if (response != null) {
          if (response["Name"] != this.existingName) {
            this.toastr.error("This Ownership name is Already Exist");
          } else {
            this.updateOwnership();
          }
        }

        if (response == null) {
          this.updateOwnership();
        }
      });
  }

  updateOwnership() {
    this.service.update(this.model.Id, this.model, "Ownerships").subscribe(
      (response) => {
        if (response) {
          this.modalRef.hide();
          this.toastr.success("Ownership Updated Successfully");
          this._service.refreshList("refresh");
        }
      },
      (error) => {
        this.toastr.error("Internal Server Error");
      }
    );
  }
}

