import { Component, OnInit } from '@angular/core';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import { GenericService } from 'src/app/generics/services/generic.service';
import { Book } from 'src/app/models/basic/book';
import { GradeLavel } from 'src/app/models/basic/grade-lavel';
import { Language } from 'src/app/models/basic/language';
import { SubjectModel } from 'src/app/models/basic/subject';
import { UserVm } from 'src/app/models/view-models/userVm.model';
import { AutehnticationService } from 'src/app/regulars/services/autehntication.service';
import { StockService } from 'src/app/regulars/services/stock.service';

@Component({
  selector: "app-bookstore-stock",
  templateUrl: "./bookstore-stock.component.html",
  styleUrls: ["./bookstore-stock.component.css"],
})
export class BookstoreStockComponent implements OnInit {
  subjectList: SubjectModel[] = [];
  languageList: Language[] = [];
  gradeLavelList: GradeLavel[] = [];
  currentUser: UserVm;
  model: Book;
  stockList: any;

  //Datatable Field
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject<any>();
  dtElement: DataTableDirective;
  constructor(
    public service: GenericService,
    public _service: StockService,

    private authenticationService: AutehnticationService
  ) {
    this.model = new Book();
  }

  ngOnInit(): void {
    this.currentUser = this.authenticationService.currentUserValue;

    this.service.getAll("Subjects").subscribe(
      (data) => {
        this.subjectList = data;
      },
      (err) => {
        this.subjectList = [];
      }
    );

    this.service.getAll("Languages").subscribe(
      (data) => {
        this.languageList = data;
      },
      (err) => {
        this.languageList = [];
      }
    );

    this.service.getAll("GradeLavels").subscribe(
      (data) => {
        this.gradeLavelList = data;
      },
      (err) => {
        this.gradeLavelList = [];
      }
    );

    this.service.getAll("HoStocks").subscribe((response) => {
      this.stockList = response;
    });
  }

  onSearch() {
    this._service.onHoStockSearch(this.model).subscribe((response) => {
      this.stockList = response;
    });
  }

  //#region Datatable Settings
  ngAfterViewInit(): void {
    setTimeout(() => {
      this.dtTrigger.next();
    }, 300);
  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }

  rerender(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      // Destroy the table first
      dtInstance.destroy();
      // Call the dtTrigger to rerender again
      this.dtTrigger.next();
    });
  }

  //#endregion
}
