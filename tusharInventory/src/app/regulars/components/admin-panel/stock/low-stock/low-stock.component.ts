import { Component, OnInit } from "@angular/core";
import { GenericService } from "src/app/generics/services/generic.service";
import { Book } from "src/app/models/basic/book";
import { GradeLavel } from "src/app/models/basic/grade-lavel";
import { Language } from "src/app/models/basic/language";
import { SubjectModel } from "src/app/models/basic/subject";
import { UserVm } from "src/app/models/view-models/userVm.model";
import { AutehnticationService } from "src/app/regulars/services/autehntication.service";
import { StockService } from "src/app/regulars/services/stock.service";

@Component({
  selector: "app-low-stock",
  templateUrl: "./low-stock.component.html",
  styleUrls: ["./low-stock.component.css"],
})
export class LowStockComponent implements OnInit {
  subjectList: SubjectModel[] = [];
  languageList: Language[] = [];
  gradeLavelList: GradeLavel[] = [];
  currentUser: UserVm;
  model: Book;
  stockList: any;
  constructor(
    public service: GenericService,
    public _service: StockService,

    private authenticationService: AutehnticationService
  ) {
    this.model = new Book();
  }

  ngOnInit(): void {
    this.currentUser = this.authenticationService.currentUserValue;
    this.service.getAll("HoStocks").subscribe((response) => {
      this.stockList = response.filter((m) => m.AvailableQty<100);    
    });
  }
}
