import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { GenericService } from 'src/app/generics/services/generic.service';
import { Book } from 'src/app/models/basic/book';
import { GradeLavel } from 'src/app/models/basic/grade-lavel';
import { Language } from 'src/app/models/basic/language';
import { School } from 'src/app/models/basic/school';
import { SubjectModel } from 'src/app/models/basic/subject';
import { SchoolStock } from 'src/app/models/inventory/school-stock';
import { SchoolStockSearchVm } from 'src/app/models/view-models/schoolStockSearchVm';
import { UserVm } from 'src/app/models/view-models/userVm.model';
import { AutehnticationService } from 'src/app/regulars/services/autehntication.service';
import { StockService } from 'src/app/regulars/services/stock.service';

@Component({
  selector: "app-school-stock",
  templateUrl: "./school-stock.component.html",
  styleUrls: ["./school-stock.component.css"],
})
export class SchoolStockComponent implements OnInit {
  schoolList: any = [];
  subjectList: SubjectModel[] = [];
  languageList: Language[] = [];
  gradeLavelList: GradeLavel[] = [];

  currentUser: UserVm;
  stockList: any;
  model: SchoolStockSearchVm;

  constructor(
    public service: GenericService,
    public _service: StockService,
    public toastr: ToastrService,
    private authenticationService: AutehnticationService
  ) {
    this.model = new SchoolStockSearchVm();
  }

  ngOnInit(): void {
    this.currentUser = this.authenticationService.currentUserValue;

    this.service.getAll("Schools/GetSchoolName").subscribe(
      response=>{
        this.schoolList=response;
      }
    )

    this.service.getAll("SchoolStocks").subscribe((response) => {
      this.stockList = response;
      this.stockList = this.stockList.filter(
        (m) => m.SchoolId == this.currentUser.SchoolId
      );
    });

    this.service.getAll("Subjects").subscribe(
      (data) => {
        console.log(data);
        this.subjectList = data;
      },
      (err) => {
        this.subjectList = [];
      }
    );

    this.service.getAll("Languages").subscribe(
      (data) => {
        this.languageList = data;
      },
      (err) => {
        this.languageList = [];
      }
    );

    this.service.getAll("GradeLavels").subscribe(
      (data) => {
        this.gradeLavelList = data;
      },
      (err) => {
        this.gradeLavelList = [];
      }
    );
  }

  onSearch() {

    if(this.currentUser.Role=="School Employee"){
      this.model.SchoolId = this.currentUser.SchoolId;
    }

    else{
      if(this.model.SchoolId==null){
        this.toastr.error("Please Select a School");
        return;
      }
    }
    
    this._service.onSchoolSearch(this.model).subscribe((response) => {
      this.stockList = response;
    });
  }
}
