import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { GenericService } from 'src/app/generics/services/generic.service';
import { GradeLavel } from 'src/app/models/basic/grade-lavel';
import { Language } from 'src/app/models/basic/language';
import { SubjectModel } from 'src/app/models/basic/subject';
import { HoStock } from 'src/app/models/inventory/ho-stock';
import { StockService } from 'src/app/regulars/services/stock.service';

@Component({
  selector: "app-stock-create",
  templateUrl: "./stock-create.component.html",
  styleUrls: ["./stock-create.component.css"],
})
export class StockCreateComponent implements OnInit {
  title = "Book";
  model: any;
  existingQuantity:any;
  newQuantity:number;

  subjectList: SubjectModel[] = [];
  languageList: Language[] = [];
  gradeLavelList: GradeLavel[] = [];

  constructor(
    public service: GenericService,
    public router: Router,
    public _service:StockService,
    public activatedRoute: ActivatedRoute,
    public spinner: NgxSpinnerService,
    public toastr: ToastrService
  ) {
    this.model = new HoStock();
  }

  ngOnInit() {
    this.service.getAll("Subjects").subscribe(
      (data) => {
        this.subjectList = data;
      },
      (err) => {
        this.subjectList = [];
      }
    );

    this.service.getAll("Languages").subscribe(
      (data) => {
        this.languageList = data;
      },
      (err) => {
        this.languageList = [];
      }
    );

    this.service.getAll("GradeLavels").subscribe(
      (data) => {
        this.gradeLavelList = data;
      },
      (err) => {
        this.gradeLavelList = [];
      }
    );
  }

  onChange(){
    if(this.model.BookSubjectId && this.model.BookLanguageId && this.model.BookGradeId)
    {
      this._service.GetBookStock(this.model).subscribe(
        response=>{
          console.log(response);
          if(response ==null){
            this.toastr.error("No Book Available. Please Add the Book First");
            this.existingQuantity = "";
          }
          else if (response == "0") {
            this.existingQuantity = response;
          } else {
            this.model=response;
            this.existingQuantity = response["AvailableQty"];
          }
           
        }
      )
    }
  }

  onSubmit(){
    this.model.AvailableQty=this.newQuantity+this.existingQuantity;
    this.service.add(this.model,"HoStocks").subscribe(
      response=>{
        if(response){
          this.toastr.success("Stock Successfully Updated");
          this.router.navigate(['stock/bookstore']);
        }
        else{
          this.toastr.error("Stock Update Failed !");
        }
      },
      error=>{
        this.toastr.error("Stock Update Failed !");
      }
    );
  }


}
