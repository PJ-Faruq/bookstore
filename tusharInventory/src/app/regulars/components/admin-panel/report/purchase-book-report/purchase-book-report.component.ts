import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { BsModalRef, BsModalService } from "ngx-bootstrap/modal";
import { NgxSpinnerService } from "ngx-spinner";
import { ToastrService } from "ngx-toastr";
import { ListComponent } from "src/app/generics/components/list/list.component";
import { GenericService } from "src/app/generics/services/generic.service";
import { BudgetType } from 'src/app/models/basic/BudgetType';
import { Supplier } from 'src/app/models/basic/supplier';
import { BookReportResultVm } from 'src/app/models/view-models/BookReportResultVm';
import { PurchaseBookSearchVm } from 'src/app/models/view-models/PurchaseBookSearchVm';
import { PurchaseReportResultVm } from 'src/app/models/view-models/PurchaseReportResultVm';
import { UserVm } from "src/app/models/view-models/userVm.model";
import { AutehnticationService } from "src/app/regulars/services/autehntication.service";
import { ConfirmService } from "src/app/regulars/services/confirm.service";
import { ExcelService } from 'src/app/regulars/services/excel.service';
import { PurchaseBookService } from "src/app/regulars/services/purchase-book.service";
import { OrderDetailModalComponent } from "../../order/order-detail-modal/order-detail-modal.component";
import { PurchaseBookDetailComponent } from '../../purchase-book/purchase-book-detail/purchase-book-detail.component';


@Component({
  selector: "app-purchase-book-report",
  templateUrl: "./purchase-book-report.component.html",
  styleUrls: ["./purchase-book-report.component.css"],
})
export class PurchaseBookReportComponent implements OnInit {
  currentUser: UserVm;
  apiUrl = "PurchaseBooks";
  modelList: any[] = [];

  modalRef: BsModalRef;
  searchVm: PurchaseBookSearchVm;
  supplierList: Supplier[] = [];
  budgetTypeList: BudgetType[] = [];
  p: number = 1;
  list: any[] = [];

  constructor(
    public service: GenericService,
    public _service: PurchaseBookService,
    public modalService: BsModalService,
    public confirmService: ConfirmService,
    public toastr: ToastrService,
    public excelService: ExcelService,
    public spinner: NgxSpinnerService,
    private router: Router,
    private authenticationService: AutehnticationService
  ) {
    this.searchVm = new PurchaseBookSearchVm();
  }

  ngOnInit(): void {
    this.currentUser = this.authenticationService.currentUserValue;

    this.service.getAll("Suppliers").subscribe(
      (data) => {
        this.supplierList = data;
      },
      (err) => {
        this.supplierList = [];
      }
    );
    this._service.GetBudgetType().subscribe(
      (data) => {
        this.budgetTypeList = data;
      },
      (err) => {
        this.budgetTypeList = [];
      }
    );
  }

  getBySearch() {
    this._service
      .getPurchaseBookBySearch(this.searchVm)
      .subscribe((response) => {
        this.modelList = response;
        console.log(this.modelList);
      });
  }

  openDetailModal(id: number) {
    this.modalRef = this.modalService.show(PurchaseBookDetailComponent, {
      initialState: {
        title: "Purchase Detail",
        data: {
          id: id,
        },
      },
      class: "modal-lg",
      ignoreBackdropClick: false,
    });
  }

  onClickReceipt(id: number) {
    const url = this.router.createUrlTree(["/report/purchase-report", id]);
    window.open(url.toString(), "_blank");
  }

  exportAsExcel() {
    for (let item of this.modelList) {
      let resultVm = new PurchaseReportResultVm();

      resultVm.Amount = item.Amount;
      resultVm.Bookstore_Employee = item.BookstoreEmployee?item.BookstoreEmployee.FullName:"";
      resultVm.Bookstore_Manager = item.BookstoreManager ? item.BookstoreManager.FullName:"";
      resultVm.Budget_Type = item.BudgetType.Name;
      resultVm.PurcahseDept_Employee = item.PurcahseDeptEmployee.FullName;
      resultVm.Purchase_No=item.PurchaseId;
      resultVm.Reference_Number=item.ReferenceNumber;
      resultVm.Transaction_Tracking_Number=item.TransactionTrackingNumber;
      resultVm.Supplier=item.Supplier.Name;

      var today = new Date(item.PurchaseDate);
      var dd = today.getDate();
      var mm = today.getMonth() + 1;
      var yyyy = today.getFullYear();
      var monthName = [
        "January",
        "February",
        "March",
        "April",
        "May",
        "June",
        "July",
        "August",
        "September",
        "October",
        "November",
        "December",
      ];

      resultVm.Purchase_Date = item.PurchaseDate
        ? dd + " " + monthName[mm - 1] + "," + yyyy
        : "";

      this.list.push(resultVm);
    }

    this.excelService.exportAsExcelFile(this.list, "Purchase Report");
  }
}
