import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import * as html2pdf from "html2pdf.js";
import { GenericService } from "src/app/generics/services/generic.service";
import { BankReceiptVoucher } from 'src/app/models/operation/BankReceiptVoucher';
import { Order } from "src/app/models/operation/order";
import { ToWords } from "to-words";

@Component({
  selector: "app-bank-receipt-voucher-detail",
  templateUrl: "./bank-receipt-voucher-detail.component.html",
  styleUrls: ["./bank-receipt-voucher-detail.component.css"],
})
export class BankReceiptVoucherDetailComponent implements OnInit {
  todayDate: Date;
  id: number;
  model: BankReceiptVoucher;
  AmountInWord: any;
  toWords: ToWords;
  constructor(private route: ActivatedRoute, public service: GenericService) {
    this.model = new BankReceiptVoucher();
    this.todayDate = new Date();
    this.toWords = new ToWords();
  }

  ngOnInit(): void {
    this.id = parseInt(this.route.snapshot.paramMap.get("id"));

    this.service
      .getById(this.id, "BankReceiptVouchers")
      .subscribe((response) => {
        this.model = response;
        this.AmountInWord = this.toWords.convert(this.model.Amount);
      });
  }

  print() {
     var originalWindow = document.body.innerHTML;
     var printContents = document.getElementById("print-section").innerHTML;
     document.body.innerHTML = printContents;
     window.print();
     document.body.innerHTML = originalWindow;
      window.location.reload(); 
  }

  downloadPdf() {
    var originalWindow = document.body.innerHTML;

    var printContents = document.getElementById("print-section");

    var opt = {
      margin: 1,
      filename: "Receipt Voucher_"+this.model.OrderId,
      image: { type: "jpeg" },
      html2canvas: {},
      jsPDF: { orientation: "portrait" },
    };

    html2pdf().from(printContents).set(opt).save();
  }
}
