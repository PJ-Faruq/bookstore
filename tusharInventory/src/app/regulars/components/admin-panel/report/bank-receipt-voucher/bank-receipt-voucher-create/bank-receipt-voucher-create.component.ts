import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { NgxSpinnerService } from "ngx-spinner";
import { ToastrService } from "ngx-toastr";
import { SubjectModel } from "src/app/models/basic/subject";
import { CreateComponent } from "src/app/generics/components/create/create.component";
import { GenericService } from "src/app/generics/services/generic.service";
import { CheckNameService } from "src/app/regulars/services/check-name.service";
import { BankReceiptVoucher } from 'src/app/models/operation/BankReceiptVoucher';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { Order } from 'src/app/models/operation/order';
import { ToWords } from "to-words";
import { BankReceiptServiceService } from 'src/app/regulars/services/bank-receipt-service.service';


@Component({
  selector: "app-bank-receipt-voucher-create",
  templateUrl: "./bank-receipt-voucher-create.component.html",
  styleUrls: ["./bank-receipt-voucher-create.component.css"],
})
export class BankReceiptVoucherCreateComponent implements OnInit {
  title = "Subject";
  model: BankReceiptVoucher;
  data: any;
  order: Order;
  subcityList: any;
  AmountInWord: any;
  toWords: ToWords;

  constructor(
    public service: GenericService,
    public bankReceiptService: BankReceiptServiceService,
    public modalRef: BsModalRef,
    public modalService: BsModalService,
    public router: Router,
    public activatedRoute: ActivatedRoute,
    public spinner: NgxSpinnerService,
    public toastr: ToastrService
  ) {
    this.toWords = new ToWords();
    this.model = new BankReceiptVoucher();
    this.order = new Order();
  }

  ngOnInit() {
    this.service.getAll("Subcities").subscribe((response) => {
      this.subcityList = response;
    });

    this.service.getById(this.data.id, "Orders").subscribe((response) => {
      this.model.Amount = response.Amount;
      this.model.BookstoreEmployeeId = response.BookstoreEmployeeId;
      this.model.BookstoreEmployee = response.BookstoreEmployee;
      this.model.OrderId = response.Id;
      this.model.SchoolId = response.SchoolId;
      this.model.School = response.School;

      this.AmountInWord = this.toWords.convert(this.model.Amount);
    });
  }

  onSubmit() {
    if (
      !this.model.Cash &&
      !this.model.Check &&
      !this.model.DepositeSlip &&
      !this.model.BankTransfer
    ) {
      this.toastr.error("Select One Mode of Collection ");
      return;
    }

    if (this.model.Check) {
      if (!this.model.CheckNo) {
        this.toastr.error("Enter Check No");
        return;
      }
    }

    if (this.model.DepositeSlip) {
      if (!this.model.DepositeDate) {
        this.toastr.error("Select Deposite Date");
        return;
      }

      if (!this.model.ReferenceNo) {
        this.toastr.error("Enter Reference No");
        return;
      }
    }

    this.model.BookstoreEmployee = null;
    this.model.School = null;

    this.bankReceiptService.createBankReceipt(this.model).subscribe(
      (response) => {
        if (response) {
          this.toastr.success("Receipt Voucher Successfully Created");
          this.modalRef.hide();
          this.bankReceiptService.refreshList("Refresh");
        }
      },
      (error) => {
        this.toastr.error("Internal Server Error");
        this.modalRef.hide();
      }
    );
  }
}
