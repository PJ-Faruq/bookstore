import {
  Component,
  OnInit
} from '@angular/core';
import {
  ActivatedRoute
} from '@angular/router';
import {
  GenericService
} from 'src/app/generics/services/generic.service';
import {
  Order
} from 'src/app/models/operation/order';
import {
  PurchaseBook
} from 'src/app/models/operation/purchase-book';
import * as html2pdf from "html2pdf.js";

@Component({
  selector: "app-purchase-report",
  templateUrl: "./purchase-report.component.html",
  styleUrls: ["./purchase-report.component.css"],
})
export class PurchaseReportComponent implements OnInit {
  id: number = null;
  todayDate: Date;
  purchaseRequestId: number = null;
  model: PurchaseBook;
  constructor(private route: ActivatedRoute, public service: GenericService) {
    this.model = new PurchaseBook();
    this.todayDate = new Date();
  }

  ngOnInit(): void {
    let purchaseBookId = this.route.snapshot.paramMap.get("id");

    if (purchaseBookId != null) {
      this.id = parseInt(this.route.snapshot.paramMap.get("id"));
      this.service.getById(this.id, "PurchaseBooks").subscribe((response) => {
        this.model = response;
      });
    }
  }

  print() {
    var originalWindow = document.body.innerHTML;
    var printContents = document.getElementById("print-section").innerHTML;
    document.body.innerHTML = printContents;
    window.print();
    document.body.innerHTML = originalWindow;
    window.location.reload();
  }

  downloadPdf() {
    var originalWindow = document.body.innerHTML;

    var printContents = document.getElementById("print-section");

    var opt = {
      margin: 1,
      filename: this.model.PurchaseId,
      image: { type: "jpeg" },
      html2canvas: {},
      jsPDF: { orientation: "portrait" },
    };

    html2pdf().from(printContents).set(opt).save();
  }
}
