import { Component, OnInit } from "@angular/core";
import { BsModalService } from "ngx-bootstrap/modal";
import { NgxSpinnerService } from "ngx-spinner";
import { ToastrService } from "ngx-toastr";
import { ListComponent } from "src/app/generics/components/list/list.component";
import { GenericService } from "src/app/generics/services/generic.service";
import { Ownership } from "src/app/models/basic/ownership";
import { Subcity } from "src/app/models/basic/subcity";
import { Woreda } from "src/app/models/basic/woreda";
import { UserActivity } from 'src/app/models/configurations/user-activity';
import { User } from "src/app/models/user";
import { LogSearchVm } from "src/app/models/view-models/LogSearchVm";
import { UserVm } from "src/app/models/view-models/userVm.model";
import { AutehnticationService } from "src/app/regulars/services/autehntication.service";
import { ConfirmService } from "src/app/regulars/services/confirm.service";
import { UserService } from 'src/app/regulars/services/user.service';
import { UserDetailModalComponent } from '../../../user/user-detail-modal/user-detail-modal.component';
import { LogAuditDetailComponent } from '../log-audit-detail/log-audit-detail.component';



@Component({
  selector: "app-log-audit-list",
  templateUrl: "./log-audit-list.component.html",
  styleUrls: ["./log-audit-list.component.css"],
})
export class LogAuditListComponent extends ListComponent implements OnInit {
  currentUser: UserVm;
  title = "UserActivity";
  apiUrl = "Users/GetUserActivity";
  createLink = "/" + this.title.toLowerCase() + "/create";
  searchVm: LogSearchVm;

  p: number = 1;
  model: UserActivity;
  activityList: UserActivity[] = [];
  userList: User[] = [];

  constructor(
    public service: GenericService,
    public _service: UserService,
    public modalService: BsModalService,
    public confirmService: ConfirmService,
    public toastr: ToastrService,
    public spinner: NgxSpinnerService,
    private authenticationService: AutehnticationService
  ) {
    super(modalService, confirmService, toastr, spinner);
    this.model = new UserActivity();
    this.searchVm = new LogSearchVm();

    // For Refreshing List from outside
    this._service.listen().subscribe((m) => {
      this.service.setAll(this.apiUrl);
    });
  }

  ngOnInit(): void {
    this.currentUser = this.authenticationService.currentUserValue;

    this.service.getAll("Users").subscribe((response) => {
      this.userList = response;
    });

    this.service.getAll("Users/GetUserActivity").subscribe((response) => {
      //var res= JSON.stringify(response);
      //this.modelList= response;

      for (let item of response) {
        var userActivity = new UserActivity();
        userActivity.Data = JSON.parse(item.Data);
        //userActivity.Data = item.Data;
        userActivity.Date = item.Date;
        userActivity.Url = item.Url;
        userActivity.UserName = item.UserName;
        userActivity.UserId = item.UserId;
        this.activityList.push(userActivity);
      }

      this.activityList = this.activityList.reverse();
    });
  }

  openDetailModal(id: number) {
    this.modalRef = this.modalService.show(UserDetailModalComponent, {
      initialState: {
        title: this.title,
        data: {
          id: id,
        },
      },
      class: "modal-lg",
    });
  }

  openDataModal(data: any) {
    this.modalRef = this.modalService.show(LogAuditDetailComponent, {
      initialState: {
        title: this.title,
        data: {
          data: data,
        },
      },
      class: "modal-lg",
    });
  }

  getBySearch() {
    this._service
      .getUserActivityBySearch(this.searchVm)
      .subscribe((response) => {
        this.activityList = [];

        for (let item of response) {
          var userActivity = new UserActivity();
          userActivity.Data = JSON.parse(item.Data);
          //userActivity.Data = item.Data;
          userActivity.Date = item.Date;
          userActivity.Url = item.Url;
          userActivity.UserName = item.UserName;
          userActivity.UserId = item.UserId;
          this.activityList.push(userActivity);
        }

        this.activityList = this.activityList;
      });
  }
}
