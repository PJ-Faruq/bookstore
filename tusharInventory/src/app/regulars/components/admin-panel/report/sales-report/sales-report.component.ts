import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import * as html2pdf from "html2pdf.js";
import { GenericService } from 'src/app/generics/services/generic.service';
import { Order } from 'src/app/models/operation/order';

@Component({
  selector: "app-sales-report",
  templateUrl: "./sales-report.component.html",
  styleUrls: ["./sales-report.component.css"],
})
export class SalesReportComponent implements OnInit {
  todayDate: Date;
  id: number;
  model: Order;
  constructor(private route: ActivatedRoute, public service: GenericService) {
    this.model = new Order();
    this.todayDate = new Date();
  }

  ngOnInit(): void {
    this.id = parseInt(this.route.snapshot.paramMap.get("id"));

    this.service.getById(this.id, "Orders").subscribe((response) => {
      this.model = response;
    });
  }

  print() {
    var originalWindow = document.body.innerHTML;
    var printContents = document.getElementById("print-section").innerHTML;
    document.body.innerHTML = printContents;
    window.print();
    document.body.innerHTML = originalWindow;
    window.location.reload();
  }

  downloadPdf(){
       var originalWindow = document.body.innerHTML;

       var printContents = document.getElementById("print-section");

       var opt = {
         margin: 1,
         filename: this.model.OrderId,
         image: { type: "jpeg"},
         html2canvas: {  },
         jsPDF: {  orientation: "portrait" },
       };

        html2pdf().from(printContents).set(opt).save();

  }
}
