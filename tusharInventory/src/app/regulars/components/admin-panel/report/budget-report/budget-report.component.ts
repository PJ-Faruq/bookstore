import { AfterViewInit, Component, OnInit } from "@angular/core";
import { BsModalService } from "ngx-bootstrap/modal";
import { Budget } from "src/app/models/basic/budget";
import { ListComponent } from "src/app/generics/components/list/list.component";
import { GenericService } from "src/app/generics/services/generic.service";
import { UserVm } from "src/app/models/view-models/userVm.model";
import { ConfirmService } from "src/app/regulars/services/confirm.service";
import { ToastrService } from "ngx-toastr";
import { NgxSpinnerService } from "ngx-spinner";
import { AutehnticationService } from "src/app/regulars/services/autehntication.service";
import { BudgetService } from "src/app/regulars/services/budget.service";
import { Subject } from "rxjs";
import { DataTableDirective } from "angular-datatables";
import { LogSearchVm } from "src/app/models/view-models/LogSearchVm";
import { ExcelService } from "src/app/regulars/services/excel.service";
import { BudgetReport } from "src/app/models/view-models/BudgetReport";

@Component({
  selector: "app-budget-report",
  templateUrl: "./budget-report.component.html",
  styleUrls: ["./budget-report.component.css"],
})
export class BudgetReportComponent implements OnInit {
  currentUser: UserVm;
  title = "Budget";
  apiUrl = "Budgets";
  createLink = "/" + this.title.toLowerCase() + "/create";

  model: Budget;
  budgetList: any = [];
  searchVm: LogSearchVm;

  //Datatable Setting
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject<any>();
  dtElement: DataTableDirective;
  modelList: any;
  BudgetTypeList: any;
  budgetReportList:BudgetReport[]=[];

  constructor(
    public service: GenericService,
    public _service: BudgetService,
    public excelService: ExcelService,
    public modalService: BsModalService,
    public confirmService: ConfirmService,
    public toastr: ToastrService,
    public spinner: NgxSpinnerService,
    private authenticationService: AutehnticationService
  ) {
    this.model = new Budget();
    this.searchVm = new LogSearchVm();
    this._service.listen().subscribe((response) => {
      this.service.setAll(this.apiUrl);
    });
  }

  ngOnInit(): void {
    this.currentUser = this.authenticationService.currentUserValue;
    this.service.getAll("Budgets").subscribe((response) => {
      this.budgetList = response;
    });

    this.service.getAll("Budgets/GetBudgetType").subscribe((response) => {
      this.BudgetTypeList = response;
    });
  }

  getBySearch() {
    this._service.GetBudgetBySearch(this.searchVm).subscribe((response) => {
      this.budgetList = [];
      this.budgetList = response;

    });
  }

  //Datatable Settings Start
  ngAfterViewInit(): void {
    setTimeout(() => {
      this.dtTrigger.next();
    }, 200);
  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }

  rerender(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      // Destroy the table first
      dtInstance.destroy();
      // Call the dtTrigger to rerender again
      this.dtTrigger.next();
    });
  }

  //Datatable Settings End

  exportAsExcel() {
    for (let item of this.budgetList) {
      let resultVm = new BudgetReport();

      var today = new Date(item.BudgetDate);
      var dd = today.getDate();
      var mm = today.getMonth() + 1;
      var yyyy = today.getFullYear();
      var monthName = [
        "January",
        "February",
        "March",
        "April",
        "May",
        "June",
        "July",
        "August",
        "September",
        "October",
        "November",
        "December",
      ];

      resultVm.Date = item.BudgetDate
        ? dd + " " + monthName[mm - 1] + "," + yyyy
        : "";

      resultVm.Budget_Type = item.BudgetType.Name;
      resultVm.Amount = item.Amount;
      resultVm.Company_Name = item.CompanyName ? item.CompanyName:"-";
      resultVm.Company_Address = item.CompanyAddress ? item.CompanyAddress : "-";
      resultVm.Description = item.Description ? item.Description: "";

      this.budgetReportList.push(resultVm);
    }

    this.excelService.exportAsExcelFile(
      this.budgetReportList,
      "Budget Report"
    );
  }
}
