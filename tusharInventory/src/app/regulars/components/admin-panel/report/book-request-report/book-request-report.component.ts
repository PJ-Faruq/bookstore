import { Component, OnInit } from "@angular/core";

import { ActivatedRoute, Router } from "@angular/router";

import { BsModalService } from "ngx-bootstrap/modal";

import { NgxSpinnerService } from "ngx-spinner";

import { ToastrService } from "ngx-toastr";

import { ListComponent } from "src/app/generics/components/list/list.component";

import { GenericService } from "src/app/generics/services/generic.service";
import { School } from "src/app/models/basic/school";

import { Order } from "src/app/models/operation/order";

import { BookReportResultVm } from "src/app/models/view-models/BookReportResultVm";

import { BookReportVm } from "src/app/models/view-models/BookReportVm";

import { OrderSearchVm } from "src/app/models/view-models/OrderSearchVm";

import { UserVm } from "src/app/models/view-models/userVm.model";

import { AutehnticationService } from "src/app/regulars/services/autehntication.service";

import { ConfirmService } from "src/app/regulars/services/confirm.service";

import { ExcelService } from "src/app/regulars/services/excel.service";

import { OrderService } from "src/app/regulars/services/order.service";

import { OrderDetailModalComponent } from "../../order/order-detail-modal/order-detail-modal.component";

import { OrderFileUploadModalComponent } from "../../order/order-file-upload-modal/order-file-upload-modal.component";

@Component({
  selector: "app-book-request-report",
  templateUrl: "./book-request-report.component.html",
  styleUrls: ["./book-request-report.component.css"],
})
export class BookRequestReportComponent
  extends ListComponent
  implements OnInit {
  title = "Order";
  apiUrl = "Orders";
  searchVm: BookReportVm;
  createLink = "/" + this.title.toLowerCase() + "/create";
  currentUser: UserVm;
  p: number = 1;
  list: BookReportResultVm[] = [];

  bsValue = new Date();

  orderList: Order[]=[];
  schoolList:School[]=[];

  statusList = [
    {
      Name: "Approved",
      Id: 1,
    },

    {
      Name: "Completed",
      Id: 2,
    },

    {
      Name: "Approved & Completed",
      Id: 3,
    },

    {
      Name: "Rejected",
      Id: 4,
    },
    {
      Name: "Pending",
      Id: 5,
    },
  ];

  constructor(
    public service: GenericService,
    public _service: OrderService,
    public excelService: ExcelService,
    public modalService: BsModalService,
    public confirmService: ConfirmService,
    public toastr: ToastrService,
    public spinner: NgxSpinnerService,
    private router: Router,
    private route: ActivatedRoute,
    private authenticationService: AutehnticationService
  ) {
    super(modalService, confirmService, toastr, spinner);
    this.searchVm = new BookReportVm();

    this._service.listen().subscribe((response) => {
      this.getBySearch();
    });
  }

  ngOnInit() {
    this.currentUser = this.authenticationService.currentUserValue;
    if (this.currentUser.Role != "School Employee") {
      this.service.getAll("Schools/GetSchoolName").subscribe((response) => {
        this.schoolList = response;
        console.log(this.schoolList);
      });
    }
    //this.getBySearch();
  }

  getBySearch() {
    if (this.currentUser.Role == "School Employee") {
      this.searchVm.SchoolId = this.currentUser.SchoolId;
    }
    this._service.bookReportSearch(this.searchVm).subscribe((response) => {
      this.orderList = response;
    });
  }

  openDetailModal(id: number) {
    this.modalRef = this.modalService.show(OrderDetailModalComponent, {
      initialState: {
        title: this.title,
        data: {
          id: id,
        },
      },

      class: "modal-lg",
      ignoreBackdropClick: false,
    });
  }

  onClickReceipt(id: number) {
    const url = this.router.createUrlTree(["/report/sales-report", id]);
    window.open(url.toString(), "_blank");
  }

  exportAsExcel() {
    for (let item of this.orderList) {
      let resultVm = new BookReportResultVm();

      resultVm.TrackingNo = item.TrackingNo;
      resultVm.OrderId = item.OrderId;

      var today = new Date(item.OrderDate);
      var dd = today.getDate();
      var mm = today.getMonth() + 1;
      var yyyy = today.getFullYear();
      var monthName = [
        "January",
        "February",
        "March",
        "April",
        "May",
        "June",
        "July",
        "August",
        "September",
        "October",
        "November",
        "December",
      ];

      resultVm.OrderDate = item.OrderDate
        ? dd + " " + monthName[mm - 1] + "," + yyyy
        : "";

      resultVm.SchoolName = item.School.Name;
      resultVm.RequestedBy = item.SchoolEmployee
        ? item.SchoolEmployee.FullName
        : "";
      resultVm.RequestedTo = item.BookstoreEmployee
        ? item.BookstoreEmployee.FullName
        : "";
      resultVm.ApprovedBy = item.BookstoreManager
        ? item.BookstoreManager.FullName
        : "";
      resultVm.Amount = item.Amount;

      if (item.Status == 4) {
        resultVm.Status = "Approved";
      }

      if (item.Status == 7) {
        resultVm.Status = "Completed";
      }

      if (item.Status == 5) {
        resultVm.Status = "Rejected";
      }

      if (
        item.Status == 1 ||
        item.Status == 2 ||
        item.Status == 3 ||
        item.Status == 6
      ) {
        resultVm.Status = "Pending";
      }

      resultVm.ReasonForReject = item.ReasonForReject;
      resultVm.BankSlip = item.File ? "Uploaded" : "No";
      resultVm.Note = item.Note;

      this.list.push(resultVm);
    }

    this.excelService.exportAsExcelFile(this.list, "Book Request Report");
  }
}
