import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DataTableDirective } from 'angular-datatables';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { Subject } from 'rxjs';
import { GenericService } from 'src/app/generics/services/generic.service';
import { OrderSearchVm } from 'src/app/models/view-models/OrderSearchVm';
import { UserVm } from 'src/app/models/view-models/userVm.model';
import { AutehnticationService } from 'src/app/regulars/services/autehntication.service';
import { BankReceiptServiceService } from 'src/app/regulars/services/bank-receipt-service.service';
import { OrderService } from 'src/app/regulars/services/order.service';
import { PurchaseService } from 'src/app/regulars/services/purchase.service';
import { AppSettings } from 'src/app/shared/app-settings';
import { OrderDetailModalComponent } from '../../order/order-detail-modal/order-detail-modal.component';
import { BankReceiptVoucherCreateComponent } from '../bank-receipt-voucher/bank-receipt-voucher-create/bank-receipt-voucher-create.component';

@Component({
  selector: "app-bank-slip",
  templateUrl: "./bank-slip.component.html",
  styleUrls: ["./bank-slip.component.css"],
})
export class BankSlipComponent implements OnInit {
  currentUser: UserVm;
  outOfStock = false;
  modelList: any[];
  modalRef: BsModalRef;

  //Datatable Field
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject<any>();
  dtElement: DataTableDirective;

  constructor(
    public modalService: BsModalService,
    public bankReceiptService: BankReceiptServiceService,
    public service: GenericService,
    public _service: BankReceiptServiceService,
    public spinner: NgxSpinnerService,
    private router: Router,
    private toastr: ToastrService,
    private authenticationService: AutehnticationService
  ) {
    this.bankReceiptService.listen().subscribe((response) => {
      this.GetReceiptAndSlipList();
    });
  }

  ngOnInit(): void {
    this.currentUser = this.authenticationService.currentUserValue;

    this.GetReceiptAndSlipList();
  }

  GetReceiptAndSlipList() {
    this._service.GetReceiptAndSlip().subscribe((response) => {
      this.modelList = response;
    });
  }

  openDetailModal(id: number) {
    this.modalRef = this.modalService.show(OrderDetailModalComponent, {
      initialState: {
        title: "Order Details",
        data: {
          id: id,
        },
      },
      class: "modal-lg",
      ignoreBackdropClick: false,
    });
  }

  onReceiptVoucher(id: number) {
    this.modalRef = this.modalService.show(BankReceiptVoucherCreateComponent, {
      initialState: {
        title: "Order Details",
        data: {
          id: id,
        },
      },
      class: "modal-lg",
      ignoreBackdropClick: false,
    });
  }

  downloadFile(fileName) {
    window.open(AppSettings.ROOT_IMAGE_PATH + fileName, "_blank");
  }

  onClickReceipt(id: number) {
    const url = this.router.createUrlTree(["/report/bank-receipt-voucher", id]);
    window.open(url.toString(), "_blank");
  }

  //#region Datatable Settings
  ngAfterViewInit(): void {
    setTimeout(() => {
      this.dtTrigger.next();
    }, 300);
  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }

  rerender(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      // Destroy the table first
      dtInstance.destroy();
      // Call the dtTrigger to rerender again
      this.dtTrigger.next();
    });
  }

  //#endregion
}
