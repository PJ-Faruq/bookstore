import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { CreateComponent } from 'src/app/generics/components/create/create.component';
import { GenericService } from 'src/app/generics/services/generic.service';
import { Supplier } from 'src/app/models/basic/supplier';
import { CheckNameService } from 'src/app/regulars/services/check-name.service';

@Component({
  selector: "app-supplier-create",
  templateUrl: "./supplier-create.component.html",
  styleUrls: ["./supplier-create.component.css"],
})
export class SupplierCreateComponent extends CreateComponent implements OnInit {
  title = "Supplier";

  constructor(
    public service: GenericService,
    public checkNameService: CheckNameService,

    public router: Router,
    public activatedRoute: ActivatedRoute,
    public spinner: NgxSpinnerService,
    public toastr: ToastrService
  ) {
    super(service, router, activatedRoute, spinner, toastr);
    this.model = new Supplier();
    this.apiUrl = "Suppliers";
  }

  ngOnInit() {
    this.setDataByParams();
  }

  reset() {
    this.editMode = false;
    this.model = new Supplier();
  }

  onSubmit() {
    this.AddItem();
  }

  AddItem() {
    this.service.add(this.model, "Suppliers").subscribe(
      (resp) => {
        if (resp) {
          this.toastr.success("Supplier Successfully Added");
          this.router.navigate(["/supplier/list"]);
        } else {
          this.toastr.error("Supplier Added Failed !");
        }
      },
      (err) => {
        this.toastr.error("Internal Server Error");
      }
    );
  }
}

