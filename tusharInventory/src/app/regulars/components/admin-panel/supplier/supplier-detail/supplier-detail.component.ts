import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { NgxSpinnerService } from 'ngx-spinner';
import { DetailComponent } from 'src/app/generics/components/detail/detail.component';
import { GenericService } from 'src/app/generics/services/generic.service';
import { Supplier } from 'src/app/models/basic/supplier';


@Component({
  selector: 'app-supplier-detail-modal',
  templateUrl: './supplier-datail.component.html',
  styleUrls: ['./supplier-detail.component.css']
})
export class SupplierDetailComponent extends DetailComponent implements OnInit {

  constructor(public modalRef: BsModalRef,
    public service: GenericService,
    public spinner: NgxSpinnerService) {

    super(modalRef, service, spinner);
    this.model = new Supplier();
    this.apiUrl = "Suppliers"

  }
}
