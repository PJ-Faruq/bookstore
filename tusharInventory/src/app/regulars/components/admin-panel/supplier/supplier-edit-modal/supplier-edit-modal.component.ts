import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { BsModalRef } from "ngx-bootstrap/modal";
import { NgxSpinnerService } from "ngx-spinner";
import { ToastrService } from "ngx-toastr";
import { EditComponent } from "src/app/generics/components/edit/edit.component";
import { GenericService } from "src/app/generics/services/generic.service";
import { Supplier } from "src/app/models/basic/supplier";
import { CheckNameService } from "src/app/regulars/services/check-name.service";
import { SupplierService } from "src/app/regulars/services/supplier.service";

@Component({
  selector: "app-supplier-edit-modal",
  templateUrl: "./supplier-edit-modal.component.html",
  styleUrls: ["./supplier-edit-modal.component.css"],
})
export class SupplierEditModalComponent
  extends EditComponent
  implements OnInit {
  editMode: boolean = true;
  existingName: string = null;

  constructor(
    public modalRef: BsModalRef,
    public service: GenericService,
    public checkNameService: CheckNameService,
    public _service: SupplierService,
    public router: Router,
    public toastr: ToastrService,
    public spinner: NgxSpinnerService
  ) {
    super(modalRef, service, router, toastr, spinner);
    this.model = new Supplier();
    this.apiUrl = "Suppliers";
  }

  ngOnInit() {
    this.setDataByParams();

    setTimeout(() => {
      this.existingName = this.model.Name;
    }, 1000);
  }

  onUpdate() {
    this.updateSupplier();
  }

  updateSupplier() {
    this.service.update(this.model.Id, this.model, "Suppliers").subscribe(
      (response) => {
        if (response) {
          this.modalRef.hide();
          this.toastr.success("Supplier Updated Successfully");
          this._service.refreshList("refresh");
        }
      },
      (error) => {
        this.toastr.error("Internal Server Error");
      }
    );
  }
}

