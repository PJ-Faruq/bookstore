import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { BsModalRef } from "ngx-bootstrap/modal";
import { NgxSpinnerService } from "ngx-spinner";
import { ToastrService } from "ngx-toastr";
import { EditComponent } from "src/app/generics/components/edit/edit.component";
import { GenericService } from "src/app/generics/services/generic.service";
import { Subcity } from "src/app/models/basic/subcity";
import { CheckNameService } from "src/app/regulars/services/check-name.service";
import { SubcityService } from "src/app/regulars/services/subcity.service";

@Component({
  selector: "app-subcity-edit-modal",
  templateUrl: "./subcity-edit-modal.component.html",
  styleUrls: ["./subcity-edit-modal.component.css"],
})
export class SubcityEditModalComponent
  extends EditComponent
  implements OnInit {
  editMode: boolean = true;
  existingName: string = null;

  constructor(
    public modalRef: BsModalRef,
    public service: GenericService,
    public checkNameService: CheckNameService,
    public _service: SubcityService,
    public router: Router,
    public toastr: ToastrService,
    public spinner: NgxSpinnerService
  ) {
    super(modalRef, service, router, toastr, spinner);
    this.model = new Subcity();
    this.apiUrl = "Subcities";
  }

  ngOnInit() {
    this.setDataByParams();

    setTimeout(() => {
      this.existingName = this.model.Name;
    }, 1000);
  }

  onUpdate() {
    this.checkNameService
      .checkName("Subcities", this.model)
      .subscribe((response) => {
        if (response != null) {
          if (response["Name"] != this.existingName) {
            this.toastr.error("This Subcity name is Already Exist");
          } else {
            this.updateSubcity();
          }
        }

        if (response == null) {
          this.updateSubcity();
        }
      });
  }

  updateSubcity() {
    this.service.update(this.model.Id, this.model, "Subcities").subscribe(
      (response) => {
        if (response) {
          this.modalRef.hide();
          this.toastr.success("Subcity Updated Successfully");
          this._service.refreshList("refresh");
        }
      },
      (error) => {
        this.toastr.error("Internal Server Error");
      }
    );
  }
}
