import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { NgxSpinnerService } from "ngx-spinner";
import { ToastrService } from "ngx-toastr";
import { CreateComponent } from "src/app/generics/components/create/create.component";
import { GenericService } from "src/app/generics/services/generic.service";
import { Subcity } from "src/app/models/basic/subcity";
import { CheckNameService } from "src/app/regulars/services/check-name.service";

@Component({
  selector: "app-subcity-create",
  templateUrl: "./subcity-create.component.html",
  styleUrls: ["./subcity-create.component.css"],
})
export class SubcityCreateComponent extends CreateComponent implements OnInit {
  title = "Subcity";

  constructor(
    public service: GenericService,
    public checkNameService: CheckNameService,

    public router: Router,
    public activatedRoute: ActivatedRoute,
    public spinner: NgxSpinnerService,
    public toastr: ToastrService
  ) {
    super(service, router, activatedRoute, spinner, toastr);
    this.model = new Subcity();
    this.apiUrl = "Subcities";
  }

  ngOnInit() {
    this.setDataByParams();
  }

  reset() {
    this.editMode = false;
    this.model = new Subcity();
  }

  onSubmit() {
    this.checkNameService.checkName("Subcities", this.model).subscribe(
      (response) => {
        if (response == null) {
          this.AddItem();
        } else {
          this.toastr.error("This Name is Already Exist !");
        }
      },
      (error) => {
        this.toastr.error("Internal Server Error");
      }
    );
  }

  AddItem() {
    this.service.add(this.model, "Subcities").subscribe(
      (resp) => {
        if (resp) {
          this.toastr.success("Subcity Successfully Added");
          this.router.navigate(["/subcity/list"]);
        } else {
          this.toastr.error("Subcity Added Failed !");
        }
      },
      (err) => {
        this.toastr.error("Internal Server Error");
      }
    );
  }
}
