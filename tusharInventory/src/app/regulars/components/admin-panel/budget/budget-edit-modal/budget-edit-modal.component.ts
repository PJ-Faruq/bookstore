import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { BsModalRef } from "ngx-bootstrap/modal";
import { NgxSpinnerService } from "ngx-spinner";
import { ToastrService } from "ngx-toastr";
import { EditComponent } from "src/app/generics/components/edit/edit.component";
import { GenericService } from "src/app/generics/services/generic.service";
import { Budget } from "src/app/models/basic/budget";
import { BudgetService } from "src/app/regulars/services/budget.service";
import { CheckNameService } from "src/app/regulars/services/check-name.service";

@Component({
  selector: "app-budget-edit-modal",
  templateUrl: "./budget-edit-modal.component.html",
  styleUrls: ["./budget-edit-modal.component.css"],
})
export class BudgetEditModalComponent extends EditComponent implements OnInit {
  editMode: boolean = true;
  existingName: string = null;
  budgetList: any;
  isNonGovt: boolean = false;

  constructor(
    public modalRef: BsModalRef,
    public service: GenericService,
    public checkNameService: CheckNameService,
    public _service: BudgetService,
    public router: Router,
    public toastr: ToastrService,
    public spinner: NgxSpinnerService
  ) {
    super(modalRef, service, router, toastr, spinner);
    this.model = new Budget();
    this.apiUrl = "Budgets";
  }

  ngOnInit() {

        this.service.getAll("Budgets/GetBudgetType").subscribe((response) => {
          this.budgetList = response;
        });

        this.service.getById(this.data.id,"Budgets").subscribe(
          response=>{
            this.model=response;
            this.model.BudgetDate = new Date(this.model.BudgetDate);
            if(this.model.BudgetTypeId==2){
              this.isNonGovt=true;
            }
          }
        ) 
  }

  onUpdate() {
    this.updateBudget();
  }

  updateBudget() {
    this.service.update(this.model.Id, this.model, "Budgets").subscribe(
      (response) => {
        if (response) {
          this.modalRef.hide();
          this.toastr.success("Budget Updated Successfully");
          this._service.refreshList("refresh");
        }
      },
      (error) => {
        this.toastr.error("Internal Server Error");
      }
    );
  }

  onBudgetTypeChange(id: number) {
    if (id == 2) {
      this.isNonGovt = true;
    } else {
      this.isNonGovt = false;
    }
  }
}
