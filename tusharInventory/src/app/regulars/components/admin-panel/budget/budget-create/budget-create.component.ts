import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { NgxSpinnerService } from "ngx-spinner";
import { ToastrService } from "ngx-toastr";
import { CreateComponent } from "src/app/generics/components/create/create.component";
import { GenericService } from "src/app/generics/services/generic.service";
import { Budget } from "src/app/models/basic/budget";
import { CheckNameService } from "src/app/regulars/services/check-name.service";
import { BsDatepickerModule } from "ngx-bootstrap/datepicker";


@Component({
  selector: "app-budget-create",
  templateUrl: "./budget-create.component.html",
  styleUrls: ["./budget-create.component.css"],
})
export class BudgetCreateComponent extends CreateComponent implements OnInit {
  title = "Budget";
  budgetList: any;
  isNonGovt: boolean = false;

  constructor(
    public service: GenericService,
    public checkNameService: CheckNameService,

    public router: Router,
    public activatedRoute: ActivatedRoute,
    public spinner: NgxSpinnerService,
    public toastr: ToastrService
  ) {
    super(service, router, activatedRoute, spinner, toastr);
    this.model = new Budget();
    this.apiUrl = "Budgets";
  }

  ngOnInit() {
    this.spinner.show();
    this.setDataByParams();
    this.service.getAll("Budgets/GetBudgetType").subscribe((response) => {
      this.budgetList = response;
    });
  }

  onBudgetTypeChange(id:number){
    if(id==2){
      this.isNonGovt=true;
    }
    else{
      this.isNonGovt=false;
    }
  }

  reset() {
    this.editMode = false;
    this.model = new Budget();
  }

  onSubmit() {
    this.AddItem();
  }

  AddItem() {
    this.service.add(this.model, "Budgets").subscribe(
      (resp) => {
        if (resp) {
          this.toastr.success("Budget Successfully Added");
          this.router.navigate(["/budget/list"]);
        } else {
          this.toastr.error("Budget Added Failed !");
        }
      },
      (err) => {
        this.toastr.error("Internal Server Error");
      }
    );
  }
}
