import { Component, OnInit } from '@angular/core';
import { DataTableDirective } from 'angular-datatables';
import { BsModalService } from 'ngx-bootstrap/modal';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { Subject } from 'rxjs';
import { ListComponent } from 'src/app/generics/components/list/list.component';
import { GenericService } from 'src/app/generics/services/generic.service';
import { ConfirmService } from 'src/app/regulars/services/confirm.service';
import { BookDetailModalComponent } from '../book-detail-modal/book-detail-modal.component';
import { BookEditModalComponent } from '../book-edit-modal/book-edit-modal.component';

@Component({
  selector: "app-book-list",
  templateUrl: "./book-list.component.html",
  styleUrls: ["./book-list.component.css"],
})
export class BookListComponent extends ListComponent implements OnInit {
  title = "Book";
  apiUrl = "Books";
  createLink = "/" + this.title.toLowerCase() + "/create";

  //Datatable Field
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject<any>();
  dtElement: DataTableDirective;

  constructor(
    public service: GenericService,
    public modalService: BsModalService,
    public confirmService: ConfirmService,
    public toastr: ToastrService,
    public spinner: NgxSpinnerService
  ) {
    super(modalService, confirmService, toastr, spinner);
    this.confirmService.listen().subscribe((response) => {
      this.service.setAll(this.apiUrl);
    });
  }

  ngOnInit(): void {
    this.spinner.show();
    this.listChangedSubscribe = this.service.listChanged.subscribe(
      (data) => {
        this.modelList = data;
        this.spinner.hide();
      },
      (error) => {
        this.modelList = [];
        this.spinner.hide();
      }
    );
    this.confirmSubscription = this.confirmService.deleted.subscribe((data) => {
      if (data) {
        this.service.setAll(this.apiUrl);
        this.toastr.warning("Deleted Successfully", "Success");
      } else {
        this.toastr.warning("Can't Delete this item", "Warning");
      }
    });
    this.service.setAll(this.apiUrl);
  }

  openDetailModal(id: number) {
    this.modalRef = this.modalService.show(BookDetailModalComponent, {
      initialState: {
        title: this.title,
        data: {
          id: id,
        },
      },
      class: "modal-lg",
    });
  }

  openEditModal(id: number) {
    this.modalRef = this.modalService.show(BookEditModalComponent, {
      initialState: {
        title: this.title,
        data: {
          id: id,
        },
      },
      class: "modal-lg",
    });
  }

  //#region Datatable Settings
  ngAfterViewInit(): void {
    setTimeout(() => {
      this.dtTrigger.next();
    }, 300);
  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }

  rerender(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      // Destroy the table first
      dtInstance.destroy();
      // Call the dtTrigger to rerender again
      this.dtTrigger.next();
    });
  }

  //#endregion

}
