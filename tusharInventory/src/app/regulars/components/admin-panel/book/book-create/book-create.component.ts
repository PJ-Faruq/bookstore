import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { CreateComponent } from 'src/app/generics/components/create/create.component';
import { GenericService } from 'src/app/generics/services/generic.service';
import { Book } from 'src/app/models/basic/book';
import { GradeLavel } from 'src/app/models/basic/grade-lavel';
import { Language } from 'src/app/models/basic/language';
import { SubjectModel } from 'src/app/models/basic/subject';
import { CheckNameService } from 'src/app/regulars/services/check-name.service';

@Component({
  selector: "app-book-create",
  templateUrl: "./book-create.component.html",
  styleUrls: ["./book-create.component.css"],
})
export class BookCreateComponent extends CreateComponent implements OnInit {
  title = "Book";

  subjectList: SubjectModel[] = [];
  languageList: Language[] = [];
  gradeLavelList: GradeLavel[] = [];

  constructor(
    public service: GenericService,
    public router: Router,
    public activatedRoute: ActivatedRoute,
    public spinner: NgxSpinnerService,
    public toastr: ToastrService,
    public checkNameService:CheckNameService,
  ) {
    super(service, router, activatedRoute, spinner, toastr);
    this.model = new Book();
    this.apiUrl = "Books";
  }

  ngOnInit() {
    this.service.getAll("Subjects").subscribe(
      (data) => {
        this.subjectList = data;
      },
      (err) => {
        this.subjectList = [];
      }
    );

    this.service.getAll("Languages").subscribe(
      (data) => {
        this.languageList = data;
      },
      (err) => {
        this.languageList = [];
      }
    );

    this.service.getAll("GradeLavels").subscribe(
      (data) => {
        this.gradeLavelList = data;
      },
      (err) => {
        this.gradeLavelList = [];
      }
    );

    this.setDataByParams();
  }

  onAdd() {
    this.checkNameService.checkName("Books", this.model).subscribe(
      (response) => {
        if (response == null) {
          this.AddItem();
        } else {
          this.toastr.error("This Book is Already Exist !");
        }
      },
      (error) => {
        this.toastr.error("Internal Server Error");
      }
    );
  }

  AddItem() {
    this.service.add(this.model, "Books").subscribe(
      (resp) => {
        if (resp) {
          this.toastr.success("Book Successfully Added");
          this.router.navigate(["/book/list"]);
        } else {
          this.toastr.error("Book Added Failed !");
        }
      },
      (err) => {
        this.toastr.error("Internal Server Error");
      }
    );
  }

  reset() {
    this.editMode = false;
    this.model = new Book();
  }
}
