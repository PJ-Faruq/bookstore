import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { EditComponent } from 'src/app/generics/components/edit/edit.component';
import { GenericService } from 'src/app/generics/services/generic.service';
import { Book } from 'src/app/models/basic/book';
import { GradeLavel } from 'src/app/models/basic/grade-lavel';
import { Language } from 'src/app/models/basic/language';
import { SubjectModel } from 'src/app/models/basic/subject';

@Component({
  selector: 'app-book-edit-modal',
  templateUrl: './book-edit-modal.component.html',
  styleUrls: ['./book-edit-modal.component.css']
})
export class BookEditModalComponent extends EditComponent implements OnInit {

  editMode: boolean = true;
  subjectList: SubjectModel[] = [];
  languageList: Language[] = [];
  gradeLavelList: GradeLavel[] = [];

  constructor(
    public modalRef: BsModalRef,
    public service: GenericService,
    public router: Router,
    public toastr: ToastrService,
    public spinner: NgxSpinnerService) {

    super(modalRef, service, router, toastr, spinner);
    this.model = new Book();
    this.apiUrl = "Books";



  }

  ngOnInit() {
    this.service.getAll("Subjects").subscribe((data) => {
      this.subjectList = data;
    }, (err) => {
      this.subjectList = [];
    })

    this.service.getAll("Languages").subscribe((data) => {
      this.languageList = data;
    }, (err) => {
      this.languageList = [];
    })

    this.service.getAll("GradeLavels").subscribe((data) => {
      this.gradeLavelList = data;
    }, (err) => {
      this.gradeLavelList = [];
    })

    this.setDataByParams();
  }
}
