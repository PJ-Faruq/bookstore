import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { NgxSpinnerService } from 'ngx-spinner';
import { DetailComponent } from 'src/app/generics/components/detail/detail.component';
import { GenericService } from 'src/app/generics/services/generic.service';
import { Book } from 'src/app/models/basic/book';

@Component({
  selector: 'app-book-detail-modal',
  templateUrl: './book-detail-modal.component.html',
  styleUrls: ['./book-detail-modal.component.css']
})
export class BookDetailModalComponent extends DetailComponent implements OnInit {

  constructor(public modalRef: BsModalRef,
    public service: GenericService,
    public spinner: NgxSpinnerService) {

    super(modalRef, service, spinner);
    this.model = new Book();
    this.apiUrl = "Books"

  }
}
