import { Component, OnInit } from "@angular/core";
import { BsModalService } from "ngx-bootstrap/modal";
import { Woreda } from "src/app/models/basic/woreda";
import { ListComponent } from "src/app/generics/components/list/list.component";
import { GenericService } from "src/app/generics/services/generic.service";
import { UserVm } from "src/app/models/view-models/userVm.model";
import { ConfirmService } from "src/app/regulars/services/confirm.service";
import { ToastrService } from "ngx-toastr";
import { NgxSpinnerService } from "ngx-spinner";
import { AutehnticationService } from "src/app/regulars/services/autehntication.service";
import { WoredaEditModalComponent } from "../woreda-edit-modal/woreda-edit-modal.component";
import { WoredaService } from 'src/app/regulars/services/woreda.service';
import { Subject } from "rxjs";
import { DataTableDirective } from "angular-datatables";


@Component({
  selector: "app-woreda-list",
  templateUrl: "./woreda-list.component.html",
  styleUrls: ["./woreda-list.component.css"],
})
export class WoredaListComponent extends ListComponent implements OnInit {
  currentUser: UserVm;
  title = "Woreda";
  apiUrl = "Woredas";
  createLink = "/" + this.title.toLowerCase() + "/create";

  model: Woreda;
  woredaList: any = [];

  //Datatable Field
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject<any>();
  dtElement: DataTableDirective;

  constructor(
    public service: GenericService,
    public _service: WoredaService,
    public modalService: BsModalService,
    public confirmService: ConfirmService,
    public toastr: ToastrService,
    public spinner: NgxSpinnerService,
    private authenticationService: AutehnticationService
  ) {
    super(modalService, confirmService, toastr, spinner);
    this.model = new Woreda();
    this._service.listen().subscribe((response) => {
      this.service.setAll(this.apiUrl);
    });
        this.confirmService.listen().subscribe((response) => {
          this.service.setAll(this.apiUrl);
        });
  }

  ngOnInit(): void {
    this.currentUser = this.authenticationService.currentUserValue;
    this.listChangedSubscribe = this.service.listChanged.subscribe(
      (data) => {
        this.modelList = data.reverse();
      },
      (error) => {
        this.modelList = [];
      }
    );
    this.confirmSubscription = this.confirmService.deleted.subscribe((data) => {
      if (data) {
        this.service.setAll(this.apiUrl);
        this.toastr.info("Deleted Successfully", "Success");
      } else {
        this.toastr.warning("Can't Delete this item", "Warning");
      }
    });
    this.service.setAll(this.apiUrl);
  }

  openEditModal(id: number) {
    this.modalRef = this.modalService.show(WoredaEditModalComponent, {
      initialState: {
        title: this.title,
        data: {
          id: id,
        },
      },
      class: "modal-lg",
    });
  }

  //#region Datatable Settings
  ngAfterViewInit(): void {
    setTimeout(() => {
      this.dtTrigger.next();
    }, 300);
  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }

  rerender(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      // Destroy the table first
      dtInstance.destroy();
      // Call the dtTrigger to rerender again
      this.dtTrigger.next();
    });
  }

  //#endregion
}



