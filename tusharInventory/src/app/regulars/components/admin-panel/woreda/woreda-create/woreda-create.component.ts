import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { NgxSpinnerService } from "ngx-spinner";
import { ToastrService } from "ngx-toastr";
import { CreateComponent } from "src/app/generics/components/create/create.component";
import { GenericService } from "src/app/generics/services/generic.service";
import { Woreda } from "src/app/models/basic/woreda";
import { CheckNameService } from "src/app/regulars/services/check-name.service";

@Component({
  selector: "app-woreda-create",
  templateUrl: "./woreda-create.component.html",
  styleUrls: ["./woreda-create.component.css"],
})
export class WoredaCreateComponent extends CreateComponent implements OnInit {
  title = "Woreda";

  constructor(
    public service: GenericService,
    public checkNameService: CheckNameService,

    public router: Router,
    public activatedRoute: ActivatedRoute,
    public spinner: NgxSpinnerService,
    public toastr: ToastrService
  ) {
    super(service, router, activatedRoute, spinner, toastr);
    this.model = new Woreda();
    this.apiUrl = "Woredas";
  }

  ngOnInit() {
    this.setDataByParams();
  }

  reset() {
    this.editMode = false;
    this.model = new Woreda();
  }

  onSubmit() {
    this.checkNameService.checkName("Woredas", this.model).subscribe(
      (response) => {
        if (response == null) {
          this.AddItem();
        } else {
          this.toastr.error("This Name is Already Exist !");
        }
      },
      (error) => {
        this.toastr.error("Internal Server Error");
      }
    );
  }

  AddItem() {
    this.service.add(this.model, "Woredas").subscribe(
      (resp) => {
        if (resp) {
          this.toastr.success("Woreda Successfully Added");
          this.router.navigate(["/woreda/list"]);
        } else {
          this.toastr.error("Woreda Added Failed !");
        }
      },
      (err) => {
        this.toastr.error("Internal Server Error");
      }
    );
  }
}
