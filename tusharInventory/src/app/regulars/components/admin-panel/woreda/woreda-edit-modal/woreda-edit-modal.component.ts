import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { BsModalRef } from "ngx-bootstrap/modal";
import { NgxSpinnerService } from "ngx-spinner";
import { ToastrService } from "ngx-toastr";
import { EditComponent } from "src/app/generics/components/edit/edit.component";
import { GenericService } from "src/app/generics/services/generic.service";
import { Woreda } from "src/app/models/basic/woreda";
import { CheckNameService } from "src/app/regulars/services/check-name.service";
import { WoredaService } from "src/app/regulars/services/woreda.service";

@Component({
  selector: "app-woreda-edit-modal",
  templateUrl: "./woreda-edit-modal.component.html",
  styleUrls: ["./woreda-edit-modal.component.css"],
})
export class WoredaEditModalComponent
  extends EditComponent
  implements OnInit {
  editMode: boolean = true;
  existingName: string = null;

  constructor(
    public modalRef: BsModalRef,
    public service: GenericService,
    public checkNameService: CheckNameService,
    public _service: WoredaService,
    public router: Router,
    public toastr: ToastrService,
    public spinner: NgxSpinnerService
  ) {
    super(modalRef, service, router, toastr, spinner);
    this.model = new Woreda();
    this.apiUrl = "Woredas";
  }

  ngOnInit() {
    this.setDataByParams();

    setTimeout(() => {
      this.existingName = this.model.Name;
    }, 1000);
  }

  onUpdate() {
    this.checkNameService
      .checkName("Woredas", this.model)
      .subscribe((response) => {
        if (response != null) {
          if (response["Name"] != this.existingName) {
            this.toastr.error("This Woreda name is Already Exist");
          } else {
            this.updateWoreda();
          }
        }

        if (response == null) {
          this.updateWoreda();
        }
      });
  }

  updateWoreda() {
    this.service.update(this.model.Id, this.model, "Woredas").subscribe(
      (response) => {
        if (response) {
          this.modalRef.hide();
          this.toastr.success("Woreda Updated Successfully");
          this._service.refreshList("refresh");
        }
      },
      (error) => {
        this.toastr.error("Internal Server Error");
      }
    );
  }
}

