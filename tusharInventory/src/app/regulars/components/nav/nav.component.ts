import { Component, OnInit } from '@angular/core';
import { AppSettings } from '../../../shared/app-settings';
import { Router } from '@angular/router';
import { UserVm } from '../../../models/view-models/userVm.model';
import { AutehnticationService } from '../../services/autehntication.service';
import {TranslateService } from '@ngx-translate/core';
@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {

  app: AppSettings = new AppSettings();

  constructor(private router: Router,
    public translate: TranslateService,
    private authenticationService: AutehnticationService) {
      translate.addLangs(['en','am']);
      translate.setDefaultLang('en');
      const browserLang=translate.getBrowserLang();
      translate.use(browserLang.match(/en|fr/)?browserLang:'en');
  }

  currentUser: UserVm;
  ngOnInit() {
    this.currentUser = this.authenticationService.currentUserValue;
  }

  logOut() {
    this.authenticationService.logout();
    this.router.navigate(['/user-auth/login']);
  }
}
