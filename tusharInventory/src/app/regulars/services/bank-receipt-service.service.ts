import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Observable, Subject } from "rxjs";
import { AppSettings } from 'src/app/shared/app-settings';
@Injectable({
  providedIn: "root",
})
export class BankReceiptServiceService {
  constructor(private httpClient: HttpClient) {}

  //For Refreshing List start

  private _listener = new Subject<any>();
  listen(): Observable<any> {
    return this._listener.asObservable();
  }

  refreshList(filterBy: string) {
    this._listener.next(filterBy);
  }

  //For Refreshing List End

  createBankReceipt(model: any): Observable<any[]> {
    return this.httpClient.post<any>(
      AppSettings.API_ROOT + "BankReceiptVouchers/Create",
      model
    );
  }

  GetReceiptAndSlip(): Observable<any[]> {
    return this.httpClient.get<any[]>(
      AppSettings.API_ROOT + "BankReceiptVouchers/GetReceiptAndSlip"
    );
  } 
}
