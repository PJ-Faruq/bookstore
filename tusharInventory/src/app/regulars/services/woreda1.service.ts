import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class Woreda1Service {

  constructor() { }

    //For Refreshing List start

  private _listener = new Subject<any>();
  listen(): Observable<any> {
    return this._listener.asObservable();
  }

  refreshList(filterBy: string) {
    this._listener.next(filterBy);
  }

  //For Refreshing List End
}


