import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { PurchaseBook } from 'src/app/models/operation/purchase-book';
import { AppSettings } from 'src/app/shared/app-settings';

@Injectable({
  providedIn: "root",
})
export class PurchaseBookService {
  constructor(private httpClient: HttpClient) {}

  GetBudgetType(): Observable<any[]> {
    return this.httpClient.get<any[]>(
      AppSettings.API_ROOT + "Budgets/GetBudgetType"
    );
  }

  getPurchasePrice(model: any) {
    return this.httpClient.post(
      AppSettings.API_ROOT + "PurchaseBooks/GetBookPurchasePrice",
      model
    );
  }

  getPurchaseBookBySearch(model: any):Observable<PurchaseBook[]> {
    return this.httpClient.post<PurchaseBook[]>(
      AppSettings.API_ROOT + "PurchaseBooks/GetBySearch",
      model
    );
  }

  getBudget(id: number) {
    return this.httpClient.get(
      AppSettings.API_ROOT + "PurchaseBooks/GetBudget/" + id
    );
  }
}
