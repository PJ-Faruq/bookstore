import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AppSettings } from 'src/app/shared/app-settings';

@Injectable({
  providedIn: "root",
})
export class DashboardService {
  constructor(private httpClient: HttpClient) {}

  getAdminDashboardData() {
    // let params = new HttpParams();
    // params.set("status",status);

    // return this.httpClient.get(
    //   AppSettings.API_ROOT + "Orders/GetAdminDashboardData?status=" + status
    // );

    return this.httpClient.get(
      AppSettings.API_ROOT + "Dashboards/GetAdminDashboardData"
    );
  }

  getBookstoreEmployeeDashboardData(id: number) {
    return this.httpClient.get(
      AppSettings.API_ROOT +
        "Dashboards/GetBookstoreEmployeeDashboardData/" +
        id
    );
  }

  getSchoolDashboardData(schoolId: number) {
    return this.httpClient.get(
      AppSettings.API_ROOT + "Dashboards/GetSchoolDashboardData/" + schoolId
    );
  }

  getManagerDashboardData(id: number) {
    return this.httpClient.get(
      AppSettings.API_ROOT + "Dashboards/GetManagerDashboardData/" + id
    );
  }

  getSubcityDashboardData(id: number) {
    return this.httpClient.get(
      AppSettings.API_ROOT + "Dashboards/GetSubcityDashboardData/" + id
    );
  }

  getPurchaseDashboardData(id: number) {
    return this.httpClient.get(
      AppSettings.API_ROOT + "Dashboards/GetPurchaseDashboardData/" + id
    );
  }
}
