import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable, Subject } from "rxjs";
import { SubcityOrder } from 'src/app/models/operation/SubcityOrder';
import { StockSearchVm } from 'src/app/models/view-models/stockSearchVm';
import { AppSettings } from "src/app/shared/app-settings";

@Injectable({
  providedIn: "root",
})
export class SubcityStoreService {
  constructor(private httpClient: HttpClient) {}

  //For Refreshing List start

  private _listener = new Subject<any>();
  listen(): Observable<any> {
    return this._listener.asObservable();
  }

  refreshList(filterBy: string) {
    this._listener.next(filterBy);
  }

  //For Refreshing List End

  onHoStockSearch(model: any) {
    return this.httpClient.post(
      AppSettings.API_ROOT + "HoStocks/Search",
      model
    );
  }

  getAllBySearch(model: any): Observable<StockSearchVm[]> {
    return this.httpClient.post<StockSearchVm[]>(
      AppSettings.API_ROOT + "SubcityStocks/GetAllBySearch",
      model
    );
  }

  getOrderForManager(model: any): Observable<SubcityOrder> {
    return this.httpClient.post<SubcityOrder>(
      AppSettings.API_ROOT + "SubcityStocks/GetOrderForManager",
      model
    );
  }

  getAllByUserId(id: number): Observable<StockSearchVm[]> {
    return this.httpClient.get<StockSearchVm[]>(
      AppSettings.API_ROOT + "SubcityStocks/GetAllByUserId/" + id
    );
  }

  getManagerList() {
    return this.httpClient.get(
      AppSettings.API_ROOT + "SubcityStocks/GetBookstoreManager"
    );
  }

  getSubcityManagerListBySubcityId(id:number) {
    return this.httpClient.get(
      AppSettings.API_ROOT + "SubcityStocks/GetSubcityManagerListBySubcityId/"+id
    );
  }

  getSubcityOrder(model: any): Observable<SubcityOrder[]> {
    return this.httpClient.post<SubcityOrder[]>(
      AppSettings.API_ROOT + "SubcityStocks/GetSubcityOrder/",model
    );
  }

  getOrderById(id: number): Observable<SubcityOrder> {
    return this.httpClient.get<SubcityOrder>(
      AppSettings.API_ROOT + "SubcityStocks/GetOrderById/" + id
    );
  }

  getBookstoreEmployee(): Observable<any[]> {
    return this.httpClient.get<any[]>(
      AppSettings.API_ROOT + "Users/GetBookstoreEmployee"
    );
  }

  createRequest(model: any) {
    return this.httpClient.post(
      AppSettings.API_ROOT + "SubcityStocks/CreateRequest/",
      model
    );
  }

  updateRequest(model: any) {
    return this.httpClient.post(
      AppSettings.API_ROOT + "SubcityStocks/UpdateRequest/",
      model
    );
  }
}
