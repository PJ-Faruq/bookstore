import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Subject, Observable } from 'rxjs';
import { AppConfigService } from 'src/app/shared/AppConfigService';


@Injectable({
  providedIn: "root",
})
export class ConfirmService {
  deleted = new Subject<boolean>();
  constructor(private http: HttpClient, private appConfig: AppConfigService) {}

  //For Refreshing List start

  private _listener = new Subject<any>();
  listen(): Observable<any> {
    return this._listener.asObservable();
  }

  refreshList(filterBy: string) {
    this._listener.next(filterBy);
  }

  //For Refreshing List End

  delete(id: number, model: any, url: string): Observable<any> {
    const options = {
      headers: new HttpHeaders({
        "Content-Type": "application/json",
      }),
      body: model,
    };
    return this.http.delete(
      this.appConfig.apiBaseUrl + url + "/" + id,
      options
    );
  }
}
