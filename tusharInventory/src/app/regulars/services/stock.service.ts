import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AppSettings } from 'src/app/shared/app-settings';

@Injectable({
  providedIn: "root",
})
export class StockService {
  constructor(private httpClient: HttpClient) {}

  onHoStockSearch(model: any) {
    return this.httpClient.post(
      AppSettings.API_ROOT + "HoStocks/Search",
      model
    );
  }

  onSchoolSearch(model: any) {
    return this.httpClient.post(
      AppSettings.API_ROOT + "SchoolStocks/Search",
      model
    );
  }

  GetBookStock(model: any) {
    return this.httpClient.post(
      AppSettings.API_ROOT + "HoStocks/GetBookStock",
      model
    );
  }
}
