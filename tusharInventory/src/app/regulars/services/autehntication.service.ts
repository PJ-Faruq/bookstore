import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { UserVm } from 'src/app/models/view-models/userVm.model';
import { AppConfigService } from 'src/app/shared/AppConfigService';
import { AppSettings } from 'src/app/shared/app-settings';

@Injectable({
  providedIn: 'root'
})
export class AutehnticationService {
  private currentUserSubject: BehaviorSubject<UserVm>;
  public currentUser: Observable<UserVm>;

  constructor(private http: HttpClient) {
    this.currentUserSubject = new BehaviorSubject<UserVm>(JSON.parse(localStorage.getItem('currentUser')));
    this.currentUser = this.currentUserSubject.asObservable();
  }

  public get currentUserValue(): UserVm {
    return this.currentUserSubject.value;
  }

  login(Email: string, Password: string) {
    return this.http.post<UserVm>(AppSettings.API_ROOT + 'Users/Authenticate', { Email, Password })
      .pipe(map(user => {
        // login successful if there's a jwt token in the response
        if (user && user.Token) {
          // store user details and jwt token in local storage to keep user logged in between page refreshes
          localStorage.setItem('currentUser', JSON.stringify(user));
          this.currentUserSubject.next(user);
        }
        return user;
      }));
  }

  logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('currentUser');
    this.currentUserSubject.next(null);
  }
}
