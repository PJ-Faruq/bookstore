import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable, Subject } from 'rxjs';
import { AppSettings } from "src/app/shared/app-settings";

@Injectable({
  providedIn: "root",
})
export class OrderService {
  constructor(private httpClient: HttpClient) {}

  //For Refreshing List start

  private _listener = new Subject<any>();
  listen(): Observable<any> {
    return this._listener.asObservable();
  }

  refreshList(filterBy: string) {
    this._listener.next(filterBy);
  }

  //For Refreshing List End

  updateStatus(id: number, model: any) {
    return this.httpClient.put(
      AppSettings.API_ROOT + "Orders/UpdateStatus/" + id,
      model
    );
  }

  updateAllocatedStock(model: any) {
    return this.httpClient.put(
      AppSettings.API_ROOT + "Orders/UpdateAllocatedStock",
      model
    );
  }

  updateMainStock(model: any) {
    return this.httpClient.put(
      AppSettings.API_ROOT + "Orders/UpdateMainStock",
      model
    );
  }

  updateSubcityStock(model: any) {
    return this.httpClient.put(
      AppSettings.API_ROOT + "Orders/UpdateSubcityStock",
      model
    );
  }

  updateFile(model: any) {
    return this.httpClient.put(
      AppSettings.API_ROOT + "Orders/UpdateFile",
      model
    );
  }

  getPrice(model: any) {
    return this.httpClient.post(
      AppSettings.API_ROOT + "Orders/GetBookPrice",
      model
    );
  }

  getBookstoreEmployee(): Observable<any[]> {
    return this.httpClient.get<any[]>(
      AppSettings.API_ROOT + "Users/GetBookstoreEmployee"
    );
  }

  search(model: any): Observable<any[]> {
    return this.httpClient.post<any[]>(
      AppSettings.API_ROOT + "Orders/Search",
      model
    );
  }

  bookReportSearch(model: any): Observable<any[]> {
    return this.httpClient.post<any[]>(
      AppSettings.API_ROOT + "Orders/BookReportSearch",
      model
    );
  }

  getOrderCount(): Observable<number> {
    return this.httpClient.get<number>(
      AppSettings.API_ROOT + "Orders/GetOrderCount"
    );
  }

  checkSubcityStock(model: any): Observable<any[]> {
    return this.httpClient.post<any[]>(
      AppSettings.API_ROOT + "Orders/CheckSubcityStock",
      model
    );
  }
}
