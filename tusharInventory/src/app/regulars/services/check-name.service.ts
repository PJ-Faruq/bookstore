import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AppSettings } from 'src/app/shared/app-settings';

@Injectable({
  providedIn: "root",
})
export class CheckNameService {
  constructor(private httpClient: HttpClient) {}

  checkName(url:string,model: any): Observable<any[]> {
    return this.httpClient.post<any>(
      AppSettings.API_ROOT + url+ "/CheckName",
      model
    );
  }
}

