import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { AppSettings } from 'src/app/shared/app-settings';

@Injectable({
  providedIn: "root",
})
export class UserService {
  constructor(private httpClient: HttpClient) {}

  //For Refreshing List start

  private _listener = new Subject<any>();
  listen(): Observable<any> {
    return this._listener.asObservable();
  }

  refreshList(filterBy: string) {
    this._listener.next(filterBy);
  }

  //For Refreshing List End

  checkEmail(email: string) {
    return this.httpClient.get(
      AppSettings.API_ROOT + "Users/CheckEmail?email=" + email
    );
  }

  update(model: any) {
    return this.httpClient.put(AppSettings.API_ROOT + "Users", model);
  }

  getUserForAdmin(id: number): Observable<any> {
    return this.httpClient.get<any>(
      AppSettings.API_ROOT + "Users/GetUserForAdmin/" + id
    );
  }

  getUserActivityBySearch(model: any) {
    return this.httpClient.post<any[]>(
      AppSettings.API_ROOT + "Users/GetUserActivityBySearch",
      model
    );
  }
}
