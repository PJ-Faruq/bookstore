import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AppSettings } from 'src/app/shared/app-settings';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: "root",
})
export class PurchaseService {
  constructor(private httpClient: HttpClient) {}

  //For Refreshing List start

  private _listener = new Subject<any>();
  listen(): Observable<any> {
    return this._listener.asObservable();
  }

  refreshList(filterBy: string) {
    this._listener.next(filterBy);
  }

  //For Refreshing List End

  getManagerList() {
    return this.httpClient.get(
      AppSettings.API_ROOT + "Purchases/GetBookstoreManager"
    );
  }

  getPurchaseEmployeeList() {
    return this.httpClient.get(
      AppSettings.API_ROOT + "Purchases/GetPurchaseEmployee"
    );
  }

  changeStatus(model: any) {
    return this.httpClient.put(
      AppSettings.API_ROOT + "Purchases/ChangeStatus",
      model
    );
  }

  AddBook(model: any) {
    return this.httpClient.post(
      AppSettings.API_ROOT + "Purchases/AddBook",
      model
    );
  }

  getPurchaseBookByPurchaseId(purchaseId: number) {
    return this.httpClient.get(
      AppSettings.API_ROOT + "PurchaseBooks/GetPurchaseBookByPurchaseId/"+purchaseId,
    );
  }
}
