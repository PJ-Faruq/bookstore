import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { AppSettings } from 'src/app/shared/app-settings';

@Injectable({
  providedIn: "root",
})
export class SchoolService {
  constructor(private httpClient: HttpClient) {}

  //For Refreshing List start

  private _listener = new Subject<any>();
  listen(): Observable<any> {
    return this._listener.asObservable();
  }

  refreshList(filterBy: string) {
    this._listener.next(filterBy);
  }

  //For Refreshing List End

  bulkImport(model: any) {
    return this.httpClient.post(
      AppSettings.API_ROOT + "Schools/BulkImport",
      model
    );
  }

  search(model: any): Observable<any[]> {
    return this.httpClient.post<any>(
      AppSettings.API_ROOT + "Schools/Search",
      model
    );
  }

  checkSchoolName(model: any): Observable<any[]> {
    return this.httpClient.post<any>(
      AppSettings.API_ROOT + "Schools/CheckSchoolName",
      model
    );
  }
}
