export class AppSettings {
    public static API_ROOT = 'http://localhost:60703/api/';
    public static APP_NAME;
    public static ROOT_IMAGE_PATH='http://localhost:60703/Resources/Files/';

    constructor() {
        AppSettings.APP_NAME = 'Service Provider Management'
    }

    get APP_NAME() {
        return AppSettings.APP_NAME;
    }
}
