import { Component, OnInit } from "@angular/core";

import { BsModalRef } from "ngx-bootstrap/modal";
import { NgxSpinnerService } from "ngx-spinner";
import { ToastrService } from "ngx-toastr";
import { ConfirmService } from '../regulars/services/confirm.service';

@Component({
  selector: "app-confirm-modal",
  templateUrl: "./confirm-modal.component.html",
  styleUrls: ["./confirm-modal.component.css"],
})
export class ConfirmModalComponent implements OnInit {
  title;
  data: any;

  constructor(
    public modalRef: BsModalRef,
    private confirmService: ConfirmService,
    private spinner: NgxSpinnerService,
    public toastr: ToastrService
  ) {}

  ngOnInit() {}

  onDelete() {
    this.spinner.show();
    this.confirmService
      .delete(this.data.id, this.data.model, this.data.url)
      .subscribe(
        (data) => {
          //this.confirmService.deleted.next(true);
          this.toastr.success("Successfully Deleted");
          this.modalRef.hide();
          this.spinner.hide();
          this.confirmService.refreshList("Refresh");
        },
        (error) => {
          //this.confirmService.deleted.next(false);
          this.toastr.error("Can't be Deleted");
          this.spinner.hide();
        }
      );
  }
}
