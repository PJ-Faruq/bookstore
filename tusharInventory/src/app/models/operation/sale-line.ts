import { Book } from '../basic/book';
import { GradeLavel } from '../basic/grade-lavel';
import { Language } from '../basic/language';
import { SubjectModel } from '../basic/subject';
import { EntityModel } from '../configurations/entity-model';
import { Sale } from './sale';

export class SaleLine extends EntityModel {
    SaleId: number;
    Sale: Sale;

    BookSubjectId: number;
    BookSubject: SubjectModel;
    BookGradeId: number;
    BookGrade: GradeLavel;
    BookLanguageId: number;
    BookLanguage: Language;
    Quantity: number;

    constructor(
        SaleId?: number,
        Sale?: Sale,

        BookSubjectId?: number,
        BookSubject?: SubjectModel,
        BookGradeId?: number,
        BookGrade?: GradeLavel,
        BookLanguageId?: number,
        BookLanguage?: Language,
        Quantity?: number,
    ) {
        super();
        this.SaleId = SaleId ? SaleId : null;
        this.Sale = Sale ? Sale : null;
        this.BookSubjectId = BookSubjectId ? BookSubjectId : null;
        this.BookSubject = BookSubject ? BookSubject : null;
        this.BookGradeId = BookGradeId ? BookGradeId : null;
        this.BookGrade = BookGrade ? BookGrade : null;
        this.BookLanguageId = BookLanguageId ? BookLanguageId : null;
        this.BookLanguage = BookLanguage ? BookLanguage : null;

        this.Quantity = Quantity ? Quantity : null;
    }
}
