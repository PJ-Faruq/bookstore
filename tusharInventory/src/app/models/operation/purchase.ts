import { AuditModel } from '../configurations/audit-model';
import { User } from '../user';
import { PurchaseLine } from './purchase-line';

export class Purchase extends AuditModel{

   BookstoreEmployeeId : number=null;
   BookstoreEmployee :User=null;
   BookstoreManagerId:number;
   BookstoreManager:User=null;
   PurcahseDeptEmployeeId:number=null;
   PurcahseDeptEmployee:User=null;
   PurchaseDate?:Date=null;
   Status:number=1;
   Amount:number=0;
   PurchaseLines:PurchaseLine[]=[new PurchaseLine()];

}

