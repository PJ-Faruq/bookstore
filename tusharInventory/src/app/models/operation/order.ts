import { HeadOffice } from "../basic/head-office";
import { School } from "../basic/school";
import { Subcity } from '../basic/subcity';
import { Supplier } from "../basic/supplier";
import { AuditModel } from "../configurations/audit-model";
import { User } from "../user";
import { OrderLine } from "./order-line";

export class Order extends AuditModel {
  OrderId: string;
  IsSchool: boolean;

  SchoolId: number;
  School: School;

  BookstoreEmployeeId: number = null;
  BookstoreEmployee: User;

  SchoolEmployeeId?: number;
  SchoolEmployee?: User;

  BookstoreManagerId: number = null;
  BookstoreManager: User;

  HoId: number = null;
  Ho: HeadOffice;

  SupplierId: number = null;
  Supplier: Supplier;

  SubcityId: number=null;
  Subcity: Subcity;

  OrderDate: Date;
  Amount: number;
  Note: string;
  FilePath: string;
  File: File;
  Status: number;
  BookstoreEmployeeStatus: number;
  BookstoreManagerStatus: number;
  IsRequestedToManger: boolean;
  IsRequestedToPurchase: boolean;
  IsCollected: boolean;
  TrackingNo?: string;
  ReasonForReject?: string;
  ApprovalInfo: string = "";

  OrderLines: OrderLine[];

  constructor(
    IsSchool?: boolean,

    SchoolId?: number,
    School?: School,

    BookstoreEmployeeId?: number,
    BookstoreEmployee?: User,

    SchoolEmployeeId?: number,
    SchoolEmployee?: User,

    HoId?: number,
    Ho?: HeadOffice,

    SupplierId?: number,
    Supplier?: Supplier,

    OrderDate?: Date,
    Amount?: number,
    Note?: string,
    FilePath?: string,
    File?: File,
    Status?: number,
    BookstoreEmployeeStatus?: number,
    BookstoreManagerStatus?: number,
    IsRequestedToManger?: boolean,
    TrackingNo?: string,
    ReasonForReject?: string,
    OrderLines?: OrderLine[]
  ) {
    super();

    this.IsSchool = IsSchool ? IsSchool : true;

    this.SchoolId = SchoolId ? SchoolId : null;
    this.School = School ? School : null;

    this.BookstoreEmployeeId = BookstoreEmployeeId ? BookstoreEmployeeId : null;
    this.BookstoreEmployee = BookstoreEmployee ? BookstoreEmployee : null;

    this.HoId = HoId ? HoId : 1;
    this.Ho = Ho ? Ho : null;

    this.SupplierId = SupplierId ? SupplierId : null;
    this.Supplier = Supplier ? Supplier : null;

    this.OrderDate = OrderDate ? OrderDate : null;
    this.Amount = Amount ? Amount : null;
    this.Note = Note ? Note : "";
    this.FilePath = FilePath ? FilePath : "";
    this.Status = Status ? Status : 1;
    this.BookstoreEmployeeStatus = BookstoreEmployeeStatus
      ? BookstoreEmployeeStatus
      : 0;
    this.BookstoreManagerStatus = BookstoreManagerStatus
      ? BookstoreManagerStatus
      : 0;
    this.IsRequestedToManger = IsRequestedToManger
      ? IsRequestedToManger
      : false;

    this.OrderLines = OrderLines ? OrderLines : [];
  }
}
