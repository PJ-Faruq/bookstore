import { HeadOffice } from '../basic/head-office';
import { School } from '../basic/school';
import { AuditModel } from '../configurations/audit-model';
import { Order } from './order';
import { ReceiveLine } from './receive-line';
import { Sale } from './sale';

export class Receive extends AuditModel {
    IsSchool: boolean;

    OrderId: number;
    Order: Order;

    SaleId: number;
    Sale: Sale;

    SchoolId: number;
    School: School;

    HoId: number;
    Ho: HeadOffice;

    ReceiveDate: Date;
    Amount: number;
    Note: string;
    Status: number;

    ReceiveLines: ReceiveLine[];

    constructor(
        IsSchool?: boolean,

        OrderId?: number,
        Order?: Order,

        SaleId?: number,
        Sale?: Sale,

        SchoolId?: number,
        School?: School,

        HoId?: number,
        Ho?: HeadOffice,

        ReceiveDate?: Date,
        Amount?: number,
        Note?: string,
        Status?: number,

        ReceiveLines?: ReceiveLine[],
    ) {
        super();
        this.IsSchool = IsSchool ? IsSchool : null;

        this.OrderId = OrderId ? OrderId : null;
        this.Order = Order ? Order : null;


        this.SaleId = SaleId ? SaleId : null;
        this.Sale = Sale ? Sale : null;

        this.SchoolId = SchoolId ? SchoolId : null;
        this.School = School ? School : null;

        this.HoId = HoId ? HoId : null;
        this.Ho = Ho ? Ho : null;

        this.ReceiveDate = ReceiveDate ? ReceiveDate : new Date();
        this.Amount = Amount ? Amount : null;
        this.Note = Note ? Note : '';
        this.Status = Status ? Status : null;

        this.ReceiveLines = ReceiveLines ? ReceiveLines : [new ReceiveLine()];
    }
}
