import { Book } from '../basic/book';
import { GradeLavel } from '../basic/grade-lavel';
import { Language } from '../basic/language';
import { SubjectModel } from '../basic/subject';
import { EntityModel } from '../configurations/entity-model';
import { Order } from './order';

export class OrderLine extends EntityModel {
  OrderId: number;
  Order: Order;

  BookSubjectId: number;
  BookSubject: SubjectModel;
  BookGradeLavelId: number;
  BookGradeLavel: GradeLavel;
  BookLanguageId: number;
  BookLanguage: Language;
  BookPrice?: number;

  Quantity: number;
  AvailableQty: number;

  constructor(
    OrderId?: number,
    Order?: Order,

    BookSubjectId?: number,
    BookSubject?: SubjectModel,
    BookGradeLavelId?: number,
    BookGradeLavel?: GradeLavel,
    BookLanguageId?: number,
    BookLanguage?: Language,
    BookPrice?: number,

    Quantity?: number,
    AvailableQty?: number
  ) {
    super();
    this.OrderId = OrderId ? OrderId : 0;
    this.Order = Order ? Order : null;

    this.BookSubjectId = BookSubjectId ? BookSubjectId : null;
    this.BookSubject = BookSubject ? BookSubject : null;
    this.BookGradeLavelId = BookGradeLavelId ? BookGradeLavelId : null;
    this.BookGradeLavel = BookGradeLavel ? BookGradeLavel : null;
    this.BookLanguageId = BookLanguageId ? BookLanguageId : null;
    this.BookLanguage = BookLanguage ? BookLanguage : null;
    this.BookPrice = BookPrice ? BookPrice : null;

    this.Quantity = Quantity ? Quantity : null;
    this.AvailableQty = AvailableQty ? AvailableQty : 0;
  }
}
