import { GradeLavel } from '../basic/grade-lavel';
import { Language } from '../basic/language';
import { SubjectModel } from '../basic/subject';
import { SubcityOrder } from './SubcityOrder';

export class SubcityOrderLine {
    Id: number;
    SubcityOrderId: number;
    SubcityOrder:SubcityOrder;
    BookSubjectId?: number;
    BookSubject?: SubjectModel;
    BookGradeLavelId?: number;
    BookGradeLavel?: GradeLavel;
    BookLanguageId?: number;
    BookLanguage?: Language;
    Quantity: number;
    MainStockQty:number
    SubcityStockQty:number;
}