import { School } from '../basic/school';
import { Subcity } from '../basic/subcity';
import { User } from '../user';
import { Order } from './order';

export class BankReceiptVoucher {
  Id: number;
  OrderId: number ;
  Order: Order;
  Date: Date ;
  SubcityId: number ;
  Subcity: Subcity ;
  Kebele: string ;
  SchoolId: number ;
  School: School ;
  BookstoreEmployeeId: number ;
  BookstoreEmployee: User ;
  City: string ;
  HouseNumber: string ;
  Cash: boolean = false;
  Check: boolean = false;
  CheckNo: string ;
  DepositeSlip: boolean = false;
  ReferenceNo: string ;
  DepositeDate: Date ;
  BankTransfer: boolean = false;
  Amount: number ;
}