import { BudgetType } from '../basic/BudgetType';
import { Supplier } from "../basic/supplier";
import { AuditModel } from "../configurations/audit-model";
import { User } from "../user";
import { Purchase } from './purchase';
import { PurchaseBookLine } from "./purchase-book-line";
import { PurchaseLine } from "./purchase-line";

export class PurchaseBook extends AuditModel {
  BudgetTypeId: number;
  BudgetType: BudgetType;
  SupplierId: number;
  Supplier: Supplier = null;
  PurchaseId: string = "";
  BookstoreEmployeeId: number = null;
  BookstoreEmployee: User = null;
  BookstoreManagerId: number;
  BookstoreManager: User = null;
  PurcahseDeptEmployeeId: number = 0;
  PurcahseDeptEmployee: User = null;
  PurchaseDate: Date = null;

  TransactionTrackingNumber: string = "";
  ReferenceNumber: string = "";

  Amount: number = 0;

  PurchaseRequestId: number = null;
  PurchaseRequest: Purchase = null;

  PurchaseBookLines: PurchaseBookLine[] = [];
}
