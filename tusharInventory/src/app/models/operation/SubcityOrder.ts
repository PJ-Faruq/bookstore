import { Subcity } from '../basic/subcity';
import { User } from '../user';
import { SubcityOrderLine } from './SubcityOrderLine';

export class SubcityOrder {
  Id: number;
  RequestId: string;
  Date: Date;
  BookstoreManagerId: number;
  BookstoreManager: User;

  BookstoreEmployeeId: number;
  BookstoreEmployee: User;

  SubcityManagerId: number;
  SubcityManager: User;

  SubcityId: number;
  Subcity: Subcity;

  Status: number;
  OrderLines: SubcityOrderLine[] = [];
}