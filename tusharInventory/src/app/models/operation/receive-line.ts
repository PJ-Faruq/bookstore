import { Book } from '../basic/book';
import { GradeLavel } from '../basic/grade-lavel';
import { Language } from '../basic/language';
import { SubjectModel } from '../basic/subject';
import { EntityModel } from '../configurations/entity-model';
import { Receive } from './receive';

export class ReceiveLine extends EntityModel {
    ReceiveId: number;
    Receive: Receive;
    
    BookSubjectId: number;
    BookSubject: SubjectModel;
    BookGradeId: number;
    BookGrade: GradeLavel;
    BookLanguageId: number;
    BookLanguage: Language;

    OrderedQuantity: number;
    ReceivedQuantity: number;

    constructor(
        ReceiveId?: number,
        Receive?: Receive,

        BookSubjectId?: number,
        BookSubject?: SubjectModel,
        BookGradeId?: number,
        BookGrade?: GradeLavel,
        BookLanguageId?: number,
        BookLanguage?: Language,

        OrderedQuantity?: number,
        ReceivedQuantity?: number
    ) {
        super();
        this.ReceiveId = ReceiveId ? ReceiveId : null;
        this.Receive = Receive ? Receive : null;
        
        this.BookSubjectId = BookSubjectId ? BookSubjectId : null;
        this.BookSubject = BookSubject ? BookSubject : null;
        this.BookGradeId = BookGradeId ? BookGradeId : null;
        this.BookGrade = BookGrade ? BookGrade : null;
        this.BookLanguageId = BookLanguageId ? BookLanguageId : null;
        this.BookLanguage = BookLanguage ? BookLanguage : null;

        this.OrderedQuantity = OrderedQuantity ? OrderedQuantity : null;
        this.ReceivedQuantity = ReceivedQuantity ? ReceivedQuantity : null;
    }
}
