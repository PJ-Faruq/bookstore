import { GradeLavel } from '../basic/grade-lavel';
import { Language } from '../basic/language';
import { SubjectModel } from '../basic/subject';
import { EntityModel } from '../configurations/entity-model';
import { Purchase } from './purchase';

export class PurchaseLine extends EntityModel{

    PurchaseId:number=0;
    Purchase: Purchase=null;
    BookSubjectId: number=0;
    BookSubject: SubjectModel=null;
    BookGradeLavelId: number=0;
    BookGradeLavel: GradeLavel=null;
    BookLanguageId: number=0;
    BookLanguage: Language=null;
    BookPrice:number=0;

    Quantity?: number=0;
    AvailableQty?: number=0;
    PurchaseQuantity:number=0;
}