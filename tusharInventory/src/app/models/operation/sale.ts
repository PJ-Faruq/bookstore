import { HeadOffice } from '../basic/head-office';
import { AuditModel } from '../configurations/audit-model';
import { Order } from './order';
import { SaleLine } from './sale-line';

export class Sale extends AuditModel {

    HoId: number;
    Ho: HeadOffice;

    OrderId: number;
    Order: Order;

    SaleDate: Date;
    Amount: number;
    Note: string;
    Status: number;

    SaleLines: SaleLine[]

    constructor(
        HoId?: number,
        Ho?: HeadOffice,

        OrderId?: number,
        Order?: Order,

        SaleDate?: Date,
        Amount?: number,
        Note?: string,
        Status?: number,

        SaleLines?: SaleLine[],
    ) {
        super();

        this.HoId = HoId ? HoId : null;
        this.Ho = Ho ? Ho : null;

        this.OrderId = OrderId ? OrderId : null;
        this.Order = Order ? Order : null;

        this.SaleDate = SaleDate ? SaleDate : new Date();
        this.Amount = Amount ? Amount : null;
        this.Note = Note ? Note : '';
        this.Status = Status ? Status : null;

        this.SaleLines = SaleLines ? SaleLines : [new SaleLine()];
    }
}
