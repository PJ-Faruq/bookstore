import { EntityModel } from './entity-model';

export class AuditModel extends EntityModel {
    CreatedBy: string;
    CreateDate: Date;
    LastModifiedBy: string;
    LastModifiedDate: Date;

    constructor(
        CreatedBy?: string,
        CreateDate?: Date,
        LastModifiedBy?: string,
        LastModifiedDate?: Date,
    ) {
        super();
        this.CreatedBy = CreatedBy ? CreatedBy : '';
        this.CreateDate = CreateDate ? CreateDate : new Date();
        this.LastModifiedBy = LastModifiedBy ? LastModifiedBy : '';
        this.LastModifiedDate = LastModifiedDate ? LastModifiedDate : new Date();
    }
}
