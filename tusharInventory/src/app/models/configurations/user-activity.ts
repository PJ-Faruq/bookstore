export class UserActivity {
  Id: number;
  UserId: number;
  Url: string;
  UserName: string;
  Data: string;
  Date: Date;
}
