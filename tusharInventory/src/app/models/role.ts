import { EntityModel } from './configurations/entity-model';

export class Role extends EntityModel {
    Name: string;

    constructor(Name?: string) {
        super();
        this.Name = Name ? Name : '';
    }
}
