import { Subject } from 'rxjs';
import { EntityModel } from '../configurations/entity-model'
import { GradeLavel } from './grade-lavel';
import { Language } from './language';
import { SubjectModel } from './subject'

export class Book extends EntityModel {
  BookSubjectId: number;
  BookSubject: SubjectModel;

  BookLanguageId: number;
  BookLanguage: Language;

  BookGradeLavelId: number;
  BookGradeLavel: GradeLavel;

  PurchasePrice: number;
  SalePrice: number;
  PageNo:number;
}
