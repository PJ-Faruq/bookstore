
import { EntityModel } from "./../configurations/entity-model";
import { BudgetType } from './BudgetType';
export class Budget extends EntityModel {
  BudgetTypeId : number=null;
  BudgetType :BudgetType=null;
  BudgetDate:Date=null;
  Amount:number;
  Description:string=null;
  CompanyName:string=null;
  CompanyAddress:string=null;

}
