import { EntityModel } from '../configurations/entity-model';
import { Ownership } from './ownership';
import { SchoolLavel } from './school-lavel';
import { Subcity } from './subcity';
import { Woreda } from './woreda';
import { Woreda1 } from './woreda1';

export class School extends EntityModel {
  Id: number;
  Name: string;
  NumberOfStudent:number=null;

  SchoolSubcityId: number = null;
  SchoolSubcity: Subcity;

  SchoolWoredaId: number = null;
  SchoolWoreda: Woreda;

  Woreda1Id: number = null;
  Woreda1: Woreda1;

  SchoolLavelId: number = null;
  SchoolLavel: SchoolLavel;

  SchoolOwnershipId: number = null;
  SchoolOwnership: Ownership;
}
