import { EntityModel } from '../configurations/entity-model';

export class Supplier  extends EntityModel {
    Name: string;
    Mobile: string;
    Phone: string;
    Email: string;
    Contact: string;
    Fax: string;
    City: string;
    State: string;
    ZipCode: string;
    Country: string;
    Address: string;
    Address2: string;
    Details: string;
    PreviousCreditBalance: number;

}
