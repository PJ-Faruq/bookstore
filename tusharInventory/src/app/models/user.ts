import { Ownership } from './basic/ownership';
import { Region } from './basic/Region';
import { School } from './basic/school';
import { SchoolLavel } from './basic/school-lavel';
import { Subcity } from './basic/subcity';
import { Woreda } from './basic/woreda';
import { EntityModel } from './configurations/entity-model';
import { Role } from './role';

export class User extends EntityModel {
  FullName: string;
  FirstName: string;
  SecondName: string;
  LastName: string;
  Email: string;
  Password: string;
  PhotographFile: File;
  Photograph: string;
  RoleId: number;
  Role: Role;
  EmployeeId: string;
  SchoolId: number = null;
  School: School;
  Address: string;
  IsActive: boolean;
  IsApproved:boolean;
  PhoneNumber: string;
  WoredaId: number;
  Woreda: Woreda;
  RegionId: number;
  Region: Region;
  SubcityId: number;
  Subcity: Subcity;

  SchoolLavelId: number=null;
  SchoolLavel: SchoolLavel=null;
  OwnershipId: number=null;
  Ownership: Ownership=null;
}
