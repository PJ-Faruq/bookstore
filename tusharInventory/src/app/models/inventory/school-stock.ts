import { Book } from '../basic/book';
import { GradeLavel } from '../basic/grade-lavel';
import { Language } from '../basic/language';
import { School } from '../basic/school';
import { SubjectModel } from '../basic/subject';
import { AuditModel } from '../configurations/audit-model';

export class SchoolStock extends AuditModel {

    SchoolId: number;
    School: School;

    BookSubjectId: number;
    BookSubject: SubjectModel;
    BookGradeId: number;
    BookGrade: GradeLavel;
    BookLanguageId: number;
    BookLanguage: Language;

    Quantity: number;
}
