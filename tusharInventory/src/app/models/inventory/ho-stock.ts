import { Book } from '../basic/book';
import { GradeLavel } from '../basic/grade-lavel';
import { HeadOffice } from '../basic/head-office';
import { Language } from '../basic/language';
import { SubjectModel } from '../basic/subject';
import { AuditModel } from '../configurations/audit-model'

export class HoStock extends AuditModel {
    HoId: number;
    Ho: HeadOffice;

    BookSubjectId: number;
    BookSubject: SubjectModel;
    BookGradeId: number;
    BookGrade: GradeLavel;
    BookLanguageId: number;
    BookLanguage: Language;

    AvailableQty: number;
    AllocatedQty: number;

    
}
