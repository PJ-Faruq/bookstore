export class AdminDashboardVm {
    PendingRequest: number;
    RejectedRequest: number;
    ApprovedRequest: number;
    TodayRequest: number;
    ThisWeekRequest: number;
    TotalRequest: number;
    TotalBook: number;
    TotalSchool: number;
}