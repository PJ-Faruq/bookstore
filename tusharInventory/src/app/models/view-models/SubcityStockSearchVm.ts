export class SubcityStockSearchVm {
  Id: number = null;
  SubcityId: number = null;
  BookSubjectId: number = null;
  BookLanguageId: number = null;
  BookGradeId: number = null;
  IsRequested: boolean;
  StockStatusId: number = null;
  UserId:number=null;
}