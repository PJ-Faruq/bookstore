export class OrderShowVm {

    BookSubjectName?: string;
    BookGradeName?: string;
    BookLanguageName?: string;
    Quantity?: number;
    BookPrice?: number;
}
