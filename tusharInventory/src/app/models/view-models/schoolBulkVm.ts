export class SchoolBulkVm {

    Subcity?: string;
    Woreda1?: string;
    Woreda?: string;
    SchoolName?: string;
    SchoolLevel?: string;
    SchoolOwnership?: string;
}