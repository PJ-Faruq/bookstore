import { Subject } from 'rxjs';
import { GradeLavel } from '../basic/grade-lavel';
import { Language } from '../basic/language';
import { SubjectModel } from '../basic/subject';
import { EntityModel } from '../configurations/entity-model'

export class SchoolStockSearchVm extends EntityModel {
    
    BookSubjectId: number;
    BookSubject: SubjectModel;

    BookLanguageId: number;
    BookLanguage: Language;

    BookGradeLavelId: number;
    BookGradeLavel: GradeLavel;

    PurchasePrice: number;
    SalePrice: number;

    SchoolId:number=null;
}