export class BookReportResultVm{
        OrderId:string
         TrackingNo:string
         OrderDate:string
         SchoolName :string
         RequestedBy :string
         RequestedTo :string
         ApprovedBy :string
         Amount :number
         Status :string
         ReasonForReject :string
         BankSlip :string
         Note :string
}