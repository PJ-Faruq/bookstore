export class PurchaseReportResultVm {
  Budget_Type: string = "";
  Supplier: string = "";
  Purchase_No: string = "";
  Bookstore_Employee: string = "";
  Bookstore_Manager: string = "";
  PurcahseDept_Employee: string = "";
  Purchase_Date: string = "";

  Transaction_Tracking_Number: string = "";
  Reference_Number: string = "";

  Amount: number = 0;
}