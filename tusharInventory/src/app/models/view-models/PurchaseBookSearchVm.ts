export class PurchaseBookSearchVm {
  BudgetTypeId: number = null;
  SupplierId: number = null;
  PurchaseId: number = null;
  FromDate: Date = null;
  ToDate: Date = null;
  TransactionTrackingNumber: string = null;
  ReferenceNumber:string=null;
}