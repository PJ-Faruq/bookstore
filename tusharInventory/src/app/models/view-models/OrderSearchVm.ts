export class OrderSearchVm {
  CurrentUserId?: number = null;
  RoleId?: number = null;

  AdminStatus?: string = null;
  EmpStatus?: string = null;
  SchoolStatus?: string = null;
  ManagerStatus?: string = null;
  SubcityStatus?: string = null;

  BookSubjectName?: string;
  BookGradeName?: string;
  BookLanguageName?: string;
  Quantity?: number;
  BookPrice?: number;
  isNeedBankSlip: boolean = false;
}
