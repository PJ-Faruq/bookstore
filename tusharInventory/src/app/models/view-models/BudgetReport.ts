export class BudgetReport  {
  Date: string = null;
  Budget_Type: string = null;
  Amount: number=null;
  Description: string = null;
  Company_Name: string = null;
  Company_Address: string = null;
}
