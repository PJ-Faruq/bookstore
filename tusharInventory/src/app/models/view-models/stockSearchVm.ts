import { GradeLavel } from '../basic/grade-lavel';
import { Language } from '../basic/language';
import { Subcity } from '../basic/subcity';
import { SubjectModel } from '../basic/subject';

export class StockSearchVm {
  Id: number=null;

  SubcityId: number = null;
  Subcity: Subcity;

  BookSubjectId: number = null;
  BookSubject: SubjectModel;

  BookLanguageId: number = null;
  BookLanguage: Language;

  BookGradeLavelId: number = null;
  BookGradeLavel: GradeLavel;
  Quantity: number;
  AvailableQty: number=0;
  IsRequested: boolean;

  StockStatusId: number = null;
  isChecked: boolean = false;
  isDisabled: boolean = false;
}