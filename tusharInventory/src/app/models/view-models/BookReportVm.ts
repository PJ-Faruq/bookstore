export class BookReportVm {
  FromDate: Date=null;
  ToDate: Date=null;
  Status: number=null;
  TrackingNo:string=null;
  SchoolId:number=null;
}
