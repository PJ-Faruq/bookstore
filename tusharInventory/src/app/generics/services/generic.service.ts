import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { AppSettings } from 'src/app/shared/app-settings';

@Injectable({
  providedIn: 'root'
})
export class GenericService {

  listChanged = new Subject<any[]>();
  entityList: any[] = [];


  constructor(private http: HttpClient) {
  }

  add(entity: any, url: string): Observable<any> {
    return this.http.post<any>(AppSettings.API_ROOT + url, entity);
  }

  update(id: number, entity: any, url: string): Observable<any> {
    return this.http.put(AppSettings.API_ROOT + url + "/" + id, entity);
  }

  changeStatus(id: number, entity: any, url: string, sufix: string): Observable<any> {
    return this.http.put(AppSettings.API_ROOT + url + "/" + id + sufix, entity);
  }

  delete(id: number, url: string): Observable<any> {
    return this.http.delete<any>(AppSettings.API_ROOT + url + "/" + id);
  }

  getById(id: number, url: string): Observable<any> {
    return this.http.get<any>(AppSettings.API_ROOT + url + "/" + id);
  }

  getAll(url: string): Observable<any[]> {
    return this.http.get<any[]>(AppSettings.API_ROOT + url);
  }

  getAllByParam(url: string, id: number, sufixUrl: string): Observable<any[]> {
    return this.http.get<any[]>(AppSettings.API_ROOT + url + "/" + id + "/" + sufixUrl);
  }

  setAll(url: string) {
    this.getAll(url).subscribe((data) => {
      this.entityList = data;
      this.listChanged.next(this.entityList.slice())
    },
      (error) => {
        this.entityList = [];
      })
  }

  setAllByParam(url: string, id: number, sufixUrl: string) {
    this.getAllByParam(url, id, sufixUrl).subscribe((data) => {
      this.entityList = data;
      this.listChanged.next(this.entityList.slice())
    },
      (error) => {
        this.entityList = [];
      })
  }
}
