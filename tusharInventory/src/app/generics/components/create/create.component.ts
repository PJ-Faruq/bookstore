import { Router, ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { GenericService } from '../../services/generic.service';

export class CreateComponent {

  editMode: Boolean = false;
  modelId: number;
  model: any;

  apiUrl: string;
  constructor(public service?: GenericService,
    public router?: Router,
    public activatedRoute?: ActivatedRoute,
    public spinner?: NgxSpinnerService,
    public toastr?: ToastrService
  ) {
  }

  onSubmit(listLink) {
    if (this.editMode) {
      this.service.update(this.modelId, this.model, this.apiUrl).subscribe((data) => {
        this.toastr.success('Update Successfull', 'Success');
        this.router.navigate([listLink]);
      },
        (error) => {
          this.toastr.error('Ops! network or server related error occured', 'Error');
        })
    } else {
      this.service.add(this.model, this.apiUrl).subscribe((data) => {
        this.router.navigate([listLink]);
        this.toastr.success('Save Successfull', 'Success');
        
      },
        (error) => {
          this.toastr.error('Ops! network or server related error occured', 'Error');
        })
    }
    this.reset();
  }


  reset() {
    this.editMode = false;
  }

  setDataByParams() {
    let id = this.activatedRoute.snapshot.params['id'];
    if (id != null) {
      this.spinner.show();
      this.service.getById(id, this.apiUrl).subscribe(resp => {
        this.spinner.hide();

        this.editMode = true;
        this.modelId = id;
        this.model = resp;
      });
    }
  }
  
}


