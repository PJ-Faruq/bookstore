import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { GenericService } from '../../services/generic.service';


export class EditComponent {

  title;
  data: any;

  modelId: number;
  model: any;
  apiUrl: string;

  constructor(
    public modalRef?: BsModalRef,
    public service?: GenericService,
    public router?: Router,
    public toastr?: ToastrService,
    public spinner?: NgxSpinnerService) {
  }


  onSubmit() {
    this.service.update(this.modelId, this.model, this.apiUrl).subscribe((data) => {
      this.toastr.success('Update Successfull', 'Success');
      this.modalRef.hide();
      this.service.setAll(this.apiUrl);
    },
      (error) => {
        this.toastr.error('Ops! network or server related error occured', 'Error');
      })
    this.reset();
  }

  reset() {
    this.modalRef.hide();
  }

  setDataByParams() {
    let id = this.data.id;
    if (id != null) {
      this.spinner.show();
      this.service.getById(id, this.apiUrl).subscribe(resp => {
        this.spinner.hide();
        this.modelId = id;
        this.model = resp;
      });
    }
  }
}
