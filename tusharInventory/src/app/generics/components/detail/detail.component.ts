import { BsModalRef } from 'ngx-bootstrap/modal';
import { NgxSpinnerService } from 'ngx-spinner';
import { GenericService } from '../../services/generic.service';


export class DetailComponent {

  title;
  data: any;
  model: any;
  apiUrl: string;
  
  constructor(public modalRef: BsModalRef,
    public service: GenericService,
    public spinner: NgxSpinnerService) {
  }

  ngOnInit(): void {
    this.setDataByParams(this.data.id);
  }

  setDataByParams(id) {
    if (id != null) {
      this.spinner.show();
      this.service.getById(id, this.apiUrl).subscribe(resp => {
        this.spinner.hide();
        this.model = resp;
        
      });
    }
  }
}
