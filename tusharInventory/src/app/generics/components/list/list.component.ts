import { Component, OnInit } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
import { ConfirmModalComponent } from 'src/app/confirm-modal/confirm-modal.component';
import { ConfirmService } from 'src/app/regulars/services/confirm.service';


export class ListComponent {

  listChangedSubscribe: Subscription;
  confirmSubscription: Subscription;

  modelList: any[];
  p: number = 1;
  modalRef: any;

  detailModal: Component;
  editModal: Component;

  constructor(public modalService?: BsModalService,
    public confirmService?: ConfirmService,
    public toastr?: ToastrService,
    public spinner?: NgxSpinnerService
  ) { }



  onDetail(UserId: number) {
    this.openDetailModal(UserId);
  }

  onEdit(UserId: number) {
    this.openEditModal(UserId);
  }

  onDelete(id: number, model:any,url: string) {
    this.openConfirmModal(id,model, url);
  }

  openDetailModal(id: number) {

  }

  openEditModal(id: number) {
 
  }

  openConfirmModal(id: number,model:any, url: string) {
    this.modalRef = this.modalService.show(ConfirmModalComponent, {
      initialState: {
        title: "Confirmation!!",
        data: {
          id: id,
          model:model,
          url: url,
        },
      },
    });
  }

  ngOnDestroy(): void {
    if (this.confirmSubscription != null) {
      this.confirmSubscription.unsubscribe();
    }
    if (this.listChangedSubscribe != null) {
      this.listChangedSubscribe.unsubscribe();
    }
  }
}
