import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { TranslateModule, TranslateLoader } from "@ngx-translate/core";

import { AppRoutingModule } from './app-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ToastrModule } from 'ngx-toastr';
import { NgxSpinnerModule } from "ngx-spinner";
import { NgSelectModule } from '@ng-select/ng-select';
import { ModalModule } from 'ngx-bootstrap/modal';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';

import { AuthGuard } from './auth/auth.guard';
import { HTTP_INTERCEPTORS, HttpClientModule,HttpClient } from '@angular/common/http';
import { JwtInterceptor } from './auth/jwt.interceptor';
import { ErrorInterceptor } from './auth/error.interceptor';
import { AppComponent } from './app.component';
import { NavComponent } from './regulars/components/nav/nav.component';
import { ConfirmModalComponent } from './confirm-modal/confirm-modal.component';
import { AdminPanelComponent } from './regulars/components/admin-panel/admin-panel.component';
import { DashboardComponent } from './regulars/components/admin-panel/dashboard/dashboard.component';

import { AsideComponent } from './regulars/components/aside/aside.component';
import { FooterComponent } from './regulars/components/footer/footer.component';
import { ForbiddenComponent } from './regulars/components/forbidden/forbidden.component';
import { PageNotFoundComponent } from './regulars/components/page-not-found/page-not-found.component';
import { LoginComponent } from './regulars/components/user-auth/login/login.component';
import { UserAuthComponent } from './regulars/components/user-auth/user-auth.component';

import { UserComponent } from './regulars/components/admin-panel/user/user.component';
import { UserCreateComponent } from './regulars/components/admin-panel/user/user-create/user-create.component';
import { UserListComponent } from './regulars/components/admin-panel/user/user-list/user-list.component';
import { UserDetailModalComponent } from './regulars/components/admin-panel/user/user-detail-modal/user-detail-modal.component';
import { UserEditModalComponent } from './regulars/components/admin-panel/user/user-edit-modal/user-edit-modal.component';
import { RoleComponent } from './regulars/components/admin-panel/role/role.component';
import { RoleCreateComponent } from './regulars/components/admin-panel/role/role-create/role-create.component';
import { RoleListComponent } from './regulars/components/admin-panel/role/role-list/role-list.component';
import { RoleEditModalComponent } from './regulars/components/admin-panel/role/role-edit-modal/role-edit-modal.component';
import { RoleDetailModalComponent } from './regulars/components/admin-panel/role/role-detail-modal/role-detail-modal.component';
import { BookComponent } from './regulars/components/admin-panel/book/book.component';

import { BookListComponent } from './regulars/components/admin-panel/book/book-list/book-list.component';
import { BookEditModalComponent } from './regulars/components/admin-panel/book/book-edit-modal/book-edit-modal.component';
import { BookDetailModalComponent } from './regulars/components/admin-panel/book/book-detail-modal/book-detail-modal.component';
import { SchoolComponent } from './regulars/components/admin-panel/school/school.component';
import { SchoolCreateComponent } from './regulars/components/admin-panel/school/school-create/school-create.component';
import { SchoolListComponent } from './regulars/components/admin-panel/school/school-list/school-list.component';
import { SchoolEditModalComponent } from './regulars/components/admin-panel/school/school-edit-modal/school-edit-modal.component';
import { SchoolDetailModalComponent } from './regulars/components/admin-panel/school/school-detail-modal/school-detail-modal.component';
import { OrderComponent } from './regulars/components/admin-panel/order/order.component';
import { OrderCreateComponent } from './regulars/components/admin-panel/order/order-create/order-create.component';
import { OrderListComponent } from './regulars/components/admin-panel/order/order-list/order-list.component';
import { OrderEditModalComponent } from './regulars/components/admin-panel/order/order-edit-modal/order-edit-modal.component';
import { OrderDetailModalComponent } from './regulars/components/admin-panel/order/order-detail-modal/order-detail-modal.component';
import { ReceiveComponent } from './regulars/components/admin-panel/receive/receive.component';
import { ReceiveCreateComponent } from './regulars/components/admin-panel/receive/receive-create/receive-create.component';
import { ReceiveListComponent } from './regulars/components/admin-panel/receive/receive-list/receive-list.component';
import { ReceiveEditModalComponent } from './regulars/components/admin-panel/receive/receive-edit-modal/receive-edit-modal.component';
import { ReceiveDetailModalComponent } from './regulars/components/admin-panel/receive/receive-detail-modal/receive-detail-modal.component';
import { SaleComponent } from './regulars/components/admin-panel/sale/sale.component';
import { SaleCreateComponent } from './regulars/components/admin-panel/sale/sale-create/sale-create.component';
import { SaleListComponent } from './regulars/components/admin-panel/sale/sale-list/sale-list.component';
import { SaleEditModalComponent } from './regulars/components/admin-panel/sale/sale-edit-modal/sale-edit-modal.component';
import { SaleDetailModalComponent } from './regulars/components/admin-panel/sale/sale-detail-modal/sale-detail-modal.component';
import { BookCreateComponent } from './regulars/components/admin-panel/book/book-create/book-create.component';
import { PendingOrderListComponent } from './regulars/components/admin-panel/order/pending-order-list/pending-order-list.component';
import { OrderFileUploadModalComponent } from './regulars/components/admin-panel/order/order-file-upload-modal/order-file-upload-modal.component';
import { PurchaseCreateComponent } from "./regulars/components/admin-panel/purchase/purchase-create/purchase-create.component";
import { PurchaseListComponent } from './regulars/components/admin-panel/purchase/purchase-list/purchase-list.component';
import { PurchaseDetailComponent } from './regulars/components/admin-panel/purchase/purchase-detail/purchase-detail.component';
import { ReportComponent } from './regulars/components/admin-panel/report/report.component';
import { SalesReportComponent } from './regulars/components/admin-panel/report/sales-report/sales-report.component';
import { PurchaseBookComponent } from './regulars/components/admin-panel/purchase-book/purchase-book.component';
import { PurchaseBookCreateComponent } from './regulars/components/admin-panel/purchase-book/purchase-book-create/purchase-book-create.component';
import { PurchaseBookListComponent } from './regulars/components/admin-panel/purchase-book/purchase-book-list/purchase-book-list.component';
import { PurchaseBookDetailComponent } from './regulars/components/admin-panel/purchase-book/purchase-book-detail/purchase-book-detail.component';
import { PurchaseBookCreateModalComponent } from './regulars/components/admin-panel/purchase-book/purchase-book-create-modal/purchase-book-create-modal.component';
import { AdminDashboardComponent } from './regulars/components/admin-panel/dashboard/admin-dashboard/admin-dashboard.component';
import { StockComponent } from './regulars/components/admin-panel/stock/stock.component';
import { BookstoreStockComponent } from './regulars/components/admin-panel/stock/bookstore-stock/bookstore-stock.component';
import { BookstoreEmployeeDashboardComponent } from './regulars/components/admin-panel/dashboard/bookstore-employee-dashboard/bookstore-employee-dashboard.component';
import { SchoolStockComponent } from './regulars/components/admin-panel/stock/school-stock/school-stock.component';
import { SchoolBulkImportComponent } from './regulars/components/admin-panel/school/school-bulk-import/school-bulk-import.component';
import { SchoolDashboardComponent } from './regulars/components/admin-panel/dashboard/school-dashboard/school-dashboard.component';
import { ManagerDashboardComponent } from './regulars/components/admin-panel/dashboard/manager-dashboard/manager-dashboard.component';
import { PurchaseDashboardComponent } from './regulars/components/admin-panel/dashboard/purchase-dashboard/purchase-dashboard.component';
import { PurchaseReportComponent } from './regulars/components/admin-panel/report/purchase-report/purchase-report.component';
import { ConfirmRejectModalComponent } from './regulars/components/admin-panel/order/confirm-reject-modal/confirm-reject-modal.component';
import { SubjectComponent } from './regulars/components/admin-panel/subject/subject.component';
import { SubjectCreateComponent } from './regulars/components/admin-panel/subject/subject-create/subject-create.component';
import { SubjectEditModalComponent } from './regulars/components/admin-panel/subject/subject-edit-modal/subject-edit-modal.component';
import { SubjectListComponent } from './regulars/components/admin-panel/subject/subject-list/subject-list.component';
import { LanguageComponent } from './regulars/components/admin-panel/language/language.component';
import { LanguageCreateComponent } from './regulars/components/admin-panel/language/language-create/language-create.component';
import { LanguageEditModalComponent } from './regulars/components/admin-panel/language/language-edit-modal/language-edit-modal.component';
import { LanguageListComponent } from './regulars/components/admin-panel/language/language-list/language-list.component';
import { GradeLevelComponent } from './regulars/components/admin-panel/grade-level/grade-level.component';
import { GradeLevelCreateComponent } from './regulars/components/admin-panel/grade-level/grade-level-create/grade-level-create.component';
import { GradeLevelEditModalComponent } from './regulars/components/admin-panel/grade-level/grade-level-edit-modal/grade-level-edit-modal.component';
import { GradeLevelListComponent } from './regulars/components/admin-panel/grade-level/grade-level-list/grade-level-list.component';
import { SubcityComponent } from './regulars/components/admin-panel/subcity/subcity.component';
import { SubcityCreateComponent } from './regulars/components/admin-panel/subcity/subcity-create/subcity-create.component';
import { SubcityEditModalComponent } from './regulars/components/admin-panel/subcity/subcity-edit-modal/subcity-edit-modal.component';
import { SubcityListComponent } from './regulars/components/admin-panel/subcity/subcity-list/subcity-list.component';
import { NgxChartsModule } from "@swimlane/ngx-charts";
import { BudgetComponent } from './regulars/components/admin-panel/budget/budget.component';
import { BudgetCreateComponent } from './regulars/components/admin-panel/budget/budget-create/budget-create.component';
import { BudgetEditModalComponent } from './regulars/components/admin-panel/budget/budget-edit-modal/budget-edit-modal.component';
import { BudgetListComponent } from './regulars/components/admin-panel/budget/budget-list/budget-list.component';
import { BankSlipComponent } from './regulars/components/admin-panel/report/bank-slip/bank-slip.component';
import { WoredaComponent } from './regulars/components/admin-panel/woreda/woreda.component';
import { WoredaCreateComponent } from './regulars/components/admin-panel/woreda/woreda-create/woreda-create.component';
import { WoredaEditModalComponent } from './regulars/components/admin-panel/woreda/woreda-edit-modal/woreda-edit-modal.component';
import { WoredaListComponent } from './regulars/components/admin-panel/woreda/woreda-list/woreda-list.component';
import { Woreda1Component } from './regulars/components/admin-panel/woreda1/woreda1.component';
import { Woreda1CreateComponent } from './regulars/components/admin-panel/woreda1/woreda1-create/woreda1-create.component';
import { Woreda1EditModalComponent } from './regulars/components/admin-panel/woreda1/woreda1-edit-modal/woreda1-edit-modal.component';
import { Woreda1ListComponent } from './regulars/components/admin-panel/woreda1/woreda1-list/woreda1-list.component';
import { SchoolLevelComponent } from './regulars/components/admin-panel/school-level/school-level.component';
import { SchoolOwnershipComponent } from './regulars/components/admin-panel/school-ownership/school-ownership.component';
import { SchoolLevelCreateComponent } from './regulars/components/admin-panel/school-level/school-level-create/school-level-create.component';
import { SchoolLevelEditModalComponent } from './regulars/components/admin-panel/school-level/school-level-edit-modal/school-level-edit-modal.component';
import { SchoolLevelListComponent } from './regulars/components/admin-panel/school-level/school-level-list/school-level-list.component';

import { SchoolOwnershipEditModalComponent } from './regulars/components/admin-panel/school-ownership/school-ownership-edit-modal/school-ownership-edit-modal.component';
import { SchoolOwnershipListComponent } from './regulars/components/admin-panel/school-ownership/school-ownership-list/school-ownership-list.component';
//import { NgxDatesPickerModule } from "ngx-dates-picker";
import {NgxPaginationModule} from 'ngx-pagination';
import { SchoolOwnershipCreateComponent } from './regulars/components/admin-panel/school-ownership/school-ownership-create/school-ownership-create.component';
import { RegionComponent } from './regulars/components/admin-panel/region/region.component';
import { RegionCreateComponent } from './regulars/components/admin-panel/region/region-create/region-create.component';
import { RegionEditModalComponent } from './regulars/components/admin-panel/region/region-edit-modal/region-edit-modal.component';
import { RegionListComponent } from './regulars/components/admin-panel/region/region-list/region-list.component';
import { SupplierComponent } from './regulars/components/admin-panel/supplier/supplier.component';
import { SupplierCreateComponent } from './regulars/components/admin-panel/supplier/supplier-create/supplier-create.component';
import { SupplierEditModalComponent } from './regulars/components/admin-panel/supplier/supplier-edit-modal/supplier-edit-modal.component';
import { SupplierListComponent } from './regulars/components/admin-panel/supplier/supplier-list/supplier-list.component';
import { SupplierDetailComponent } from './regulars/components/admin-panel/supplier/supplier-detail/supplier-detail.component';
import { LowStockComponent } from './regulars/components/admin-panel/stock/low-stock/low-stock.component';
import { SignUpComponent } from './regulars/components/user-auth/sign-up/sign-up.component';
import { BookRequestReportComponent } from './regulars/components/admin-panel/report/book-request-report/book-request-report.component';
import { RequestExceedPageComponent } from './regulars/components/request-exceed-page/request-exceed-page.component';
import { LogAuditComponent } from './regulars/components/admin-panel/report/log-audit/log-audit.component';
import { LogAuditListComponent } from './regulars/components/admin-panel/report/log-audit/log-audit-list/log-audit-list.component';
import { LogAuditDetailComponent } from './regulars/components/admin-panel/report/log-audit/log-audit-detail/log-audit-detail.component';
import { StockCreateComponent } from './regulars/components/admin-panel/stock/stock-create/stock-create.component';
import { SubcityStoreDashboardComponent } from './regulars/components/admin-panel/dashboard/subcity-store-dashboard/subcity-store-dashboard.component';
import { SubcityStoreComponent } from './regulars/components/admin-panel/subcity-store/subcity-store.component';
import { SubcityStoreListComponent } from './regulars/components/admin-panel/subcity-store/subcity-store-list/subcity-store-list.component';
import { SubcityStoreDetailComponent } from './regulars/components/admin-panel/subcity-store/subcity-store-detail/subcity-store-detail.component';
import { SubcityStoreRequestListComponent } from './regulars/components/admin-panel/subcity-store/subcity-store-request-list/subcity-store-request-list.component';
import { SubcityStoreRequestDetailComponent } from './regulars/components/admin-panel/subcity-store/subcity-store-request-detail/subcity-store-request-detail.component';
import { PurchaseBookReportComponent } from './regulars/components/admin-panel/report/purchase-book-report/purchase-book-report.component';
import { BankReceiptVoucherComponent } from './regulars/components/admin-panel/report/bank-receipt-voucher/bank-receipt-voucher.component';
import { BankReceiptVoucherCreateComponent } from './regulars/components/admin-panel/report/bank-receipt-voucher/bank-receipt-voucher-create/bank-receipt-voucher-create.component';
import { BankReceiptVoucherDetailComponent } from './regulars/components/admin-panel/report/bank-receipt-voucher/bank-receipt-voucher-detail/bank-receipt-voucher-detail.component';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { DataTablesModule } from "angular-datatables";
import { BudgetReportComponent } from './regulars/components/admin-panel/report/budget-report/budget-report.component';

export function HttpLoaderFactory(http:HttpClient){
  return new TranslateHttpLoader(http);
}

@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    AsideComponent,
    FooterComponent,
    LoginComponent,
    UserAuthComponent,
    AdminPanelComponent,
    DashboardComponent,
    PageNotFoundComponent,
    ForbiddenComponent,
    ConfirmModalComponent,

    UserComponent,
    UserCreateComponent,
    UserListComponent,
    UserDetailModalComponent,
    UserEditModalComponent,

    RoleComponent,
    RoleCreateComponent,
    RoleListComponent,
    RoleEditModalComponent,
    RoleDetailModalComponent,

    BookComponent,
    BookListComponent,
    BookEditModalComponent,
    BookDetailModalComponent,

    SchoolComponent,
    SchoolCreateComponent,
    SchoolListComponent,
    SchoolEditModalComponent,
    SchoolDetailModalComponent,
    OrderComponent,
    OrderCreateComponent,
    OrderListComponent,
    OrderEditModalComponent,
    OrderDetailModalComponent,
    ReceiveComponent,
    ReceiveCreateComponent,
    ReceiveListComponent,
    ReceiveEditModalComponent,
    ReceiveDetailModalComponent,
    SaleComponent,
    SaleCreateComponent,
    SaleListComponent,
    SaleEditModalComponent,
    SaleDetailModalComponent,
    BookCreateComponent,
    PendingOrderListComponent,
    OrderFileUploadModalComponent,
    PurchaseCreateComponent,
    PurchaseListComponent,
    PurchaseDetailComponent,
    ReportComponent,
    SalesReportComponent,
    PurchaseBookComponent,
    PurchaseBookCreateComponent,
    PurchaseBookListComponent,
    PurchaseBookDetailComponent,
    PurchaseBookCreateModalComponent,
    AdminDashboardComponent,
    StockComponent,
    BookstoreStockComponent,
    BookstoreEmployeeDashboardComponent,
    SchoolStockComponent,
    SchoolBulkImportComponent,
    SchoolDashboardComponent,
    ManagerDashboardComponent,
    PurchaseDashboardComponent,
    PurchaseReportComponent,
    ConfirmRejectModalComponent,
    SubjectComponent,
    SubjectCreateComponent,
    SubjectEditModalComponent,
    SubjectListComponent,
    LanguageComponent,
    LanguageCreateComponent,
    LanguageEditModalComponent,
    LanguageListComponent,
    GradeLevelComponent,
    GradeLevelCreateComponent,
    GradeLevelEditModalComponent,
    GradeLevelListComponent,
    SubcityComponent,
    SubcityCreateComponent,
    SubcityEditModalComponent,
    SubcityListComponent,
    BudgetComponent,
    BudgetCreateComponent,
    BudgetEditModalComponent,
    BudgetListComponent,
    BankSlipComponent,
    WoredaComponent,
    WoredaCreateComponent,
    WoredaEditModalComponent,
    WoredaListComponent,
    Woreda1Component,
    Woreda1CreateComponent,
    Woreda1EditModalComponent,
    Woreda1ListComponent,
    SchoolLevelComponent,
    SchoolOwnershipComponent,
    SchoolLevelCreateComponent,
    SchoolLevelEditModalComponent,
    SchoolLevelListComponent,
    SchoolOwnershipCreateComponent,
    SchoolOwnershipEditModalComponent,
    SchoolOwnershipListComponent,
    RegionComponent,
    RegionCreateComponent,
    RegionEditModalComponent,
    RegionListComponent,
    SupplierComponent,
    SupplierCreateComponent,
    SupplierEditModalComponent,
    SupplierListComponent,
    SupplierDetailComponent,
    LowStockComponent,
    SignUpComponent,
    BookRequestReportComponent,
    RequestExceedPageComponent,
    LogAuditComponent,
    LogAuditListComponent,
    LogAuditDetailComponent,
    StockCreateComponent,
    SubcityStoreDashboardComponent,
    SubcityStoreComponent,
    SubcityStoreListComponent,
    SubcityStoreDetailComponent,
    SubcityStoreRequestListComponent,
    SubcityStoreRequestDetailComponent,
    PurchaseBookReportComponent,
    BankReceiptVoucherComponent,
    BankReceiptVoucherCreateComponent,
    BankReceiptVoucherDetailComponent,
    BudgetReportComponent,
  ],
  imports: [
    BrowserModule,
    DataTablesModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    ToastrModule.forRoot(),
    NgxSpinnerModule,
    ModalModule.forRoot(),
    BsDatepickerModule.forRoot(),
    NgSelectModule,
    // NgxDatesPickerModule,
    NgxChartsModule,
    NgxPaginationModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient],
      },
    }),
  ],
  providers: [
    AuthGuard,
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
  ],
  bootstrap: [AppComponent],
  entryComponents: [
    ConfirmModalComponent,

    UserDetailModalComponent,
    UserEditModalComponent,

    RoleEditModalComponent,
    RoleDetailModalComponent,

    BookEditModalComponent,
    BookDetailModalComponent,

    SchoolEditModalComponent,
    SchoolDetailModalComponent,

    OrderDetailModalComponent,
    OrderFileUploadModalComponent,
  ],
})
export class AppModule {}
