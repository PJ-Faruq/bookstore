using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TusharInventory.Entitiey.ViewModels.AppViewModels
{
    public class SearchVm
    {
        public int? RoomId { get; set; }
        public int? PlaceId { get; set; }
        public int? LocationId { get; set; }
        public int? TechnicianId { get; set; }
        public int? CategoryId { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }

    }
}
