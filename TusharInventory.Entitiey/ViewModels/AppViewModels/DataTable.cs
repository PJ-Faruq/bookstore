using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TusharInventory.Entity.ViewModels.AppViewModels
{
    public class DataTable<TSearchVm, TEntityModel> where TSearchVm : class where TEntityModel : class, new()
    {
        public long? Id { get; set; }
        public long? UserId { get; set; }
        public int? SerialNo { get; set; }

        public bool? CanCreate { get; set; }
        public bool? CanUpdate { get; set; }
        public bool? CanView { get; set; }
        public bool? CanDelete { get; set; }

        public string SortField { get; set; }
        public string SortOrder { get; set; }
        public string Filter { get; set; }

        public int? Count { get; set; }
        public int CurrentPage { get; set; }
        public int ItemsPerPage { get; set; }

        public int StartPoint => (CurrentPage - 1) * ItemsPerPage;
        public int EndPoint => StartPoint + ItemsPerPage;

        public ICollection<TEntityModel> DataList { get; set; }
        public TSearchVm SearchModel { get; set; }

    }
}
