﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TusharInventory.Entity.ViewModels
{
    public class SchoolBulkVm
    {

        public string Subcity { get; set; }
        public string Woreda1 { get; set; }
        public string Woreda { get; set; }
        public string SchoolName { get; set; }
        public string SchoolLevel { get; set; }
        public string SchoolOwnership { get; set; }

    }
}
