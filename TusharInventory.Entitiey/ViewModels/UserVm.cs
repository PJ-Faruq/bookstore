﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TusharInventory.Entitiey.ViewModels
{
    public class UserVm
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Role { get; set; }
        public string Token { get; set; }
        public string Image { get; set; }
        public int? SchoolId { get; set; }

        public int ? RoleId { get; set; }


    }
}
