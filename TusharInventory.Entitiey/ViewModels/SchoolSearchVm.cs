﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TusharInventory.Entity.ViewModels
{
    public class SchoolSearchVm
    {
        public int? Id { get; set; }
        private string _Name;
        public string Name
        {
            get;
            set;
        }


        private byte[] _NameFile;

        public byte[] NameFile
        {
            get { return _NameFile; }
            set { _NameFile = _Name != null ? Encoding.UTF8.GetBytes(Name) : null; }
        }


        //public byte[] NameFile { get; set; }

        public int? SchoolSubcityId { get; set; }

        public int? SchoolWoredaId { get; set; }

        public int? SchoolLavelId { get; set; }

        public int? SchoolOwnershipId { get; set; }

        public int? Woreda1Id { get; set; }
    }
}
