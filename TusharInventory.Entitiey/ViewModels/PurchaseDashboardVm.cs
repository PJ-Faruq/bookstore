﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TusharInventory.Entity.ViewModels
{
    public class PurchaseDashboardVm
    {
        public int Id { get; set; }
        public int PendingRequest { get; set; }

        public int CompletedRequest { get; set; }

        public int TotalRequest { get; set; }
        public double TotalPurchase { get; set; }
        public double TotalSupplier { get; set; }
        public double LowStock { get; set; }

        public List<ChartVm> BudgetChartdata { get; set; }

    }
}
