﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TusharInventory.Entity.ViewModels
{
    public class SubcityStockSearchVm
    {
        public int? Id { get; set; }
        public int? SubcityId { get; set; }
        public int? BookSubjectId { get; set; }
        public int? BookGradeId { get; set; }
        public int? BookLanguageId { get; set; }
        public int? StockStatusId { get; set; }
        public int? UserId { get; set; }
    }
}
