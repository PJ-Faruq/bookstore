﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TusharInventory.Entity.ViewModels
{
    public class BookReportVm
    {
        public int? SchoolId { get; set; }
        public DateTime? FromDate { get; set; } 
        public DateTime? ToDate { get; set; }
        public int? Status { get; set; }
        public string TrackingNo { get; set; }
    }
}
