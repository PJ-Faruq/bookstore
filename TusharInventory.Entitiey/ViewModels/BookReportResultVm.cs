﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TusharInventory.Entity.ViewModels
{
    public class BookReportResultVm
    {
        public string OrderId { get; set; }
        public string TrackingNo { get; set; }
        public string OrderDate { get; set; }
        public string SchoolName { get; set; }
        public string RequestedBy { get; set; }
        public string RequestedTo { get; set; }
        public string ApprovedBy { get; set; }
        public double Amount { get; set; }
        public string Status { get; set; }
        public string ReasonForReject { get; set; }
        public string BankSlip { get; set; }
        public string Note { get; set; }


    }
}
