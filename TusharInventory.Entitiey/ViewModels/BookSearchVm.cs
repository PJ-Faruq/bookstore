﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace TusharInventory.Entity.ViewModels
{
    public class BookSearchVm
    {
        public int? Id { get; set; }

        public int? SchoolId { get; set; }

        public int? BookSubjectId { get; set; }

        public int? BookLanguageId { get; set; }


        public int? BookGradeLavelId { get; set; }


        public double? PurchasePrice { get; set; }

        public double? SalePrice { get; set; }
    }
}
