﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TusharInventory.Entity.ViewModels
{
    public class SubcityOrderSearchVm
    {
        public int UserId { get; set; }
        public string RoleName { get; set; }
        public string Status { get; set; }
        public string RequestId { get; set; }
        
    }
}
