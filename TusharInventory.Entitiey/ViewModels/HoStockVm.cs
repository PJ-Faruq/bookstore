﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TusharInventory.Entity.ViewModels
{
    public class HoStockVm
    {
        public int BookId { get; set; }
        public string BookSubject { get; set; }
        public string BookGrade { get; set; }
        public string BookLanguage { get; set; }
        public double AvailableQty { get; set; }
        public double PurchasePrice { get; set; }
        public double SalePrice { get; set; }
    }
}
