﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TusharInventory.Entity.ViewModels
{
    public class LogSearchVm
    {
        public int? UserId { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public int? BudgetTypeId { get; set; }
    }
}
