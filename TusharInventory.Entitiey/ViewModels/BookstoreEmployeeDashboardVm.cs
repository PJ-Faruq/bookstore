﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TusharInventory.Entity.ViewModels
{
    public class BookstoreEmployeeDashboardVm
    {
        public int Id { get; set; }
        public int PendingRequest { get; set; }
        public int RejectedRequest { get; set; }
        public int ApprovedRequest { get; set; }
        public double CompletedRequest { get; set; }
        public int UnderReviewRequest { get; set; }
        public int PendingWithBankSlipRequest { get; set; }
        public double TotalRequest { get; set; }
 
        public double TotalBook { get; set; }
        public int TotalSchool { get; set; }
        public int TotalPurchase { get; set; }
        public int TotalBankSlip { get; set; }
        public double PendingForOutOfStock { get; set; }


        public double PendingSubcityStoreRequest { get; set; }
        public double ApprovedSubcityStoreRequest { get; set; }
        public double CompletedSubcityStoreRequest { get; set; }
        public double RejectedSubcityStoreRequest { get; set; }
        public double TotalSubcityStoreRequest { get; set; }


    }
}
