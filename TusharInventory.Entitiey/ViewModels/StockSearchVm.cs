﻿using System;
using System.Collections.Generic;
using System.Text;
using TusharInventory.Entity.Models.Basic;

namespace TusharInventory.Entity.ViewModels
{
    public class StockSearchVm
    {
        public int? Id { get; set; }
        public int? SubcityId { get; set; }
        public Subcity Subcity { get; set; }

        public int BookSubjectId { get; set; }
        public Subject BookSubject { get; set; }

        public int BookGradeId { get; set; }
        public GradeLavel BookGrade { get; set; }

        public int BookLanguageId { get; set; }
        public Language BookLanguage { get; set; }
        public double Quantity { get; set; }
        public double AvailableQty { get; set; }
    }
}
