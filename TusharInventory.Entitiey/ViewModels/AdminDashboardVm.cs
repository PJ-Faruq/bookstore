﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TusharInventory.Entity.ViewModels
{
    public class AdminDashboardVm
    {
        public AdminDashboardVm()
        {
            SubjectChartdata = new List<ChartVm>();
            GradeChartdata = new List<ChartVm>();
            LanguageChartdata = new List<ChartVm>();
        }
        public int Id { get; set; }
        public int PendingRequest { get; set; }
        public int RejectedRequest { get; set; }
        public int ApprovedRequest { get; set; }
        public int TodayRequest { get; set; }
        public int ThisWeekRequest { get; set; }
        public int TotalRequest { get; set; }
        public double TotalBook { get; set; }
        public int TotalSchool { get; set; }
        public int TotalActiveUsers { get; set; }
        public int TotalUserPendingRequest { get; set; }


        public List<ChartVm> WeekChartData { get; set; }
        public List<ChartVm> MonthChartData { get; set; }
        public List<ChartVm> YearChartData { get; set; }

        public List<ChartVm> SubjectChartdata { get; set; }
        public List<ChartVm> GradeChartdata { get; set; }
        public List<ChartVm> LanguageChartdata { get; set; }

        public List<ChartVm> UserInfoChartdata { get; set; }

    }
}
