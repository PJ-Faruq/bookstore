﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TusharInventory.Entity.ViewModels
{
    public class ManagerDashboardVm
    {

        public ManagerDashboardVm()
        {
            SubjectChartdata = new List<ChartVm>();
            GradeChartdata = new List<ChartVm>();
            LanguageChartdata = new List<ChartVm>();
        }

        public int Id { get; set; }
        public int PendingRequest { get; set; }
        public int RejectedRequest { get; set; }
        public int ApprovedRequest { get; set; }
        public int CompletedRequest { get; set; }
        public int TotalRequest { get; set; }

        public int PurchasePendingRequest { get; set; }
        public int PurchaseRejectedRequest { get; set; }
        public int PurchaseApprovedRequest { get; set; }
        public int PurchaseTotalRequest { get; set; }
        public double TotalBook { get; set; }
        public int TotalPurchase { get; set; }


        public double PendingSubcityStoreRequest { get; set; }
        public double ApprovedSubcityStoreRequest { get; set; }
        public double CompletedSubcityStoreRequest { get; set; }
        public double RejectedSubcityStoreRequest { get; set; }
        public double TotalSubcityStoreRequest { get; set; }


        public List<ChartVm> WeekChartData { get; set; }
        public List<ChartVm> MonthChartData { get; set; }
        public List<ChartVm> YearChartData { get; set; }

        public List<ChartVm> SubjectChartdata { get; set; }
        public List<ChartVm> GradeChartdata { get; set; }
        public List<ChartVm> LanguageChartdata { get; set; }

        public List<ChartVm> UserInfoChartdata { get; set; }
        public List<ChartVm> BudgetChartdata { get; set; }
    }
}
