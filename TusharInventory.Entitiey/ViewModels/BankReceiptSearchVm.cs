﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TusharInventory.Entity.ViewModels
{
    public class BankReceiptSearchVm
    {
        public int OrderId { get; set; }
        public string OrderNo { get; set; }
        public int? BankReceiptId { get; set; }
        public string SchoolName { get; set; }
        public double TotalAmount { get; set; }
        public string File { get; set; }
    }
}
