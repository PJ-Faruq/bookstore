﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TusharInventory.Entity.ViewModels
{
    public class SchoolDashboardData
    {
        public int Id { get; set; }
        public int PendingRequest { get; set; }
        public int RejectedRequest { get; set; }
        public int ApprovedRequest { get; set; }
        public double CompletedRequest { get; set; }
        public int TotalRequest { get; set; }
        public double TotalBook { get; set; }
    }
}
