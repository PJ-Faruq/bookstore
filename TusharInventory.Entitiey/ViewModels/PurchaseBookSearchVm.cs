﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TusharInventory.Entity.ViewModels
{
    public class PurchaseBookSearchVm
    {
        public int? BudgetTypeId { get; set; }
        public int? SupplierId { get; set; }
        public string PurchaseId { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public string TransactionTrackingNumber { get; set; }
        public string ReferenceNumber { get; set; }

    }
}
