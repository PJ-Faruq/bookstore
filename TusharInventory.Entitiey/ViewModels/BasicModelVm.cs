﻿using System;
using System.Collections.Generic;
using System.Text;
using TusharInventory.Entity.ConfigurationModels;
using TusharInventory.Entity.Models.Basic;

namespace TusharInventory.Entity.ViewModels
{
    public class BasicModelVm: EntityModel
    {
        public string Name { get; set; }
        public byte[] NameFile { get; set; }

    }
}
