﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TusharInventory.Entity.ViewModels
{
    public class ChartVm
    {
        public string name { get; set; }
        public double value { get; set; }
    }
}
