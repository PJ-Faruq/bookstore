﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TusharInventory.Entity.ViewModels
{
    public class StockVm
    {
        public int Id { get; set; }
        public int BookSubjectId { get; set; }
        public int BookGradeId { get; set; }
        public int BookLanguageId { get; set; }
        public double AvailableQty { get; set; }
        public double AllocatedQty { get; set; }
    }
}
