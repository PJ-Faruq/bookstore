﻿
using System;
using System.Collections.Generic;
using System.Text;

namespace TusharInventory.Entity.ViewModels
{
    public class SubcityDashboardVm
    {
        public int Id { get; set; }
        public double ApprovedRequest { get; set; }
        public double CompletedRequest { get; set; }
        public double TotalBook { get; set; }
    }
}
