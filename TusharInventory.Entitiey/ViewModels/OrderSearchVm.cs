﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TusharInventory.Entity.ViewModels
{
    public class OrderSearchVm
    {
        public int? CurrentUserId { get; set; }
        public string RoleName { get;set; }

        public string AdminStatus { get; set; }
        public string EmpStatus { get; set; }
        public string SchoolStatus { get; set; }
        public string ManagerStatus { get; set; }
        public bool isNeedBankSlip { get; set; }
        public string SubcityStatus { get; set; }
    }
}
