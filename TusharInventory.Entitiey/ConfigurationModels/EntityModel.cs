﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using TusharInventory.Entity.IModels;

namespace TusharInventory.Entity.ConfigurationModels
{
    public class EntityModel : IEntity
    {
        [Key]
        public int Id { get; set; }
    }
}
