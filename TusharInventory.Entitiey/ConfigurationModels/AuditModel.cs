using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TusharInventory.Entity.IModels;

namespace TusharInventory.Entity.ConfigurationModels
{
    public class AuditModel : EntityModel, IEntity
    {
        public string CreatedBy { get; set; }
        public DateTime CreateDate { get; set; }
        public string LastModifiedBy { get; set; }
        public DateTime LastModifiedDate { get; set; }

    }
}
