using System;
using System.Collections.Generic;
using System.Text;

namespace TusharInventory.Entity.IModels
{
  public interface IEntity
  {
     int Id { get; set; }
  }
}
