﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TusharInventory.Entity.Models.Settings
{
    public class UserActivity
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string Url { get; set; }
        public string UserName { get; set; }
        public string Data { get; set; }
        public DateTime Date { get; set; } = DateTime.Now;
    }
}
