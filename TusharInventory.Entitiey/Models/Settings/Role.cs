﻿using System;
using System.Collections.Generic;
using System.Text;
using TusharInventory.Entity.ConfigurationModels;

namespace TusharInventory.Entity.Models.Settings
{
    public class Role : EntityModel
    {
        public string Name { get; set; }
    }
}
