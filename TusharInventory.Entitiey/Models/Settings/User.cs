﻿using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TusharInventory.Entity.ConfigurationModels;
using TusharInventory.Entity.Models.Basic;
namespace TusharInventory.Entity.Models.Settings
{
    public class User : EntityModel
    {
        [NotMapped]
        public string FullName { get { return FirstName + " " + SecondName + " " + LastName; } }


        public string FirstName { get; set; }


        public string SecondName { get; set; }


        public string LastName { get; set; }


        [Required]
        public string Email { get; set; }

        [Required]
        public string Password { get; set; }

        [Required]
        public int RoleId { get; set; }
        public Role Role { get; set; }


        public string Photograph { get; set; }


        public string EmployeeId { get; set; }

        public int? SchoolId { get; set; }
        public School School { get; set; }

        public string Address { get; set; }

        public bool IsActive { get; set; }
        public bool IsApproved { get; set; }

        public string PhoneNumber { get; set; }


        public int? WoredaId { get; set; }
        public Woreda Woreda { get; set; }


        public int? RegionId { get; set; }
        public Region Region { get; set; }



        public int? SubcityId { get; set; }
        public Subcity Subcity { get; set; }

        public int? SchoolLavelId { get; set; }
        public SchoolLavel SchoolLavel { get; set; }

        public int? OwnershipId { get; set; }
        public Ownership Ownership { get; set; }




    }
}
