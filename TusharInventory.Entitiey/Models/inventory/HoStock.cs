﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using TusharInventory.Entity.ConfigurationModels;
using TusharInventory.Entity.Models.Basic;

namespace TusharInventory.Entity.Models.inventory
{
    public class HoStock : AuditModel
    {
        public int HoId { get; set; }
        public HeadOffice Ho { get; set; }

        [Required]
        public int BookSubjectId { get; set; }
        public Subject BookSubject { get; set; }

        [Required]
        public int BookGradeId { get; set; }
        public GradeLavel BookGrade { get; set; }

        [Required]
        public int BookLanguageId { get; set; }
        public Language BookLanguage { get; set; }

        [Required]
        public double AvailableQty { get; set; }
        [Required]
        public double AllocatedQty { get; set; }
    }
}
