﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using TusharInventory.Entity.ConfigurationModels;

namespace TusharInventory.Entity.Models.Basic
{
    public class Ownership : EntityModel
    {
        private string _Name;

        [NotMapped]
        public string Name
        {
            get { return NameFile != null ? Encoding.UTF8.GetString(NameFile) : _Name; }
            set { _Name = value; }
        }
        public byte[] NameFile { get; set; }
    }
}
