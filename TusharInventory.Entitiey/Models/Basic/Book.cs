﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using TusharInventory.Entity.ConfigurationModels;

namespace TusharInventory.Entity.Models.Basic
{
    public class Book : EntityModel
    {

        [Required]
        public int BookSubjectId { get; set; }
        public Subject BookSubject { get; set; }

        [Required]
        public int BookLanguageId { get; set; }
        public Language BookLanguage { get; set; }

        [Required]
        public int BookGradeLavelId { get; set; }
        public GradeLavel BookGradeLavel { get; set; }


        [Required]
        public double PurchasePrice { get; set; }

        [Required]
        public double SalePrice { get; set; }
        public double PageNo { get; set; }

    }
}
