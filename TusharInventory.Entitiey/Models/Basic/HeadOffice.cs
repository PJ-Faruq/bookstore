﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using TusharInventory.Entity.ConfigurationModels;

namespace TusharInventory.Entity.Models.Basic
{
    public class HeadOffice : EntityModel
    {
        [Required]
        public string Name { get; set; }
    }
}
