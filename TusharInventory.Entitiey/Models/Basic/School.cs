﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using TusharInventory.Entity.ConfigurationModels;

namespace TusharInventory.Entity.Models.Basic
{
    public class School : EntityModel
    {
        
        private string _Name;

        [NotMapped]
        public string Name
        {
            get { return NameFile != null ? Encoding.UTF8.GetString(NameFile) : _Name; }
            set { _Name = value; }
        }


        //private byte[] _NameFile;

        //public byte[] NameFile
        //{
        //    get { return _NameFile; }
        //    set { _NameFile= _Name !=null? Encoding.UTF8.GetBytes(Name):null; }
        //}


        public byte[] NameFile { get; set; }

        public double? NumberOfStudent { get; set; }

        public int? SchoolSubcityId { get; set; }
        public Subcity SchoolSubcity { get; set; }

        public int? SchoolWoredaId { get; set; }
        public Woreda SchoolWoreda { get; set; }

        public int? SchoolLavelId { get; set; }
        public SchoolLavel SchoolLavel { get; set; }

        public int? SchoolOwnershipId { get; set; }
        public Ownership SchoolOwnership { get; set; }

        public int? Woreda1Id { get; set; }
        public Woreda1 Woreda1 { get; set; }

    }
}
