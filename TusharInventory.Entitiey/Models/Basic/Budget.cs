﻿using System;
using System.Collections.Generic;
using System.Text;
using TusharInventory.Entity.ConfigurationModels;

namespace TusharInventory.Entity.Models.Basic
{
    public class Budget:EntityModel
    {
        public int? BudgetTypeId  { get; set; }
        public BudgetType BudgetType { get; set; }
        public double Amount { get; set; }
        public DateTime BudgetDate { get; set; }
        public string Description { get; set; }
        public string CompanyName { get; set; }
        public string CompanyAddress { get; set; }
    }
}
