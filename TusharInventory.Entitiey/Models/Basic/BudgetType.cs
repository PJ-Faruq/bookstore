﻿using System;
using System.Collections.Generic;
using System.Text;
using TusharInventory.Entity.ConfigurationModels;
using TusharInventory.Entity.Models.Operation;

namespace TusharInventory.Entity.Models.Basic
{
    public class BudgetType :EntityModel
    {
        public string Name { get; set; }

        public ICollection<Budget> Budgets { get; set; }
        public ICollection<PurchaseBook> PurchaseBooks { get; set; }
    }
}
