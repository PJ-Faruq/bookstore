﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using TusharInventory.Entity.ConfigurationModels;
using TusharInventory.Entity.Models.Basic;

namespace TusharInventory.Entity.Models.Operation
{
    public class Sale : AuditModel
    {
        public int HoId { get; set; }
        public HeadOffice Ho { get; set; }

        [Required]
        public int OrderId { get; set; }
        public Order Order { get; set; }

        public DateTime SaleDate { get; set; }
        public double Amount { get; set; }
        public string Note { get; set; }
        public int Status { get; set; }

        public ICollection<SaleLine> SaleLines { get; set; }
    }
}
