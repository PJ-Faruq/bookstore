﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using TusharInventory.Entity.ConfigurationModels;
using TusharInventory.Entity.Models.Basic;

namespace TusharInventory.Entity.Models.Operation
{
    public class ReceiveLine : EntityModel
    {
        [Required]
        public int ReceiveId { get; set; }
        public Receive Receive { get; set; }

        [Required]
        public int BookSubjectId { get; set; }
        public Subject BookSubject { get; set; }

        [Required]
        public int BookGradeId { get; set; }
        public GradeLavel BookGrade { get; set; }
         
        [Required]
        public int BookLanguageId { get; set; }
        public Language BookLanguage { get; set; }

        [Required]
        public double OrderedQuantity { get; set; }

        [Required]
        public double ReceivedQuantity { get; set; }
    }
}
