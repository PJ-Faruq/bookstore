﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using TusharInventory.Entity.ConfigurationModels;
using TusharInventory.Entity.Models.Basic;
using TusharInventory.Entity.Models.Settings;

namespace TusharInventory.Entity.Models.Operation
{
    public class PurchaseBook: AuditModel
    {

        public int? PurchaseRequestId { get; set; }
        public Purchase PurchaseRequest { get; set; }

        public int? BudgetTypeId { get; set; }
        public BudgetType BudgetType { get; set; }

        public int SupplierId { get; set; }
        public Supplier Supplier { get; set; }

        public string PurchaseId { get; set; }

        public int? BookstoreEmployeeId { get; set; }
        public User BookstoreEmployee { get; set; }

 
        public int? BookstoreManagerId { get; set; }
        public User BookstoreManager { get; set; }


        [Required]
        public int PurcahseDeptEmployeeId { get; set; }
        public User PurcahseDeptEmployee { get; set; }

        public DateTime PurchaseDate { get; set; }

        [Required]
        public double Amount { get; set; }

        public string TransactionTrackingNumber { get; set; }
        public string ReferenceNumber { get; set; }

        public ICollection<PurchaseBookLine> PurchaseBookLines { get; set; }
    }
}
