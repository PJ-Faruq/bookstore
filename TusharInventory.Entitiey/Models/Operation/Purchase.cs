﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using TusharInventory.Entity.ConfigurationModels;
using TusharInventory.Entity.Models.Settings;

namespace TusharInventory.Entity.Models.Operation
{
    public class Purchase : AuditModel
    {
        [Required]
        public int BookstoreEmployeeId { get; set; }
        public User BookstoreEmployee { get; set; }

        public int? BookstoreManagerId { get; set; }
        public User BookstoreManager { get; set; }


        public int? PurcahseDeptEmployeeId { get; set; }
        public User PurcahseDeptEmployee { get; set; }

        public DateTime? Date { get; set; }

        [Required]
        public int Status { get; set; }

        public double Amount { get; set; }

        public ICollection<PurchaseLine> PurchaseLines { get; set; }
    }
}
