﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using TusharInventory.Entity.Models.Basic;
using TusharInventory.Entity.Models.Settings;

namespace TusharInventory.Entity.Models.Operation
{
    public class BankReceiptVoucher
    {
        public int Id { get; set; }


        public int? OrderId { get; set; }
        public Order Order { get; set; }

        public DateTime Date { get; set; }
        public int SubcityId { get; set; }
        public Subcity Subcity { get; set; }

        public string Kebele { get; set; }
        public int SchoolId { get; set; }
        public School School { get; set; }

        public int BookstoreEmployeeId { get; set; }
        public User BookstoreEmployee { get; set; }

        public string City { get; set; }
        public string HouseNumber { get; set; }

        public bool Cash { get; set; }
        public bool Check { get; set; }
        public string CheckNo { get; set; }
        public bool DepositeSlip { get; set; }
        public string ReferenceNo { get; set; }
        public DateTime? DepositeDate { get; set; }

        public bool BankTransfer { get; set; }
        public double Amount { get; set; }








    }
}
