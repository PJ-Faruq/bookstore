﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using TusharInventory.Entity.Models.Basic;

namespace TusharInventory.Entity.Models.Operation
{
    public class SubcityOrderLine
    {
        public int Id { get; set; }
        [Required]
        public int SubcityOrderId { get; set; }
        public SubcityOrder SubcityOrder { get; set; }

        [Required]
        public int BookSubjectId { get; set; }
        public Subject BookSubject { get; set; }

        [Required]
        public int BookGradeLavelId { get; set; }
        public GradeLavel BookGradeLavel { get; set; }

        [Required]
        public int BookLanguageId { get; set; }
        public Language BookLanguage { get; set; }

        [Required]
        public double Quantity { get; set; }

        [NotMapped]
        public double MainStockQty { get; set; }
        public double? SubcityStockQty { get; set; }
    }
}
