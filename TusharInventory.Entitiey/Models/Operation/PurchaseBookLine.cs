﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using TusharInventory.Entity.ConfigurationModels;
using TusharInventory.Entity.Models.Basic;

namespace TusharInventory.Entity.Models.Operation
{
    public class PurchaseBookLine :EntityModel
    {
        [Required]
        public int PurchaseBookId { get; set; }
        public PurchaseBook PurchaseBook { get; set; }

        [Required]
        public int BookSubjectId { get; set; }
        public Subject BookSubject { get; set; }

        [Required]
        public int BookGradeLavelId { get; set; }
        public GradeLavel BookGradeLavel { get; set; }

        [Required]
        public int BookLanguageId { get; set; }
        public Language BookLanguage { get; set; }

        public double BookPrice { get; set; }

        [Required]
        public double Quantity { get; set; }

    }
}
