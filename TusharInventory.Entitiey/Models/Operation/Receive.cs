﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using TusharInventory.Entity.ConfigurationModels;
using TusharInventory.Entity.Models.Basic;

namespace TusharInventory.Entity.Models.Operation
{
    public class Receive : AuditModel
    {
        [Required]
        public bool IsSchool { get; set; }

        public int OrderId { get; set; }
        public Order Order { get; set; }

        public int SaleId { get; set; }
        public Sale Sale { get; set; }

        public int? SchoolId { get; set; } //if isSchool true the it will required
        public School School { get; set; }

        public int? HoId { get; set; } //if isSchool false the it will required
        public HeadOffice Ho { get; set; }



        public DateTime ReceiveDate { get; set; }
        public double Amount { get; set; }
        public string Note { get; set; }
        public int Status { get; set; }

        public ICollection<ReceiveLine> ReceiveLines { get; set; }

    }
}
