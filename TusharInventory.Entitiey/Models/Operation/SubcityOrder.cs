﻿using System;
using System.Collections.Generic;
using System.Text;
using TusharInventory.Entity.Models.Basic;
using TusharInventory.Entity.Models.Settings;

namespace TusharInventory.Entity.Models.Operation
{
    public class SubcityOrder
    {
        public int Id { get; set; }
        public string RequestId { get; set; }
        public DateTime Date { get; set; }
        public int BookstoreManagerId { get; set; }
        public User BookstoreManager { get; set; }

        public int? BookstoreEmployeeId { get; set; }
        public User BookstoreEmployee { get; set; }
        public int SubcityManagerId { get; set; }
        public User SubcityManager { get; set; }

        public int? SubcityId { get; set; }
        public Subcity Subcity { get; set; }
        public int Status { get; set; }
        public ICollection<SubcityOrderLine> OrderLines { get; set; }

    }
}
