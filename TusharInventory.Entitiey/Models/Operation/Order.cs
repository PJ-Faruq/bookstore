﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using TusharInventory.Entity.ConfigurationModels;
using TusharInventory.Entity.Models.Basic;
using TusharInventory.Entity.Models.Settings;

namespace TusharInventory.Entity.Models.Operation
{
    public class Order : AuditModel
    {
        public string OrderId { get; set; }

        public bool IsSchool { get; set; }

        public int? SchoolId { get; set; } //if isSchool true the it will required
        public School School { get; set; }

        public int? SchoolEmployeeId { get; set; }
        public User SchoolEmployee { get; set; }

        public int? BookstoreManagerId { get; set; }
        public User BookstoreManager { get; set; }

        public int? BookstoreEmployeeId { get; set; }
        public User BookstoreEmployee { get; set; }

        public int? SubcityManagerId { get; set; }
        public User SubcityManager { get; set; }

        public int? HoId { get; set; } //if isSchool false the it will required
        public HeadOffice Ho { get; set; }

        public int? SupplierId { get; set; }
        public Supplier Supplier { get; set; }
        public int? SubcityId { get; set; }
        public Subcity Subcity { get; set; }

        public DateTime OrderDate { get; set; }
        public double Amount { get; set; }
        public string Note { get; set; }
        public int? Status { get; set; }
        public int? BookstoreEmployeeStatus { get; set; }
        public int? BookstoreManagerStatus { get; set; }

        public bool IsRequestedToManger { get; set; }
        public bool IsRequestedToPurchase { get; set; }
        public bool IsCollected { get; set; }

        public string TrackingNo { get; set; }
        public string ReasonForReject { get; set; }
        public string ApprovalInfo { get; set; }

        public string File { get; set; }

        public ICollection<OrderLine> OrderLines { get; set; } 
    }
}
