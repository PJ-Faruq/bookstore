﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TusharInventory.Entitiey.ViewModels;
using TusharInventory.Entity.Models.Settings;
using TusharInventory.Entity.ViewModels;
using TusharInventory.Reporitory.Dbcontext;
using TusharInventory.Reporitory.Service.IServices.Settings;
using TusharInventory.WebApi.Extensions;

namespace TusharInventory.WebApi.Controllers.Settings
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IUserService _service;
        private readonly TusharDbContext _db;

        public UsersController(IUserService service, TusharDbContext db)
        {
            _service = service;
            _db = db;
        }

        [Authorize(Roles = "Main Admin")]
        [HttpGet]
        public async Task<IEnumerable<User>> GetAll()
        {
            return await _service.GetManager();
        }

        [Authorize(Roles = "Main Admin")]
        [HttpGet("GetUserActivity")]
        public async Task<IEnumerable<UserActivity>> GetUserActivity()
        {
            return await _db.UserActivities.Where(m=>m.Date.Date==DateTime.Now.Date).ToListAsync();
        }

        [Authorize(Roles = "Main Admin")]
        [HttpPost("GetUserActivityBySearch")]
        public async Task<IEnumerable<UserActivity>> GetUserActivityBySearch(LogSearchVm vm)
        {
            var activityList = _db.UserActivities.AsQueryable();

            if (vm.UserId != null)
            {
                activityList = activityList.Where(m => m.UserId == vm.UserId).AsQueryable();
            }

            if (vm.FromDate != null)
            {
                string[] dateValues = vm.FromDate.ToString().Split(' ');
                DateTime fromDate = Convert.ToDateTime(dateValues[0]).Date;

                activityList = activityList.Where(m => m.Date.Date >= fromDate.Date).AsQueryable();
            }

            if (vm.ToDate != null)
            {
                string[] dateValues = vm.ToDate.ToString().Split(' ');
                DateTime toDate = Convert.ToDateTime(dateValues[0]).Date;
                activityList = activityList.Where(m => m.Date.Date <= toDate.Date).AsQueryable();
            }
            return await activityList.ToListAsync();
        }

        [HttpGet("GetBookstoreEmployee")]
        public async Task<IEnumerable<User>> GetBookstoreEmployee()
        {
            return await _service.GetBookstoreEmployee();
        }
        


        // GET: api/Users/5
        [HttpGet("{id}")]
        public async Task<ActionResult<User>> GetById(int id)
        {
            try
            {
                var model = await _service.GetByIdAsync(id);

                if (model == null)
                {
                    return NotFound();
                }
                return model;
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError); ;
            }
        }

        [HttpGet("GetUserForAdmin/{id}")]
        public ActionResult<User> GetUserForAdmin(int id)
        {
            try
            {
                var model = _db.Users
                    .Include(m => m.Region)
                    .Include(m => m.Role)
                    .Include(m => m.School)
                    .Include(m => m.Subcity)
                    .Include(m => m.Woreda)
                    .Include(m => m.Ownership)
                    .Include(m => m.SchoolLavel)
                    .FirstOrDefault(m => m.Id == id);

                if (model == null)
                {
                    return NotFound();
                }
                return model;
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError); ;
            }
        }

        [HttpPost("Authenticate")]
        [UserActivityFilter]
        public IActionResult Authenticate([FromBody] UserVm userParam)
        {
            var user = _service.Authenticate(userParam.Email, userParam.Password);

            if (user == null)
                return BadRequest(new { message = "Username or password is incorrect" });

            return Ok(user);
        }

        [HttpPut]
        [UserActivityFilter]
        public async Task<IActionResult> Update()
        {

            try
            {

                User user = new User();

                user.Id = Convert.ToInt32(Request.Form["Id"].ToString());

                user.Email = Request.Form["Email"];
                user.FirstName = Request.Form["FirstName"];
                user.EmployeeId = Request.Form["EmployeeId"];
                
                user.SecondName = Request.Form["SecondName"];
                user.LastName = Request.Form["LastName"];
                user.Address = Request.Form["Address"];
                user.IsActive = Convert.ToBoolean(Request.Form["IsActive"]);
                user.IsApproved = Convert.ToBoolean(Request.Form["IsApproved"]);

                user.Password = Request.Form["Password"];
                user.PhoneNumber = Request.Form["PhoneNumber"];

                user.RegionId = Convert.ToInt32(Request.Form["RegionId"].ToString());

                user.RoleId = Convert.ToInt32(Request.Form["RoleId"].ToString());
                if (Request.Form["SchoolId"].ToString() != "null")
                {
                    user.SchoolId = Convert.ToInt32(Request.Form["SchoolId"].ToString());
                    user.SchoolLavelId = Convert.ToInt32(Request.Form["SchoolLavelId"].ToString());
                    user.OwnershipId = Convert.ToInt32(Request.Form["OwnershipId"].ToString());
                }

                user.SubcityId = Convert.ToInt32(Request.Form["SubcityId"].ToString());
                user.WoredaId = Convert.ToInt32(Request.Form["WoredaId"].ToString());


                user.Photograph = SaveAndGetImagePath(Request.Form.Files["PhotographFile"]);

                if (user.Photograph == "")
                {
                    user.Photograph = Request.Form["Photograph"];
                }

                _db.Attach(user);

                _db.Entry(user).State = EntityState.Modified;
                bool isUpdated= _db.SaveChanges() > 0;

                if (isUpdated)
                {
                    return Ok(isUpdated);
                }

                return Ok(false);

                
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return NoContent();
        }


        [HttpPost]
        [UserActivityFilter]
        public async Task<ActionResult<User>> Save()
        {
            try
            {
              

                User user = new User();

                user.Email = Request.Form["Email"];
                user.FirstName  = Request.Form["FirstName"];
                user.SecondName = Request.Form["SecondName"];
                user.LastName = Request.Form["LastName"];
                user.Address = Request.Form["Address"];
                user.IsActive=Convert.ToBoolean(Request.Form["IsActive"]);
                user.IsApproved = Convert.ToBoolean(Request.Form["IsApproved"]);

                user.Password= Request.Form["Password"];
                user.PhoneNumber= Request.Form["PhoneNumber"];
                
                user.RegionId= Convert.ToInt32(Request.Form["RegionId"].ToString());

                user.RoleId= Convert.ToInt32(Request.Form["RoleId"].ToString());
                if(Request.Form["SchoolId"].ToString() != "null")
                {
                    user.SchoolId = Convert.ToInt32(Request.Form["SchoolId"].ToString());
                    user.SchoolLavelId = Convert.ToInt32(Request.Form["SchoolLavelId"].ToString());
                    user.OwnershipId = Convert.ToInt32(Request.Form["OwnershipId"].ToString());
                }
                
                user.SubcityId= Convert.ToInt32(Request.Form["SubcityId"].ToString());
                user.WoredaId= Convert.ToInt32(Request.Form["WoredaId"].ToString());


                user.Photograph = SaveAndGetImagePath(Request.Form.Files["PhotographFile"]);
                user.EmployeeId = GenerateCode();

                bool isAdded= await _service.AddAsync(user);

                if (isAdded)
                {
                    return Ok(isAdded);
                }

                return Ok(false);

            }
            catch (Exception ex)
            {
                return StatusCode(500, "Internal Server Error");
            }

            
        }

        [HttpGet("CheckEmail")]

        public User CheckMail(string email)
        {

            var user = _db.Users.Where(x => x.Email == email).Select(x =>
            new User
            {
                Id = x.Id,
                Email = x.Email,

            }).FirstOrDefault();
            return user;
        }

        private string GenerateCode()
        {
            string code = "";
            if (_db.Users.Count() <= 0)
            {
                code = "1";
            }
            else
            {
                code = (_db.Users.Max(m => m.Id) + 1).ToString();
            }

            string generatedCode = code.ToString();

            if (generatedCode.Length == 1)
            {
                generatedCode = "Emp-" + "0" + "0" + code;
            }
            if (code.Length == 2)
            {
                generatedCode = "Emp-" + "0" + code;
            }
            if (code.Length >= 3)
            {
                generatedCode = "Emp-" + code;
            }

            return generatedCode;

        }


        private string SaveAndGetImagePath(IFormFile image)
        {
            var file = image;
            var folderName = Path.Combine("Resources", "Files");
            var pathToSave = Path.Combine(Directory.GetCurrentDirectory(), folderName);

            if (file != null)
            {
                var fileName = Guid.NewGuid().ToString() + "_" + file.FileName;
                //var fileName = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');
                var fullPath = Path.Combine(pathToSave, fileName);
                var dbPath = Path.Combine(folderName, fileName);

                using (var stream = new FileStream(fullPath, FileMode.Create))
                {
                    file.CopyTo(stream);
                }

                return fileName;
            }
            else
            {
                return "";
            }
        }

        // DELETE: api/Users/5
        [HttpDelete("{id}")]
        [UserActivityFilter]
        public async Task<ActionResult<User>> Delete(int id)
        {
            var model = await _service.GetByIdAsync(id);
            if (model == null)
            {
                return NotFound();
            }
            _service.Remove(model);

            return model;
        }
    }
}