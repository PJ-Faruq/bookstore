﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TusharInventory.Entity.Models.Settings;
using TusharInventory.Reporitory.Service.IServices.Settings;

namespace TusharInventory.WebApi.Controllers.Settings
{
    [Route("api/[controller]")]
    [ApiController]
    public class RolesController : ControllerBase
    {
        private readonly IRoleService _service;

        public RolesController(IRoleService service)
        {
            _service = service;
        }

        // GET: api/Roles
        [HttpGet]
        public async Task<IEnumerable<Role>> GetAll()
        {
            return await _service.GetManager();
        }

        // GET: api/Roles/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Role>> GetById(int id)
        {
            try
            {
                var model = await _service.GetByIdAsync(id);

                if (model == null)
                {
                    return NotFound();
                }
                return model;
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError); ;
            }

        }

        //[HttpPut("{id}")]
        //public async Task<IActionResult> Update(int id, Role model)
        //{
        //    if (id != model.Id)
        //    {
        //        return BadRequest();
        //    }
        //    try
        //    {
        //        _service.Update(model);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    return NoContent();
        //}


        //[HttpPost]
        //public async Task<ActionResult<Role>> Save(Role model)
        //{
        //    try
        //    {
        //        await _service.AddAsync(model);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }

        //    return CreatedAtAction("GetById", new { id = model.Id }, model);
        //}

        //// DELETE: api/Roles/5
        //[HttpDelete("{id}")]
        //public async Task<ActionResult<Role>> Delete(int id)
        //{
        //    var model = await _service.GetByIdAsync(id);
        //    if (model == null)
        //    {
        //        return NotFound();
        //    }
        //    _service.Remove(model);

        //    return model;
        //}
    }
}