﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TusharInventory.Entity.Models.Basic;
using TusharInventory.Entity.Models.inventory;
using TusharInventory.Entity.ViewModels;
using TusharInventory.Reporitory.Dbcontext;
using TusharInventory.Reporitory.Service.IServices.Inventory;
using TusharInventory.WebApi.Extensions;

namespace TusharInventory.WebApi.Controllers.Inventory
{
    [Route("api/[controller]")]
    [ApiController]
    public class HoStocksController : ControllerBase
    {
        private readonly IHoStockService _service;
        private readonly TusharDbContext db;

        public HoStocksController(IHoStockService service, TusharDbContext _db)
        {
            _service = service;
            db = _db;
        }

        [Authorize(Roles = "Bookstore Employee,Bookstore Manager,Main Admin,Purchase Department")]
        [HttpGet]
        public async Task<IEnumerable<HoStock>> GetAll()
        {
            return await db.HoStocks
                .Include(m => m.BookSubject)
                .Include(m => m.BookGrade)
                .Include(m => m.BookLanguage)
                .ToListAsync();

            //try
            //{
            //    List<HoStockVm> listOfStock = new List<HoStockVm>();

            //    if (db.HoStocks.Count() > 0)
            //    {
            //        foreach (var item in db.HoStocks.Include(m => m.BookSubject).Include(m => m.BookGrade).Include(m => m.BookLanguage).ToList())
            //        {
            //            HoStockVm stock = new HoStockVm();
            //            stock.AvailableQty = item.AvailableQty;
            //            stock.BookGrade = item.BookGrade.Name;
            //            stock.BookLanguage = item.BookLanguage.Name;
            //            stock.BookSubject = item.BookSubject.Name;
            //            stock.PurchasePrice = db.Books.FirstOrDefault(m => m.BookGradeLavelId == item.BookGradeId && m.BookLanguageId == item.BookLanguageId && m.BookSubjectId == item.BookSubjectId).PurchasePrice;
            //            stock.SalePrice = db.Books.FirstOrDefault(m => m.BookGradeLavelId == item.BookGradeId && m.BookLanguageId == item.BookLanguageId && m.BookSubjectId == item.BookSubjectId).SalePrice;
            //            stock.BookId = db.Books.FirstOrDefault(m => m.BookGradeLavelId == item.BookGradeId && m.BookLanguageId == item.BookLanguageId && m.BookSubjectId == item.BookSubjectId).Id;
            //            listOfStock.Add(stock);
            //        }
            //    }


            //    return Ok(listOfStock);
            //}
            //catch (Exception e)
            //{
            //    return StatusCode(500, "Internal Server Error");

            //}

        }

        // GET: api/HoStocks/5
        [HttpGet("{id}")]
        public async Task<ActionResult<HoStock>> GetById(int id)
        {
            try
            {
                var model = await _service.GetByIdAsync(id);

                if (model == null)
                {
                    return NotFound();
                }
                return model;
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError); ;
            }

        }

        
        [HttpPost("GetBookStock")]
        public async Task<IActionResult> GetBookStock(StockVm hoStock)
        {
            try
            {
                if(db.Books.Any(m => m.BookSubjectId == hoStock.BookSubjectId && m.BookLanguageId == hoStock.BookLanguageId && m.BookGradeLavelId == hoStock.BookGradeId))
                {
                    if (db.HoStocks.Any(m => m.BookSubjectId == hoStock.BookSubjectId && m.BookLanguageId == hoStock.BookLanguageId && m.BookGradeId == hoStock.BookGradeId))
                    {
                        var stock = db.HoStocks.FirstOrDefault(m => m.BookSubjectId == hoStock.BookSubjectId && m.BookLanguageId == hoStock.BookLanguageId && m.BookGradeId == hoStock.BookGradeId);
                        return Ok(stock);
                    }

                    else
                    {
                        return Ok("0");
                    }
                }

                else
                {
                    return Ok();
                }
                
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError); ;
            }

        }

        [HttpPut("{id}")]
        [UserActivityFilter]
        public async Task<IActionResult> Update(int id, HoStock model)
        {
            if (id != model.Id)
            {
                return BadRequest();
            }
            try
            {
                _service.Update(model);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return NoContent();
        }


        [Authorize(Roles = "Main Admin,Bookstore Manager")]
        [HttpPost]
        [UserActivityFilter]
        public async Task<ActionResult> Update(HoStock model)
        {
            try
            {
                model.HoId = -1;

                if (db.HoStocks.Any(m => m.BookSubjectId == model.BookSubjectId && m.BookLanguageId == model.BookLanguageId && m.BookGradeId == model.BookGradeId))
                {
                    db.Attach(model);
                    db.Entry(model).State = EntityState.Modified;
                    return Ok(db.SaveChanges()>0);
                }
                else
                {

                    bool isAdded= _service.Add(model);
                    if (isAdded)
                    {
                        return Ok(isAdded);
                    }
                }

                return Ok(false);

            }
            catch (Exception ex)
            {
                return StatusCode(500, "Internal Server Error");
            }

            
        }

        // DELETE: api/HoStocks/5
        [HttpDelete("{id}")]
        [UserActivityFilter]
        public async Task<ActionResult<HoStock>> Delete(int id)
        {
            var model = await _service.GetByIdAsync(id);
            if (model == null)
            {
                return NotFound();
            }
            _service.Remove(model);

            return model;
        }

        [Authorize(Roles = "Bookstore Employee,Bookstore Manager,Main Admin")]
        [HttpPost("Search")]
        public async Task<IEnumerable<HoStock>> Search(BookSearchVm book)
        {
            try
            {
                var bookList = db.HoStocks.AsQueryable();
                if (book.BookSubjectId != null)
                {
                    bookList = bookList.Where(m => m.BookSubjectId == book.BookSubjectId).AsQueryable();
                }
                if (book.BookGradeLavelId != null)
                {
                    bookList = bookList.Where(m => m.BookGradeId == book.BookGradeLavelId).AsQueryable();
                }
                if (book.BookLanguageId != null)
                {
                    bookList = bookList.Where(m => m.BookLanguageId == book.BookLanguageId).AsQueryable();
                }

                return await bookList
                    .Include(m => m.BookSubject)
                    .Include(m => m.BookGrade)
                    .Include(m => m.BookLanguage)
                    .ToListAsync();

            }
            catch (Exception)
            {

                return null;
            }

            
        }
    }
}