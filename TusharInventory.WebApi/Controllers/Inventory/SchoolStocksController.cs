﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TusharInventory.Entity.Models.inventory;
using TusharInventory.Entity.ViewModels;
using TusharInventory.Reporitory.Dbcontext;
using TusharInventory.Reporitory.Service.IServices.Inventory;
using TusharInventory.WebApi.Extensions;

namespace TusharInventory.WebApi.Controllers.Inventory
{
    [Authorize(Roles = "School Employee,Bookstore Manager")]
    [Route("api/[controller]")]
    [ApiController]
    public class SchoolStocksController : ControllerBase
    {
        private readonly ISchoolStockService _service;
        private readonly TusharDbContext db;

        public SchoolStocksController(ISchoolStockService service, TusharDbContext _db)
        {
            _service = service;
            db = _db;
        }

        // GET: api/SchoolStocks
        [HttpGet]
        public async Task<IEnumerable<SchoolStock>> GetAll()
        {
            return await db.SchoolStocks
                .Include(m => m.BookSubject)
                .Include(m => m.BookGrade)
                .Include(m => m.BookLanguage)
                .ToListAsync();
        }

        // GET: api/SchoolStocks/5
        [HttpGet("{id}")]
        public async Task<ActionResult<SchoolStock>> GetById(int id)
        {
            try
            {
                var model = await _service.GetByIdAsync(id);

                if (model == null)
                {
                    return NotFound();
                }
                return model;
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError); ;
            }

        }

        [HttpPut("{id}")]
        [UserActivityFilter]
        public async Task<IActionResult> Update(int id, SchoolStock model)
        {
            if (id != model.Id)
            {
                return BadRequest();
            }
            try
            {
                _service.Update(model);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return NoContent();
        }


        [HttpPost]
        [UserActivityFilter]
        public async Task<ActionResult<SchoolStock>> Save(SchoolStock model)
        {
            try
            {
                await _service.AddAsync(model);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return CreatedAtAction("GetById", new { id = model.Id }, model);
        }

        // DELETE: api/SchoolStocks/5
        [HttpDelete("{id}")]
        [UserActivityFilter]
        public async Task<ActionResult<SchoolStock>> Delete(int id)
        {
            var model = await _service.GetByIdAsync(id);
            if (model == null)
            {
                return NotFound();
            }
            _service.Remove(model);

            return model;
        }


        [Authorize(Roles = "School Employee,Bookstore Manager")]
        [HttpPost("Search")]
        public async Task<IEnumerable<SchoolStock>> Search(BookSearchVm book)
        {
            try
            {
                var bookList = db.SchoolStocks.AsQueryable();
                if (book.SchoolId != null)
                {
                    bookList = bookList.Where(m => m.SchoolId == book.SchoolId).AsQueryable();
                }
                if (book.BookSubjectId != null)
                {
                    bookList = bookList.Where(m => m.BookSubjectId == book.BookSubjectId).AsQueryable();
                }
                if (book.BookGradeLavelId != null)
                {
                    bookList = bookList.Where(m => m.BookGradeId == book.BookGradeLavelId).AsQueryable();
                }
                if (book.BookLanguageId != null)
                {
                    bookList = bookList.Where(m => m.BookLanguageId == book.BookLanguageId).AsQueryable();
                }

                return await bookList
                    .Include(m => m.BookSubject)
                    .Include(m => m.BookGrade)
                    .Include(m => m.BookLanguage)
                    .ToListAsync();
            }
            catch (Exception)
            {

                return null;
            }


        }
    }
}