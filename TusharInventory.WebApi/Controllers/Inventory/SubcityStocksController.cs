﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TusharInventory.Entity.Models.Basic;
using TusharInventory.Entity.Models.inventory;
using TusharInventory.Entity.Models.Operation;
using TusharInventory.Entity.Models.Settings;
using TusharInventory.Entity.ViewModels;
using TusharInventory.Reporitory.Dbcontext;
using TusharInventory.Reporitory.Service.IServices.Inventory;
using TusharInventory.WebApi.Extensions;

namespace TusharInventory.WebApi.Controllers.Inventory
{
    [Route("api/[controller]")]
    [ApiController]
    public class SubcityStocksController : ControllerBase
    {
        private readonly TusharDbContext db;

        public SubcityStocksController( TusharDbContext _db)
        {
            db = _db;
        }


        [HttpGet("GetBookstoreManager")]
        public async Task<IEnumerable<User>> GetBookstoreManager()
        {
            return await db.Users.Where(m => m.RoleId == 4).ToListAsync();
        }

        [HttpGet("GetSubcityManagerListBySubcityId/{id}")]
        public async Task<IEnumerable<User>> GetSubcityManagerListBySubcityId(int id)
        {

            return await db.Users.Where(m => m.SubcityId == id && m.RoleId==6).ToListAsync();
        }


        // GET: api/SubcityStocks
        [HttpGet]
        public async Task<IEnumerable<SubcityStock>> GetAll()
        {
            return await db.SubcityStocks
                .Include(m => m.BookSubject)
                .Include(m => m.BookGrade)
                .Include(m => m.BookLanguage)
                .ToListAsync();
        }



        [Authorize(Roles = "Bookstore Manager,Subcity Manager")]
        [HttpGet("GetAllByUserId/{id}")]
        public async Task<IEnumerable<SubcityStock>> GetAllByUserId(int id)
        {
            int? subcityId = db.Users.FirstOrDefault(m => m.Id == id).SubcityId;

            return await db.SubcityStocks.
                 Where(m=>m.SubcityId==subcityId)
                .Include(m => m.BookSubject)
                .Include(m => m.BookGrade)
                .Include(m => m.BookLanguage)
                .ToListAsync();
        }



        [Authorize(Roles = "Bookstore Manager,Subcity Manager")]
        [HttpPost("GetAllBySearch")]
        public async Task<IEnumerable<SubcityStock>> GetAllBySearch(SubcityStockSearchVm vm)
        {
            try
            {
                if (vm.SubcityId == null)
                {
                    vm.SubcityId = db.Users.FirstOrDefault(m => m.Id == vm.UserId).SubcityId;
                }

                var list = db.SubcityStocks.
                     Where(m => m.SubcityId == vm.SubcityId)
                    .Include(m => m.BookSubject)
                    .Include(m => m.BookGrade)
                    .Include(m => m.BookLanguage)
                    .AsQueryable();
                if (vm.BookGradeId != null)
                {
                    list = list.Where(m => m.BookGradeId == vm.BookGradeId).AsQueryable();
                }
                if (vm.BookLanguageId != null)
                {
                    list = list.Where(m => m.BookLanguageId == vm.BookLanguageId).AsQueryable();
                }
                if (vm.BookSubjectId != null)
                {
                    list = list.Where(m => m.BookSubjectId == vm.BookSubjectId).AsQueryable();
                }
                if (vm.StockStatusId != null)
                {
                    if (vm.StockStatusId == 2)
                    {
                        list = list.Where(m => m.IsRequested == true && m.AvailableQty < 1000).AsQueryable();
                    }
                    if (vm.StockStatusId == 3)
                    {
                        list = list.Where(m => m.IsRequested == false && m.AvailableQty < 1000).AsQueryable();
                    }
                }

                return await list.ToListAsync();
            }
            catch (Exception)
            {

                throw;
            }

           
        }


        [Authorize(Roles = "Bookstore Employee,Bookstore Manager,Subcity Manager")]
        [HttpPost("GetSubcityOrder")]
        public async Task<IEnumerable<SubcityOrder>> GetSubcityOrder(SubcityOrderSearchVm vm)
        {
            try
            {
                var modelList = db.SubcityOrders
                .Include(m => m.OrderLines)
                .Include(m=>m.Subcity)
                .AsQueryable();

                var user = db.Users.Include(m => m.Role).FirstOrDefault(m => m.Id == vm.UserId);

                #region Filtering By User
                    if (user.Role.Name == "Subcity Manager")
                    {

                        modelList = modelList.Where(m => m.SubcityManagerId == vm.UserId).AsQueryable();
                    }

                    if (user.Role.Name == "Bookstore Employee")
                    {
                        modelList = modelList.Where(m => m.BookstoreEmployeeId == vm.UserId).AsQueryable();
                    }

                    if (user.Role.Name == "Bookstore Manager")
                    {
                        modelList = modelList.Where(m => m.BookstoreManagerId == vm.UserId).AsQueryable();
                    }
                #endregion

                #region Filtering by Request Status

                    if (vm.Status == "pending")
                    {
                        modelList = modelList.Where(m => m.Status == 1).AsQueryable();
                    }

                    if (vm.Status == "approved")
                    {
                        modelList = modelList.Where(m => m.Status == 2).AsQueryable();
                    }

                    if (vm.Status == "completed")
                    {
                        modelList = modelList.Where(m => m.Status == 3).AsQueryable();
                    }

                    if (vm.Status == "rejected")
                    {
                        modelList = modelList.Where(m => m.Status == 4).AsQueryable();
                    }
                #endregion

                #region Filtering by SearchVm
                if (vm.RequestId != null)
                {
                    modelList = modelList.Where(m => m.RequestId.ToLower() == vm.RequestId.Trim().ToLower()).AsQueryable();
                }
                #endregion


                return await modelList.ToListAsync();

            }
            catch (Exception e)
            {

                return null;
            }


        }
        // GET: api/SubcityStocks/5
        [HttpGet("{id}")]
        public async Task<ActionResult<SubcityStock>> GetById(int id)
        {
            try
            {
                var model =  db.SubcityStocks.FirstOrDefault(m=>m.Id==id);

                if (model == null)
                {
                    return NotFound();
                }
                return model;
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError); ;
            }

        }

        [HttpGet("GetOrderById/{id}")]
        public async Task<ActionResult<SubcityOrder>> GetOrderById(int id)
        {
            try
            {
                var model = db.SubcityOrders
                    .Include(m => m.OrderLines).ThenInclude(c=>c.BookGradeLavel)
                    .Include(m => m.OrderLines).ThenInclude(c => c.BookLanguage)
                    .Include(m => m.OrderLines).ThenInclude(c => c.BookSubject)
                    .Include(m => m.BookstoreEmployee)
                    .Include(m => m.BookstoreManager)
                    .Include(m => m.SubcityManager)
                    .Include(m => m.Subcity)
                    .FirstOrDefault(m => m.Id == id);

                if (model == null)
                {
                    return NotFound();
                }
                else
                {
                    foreach (var item in model.OrderLines)
                    {
                        double mainStock= db.HoStocks.FirstOrDefault(m => m.BookGradeId == item.BookGradeLavelId && m.BookLanguageId == item.BookLanguageId && m.BookSubjectId == item.BookSubjectId).AvailableQty;
                        double allocatedStock = db.HoStocks.FirstOrDefault(m => m.BookGradeId == item.BookGradeLavelId && m.BookLanguageId == item.BookLanguageId && m.BookSubjectId == item.BookSubjectId).AllocatedQty;
                        item.MainStockQty = mainStock - allocatedStock;
                    }
                }
                return model;
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError); ;
            }

        }

        // GET: api/SubcityStocks/5
        [HttpPost("GetBookStock")]
        public async Task<IActionResult> GetBookStock(StockVm hoStock)
        {
            try
            {
                if (db.Books.Any(m => m.BookSubjectId == hoStock.BookSubjectId && m.BookLanguageId == hoStock.BookLanguageId && m.BookGradeLavelId == hoStock.BookGradeId))
                {
                    if (db.SubcityStocks.Any(m => m.BookSubjectId == hoStock.BookSubjectId && m.BookLanguageId == hoStock.BookLanguageId && m.BookGradeId == hoStock.BookGradeId))
                    {
                        var stock = db.SubcityStocks.FirstOrDefault(m => m.BookSubjectId == hoStock.BookSubjectId && m.BookLanguageId == hoStock.BookLanguageId && m.BookGradeId == hoStock.BookGradeId);
                        return Ok(stock);
                    }

                    else
                    {
                        return Ok("0");
                    }
                }

                else
                {
                    return Ok();
                }

            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError); ;
            }

        }

        [HttpPut("{id}")]
        [UserActivityFilter]
        public async Task<IActionResult> Update(int id, SubcityStock model)
        {
            if (id != model.Id)
            {
                return BadRequest();
            }
            try
            {
                //_service.Update(model);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return NoContent();
        }
        

        [HttpPost("CreateRequest")]
        [UserActivityFilter]
        public async Task<ActionResult> CreateRequest(SubcityOrder model)
        {
            try
            {
                model.RequestId = GenerateCode();

                if(model.SubcityId == 0 || model.SubcityId ==null)
                {
                    model.SubcityId = db.Users.FirstOrDefault(m => m.Id == model.SubcityManagerId).SubcityId;
                }

                foreach (var item in model.OrderLines)
                {
                    item.SubcityStockQty = db.SubcityStocks.FirstOrDefault(m => m.BookGradeId == item.BookGradeLavelId && m.BookLanguageId == item.BookLanguageId && m.BookSubjectId == item.BookSubjectId).AvailableQty;
                }

                foreach (var item in model.OrderLines)
                {
                    item.BookGradeLavel = null;
                    item.BookLanguage = null;
                    item.BookSubject = null;
                }


                db.SubcityOrders.Add(model);
                bool isAdded = db.SaveChanges() > 0;
                if (isAdded)
                {
                    foreach (var item in model.OrderLines)
                    {
                        var stock = db.SubcityStocks.FirstOrDefault(m => m.BookGradeId == item.BookGradeLavelId && m.BookLanguageId == item.BookLanguageId && m.BookSubjectId == item.BookSubjectId && m.SubcityId==model.SubcityId);
                        stock.IsRequested = true;

                        db.Attach(stock);
                        db.Entry(stock).State = EntityState.Modified;
                        db.SaveChanges();
                    }

                    //If Order Request is Approved
                    if (model.Status == 2)
                    {
                        //Increase Allocated stock in Main Stock
                        foreach (var item in model.OrderLines)
                        {
                            var mainStock = db.HoStocks.FirstOrDefault(m => m.BookGradeId == item.BookGradeLavelId && m.BookLanguageId == item.BookLanguageId && m.BookSubjectId == item.BookSubjectId);
                            mainStock.AllocatedQty = mainStock.AllocatedQty + item.Quantity;

                            db.Attach(mainStock);
                            db.Entry(mainStock).State = EntityState.Modified;
                            db.SaveChanges();
                        }
                    }

                    return Ok(isAdded);
                }

                return Ok(false);

            }
            catch (Exception ex)
            {
                return StatusCode(500, "Internal Server Error");
            }


        }

        [HttpPost("UpdateRequest")]
        [UserActivityFilter]
        public async Task<ActionResult> UpdateRequest(SubcityOrder model)
        {
            try
            {
                //For avoiding Update
                model.BookstoreManager = null;
                model.BookstoreEmployee = null;
                model.SubcityManager = null;
                model.Subcity = null;

                foreach (var item in model.OrderLines)
                {
                    item.BookGradeLavel = null;
                    item.BookLanguage = null;
                    item.BookSubject = null;
                }

                //If Order Request is Approved
                if (model.Status == 2)
                {
                    
                    foreach (var item in model.OrderLines)
                    {
                        //Increase Allocated stock in Main Stock
                        var mainStock = db.HoStocks.FirstOrDefault(m => m.BookGradeId == item.BookGradeLavelId && m.BookLanguageId == item.BookLanguageId && m.BookSubjectId == item.BookSubjectId);
                        mainStock.AllocatedQty = mainStock.AllocatedQty + item.Quantity;

                        db.Attach(mainStock);
                        db.Entry(mainStock).State = EntityState.Modified;
                        db.SaveChanges();

                        //Update the Orderlines
                        db.Attach(item);
                        db.Entry(item).State = EntityState.Modified;
                        db.SaveChanges();
                    }
                }

                //If Order Request is Completed
                if (model.Status == 3)
                {

                    //Decrease Available stock and Allocated stock in Main Stock
                    foreach (var item in model.OrderLines)
                    {
                        var mainStock = db.HoStocks.FirstOrDefault(m => m.BookGradeId == item.BookGradeLavelId && m.BookLanguageId == item.BookLanguageId && m.BookSubjectId == item.BookSubjectId);
                        mainStock.AllocatedQty = mainStock.AllocatedQty - item.Quantity;
                        mainStock.AvailableQty = mainStock.AvailableQty - item.Quantity;

                        db.Attach(mainStock);
                        db.Entry(mainStock).State = EntityState.Modified;
                        db.SaveChanges();
                    }

                    //Increase the Subcity stock and Set IsRequested to False in SubcityStock so that subcity can request later
                    foreach (var item in model.OrderLines)
                    {
                        var subcityStock = db.SubcityStocks.FirstOrDefault(m => m.BookGradeId == item.BookGradeLavelId && m.BookLanguageId == item.BookLanguageId && m.BookSubjectId == item.BookSubjectId && m.SubcityId==model.SubcityId);
                        subcityStock.AvailableQty = subcityStock.AvailableQty + item.Quantity;

                        subcityStock.IsRequested = false;

                        db.Attach(subcityStock);
                        db.Entry(subcityStock).State = EntityState.Modified;
                        db.SaveChanges();
                    }
                }

                //If Order Request is Rejected
                if (model.Status == 4)
                {
                    //Set IsRequested to False in SubcityStock so that subcity can request later
                    foreach (var item in model.OrderLines)
                    {
                        var subcityStock = db.SubcityStocks.FirstOrDefault(m => m.BookGradeId == item.BookGradeLavelId && m.BookLanguageId == item.BookLanguageId && m.BookSubjectId == item.BookSubjectId && m.SubcityId==model.SubcityId);
                        subcityStock.IsRequested = false;

                        db.Attach(subcityStock);
                        db.Entry(subcityStock).State = EntityState.Modified;
                        db.SaveChanges();
                    }
                }

                model.OrderLines = null;
                db.Attach(model);
                db.Entry(model).State = EntityState.Modified;
                bool isUpdated= db.SaveChanges()>0;

                return Ok(isUpdated);

            }
            catch (Exception ex)
            {
                return StatusCode(500, "Internal Server Error");
            }


        }
        


        [HttpPost("GetOrderForManager")]
        public async Task<IActionResult> GetOrderForManager(List<StockSearchVm> modelList)
        {
            try
            {
                SubcityOrder subcityOrder = new SubcityOrder();
                foreach (var item in modelList)
                {
                    SubcityOrderLine line = new SubcityOrderLine();
                    line.BookGradeLavel = item.BookGrade;
                    line.BookGradeLavelId = item.BookGradeId;
                    line.BookLanguage = item.BookLanguage;
                    line.BookLanguageId = item.BookLanguageId;
                    line.BookSubject = item.BookSubject;
                    line.BookSubjectId = item.BookSubjectId;
                    line.MainStockQty = db.HoStocks.FirstOrDefault(m => m.BookSubjectId == item.BookSubjectId && m.BookLanguageId == item.BookLanguageId && m.BookGradeId == item.BookGradeId).AvailableQty- db.HoStocks.FirstOrDefault(m => m.BookSubjectId == item.BookSubjectId && m.BookLanguageId == item.BookLanguageId && m.BookGradeId == item.BookGradeId).AllocatedQty;
                    line.SubcityStockQty = item.AvailableQty;

                    if(subcityOrder.OrderLines == null)
                    {
                        subcityOrder.OrderLines = new List<SubcityOrderLine>();
                    }
                    subcityOrder.OrderLines.Add(line);

                }
                
                return Ok(subcityOrder);

            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError); ;
            }

        }

        private string GenerateCode()
        {
            string code = "";
            if (db.SubcityOrders.Count() <= 0)
            {
                code = "1";
            }
            else
            {
                code = (db.SubcityOrders.Max(m => m.Id) + 1).ToString();
            }

            string generatedCode = code.ToString();

            if (generatedCode.Length == 1)
            {
                generatedCode = "RQ-" + "0" + "0" + code;
            }
            if (code.Length == 2)
            {
                generatedCode = "RQ-" + "0" + code;
            }
            if (code.Length >= 3)
            {
                generatedCode = "RQ-" + code;
            }

            return generatedCode;

        }
        [HttpPost]
        [UserActivityFilter]
        public async Task<ActionResult> Update(SubcityStock model)
        {
            try
            {
                

                if (db.SubcityStocks.Any(m => m.BookSubjectId == model.BookSubjectId && m.BookLanguageId == model.BookLanguageId && m.BookGradeId == model.BookGradeId))
                {
                    db.Attach(model);
                    db.Entry(model).State = EntityState.Modified;
                    return Ok(db.SaveChanges() > 0);
                }
                else
                {

                    bool isAdded = true;
                    if (isAdded)
                    {
                        return Ok(isAdded);
                    }
                }

                return Ok(false);

            }
            catch (Exception ex)
            {
                return StatusCode(500, "Internal Server Error");
            }


        }

        // DELETE: api/SubcityStocks/5
        [HttpDelete("{id}")]
        [UserActivityFilter]
        public async Task<ActionResult<SubcityStock>> Delete(int id)
        {
            var model =  db.SubcityStocks.Find(id);
            if (model == null)
            {
                return NotFound();
            }
            //_service.Remove(model);

            return model;
        }

        // GET: api/SubcityStocks
        [HttpPost("Search")]
        public async Task<IEnumerable<SubcityStock>> Search(BookSearchVm book)
        {
            try
            {
                var bookList = db.SubcityStocks.AsQueryable();
                if (book.BookSubjectId != null)
                {
                    bookList = bookList.Where(m => m.BookSubjectId == book.BookSubjectId).AsQueryable();
                }
                if (book.BookGradeLavelId != null)
                {
                    bookList = bookList.Where(m => m.BookGradeId == book.BookGradeLavelId).AsQueryable();
                }
                if (book.BookLanguageId != null)
                {
                    bookList = bookList.Where(m => m.BookLanguageId == book.BookLanguageId).AsQueryable();
                }

                return await bookList
                    .Include(m => m.BookSubject)
                    .Include(m => m.BookGrade)
                    .Include(m => m.BookLanguage)
                    .ToListAsync();

            }
            catch (Exception)
            {

                return null;
            }
        }
    }
}