﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using TusharInventory.Entity.Models.Basic;
using TusharInventory.Entity.Models.inventory;
using TusharInventory.Entity.Models.Settings;
using TusharInventory.Entity.ViewModels;
using TusharInventory.Reporitory.Dbcontext;
using TusharInventory.Reporitory.Service.IServices.Basic;
using TusharInventory.WebApi.Extensions;

namespace TusharInventory.WebApi.Controllers.Basic
{
    [Route("api/[controller]")]
    [ApiController]
    public class SchoolsController : ControllerBase
    {
        private readonly ISchoolService _service;
        private readonly TusharDbContext db;

        public SchoolsController(ISchoolService service, TusharDbContext _db)
        {
            _service = service;
            db = _db;
        }

        
        [HttpGet]
        public async Task<IEnumerable<School>> GetAll()
        {
            var list = db.Schools
                    .Include(x => x.SchoolSubcity)
                    .Include(x => x.SchoolWoreda)
                    .Include(x => x.Woreda1)
                    .Include(x => x.SchoolLavel)
                    .Include(x => x.SchoolOwnership).ToList();

              return  list;
        }

        [HttpGet("GetSchoolName")]
        public async Task<IActionResult> GetSchoolName()
        {
            var list = db.Schools.Select(m => new { Id = m.Id, Name = m.Name });

            return Ok(list);
        }


        [Authorize(Roles = "Bookstore Employee,Main Admin")]
        [HttpPost("Search")]
        public async Task<IEnumerable<School>> Search(SchoolSearchVm vm)
        {
            var schoolList = db.Schools.AsQueryable();

            if (vm.Id != null)
            {
                schoolList = schoolList.Where(m => m.Id == vm.Id).AsQueryable();
            }

            if (vm.SchoolLavelId != null)
            {
                schoolList = schoolList.Where(m => m.SchoolLavelId == vm.SchoolLavelId).AsQueryable();
            }

            if (vm.SchoolOwnershipId != null)
            {
                schoolList = schoolList.Where(m => m.SchoolOwnershipId == vm.SchoolOwnershipId).AsQueryable();
            }

            if (vm.SchoolSubcityId != null)
            {
                schoolList = schoolList.Where(m => m.SchoolSubcityId == vm.SchoolSubcityId).AsQueryable();
            }

            if (vm.SchoolWoredaId != null)
            {
                schoolList = schoolList.Where(m => m.SchoolWoredaId == vm.SchoolWoredaId).AsQueryable();
            }

            if (vm.Woreda1Id != null)
            {
                schoolList = schoolList.Where(m => m.Woreda1Id == vm.Woreda1Id).AsQueryable();
            }

            var list = schoolList
                    .Include(x => x.SchoolSubcity)
                    .Include(x => x.SchoolWoreda)
                    .Include(x => x.Woreda1)
                    .Include(x => x.SchoolLavel)
                    .Include(x => x.SchoolOwnership).ToList();

            return list;
        }

        // GET: api/Schools/5
        [HttpGet("{id}")]
        public async Task<ActionResult<School>> GetById(int id)
        {
            try
            {
                var model = await _service.GetByIdAsync(id);

                if (model == null)
                {
                    return NotFound();
                }
                return model;
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError); ;
            }

        }

        [HttpPut("{id}")]
        [UserActivityFilter]
        public async Task<IActionResult> Update(int id, School model)
        {
            if (id != model.Id)
            {
                return BadRequest();
            }
            try
            {
                model.SchoolWoreda = null;
                model.SchoolSubcity = null;
                model.SchoolLavel = null;
                model.SchoolOwnership = null;
                model.Woreda1 = null;

                db.Schools.Attach(model);
                db.Entry(model).State = EntityState.Modified;
                bool isUpdated = db.SaveChanges()>0;

                if (isUpdated)
                {
                    return Ok(isUpdated);
                }
                return Ok(false);
            }
            catch (Exception ex)
            {
                return StatusCode(500, "Internal Server Error");
            }
            
        }


        [HttpPost]
        [UserActivityFilter]
        public async Task<ActionResult<School>> Save(School model)
        {
            try
            {
                
                model.NameFile = Encoding.UTF8.GetBytes(model.Name.Trim());
                await _service.AddAsync(model);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return CreatedAtAction("GetById", new { id = model.Id }, model);
        }

        [HttpPost("BulkImport")]
        [UserActivityFilter]
        public async Task<ActionResult<School>> BulkImport(List<SchoolBulkVm> schoolList)
        {
            try
            {
                schoolList = schoolList.Skip(1).ToList();

                #region Adding Subcity 
                List<string> listOfSubcity = schoolList.Select(m => m.Subcity).ToList();

                foreach (var item in listOfSubcity)
                {
                    if (item != null)
                    {
                        byte [] val = Encoding.UTF8.GetBytes(item.Trim()) ;

                        bool isExist =db.Subcities.Any(m=>m.NameFile==val);
                        if (!isExist)
                        {
                            Subcity subcity = new Subcity();
                            subcity.NameFile = val;
                            db.Subcities.Add(subcity);
                            db.SaveChanges();


                            //Add book to All Subcity Store
                            foreach (var book in db.Books.ToList())
                            {

                                SubcityStock subcityStock = new SubcityStock();
                                subcityStock.AvailableQty = 0;
                                subcityStock.BookGradeId = book.BookGradeLavelId;
                                subcityStock.BookLanguageId = book.BookLanguageId;
                                subcityStock.BookSubjectId = book.BookSubjectId;
                                subcityStock.SubcityId = subcity.Id;

                                db.SubcityStocks.Add(subcityStock);
                                db.SaveChanges();
                            }
                        }
                    }
                }
                #endregion

                #region Adding Woreda1 
                List<string> listOfWoreda1 = schoolList.Select(m => m.Woreda1).ToList();

                foreach (var item in listOfWoreda1)
                {
                    if (item != null)
                    {
                        byte[] val = Encoding.UTF8.GetBytes(item.Trim());

                        bool isExist = db.Woreda1s.Any(m => m.NameFile == val);
                        if (!isExist)
                        {
                            Woreda1 woreda1 = new Woreda1();
                            woreda1.NameFile = val;
                            db.Woreda1s.Add(woreda1);
                            db.SaveChanges();
                        }
                    }
                }
                #endregion


                #region Adding Woreda 
                List<string> listOfWoreda = schoolList.Select(m => m.Woreda).ToList();

                foreach (var item in listOfWoreda)
                {
                    if (item != null)
                    {
                        byte[] val = Encoding.UTF8.GetBytes(item.Trim());

                        bool isExist = db.Woredas.Any(m => m.NameFile == val);
                        if (!isExist)
                        {
                            Woreda woreda = new Woreda();
                            woreda.NameFile = val;
                            db.Woredas.Add(woreda);
                            db.SaveChanges();
                        }
                    }
                }
                #endregion

                #region Adding SchoolLevel 
                List<string> listOfSchoolLevel = schoolList.Select(m => m.SchoolLevel).ToList();

                foreach (var item in listOfSchoolLevel)
                {
                    if (item != null)
                    {
                        byte[] val = Encoding.UTF8.GetBytes(item.Trim());

                        bool isExist = db.SchoolLavels.Any(m => m.NameFile == val);
                        if (!isExist)
                        {
                            SchoolLavel schoolLevel = new SchoolLavel();
                            schoolLevel.NameFile = val;
                            db.SchoolLavels.Add(schoolLevel);
                            db.SaveChanges();
                        }
                    }
                }
                #endregion

                #region Adding Ownership 
                List<string> listOfOwnership = schoolList.Select(m => m.SchoolOwnership).ToList();

                foreach (var item in listOfOwnership)
                {
                    if (item != null)
                    {
                        byte[] val = Encoding.UTF8.GetBytes(item.Trim());

                        bool isExist = db.Ownerships.Any(m => m.NameFile == val);
                        if (!isExist)
                        {
                            Ownership ownership = new Ownership();
                            ownership.NameFile = val;
                            db.Ownerships.Add(ownership);
                            db.SaveChanges();
                        }
                    }
                }
                #endregion

                #region Adding School 

                foreach (var item in schoolList)
                {
                    School school = new School();

                    if (item.SchoolName != null)
                    {
                        school.NameFile = Encoding.UTF8.GetBytes(item.SchoolName.Trim()) ;
                    }

                    

                    if (item.Subcity != null)
                    {
                        school.SchoolSubcityId = db.Subcities.FirstOrDefault(m => m.NameFile == Encoding.UTF8.GetBytes(item.Subcity.Trim())).Id;
                    }

                    if (item.Woreda1 != null)
                    {
                        school.Woreda1Id = db.Woreda1s.FirstOrDefault(m => m.NameFile == Encoding.UTF8.GetBytes(item.Woreda1.Trim())).Id;
                    }

                    if (item.Woreda != null)
                    {
                        school.SchoolWoredaId = db.Woredas.FirstOrDefault(m => m.NameFile == Encoding.UTF8.GetBytes(item.Woreda.Trim())).Id;
                    }

                    if (item.SchoolLevel != null)
                    {
                        school.SchoolLavelId = db.SchoolLavels.FirstOrDefault(m => m.NameFile == Encoding.UTF8.GetBytes(item.SchoolLevel.Trim())).Id;

                    }

                    if (item.SchoolOwnership != null)
                    {
                        school.SchoolOwnershipId = db.Ownerships.FirstOrDefault(m => m.NameFile == Encoding.UTF8.GetBytes(item.SchoolOwnership.Trim())).Id;
                    }

                    if (school.SchoolSubcityId != null && school.SchoolWoredaId != null && school.Woreda1Id != null && school.SchoolLavelId != null && school.SchoolOwnershipId != null && school.NameFile != null)
                    {
                        if (!db.Schools.Any(m => m.NameFile == school.NameFile &&
                            m.SchoolSubcityId==school.SchoolSubcityId &&
                            m.Woreda1Id==school.Woreda1Id &&
                            m.SchoolWoredaId==school.SchoolWoredaId &&
                            m.SchoolLavelId==school.SchoolLavelId &&
                            m.SchoolOwnershipId==school.SchoolOwnershipId))
                        {
                            db.Schools.Add(school);
                            db.SaveChanges();
                        }

                        
                    }





                }


                #endregion


                return Ok(true);
            }
            catch (Exception ex)
            {
                return StatusCode(500);
            }


        }


        //[HttpPost("BulkImport")]
        //public async Task<ActionResult<School>> BulkImport(List<SchoolBulkVm> schoolList)
        //{
        //    try
        //    {
        //        schoolList = schoolList.Skip(1).ToList();

        //        #region Adding Subcity 
        //        List<string> listOfSubcity = schoolList.Select(m => m.Subcity).ToList();
        //        List<string> existingSubcityList = db.Subcities.Select(m => m.Name).ToList();
        //        List<string> newSubcityList = new List<string>();

        //        foreach (var item in listOfSubcity)
        //        {
        //            if (item != null)
        //            {
        //                string val = item.Trim();

        //                bool isContain = existingSubcityList.Contains(val);
        //                if (!isContain)
        //                {
        //                    bool isExist = newSubcityList.Contains(val);
        //                    if (!isExist)
        //                    {
        //                        newSubcityList.Add(val);
        //                    }
        //                }
        //            }
        //        }

        //        if (newSubcityList.Count > 0)
        //        {
        //            foreach (var item in newSubcityList)
        //            {
        //                if(item != null)
        //                {
        //                    Subcity subcity = new Subcity();
        //                    subcity.Name = item;
        //                    db.Subcities.Add(subcity);
        //                    db.SaveChanges();
        //                }
        //            }
        //        }
        //        #endregion

        //        #region Adding Woreda1 
        //        List<string> listOfWoreda1 = schoolList.Select(m => m.Woreda1).ToList();
        //        List<string> existingWoreda1List = db.Woreda1s.Select(m => m.Name).ToList();
        //        List<string> newWoreda1List = new List<string>();

        //        foreach (var item in listOfWoreda1)
        //        {
        //            if(item != null)
        //            {
        //                string val = item.Trim();

        //                bool isContain = existingWoreda1List.Contains(val);
        //                if (!isContain)
        //                {
        //                    bool isExist = newWoreda1List.Contains(val);
        //                    if (!isExist)
        //                    {
        //                        newWoreda1List.Add(val);
        //                    }
        //                }
        //            }

        //        }

        //        if (newWoreda1List.Count > 0)
        //        {
        //            foreach (var item in newWoreda1List)
        //            {
        //                if(item != null)
        //                {
        //                    Woreda1 woreda1 = new Woreda1();
        //                    woreda1.Name = item;
        //                    db.Woreda1s.Add(woreda1);
        //                    db.SaveChanges();
        //                }

        //            }
        //        }
        //        #endregion

        //        #region Adding Woreda 
        //        List<string> listOfWoreda = schoolList.Select(m => m.Woreda).ToList();
        //        List<string> existingWoredaList = db.Woredas.Select(m => m.Name).ToList();
        //        List<string> newWoredaList = new List<string>();

        //        foreach (var item in listOfWoreda)
        //        {
        //            if (item != null)
        //            {
        //                string val = item.Trim();

        //                bool isContain = existingWoredaList.Contains(val);
        //                if (!isContain)
        //                {
        //                    bool isExist = newWoredaList.Contains(val);
        //                    if (!isExist)
        //                    {
        //                        newWoredaList.Add(val);
        //                    }
        //                }
        //            }
        //        }

        //        if (newWoredaList.Count > 0)
        //        {
        //            foreach (var item in newWoredaList)
        //            {
        //                if (item != null)
        //                {
        //                    Woreda woreda = new Woreda();
        //                    woreda.Name = item;
        //                    db.Woredas.Add(woreda);
        //                    db.SaveChanges();

        //                }
        //            }
        //        }
        //        #endregion

        //        #region Adding SchoolLavel 
        //        List<string> listOfSchoolLavel = schoolList.Select(m => m.SchoolLevel).ToList();
        //        List<string> existingSchoolLavelList = db.SchoolLavels.Select(m => m.Name).ToList();
        //        List<string> newSchoolLavelList = new List<string>();

        //        foreach (var item in listOfSchoolLavel)
        //        {
        //            if (item != null)
        //            {

        //                string val = item.Trim();

        //                bool isContain = existingSchoolLavelList.Contains(val);
        //                if (!isContain)
        //                {
        //                    bool isExist = newSchoolLavelList.Contains(val);
        //                    if (!isExist)
        //                    {
        //                        newSchoolLavelList.Add(val);
        //                    }
        //                }
        //            }
        //        }

        //        if (newSchoolLavelList.Count > 0)
        //        {
        //            foreach (var item in newSchoolLavelList)
        //            {
        //                if (item != null)
        //                {
        //                    SchoolLavel schoolLavel = new SchoolLavel();
        //                    schoolLavel.Name = item;
        //                    db.SchoolLavels.Add(schoolLavel);
        //                    db.SaveChanges();

        //                }
        //            }
        //        }
        //        #endregion

        //        #region Adding Ownership 
        //        List<string> listOfOwnership = schoolList.Select(m => m.SchoolOwnership).ToList();
        //        List<string> existingOwnershipList = db.Ownerships.Select(m => m.Name).ToList();
        //        List<string> newOwnershipList = new List<string>();

        //        foreach (var item in listOfOwnership)
        //        {

        //            if (item != null)
        //            {
        //                 string val = item.Trim();

        //                bool isContain = existingOwnershipList.Contains(val);
        //                if (!isContain)
        //                {
        //                    bool isExist = newOwnershipList.Contains(val);
        //                    if (!isExist)
        //                    {
        //                        newOwnershipList.Add(val);
        //                    }
        //                }
        //            }
        //        }

        //        if (newOwnershipList.Count > 0)
        //        {
        //            foreach (var item in newOwnershipList)
        //            {
        //                if (item != null)
        //                {
        //                    Ownership ownership = new Ownership();
        //                    ownership.Name = item;
        //                    db.Ownerships.Add(ownership);
        //                    db.SaveChanges();

        //                }
        //            }
        //        }
        //        #endregion


        //        #region Adding School 
        //        //List<string> listOfSchool = schoolList.Select(m => m.SchoolName).ToList();
        //        List<Subcity> subcityList = db.Subcities.ToList();
        //        List<Woreda1> woreda1List = db.Woreda1s.ToList();
        //        List<Woreda> woredaList = db.Woredas.ToList();
        //        List<SchoolLavel> levelList = db.SchoolLavels.ToList();
        //        List<Ownership> ownershipList = db.Ownerships.ToList();


        //        foreach (var item in schoolList)
        //        {
        //            School school = new School();

        //            if (item.SchoolName != null)
        //            {
        //                school.Name = item.SchoolName.Trim();
        //            }

        //            if (item.Subcity != null)
        //            {
        //                school.SchoolSubcityId = subcityList.FirstOrDefault(m => m.Name.Trim() == item.Subcity.Trim()).Id;
        //            }

        //            if (item.Woreda1 != null)
        //            {
        //                school.Woreda1Id = woreda1List.FirstOrDefault(m => m.Name.Trim() == item.Woreda1.Trim()).Id;
        //            }

        //            if (item.Woreda != null)
        //            {
        //                school.SchoolWoredaId = woredaList.FirstOrDefault(m => m.Name.Trim() == item.Woreda.Trim()).Id;
        //            }

        //            if (item.SchoolLevel != null)
        //            {
        //                school.SchoolLavelId = levelList.FirstOrDefault(m => m.Name.Trim() == item.SchoolLevel.Trim()).Id;
        //            }

        //            if (item.SchoolOwnership != null)
        //            {
        //                school.SchoolOwnershipId = ownershipList.FirstOrDefault(m => m.Name.Trim() == item.SchoolOwnership.Trim()).Id;
        //            }

        //            if(school.SchoolSubcityId!=null && school.SchoolWoredaId!=null && school.Woreda1Id!=null && school.SchoolLavelId!=null && school.SchoolOwnershipId!=null && school.Name != null)
        //            {
        //                db.Schools.Add(school);
        //                db.SaveChanges();
        //            }

        //            else
        //            {
        //                Console.WriteLine("do nothing");
        //            }




        //        }


        //        #endregion


        //        return Ok(true);
        //    }
        //    catch (Exception ex)
        //    {
        //        return StatusCode(500);
        //    }


        //}


        // DELETE: api/Schools/5
        [HttpDelete("{id}")]
        [UserActivityFilter]
        public async Task<ActionResult<School>> Delete(int id)
        {
            var model = await _service.GetByIdAsync(id);
            if (model == null)
            {
                return NotFound();
            }
            _service.Remove(model);

            return model;
        }

        [HttpPost("CheckSchoolName")]
        public ActionResult<School> CheckSchoolName(School school)
        {
            try
            {

                var existSchool = db.Schools.FirstOrDefault(m => m.NameFile == Encoding.UTF8.GetBytes(school.Name.Trim()) &&
                             m.SchoolSubcityId == school.SchoolSubcityId &&
                             m.Woreda1Id == school.Woreda1Id &&
                             m.SchoolWoredaId == school.SchoolWoredaId &&
                             m.SchoolLavelId == school.SchoolLavelId &&
                             m.SchoolOwnershipId == school.SchoolOwnershipId);

                return existSchool;
            }
            catch (Exception)
            {

                return StatusCode(500, "Internal Server Error");
            }

        }
    }
}