﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TusharInventory.Entity.Models.Basic;
using TusharInventory.Entity.ViewModels;
using TusharInventory.Reporitory.Dbcontext;
using TusharInventory.Reporitory.Service.IServices.Basic;
using TusharInventory.WebApi.Extensions;

namespace TusharInventory.WebApi.Controllers.Basic
{
    [Route("api/[controller]")]
    [ApiController]
    public class WoredasController : ControllerBase
    {
        private readonly IWoredaService _service;
        private readonly TusharDbContext db;

        public WoredasController(IWoredaService service, TusharDbContext _db)
        {
            _service = service;
            db = _db;
        }

        // GET: api/Woredas
        [HttpGet]
        public async Task<IEnumerable<Woreda>> GetAll()
        {
            return await _service.GetManager();
        }

        // GET: api/Woredas/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Woreda>> GetById(int id)
        {
            try
            {
                var model = await _service.GetByIdAsync(id);

                if (model == null)
                {
                    return NotFound();
                }
                return model;
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError); ;
            }

        }


        [Authorize(Roles = "Main Admin")]
        [HttpPut("{id}")]
        [UserActivityFilter]
        public async Task<IActionResult> Update(int id, BasicModelVm model)
        {
            if (id != model.Id)
            {
                return BadRequest();
            }
            try
            {
                Woreda woreda = new Woreda();
                woreda.NameFile = Encoding.UTF8.GetBytes(model.Name.Trim());
                woreda.Id = id;
                bool isUpdated = _service.Update(woreda);
                return Ok(isUpdated);
            }
            catch (Exception ex)
            {
                return NoContent();
            }
        }


        [Authorize(Roles = "Main Admin")]
        [HttpPost]
        [UserActivityFilter]
        public async Task<ActionResult<Woreda>> Save(Woreda model)
        {
            try
            {
                model.NameFile = Encoding.UTF8.GetBytes(model.Name.Trim());
                await _service.AddAsync(model);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return CreatedAtAction("GetById", new { id = model.Id }, model);
        }


        [Authorize(Roles = "Main Admin")]
        [HttpDelete("{id}")]
        [UserActivityFilter]
        public async Task<ActionResult<Woreda>> Delete(int id)
        {
            var model = await _service.GetByIdAsync(id);
            if (model == null)
            {
                return NotFound();
            }
            _service.Remove(model);

            return model;
        }

        [HttpPost("CheckName")]
        public ActionResult<Woreda> CheckName(BasicModelVm woreda)
        {
            try
            {

                var existWoreda = db.Woredas.FirstOrDefault(m => m.NameFile == Encoding.UTF8.GetBytes(woreda.Name.Trim()));
                return existWoreda;
            }
            catch (Exception)
            {

                return StatusCode(500, "Internal Server Error");
            }

        }
    }
}