﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TusharInventory.Entity.ViewModels;
using TusharInventory.Reporitory.Dbcontext;

namespace TusharInventory.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class DashboardsController : ControllerBase
    {
        private readonly TusharDbContext _db;
        public DashboardsController(TusharDbContext db)
        {
            _db = db;
        }

        [Authorize(Roles = "Main Admin")]
        [HttpGet("GetAdminDashboardData")]
        public async Task<ActionResult> GetAdminDashboardData()
        {
            try
            {
                AdminDashboardVm vm = new AdminDashboardVm();

                vm.PendingRequest = _db.Orders.Where(m => m.Status == 1 || m.Status == 2 || m.Status == 3).Count();
                vm.ApprovedRequest = _db.Orders.Where(m => m.Status == 4 || m.Status == 7).Count();
                vm.RejectedRequest = _db.Orders.Where(m => m.Status == 5 || m.Status == 6).Count();
                vm.TodayRequest = _db.Orders.Where(m => m.OrderDate == DateTime.Now).Count();

                DateTime startOfWeek = DateTime.Today.AddDays(
                                        (int)CultureInfo.CurrentCulture.DateTimeFormat.FirstDayOfWeek -
                                        (int)DateTime.Today.DayOfWeek);

                DateTime endOfWeek = startOfWeek.AddDays(6);

                vm.ThisWeekRequest = _db.Orders.Where(m => m.OrderDate >= startOfWeek && m.OrderDate <= endOfWeek).Count();


                vm.TotalRequest = _db.Orders.Count();
                vm.TotalBook = _db.HoStocks.Sum(m => m.AvailableQty);
                vm.TotalSchool = _db.Schools.Count();
                vm.TotalActiveUsers = _db.Users.Where(m => m.IsActive == true).Count();
                vm.TotalUserPendingRequest = _db.Users.Where(m => m.IsApproved == false).Count();


                //For Book Chart Data
                var bookList = _db.Books
                    .Include(m => m.BookSubject)
                    .Include(m=>m.BookLanguage)
                    .Include(m=>m.BookGradeLavel)
                    .AsQueryable();

                var stockList = _db.HoStocks.AsQueryable();
                foreach (var item in bookList)
                {
                    ChartVm subChart = new ChartVm();
                    subChart.name = item.BookSubject.Name;
                    subChart.value = stockList.Where(m => m.BookSubjectId == item.BookSubjectId).Sum(x=>x.AvailableQty);
                    vm.SubjectChartdata.Add(subChart);

                    ChartVm gradeChart = new ChartVm();
                    gradeChart.name = item.BookGradeLavel.Name;
                    gradeChart.value = stockList.Where(m => m.BookGradeId == item.BookGradeLavelId).Sum(x => x.AvailableQty);
                    vm.GradeChartdata.Add(gradeChart);

                    ChartVm languageChart = new ChartVm();
                    languageChart.name = item.BookLanguage.Name;
                    languageChart.value = stockList.Where(m => m.BookLanguageId == item.BookLanguageId).Sum(x => x.AvailableQty);
                    vm.LanguageChartdata.Add(languageChart);

                }


                //For Request Chart Data
                double weekApproved = _db.Orders.Where(m => m.OrderDate >= startOfWeek && m.OrderDate <= endOfWeek && (m.Status == 4 || m.Status == 7)).Count();
                double weekRejected = _db.Orders.Where(m => m.OrderDate >= startOfWeek && m.OrderDate <= endOfWeek && (m.Status == 5 || m.Status == 6)).Count();
                vm.WeekChartData = new List<ChartVm>()
                {
                    new ChartVm(){name="Approved",value=weekApproved},
                    new ChartVm(){name="Rejected",value=weekRejected}
                };



                double monthApproved = _db.Orders.Where(m => m.OrderDate.Month == DateTime.Now.Month && m.OrderDate.Year == DateTime.Now.Year  && (m.Status == 4 || m.Status == 7)).Count();
                double monthRejected = _db.Orders.Where(m => m.OrderDate.Month == DateTime.Now.Month && m.OrderDate.Year == DateTime.Now.Year  && (m.Status == 5 || m.Status == 6)).Count();
                vm.MonthChartData = new List<ChartVm>()
                {
                    new ChartVm(){name="Approved",value=monthApproved},
                    new ChartVm(){name="Rejected",value=monthRejected}
                };

                double yearApproved = _db.Orders.Where(m => m.OrderDate.Year == DateTime.Now.Year  && (m.Status == 4 || m.Status == 7)).Count();
                double yearRejected = _db.Orders.Where(m => m.OrderDate.Year == DateTime.Now.Year &&  (m.Status == 5 || m.Status == 6)).Count();
                vm.YearChartData = new List<ChartVm>()
                {
                    new ChartVm(){name="Approved",value=yearApproved},
                    new ChartVm(){name="Rejected",value=yearRejected}
                };


                //For User Info Chart Data
                double subcity = _db.Subcities.Count();
                double woreda = _db.Woredas.Count();
                double level = _db.SchoolLavels.Count();
                double ownerShip = _db.Ownerships.Count();
                vm.UserInfoChartdata = new List<ChartVm>()
                {
                    new ChartVm(){name="Subcity",value=subcity},
                    new ChartVm(){name="Woreda",value=woreda},
                    new ChartVm(){name="Level",value=level},
                    new ChartVm(){name="Ownership",value=ownerShip},
                };

                return Ok(vm);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }

        }


        [Authorize(Roles = "Bookstore Employee")]
        [HttpGet("GetBookstoreEmployeeDashboardData/{id}")]
        public async Task<ActionResult> GetBookstoreEmployeeDashboardData( int id)
        {
            try
            {
                var orderList = _db.Orders.Where(m => m.BookstoreEmployeeId == id).AsQueryable();

                BookstoreEmployeeDashboardVm vm = new BookstoreEmployeeDashboardVm();
                vm.PendingRequest = orderList.Where(m => m.BookstoreEmployeeStatus == 1).Count();
                vm.ApprovedRequest = orderList.Where(m => m.BookstoreEmployeeStatus == 5).Count();
                vm.CompletedRequest = orderList.Where(m => m.BookstoreEmployeeStatus == 7).Count();
                vm.RejectedRequest = orderList.Where(m => m.BookstoreEmployeeStatus == 6).Count();
                vm.UnderReviewRequest = orderList.Where(m => m.BookstoreEmployeeStatus == 2).Count();
                vm.PendingForOutOfStock = orderList.Where(m => m.BookstoreEmployeeStatus == 8).Count();
                vm.PendingWithBankSlipRequest = orderList.Where(m => m.BookstoreEmployeeStatus == 3 || m.BookstoreEmployeeStatus == 4).Count();
                vm.TotalBook = _db.HoStocks.Sum(m => m.AvailableQty);
                vm.TotalSchool = _db.Schools.Count();
                vm.TotalPurchase = _db.PurchaseBooks.Count();
                vm.TotalBankSlip = _db.Orders.Where(m => m.File != "").Count();
                vm.TotalRequest = orderList.Count();

                //Subcity Store Request
                var subcityReqList = _db.SubcityOrders.Where(m => m.BookstoreEmployeeId == id).AsQueryable();
                vm.ApprovedSubcityStoreRequest = subcityReqList.Where(m => m.Status == 2).Count();
                vm.CompletedSubcityStoreRequest = subcityReqList.Where(m => m.Status == 3).Count();


                return Ok(vm);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }

        }

        [Authorize(Roles = "School Employee")]
        [HttpGet("GetSchoolDashboardData/{schoolId}")]
        public async Task<ActionResult> GetSchoolDashboardData(int schoolId)
        {
            try
            {

                SchoolDashboardData vm = new SchoolDashboardData();

                vm.PendingRequest = _db.Orders.Where(m => m.Status == 1 || m.Status == 2 || m.Status == 3 || m.Status == 6).Where(e => e.SchoolId == schoolId).Count();
                vm.ApprovedRequest = _db.Orders.Where(m => m.Status == 4 || m.Status==8).Where(e => e.SchoolId == schoolId).Count();
                vm.CompletedRequest = _db.Orders.Where(m => m.Status == 7).Where(e => e.SchoolId == schoolId).Count();
                vm.RejectedRequest = _db.Orders.Where(m => m.Status == 5).Where(e => e.SchoolId == schoolId).Count();

                vm.TotalBook = _db.SchoolStocks.Where(e => e.SchoolId == schoolId).Sum(m => m.Quantity);
                vm.TotalRequest = _db.Orders.Where(e => e.SchoolId == schoolId).Count();


                return Ok(vm);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }

        }

        [Authorize(Roles = "Subcity Manager")]
        [HttpGet("GetSubcityDashboardData/{id}")]
        public async Task<ActionResult> GetSubcityDashboardData(int id)
        {
            try
            {
                //ManagerDashboardVm is used for SubcityStoreVm
                var orderList = _db.Orders.Where(m => m.BookstoreManagerId == id).AsQueryable();
                ManagerDashboardVm vm = new ManagerDashboardVm();


                


                //Subcity Store Request
                var subcityReqList=_db.SubcityOrders.Where(m => m.SubcityManagerId == id).AsQueryable();
                vm.PendingSubcityStoreRequest=subcityReqList.Where(m => m.Status == 1).Count();
                vm.ApprovedSubcityStoreRequest = subcityReqList.Where(m => m.Status == 2).Count();
                vm.CompletedSubcityStoreRequest=subcityReqList.Where(m => m.Status == 3).Count();
                vm.RejectedSubcityStoreRequest = subcityReqList.Where(m => m.Status == 4).Count();
                vm.TotalSubcityStoreRequest = subcityReqList.Count();


                //Book Requeset
                var SubcityId = _db.Users.FirstOrDefault(m => m.Id == id).SubcityId;


                vm.ApprovedRequest = _db.Orders.Where(m => m.Status == 8 && m.SubcityId==SubcityId).Count();
                vm.CompletedRequest = _db.Orders.Where(m => m.Status == 7 && m.SubcityId == SubcityId).Count();

                return Ok(vm);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }

        }


        [Authorize(Roles = "Bookstore Manager")]
        [HttpGet("GetManagerDashboardData/{id}")]
        public async Task<ActionResult> GetManagerDashboardData(int id)
        {
            try
            {
                var orderList = _db.Orders.Where(m => m.BookstoreManagerId == id).AsQueryable();
                ManagerDashboardVm vm = new ManagerDashboardVm();

                vm.PendingRequest = orderList.Where(m => m.BookstoreManagerStatus == 1).Count();
                vm.ApprovedRequest = orderList.Where(m => m.BookstoreManagerStatus == 2 || m.BookstoreManagerStatus == 4).Count();
                vm.RejectedRequest = orderList.Where(m => m.BookstoreManagerStatus == 3).Count();
                vm.TotalRequest = orderList.Where(m => m.BookstoreManagerStatus != 0).Count();

                var purchaseList = _db.Purchases.Where(m => m.BookstoreManagerId == id).AsQueryable();

                vm.PurchasePendingRequest = purchaseList.Where(m => m.Status == 1).Count();
                vm.PurchaseApprovedRequest = purchaseList.Where(m => m.Status == 2 || m.Status == 3).Count();
                vm.PurchaseRejectedRequest = purchaseList.Where(m => m.Status == 4).Count();
                vm.PurchaseTotalRequest = purchaseList.Count();

                vm.TotalBook = _db.HoStocks.Sum(m => m.AvailableQty);
                vm.TotalPurchase = _db.PurchaseBooks.Count();


                //Subcity Store Request
                var subcityReqList = _db.SubcityOrders.Where(m => m.BookstoreManagerId == id).AsQueryable();
                vm.PendingSubcityStoreRequest = subcityReqList.Where(m => m.Status == 1).Count();
                vm.ApprovedSubcityStoreRequest = subcityReqList.Where(m => m.Status == 2).Count();
                vm.CompletedSubcityStoreRequest = subcityReqList.Where(m => m.Status == 3).Count();
                vm.RejectedSubcityStoreRequest = subcityReqList.Where(m => m.Status == 4).Count();
                vm.TotalSubcityStoreRequest = subcityReqList.Count();

                //For Book Chart Data
                var bookList = _db.Books
                    .Include(m => m.BookSubject)
                    .Include(m => m.BookLanguage)
                    .Include(m => m.BookGradeLavel)
                    .AsQueryable();

                var stockList = _db.HoStocks.AsQueryable();
                foreach (var item in bookList)
                {
                    ChartVm subChart = new ChartVm();
                    subChart.name = item.BookSubject.Name;
                    subChart.value = stockList.Where(m => m.BookSubjectId == item.BookSubjectId).Sum(x => x.AvailableQty);
                    vm.SubjectChartdata.Add(subChart);

                    ChartVm gradeChart = new ChartVm();
                    gradeChart.name = item.BookGradeLavel.Name;
                    gradeChart.value = stockList.Where(m => m.BookGradeId == item.BookGradeLavelId).Sum(x => x.AvailableQty);
                    vm.GradeChartdata.Add(gradeChart);

                    ChartVm languageChart = new ChartVm();
                    languageChart.name = item.BookLanguage.Name;
                    languageChart.value = stockList.Where(m => m.BookLanguageId == item.BookLanguageId).Sum(x => x.AvailableQty);
                    vm.LanguageChartdata.Add(languageChart);

                }


                //For Request Chart Data

                DateTime startOfWeek = DateTime.Today.AddDays(
                                        (int)CultureInfo.CurrentCulture.DateTimeFormat.FirstDayOfWeek -
                                        (int)DateTime.Today.DayOfWeek);

                DateTime endOfWeek = startOfWeek.AddDays(6);

                double weekApproved = _db.Orders.Where(m => m.OrderDate >= startOfWeek && m.OrderDate <= endOfWeek && (m.Status == 4 || m.Status == 7)).Count();
                double weekRejected = _db.Orders.Where(m => m.OrderDate >= startOfWeek && m.OrderDate <= endOfWeek && (m.Status == 5 || m.Status == 6)).Count();
                vm.WeekChartData = new List<ChartVm>()
                {
                    new ChartVm(){name="Approved",value=weekApproved},
                    new ChartVm(){name="Rejected",value=weekRejected}
                };



                double monthApproved = _db.Orders.Where(m => m.OrderDate.Month == DateTime.Now.Month && m.OrderDate.Year == DateTime.Now.Year && (m.Status == 4 || m.Status == 7)).Count();
                double monthRejected = _db.Orders.Where(m => m.OrderDate.Month == DateTime.Now.Month && m.OrderDate.Year == DateTime.Now.Year && (m.Status == 5 || m.Status == 6)).Count();
                vm.MonthChartData = new List<ChartVm>()
                {
                    new ChartVm(){name="Approved",value=monthApproved},
                    new ChartVm(){name="Rejected",value=monthRejected}
                };

                double yearApproved = _db.Orders.Where(m => m.OrderDate.Year == DateTime.Now.Year && (m.Status == 4 || m.Status == 7)).Count();
                double yearRejected = _db.Orders.Where(m => m.OrderDate.Year == DateTime.Now.Year && (m.Status == 5 || m.Status == 6)).Count();
                vm.YearChartData = new List<ChartVm>()
                {
                    new ChartVm(){name="Approved",value=yearApproved},
                    new ChartVm(){name="Rejected",value=yearRejected}
                };


                //For User Info Chart Data
                double subcity = _db.Subcities.Count();
                double woreda = _db.Woredas.Count();
                double level = _db.SchoolLavels.Count();
                double ownerShip = _db.Ownerships.Count();
                vm.UserInfoChartdata = new List<ChartVm>()
                {
                    new ChartVm(){name="Subcity",value=subcity},
                    new ChartVm(){name="Woreda",value=woreda},
                    new ChartVm(){name="Level",value=level},
                    new ChartVm(){name="Ownership",value=ownerShip},
                };


                //For Budget Chart Data
                double govtBudget = _db.Budgets.Where(m => m.BudgetTypeId == 1).Sum(x => x.Amount);
                double fundedBudget = _db.Budgets.Where(m => m.BudgetTypeId == 2).Sum(x => x.Amount);

                vm.BudgetChartdata = new List<ChartVm>()
                {
                    new ChartVm(){name="Govt. Budget",value=govtBudget},
                    new ChartVm(){name="Funded Budget ",value=fundedBudget},
                };

                return Ok(vm);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }

        }

        [Authorize(Roles = "Purchase Department")]
        [HttpGet("GetPurchaseDashboardData/{id}")]
        public async Task<ActionResult> GetPurchaseDashboardData(int id)
        {
            try
            {
                var purchaseList = _db.Purchases.Where(m => m.PurcahseDeptEmployeeId == id).AsQueryable();
                PurchaseDashboardVm vm = new PurchaseDashboardVm();

                vm.PendingRequest = purchaseList.Where(m => m.Status == 2).Count();
                vm.CompletedRequest = purchaseList.Where(m => m.Status == 3).Count();
                vm.TotalPurchase = _db.PurchaseBooks.Count();
                vm.TotalRequest = purchaseList.Count();
                vm.TotalSupplier = _db.Suppliers.Count();
                vm.LowStock = _db.HoStocks.Where(m=>m.AvailableQty<100).Count();


                //For Budget Chart Data
                double govtBudget = _db.Budgets.Where(m => m.BudgetTypeId == 1).Sum(x => x.Amount);
                double fundedBudget = _db.Budgets.Where(m => m.BudgetTypeId == 2).Sum(x => x.Amount);

                vm.BudgetChartdata = new List<ChartVm>()
                {
                    new ChartVm(){name="Govt. Budget",value=govtBudget},
                    new ChartVm(){name="Funded Budget ",value=fundedBudget},
                };

                return Ok(vm);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }

        }
    }
}