﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TusharInventory.Entity.Models.Basic;
using TusharInventory.Entity.ViewModels;
using TusharInventory.Reporitory.Dbcontext;
using TusharInventory.Reporitory.Service.IServices.Basic;
using TusharInventory.WebApi.Extensions;

namespace TusharInventory.WebApi.Controllers.Basic
{
    [Route("api/[controller]")]
    [ApiController]
    public class SchoolLavelsController : ControllerBase
    {
        private readonly ISchoolLavelService _service;
        private readonly TusharDbContext _db;

        public SchoolLavelsController(ISchoolLavelService service, TusharDbContext db)
        {
            _service = service;
            _db = db;
        }

        // GET: api/SchoolLavels
        [HttpGet]
        public async Task<IEnumerable<SchoolLavel>> GetAll()
        {
            return await _service.GetManager();
        }

        // GET: api/SchoolLavels/5
        [HttpGet("{id}")]
        public async Task<ActionResult<SchoolLavel>> GetById(int id)
        {
            try
            {
                var model = await _service.GetByIdAsync(id);

                if (model == null)
                {
                    return NotFound();
                }
                return model;
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError); ;
            }

        }

        [Authorize(Roles = "Main Admin")]
        [HttpPut("{id}")]
        [UserActivityFilter]
        public IActionResult Update(int id, BasicModelVm model)
        {
            if (id != model.Id)
            {
                return BadRequest();
            }
            try
            {
                SchoolLavel schoolLavel = new SchoolLavel();
                schoolLavel.NameFile = Encoding.UTF8.GetBytes(model.Name.Trim());
                schoolLavel.Id = id;
                bool isUpdated = _service.Update(schoolLavel);
                return Ok(isUpdated);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }


        [Authorize(Roles = "Main Admin")]
        [HttpPost]
        [UserActivityFilter]
        public async Task<ActionResult<SchoolLavel>> Save(SchoolLavel model)
        {
            try
            {
                model.NameFile = Encoding.UTF8.GetBytes(model.Name.Trim());
                await _service.AddAsync(model);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return CreatedAtAction("GetById", new { id = model.Id }, model);
        }


        [Authorize(Roles = "Main Admin")]
        [HttpDelete("{id}")]
        [UserActivityFilter]
        public async Task<ActionResult<SchoolLavel>> Delete(int id)
        {
            var model = await _service.GetByIdAsync(id);
            if (model == null)
            {
                return NotFound();
            }
            _service.Remove(model);

            return model;
        }

        [HttpPost("CheckName")]
        public ActionResult<SchoolLavel> CheckName(BasicModelVm schoolLavel)
        {
            try
            {

                var existSchoolLavel = _db.SchoolLavels.FirstOrDefault(m => m.NameFile == Encoding.UTF8.GetBytes(schoolLavel.Name.Trim()));
                return existSchoolLavel;
            }
            catch (Exception)
            {

                return StatusCode(500, "Internal Server Error");
            }

        }
    }
}