﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TusharInventory.Entity.Models.Basic;
using TusharInventory.Entity.ViewModels;
using TusharInventory.Reporitory.Dbcontext;
using TusharInventory.WebApi.Extensions;

namespace TusharInventory.WebApi.Controllers.Basic
{
    [Route("api/[controller]")]
    [ApiController]
    public class BudgetsController : ControllerBase
    {

        private readonly TusharDbContext db;

        public BudgetsController( TusharDbContext _db)
        {
            db = _db;
        }

        [Authorize(Roles = "Bookstore Manager,Main Admin")]
        [HttpGet]
        public async Task<IEnumerable<Budget>> GetAll()
        {
            return await db.Budgets.Include(m=>m.BudgetType).ToListAsync();
        }

        [Authorize(Roles = "Bookstore Manager")]
        [HttpGet("{id}")]
        public async Task<ActionResult<Budget>> GetById(int id)
        {
            try
            {
                var model = db.Budgets.FirstOrDefault(m=>m.Id==id);

                if (model == null)
                {
                    return NotFound();
                }
                return model;
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError); ;
            }

        }

        [Authorize(Roles = "Bookstore Manager")]
        [HttpPost]
        [UserActivityFilter]
        public async Task<ActionResult<Budget>> Save(Budget model)
        {
            try
            {
                db.Add(model);
                bool isAdded =await db.SaveChangesAsync()>0;
                return Ok(isAdded);
            }
            catch (Exception ex)
            {
                return StatusCode(500);
            }

        }

        [Authorize(Roles = "Bookstore Manager")]
        [HttpPut("{id}")]
        [UserActivityFilter]
        public async Task<IActionResult> Update(int id, Budget model)
        {
            if (id != model.Id)
            {
                return BadRequest();
            }
            try
            {
                db.Attach(model);
                db.Entry(model).State = EntityState.Modified;
                return Ok(db.SaveChanges() > 0);
            }
            catch (Exception ex)
            {
                return NoContent();
            }
        }

        [HttpGet("GetBudgetType")]
        public async Task<IEnumerable<BudgetType>> GetBudgetType()
        
        {
            return await db.BudgetTypes.ToListAsync();
        }

        [Authorize(Roles = "Main Admin,Bookstore Manager")]
        [HttpPost("GetBudgetBySearch")]
        public async Task<IEnumerable<Budget>> GetBudgetBySearch(LogSearchVm vm)
        {
            var budgetList = db.Budgets.Include(m=>m.BudgetType).AsQueryable();

            if (vm.BudgetTypeId != null)
            {
                budgetList = budgetList.Where(m => m.BudgetTypeId == vm.BudgetTypeId).AsQueryable();
            }

            if (vm.FromDate != null)
            {
                string[] dateValues = vm.FromDate.ToString().Split(' ');
                DateTime fromDate = Convert.ToDateTime(dateValues[0]).Date;

                budgetList = budgetList.Where(m => m.BudgetDate.Date >= fromDate.Date).AsQueryable();
            }

            if (vm.ToDate != null)
            {
                string[] dateValues = vm.ToDate.ToString().Split(' ');
                DateTime toDate = Convert.ToDateTime(dateValues[0]).Date;
                budgetList = budgetList.Where(m => m.BudgetDate.Date <= toDate.Date).AsQueryable();
            }
            return await budgetList.ToListAsync();
        }

        [Authorize(Roles = "Bookstore Manager")]
        [HttpDelete("{id}")]
        [UserActivityFilter]
        public async Task<ActionResult> Delete(int id)
        {
            try
            {
                var model = db.Budgets.FirstOrDefault(m => m.Id == id);
                if (model == null)
                {
                    return NotFound();
                }
                db.Remove(model);
                bool isRemove = await db.SaveChangesAsync() > 0;
                return Ok(isRemove);
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}