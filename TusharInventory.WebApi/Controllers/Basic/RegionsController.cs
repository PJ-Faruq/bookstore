﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TusharInventory.Entity.Models.Basic;
using TusharInventory.Entity.ViewModels;
using TusharInventory.Reporitory.Dbcontext;
using TusharInventory.WebApi.Extensions;

namespace TusharInventory.WebApi.Controllers.Basic
{
    [Route("api/[controller]")]
    [ApiController]
    public class RegionsController : ControllerBase
    {
        private readonly TusharDbContext db;
        public RegionsController(TusharDbContext _db)
        {
            db = _db;
        }

        public async Task<IEnumerable<Region>> GetAll()
        {
            try
            {
                var list = await db.Region.ToListAsync();
                return list;
            }
            catch (Exception e)
            {
                return null;           
            }
            
        }

        // GET: api/Regions/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Region>> GetById(int id)
        {
            try
            {
                var model = db.Region.FirstOrDefault(m=>m.Id==id);

                if (model == null)
                {
                    return NotFound();
                }
                return model;
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError); ;
            }

        }

        [Authorize(Roles = "Main Admin")]
        [HttpPut("{id}")]
        [UserActivityFilter]
        public async Task<IActionResult> Update(int id, BasicModelVm model)
        {
            if (id != model.Id)
            {
                return BadRequest();
            }
            try
            {

                Region region = new Region();
                region.NameFile = Encoding.UTF8.GetBytes(model.Name.Trim());
                region.Id = id;

                db.Region.Attach(region);
                db.Entry(region).State = EntityState.Modified;

                bool isUpdated = db.SaveChanges() > 0;
                return Ok(isUpdated);

                
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        [Authorize(Roles = "Main Admin")]
        [HttpPost]
        [UserActivityFilter]
        public async Task<ActionResult<Region>> Save(Region model)
        {
            try
            {
                model.NameFile = Encoding.UTF8.GetBytes(model.Name.Trim());
                await db.AddAsync(model);
                return Ok(db.SaveChanges() > 0);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        [Authorize(Roles = "Main Admin")]
        [HttpDelete("{id}")]
        [UserActivityFilter]
        public async Task<ActionResult<Region>> Delete(int id)
        {
            var model = db.Region.FirstOrDefault(m=>m.Id==id);
            if (model == null)
            {
                return NotFound();
            }
            db.Region.Remove(model);
            db.SaveChanges();

            return model;
        }

        [HttpPost("CheckName")]
        public ActionResult<Region> CheckName(BasicModelVm region)
        {
            try
            {

                var existRegion = db.Region.FirstOrDefault(m => m.NameFile == Encoding.UTF8.GetBytes(region.Name.Trim()));
                return existRegion;
            }
            catch (Exception)
            {

                return StatusCode(500, "Internal Server Error");
            }

        }
    }
}