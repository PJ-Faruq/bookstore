﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TusharInventory.Entity.Models.Basic;
using TusharInventory.Reporitory.Service.IServices.Basic;
using TusharInventory.WebApi.Extensions;

namespace TusharInventory.WebApi.Controllers.Basic
{
    [Route("api/[controller]")]
    [ApiController]
    public class SuppliersController : ControllerBase
    {
        private readonly ISupplierService _service;

        public SuppliersController(ISupplierService service)
        {
            _service = service;
        }

        // GET: api/Suppliers
        [HttpGet]
        public async Task<IEnumerable<Supplier>> GetAll()
        {
            try
            {
                var list = await _service.GetManager();
                return list;
            }
            catch (Exception e)
            {

                throw;
            }
            
        }

        // GET: api/Suppliers/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Supplier>> GetById(int id)
        {
            try
            {
                var model = await _service.GetByIdAsync(id);

                if (model == null)
                {
                    return NotFound();
                }
                return model;
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError); ;
            }

        }

        [HttpPut("{id}")]
        [UserActivityFilter]
        public async Task<IActionResult> Update(int id, Supplier model)
        {
            if (id != model.Id)
            {
                return BadRequest();
            }
            try
            {
                bool isUpdated=  _service.Update(model);
                return Ok(isUpdated);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        [HttpPost]
        [UserActivityFilter]
        public async Task<ActionResult<Supplier>> Save(Supplier model)
        {
            try
            {
                await _service.AddAsync(model);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return CreatedAtAction("GetById", new { id = model.Id }, model);
        }

        // DELETE: api/Suppliers/5
        [HttpDelete("{id}")]
        [UserActivityFilter]
        public async Task<ActionResult<Supplier>> Delete(int id)
        {
            var model = await _service.GetByIdAsync(id);
            if (model == null)
            {
                return NotFound();
            }
            _service.Remove(model);

            return model;
        }
    }
}