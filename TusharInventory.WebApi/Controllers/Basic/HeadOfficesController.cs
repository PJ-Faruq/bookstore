﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TusharInventory.Entity.Models.Basic;
using TusharInventory.Reporitory.Service.IServices.Basic;

namespace TusharInventory.WebApi.Controllers.Basic
{
    [Route("api/[controller]")]
    [ApiController]
    public class HeadOfficesController : ControllerBase
    {
        private readonly IHeadOfficeService _service;

        public HeadOfficesController(IHeadOfficeService service)
        {
            _service = service;
        }

        // GET: api/HeadOffices
        [HttpGet]
        public async Task<IEnumerable<HeadOffice>> GetAll()
        {
            return await _service.GetManager();
        }

        // GET: api/HeadOffices/5
        [HttpGet("{id}")]
        public async Task<ActionResult<HeadOffice>> GetById(int id)
        {
            try
            {
                var model = await _service.GetByIdAsync(id);

                if (model == null)
                {
                    return NotFound();
                }
                return model;
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError); ;
            }

        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Update(int id, HeadOffice model)
        {
            if (id != model.Id)
            {
                return BadRequest();
            }
            try
            {
                _service.Update(model);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return NoContent();
        }


        [HttpPost]
        public async Task<ActionResult<HeadOffice>> Save(HeadOffice model)
        {
            try
            {
                await _service.AddAsync(model);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return CreatedAtAction("GetById", new { id = model.Id }, model);
        }

        // DELETE: api/HeadOffices/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<HeadOffice>> Delete(int id)
        {
            var model = await _service.GetByIdAsync(id);
            if (model == null)
            {
                return NotFound();
            }
            _service.Remove(model);

            return model;
        }
    }
}