﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TusharInventory.Entity.Models.Basic;
using TusharInventory.Entity.Models.inventory;
using TusharInventory.Reporitory.Dbcontext;
using TusharInventory.Reporitory.Service.IServices.Basic;
using TusharInventory.WebApi.Extensions;

namespace TusharInventory.WebApi.Controllers.Basic
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class BooksController : ControllerBase
    {
        private readonly IBookService _service;
        private readonly TusharDbContext db;

        public BooksController(IBookService service, TusharDbContext _db)
        {

            _service = service;
            db = _db;
        }

        // GET: api/Books
        [HttpGet]
        public async Task<IEnumerable<Book>> GetAll()
        {
            return await _service.GetManager();
        }

        // GET: api/Books/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Book>> GetById(int id)
        {
            try
            {
                var model = await _service.GetByIdAsync(id);

                if (model == null)
                {
                    return NotFound();
                }
                return model;
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError); ;
            }

        }

        [Authorize(Roles = "Main Admin")]
        [HttpPut("{id}")]
        [UserActivityFilter]
        public async Task<IActionResult> Update(int id, Book model)
        {
            if (id != model.Id)
            {
                return BadRequest();
            }
            try
            {
                model.BookGradeLavel = null;
                model.BookLanguage = null;
                model.BookSubject = null;
                _service.Update(model);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return NoContent();
        }


        [Authorize(Roles = "Main Admin")]
        [HttpPost]
        [UserActivityFilter]
        public async Task<ActionResult<Book>> Create(Book model)
        {
            try
            {

                bool isAdded= _service.Add(model);

                if (isAdded)
                {

                    //Add book to Main Store

                    HoStock hoStock = new HoStock();
                    hoStock.BookGradeId = model.BookGradeLavelId;
                    hoStock.BookLanguageId = model.BookLanguageId;
                    hoStock.BookSubjectId = model.BookSubjectId;
                    hoStock.AllocatedQty = 0;
                    hoStock.AvailableQty = 0;
                    hoStock.HoId = -1;

                    db.HoStocks.Add(hoStock);
                    db.SaveChanges();


                    //Add book to All Subcity Store
                    foreach (var item in db.Subcities.ToList())
                    {
                        
                        SubcityStock subcityStock = new SubcityStock();
                        subcityStock.AvailableQty = 0;
                        subcityStock.BookGradeId = model.BookGradeLavelId;
                        subcityStock.BookLanguageId = model.BookLanguageId;
                        subcityStock.BookSubjectId = model.BookSubjectId;
                        subcityStock.SubcityId = item.Id;

                        db.SubcityStocks.Add(subcityStock);
                        db.SaveChanges();
                    }

                    return Ok(isAdded);
                    
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return CreatedAtAction("GetById", new { id = model.Id }, model);
        }


        [Authorize(Roles = "Main Admin")]
        [HttpDelete("{id}")]
        [UserActivityFilter]
        public async Task<ActionResult<Book>> Delete(int id, Book book)
        {
            try
            {
                var model = await _service.GetByIdAsync(id);
                if (model == null)
                {
                    return NotFound();
                }
                bool isDeleted = _service.Remove(model);


                HoStock hoStock = db.HoStocks.FirstOrDefault(m => m.BookSubjectId == model.BookSubjectId && m.BookLanguageId == model.BookLanguageId && m.BookGradeId == model.BookGradeLavelId);
                db.HoStocks.Remove(hoStock);
                db.SaveChanges();

                SubcityStock subcityStock = db.SubcityStocks.FirstOrDefault(m => m.BookSubjectId == model.BookSubjectId && m.BookLanguageId == model.BookLanguageId && m.BookGradeId == model.BookGradeLavelId);
                db.SubcityStocks.Remove(subcityStock);
                db.SaveChanges();



                return model;
            }
            catch (Exception)
            {

                return StatusCode(500, "Internal Server Error");
            }

        }

        [HttpPost("CheckName")]
        public ActionResult<Book> CheckName(Book book)
        {
            try
            {

                var existBook = db.Books.FirstOrDefault(m => m.BookGradeLavelId == book.BookGradeLavelId && m.BookLanguageId==book.BookLanguageId && m.BookSubjectId==book.BookSubjectId);
                return existBook;
            }
            catch (Exception)
            {

                return StatusCode(500, "Internal Server Error");
            }

        }
    }
}