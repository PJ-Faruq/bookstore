﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TusharInventory.Entity.Models.Basic;
using TusharInventory.Reporitory.Dbcontext;
using TusharInventory.Reporitory.Service.IServices.Basic;
using TusharInventory.WebApi.Extensions;

namespace TusharInventory.WebApi.Controllers.Basic
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class LanguagesController : ControllerBase
    {
        private readonly ILanguageService _service;
        private readonly TusharDbContext db;

        public LanguagesController(ILanguageService service, TusharDbContext _db)
        {
            db = _db;
            _service = service;
        }

        // GET: api/Languages
        [HttpGet]
        public async Task<IEnumerable<Language>> GetAll()
        {
            return await _service.GetManager();
        }

        // GET: api/Languages/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Language>> GetById(int id)
        {
            try
            {
                var model = await _service.GetByIdAsync(id);

                if (model == null)
                {
                    return NotFound();
                }
                return model;
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError); ;
            }

        }

        [Authorize(Roles = "Main Admin")]
        [HttpPut("{id}")]
        [UserActivityFilter]
        public async Task<IActionResult> Update(int id, Language model)
        {
            if (id != model.Id)
            {
                return BadRequest();
            }
            try
            {
                bool isUpdated = _service.Update(model);
                return Ok(isUpdated);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        [Authorize(Roles = "Main Admin")]
        [HttpPost]
        [UserActivityFilter]
        public async Task<ActionResult<Language>> Save(Language model)
        {
            try
            {
                await _service.AddAsync(model);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return CreatedAtAction("GetById", new { id = model.Id }, model);
        }

        [Authorize(Roles = "Main Admin")]
        [HttpDelete("{id}")]
        [UserActivityFilter]
        public async Task<ActionResult<Language>> Delete(int id)
        {
            var model = await _service.GetByIdAsync(id);
            if (model == null)
            {
                return NotFound();
            }
            _service.Remove(model);

            return model;
        }

        [HttpPost("CheckName")]
        public ActionResult<Language> CheckName(Language language)
        {
            try
            {

                var existLanguage = db.Languages.FirstOrDefault(m => m.Name == language.Name.Trim());
                return existLanguage;
            }
            catch (Exception)
            {

                return StatusCode(500, "Internal Server Error");
            }

        }
    }
}