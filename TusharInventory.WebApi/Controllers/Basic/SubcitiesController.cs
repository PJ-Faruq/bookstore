﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TusharInventory.Entity.Models.Basic;
using TusharInventory.Entity.Models.inventory;
using TusharInventory.Entity.ViewModels;
using TusharInventory.Reporitory.Dbcontext;
using TusharInventory.Reporitory.Service.IServices.Basic;
using TusharInventory.WebApi.Extensions;

namespace TusharInventory.WebApi.Controllers.Basic
{
    [Route("api/[controller]")]
    [ApiController]
    public class SubcitiesController : ControllerBase
    {
        private readonly ISubcityService _service;
        private readonly TusharDbContext _db;

        public SubcitiesController(ISubcityService service, TusharDbContext db)
        {
            _service = service;
            _db = db;
        }

        // GET: api/Subcitys
        [HttpGet]
        public async Task<IEnumerable<Subcity>> GetAll()
        {
            
            return  _service.GetAll();

        }

        // GET: api/Subcitys/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Subcity>> GetById(int id)
        {
            try
            {
                var model = await _service.GetByIdAsync(id);

                if (model == null)
                {
                    return NotFound();
                }
                return model;
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError); ;
            }

        }


        [Authorize(Roles = "Main Admin")]
        [HttpPut("{id}")]
        [UserActivityFilter]
        public async Task<IActionResult> Update(int id, BasicModelVm model)
        {
            if (id != model.Id)
            {
                return BadRequest();
            }
            try
            {
                Subcity subcity = new Subcity();
                subcity.NameFile= Encoding.UTF8.GetBytes(model.Name.Trim());
                subcity.Id = id;
                bool isUpdated= _service.Update(subcity);
                return Ok(isUpdated);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        [Authorize(Roles = "Main Admin")]
        [HttpPost]
        [UserActivityFilter]
        public async Task<ActionResult<Subcity>> Save(Subcity model)
        {
            try
            {
                model.NameFile = Encoding.UTF8.GetBytes(model.Name.Trim());
                _db.Subcities.Add(model);
                _db.SaveChanges();

                //Add book to All Subcity Store
                foreach (var item in _db.Books.ToList())
                {

                    SubcityStock subcityStock = new SubcityStock();
                    subcityStock.AvailableQty = 0;
                    subcityStock.BookGradeId = item.BookGradeLavelId;
                    subcityStock.BookLanguageId = item.BookLanguageId;
                    subcityStock.BookSubjectId = item.BookSubjectId;
                    subcityStock.SubcityId = model.Id;

                    _db.SubcityStocks.Add(subcityStock);
                    _db.SaveChanges();
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return CreatedAtAction("GetById", new { id = model.Id }, model);
        }


        [Authorize(Roles = "Main Admin")]
        [HttpDelete("{id}")]
        [UserActivityFilter]
        public async Task<ActionResult<Subcity>> Delete(int id)
        {
            var model = await _service.GetByIdAsync(id);
            if (model == null)
            {
                return NotFound();
            }
            _service.Remove(model);

            return model;
        }

        [HttpPost("CheckName")]
        public ActionResult<Subcity> CheckName(BasicModelVm subcity)
        {
            try
            {

                var existSubcity = _db.Subcities.FirstOrDefault(m => m.NameFile == Encoding.UTF8.GetBytes(subcity.Name.Trim()));
                return existSubcity;
            }
            catch (Exception)
            {

                return StatusCode(500, "Internal Server Error");
            }

        }
    }
}