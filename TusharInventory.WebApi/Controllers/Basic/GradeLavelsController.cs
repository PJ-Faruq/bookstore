﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TusharInventory.Entity.Models.Basic;
using TusharInventory.Reporitory.Dbcontext;
using TusharInventory.Reporitory.Service.IServices.Basic;
using TusharInventory.WebApi.Extensions;

namespace TusharInventory.WebApi.Controllers.Basic
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class GradeLavelsController : ControllerBase
    {
        private readonly IGradeLavelService _service;
        private readonly TusharDbContext db;

        public GradeLavelsController(IGradeLavelService service, TusharDbContext _db)
        {
            db = _db;
            _service = service;
        }

        // GET: api/GradeLavels
        [HttpGet]
        public async Task<IEnumerable<GradeLavel>> GetAll()
        {
            return await _service.GetManager();
        }

        // GET: api/GradeLavels/5
        [HttpGet("{id}")]
        public async Task<ActionResult<GradeLavel>> GetById(int id)
        {
            try
            {
                var model = await _service.GetByIdAsync(id);

                if (model == null)
                {
                    return NotFound();
                }
                return model;
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError); ;
            }

        }

        [Authorize(Roles = "Main Admin")]
        [HttpPut("{id}")]
        [UserActivityFilter]
        public async Task<IActionResult> Update(int id, GradeLavel model)
        {
            if (id != model.Id)
            {
                return BadRequest();
            }
            try
            {
                bool isUpdated = _service.Update(model);
                return Ok(isUpdated);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return NoContent();
        }


        [Authorize(Roles = "Main Admin")]
        [HttpPost]
        [UserActivityFilter]
        public async Task<ActionResult<GradeLavel>> Save(GradeLavel model)
        {
            try
            {
                await _service.AddAsync(model);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return CreatedAtAction("GetById", new { id = model.Id }, model);
        }

        [Authorize(Roles = "Main Admin")]
        [HttpDelete("{id}")]
        [UserActivityFilter]
        public async Task<ActionResult<GradeLavel>> Delete(int id)
        {
            var model = await _service.GetByIdAsync(id);
            if (model == null)
            {
                return NotFound();
            }
            _service.Remove(model);

            return model;
        }

        [HttpPost("CheckName")]
        public ActionResult<GradeLavel> CheckName(GradeLavel language)
        {
            try
            {

                var existGradeLavel = db.GradeLavels.FirstOrDefault(m => m.Name == language.Name.Trim());
                return existGradeLavel;
            }
            catch (Exception)
            {

                return StatusCode(500, "Internal Server Error");
            }

        }
    }
}