﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TusharInventory.Entity.Models.Basic;
using TusharInventory.Entity.ViewModels;
using TusharInventory.Reporitory.Dbcontext;
using TusharInventory.WebApi.Extensions;

namespace TusharInventory.WebApi.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class Woreda1sController : ControllerBase
    {
        private readonly TusharDbContext db;
        public Woreda1sController(TusharDbContext _db)
        {
            db = _db;
        }

        [HttpGet]
        public async Task<IEnumerable<Woreda1>> GetAll()
        {
            return await db.Woreda1s.ToListAsync();
        }


        [Authorize(Roles = "Main Admin")]
        [HttpPut("{id}")]
        [UserActivityFilter]
        public async Task<IActionResult> Update(int id, BasicModelVm model)
        {
            if (id != model.Id)
            {
                return BadRequest();
            }
            try
            {
                Woreda1 woreda1 = new Woreda1();
                woreda1.NameFile = Encoding.UTF8.GetBytes(model.Name.Trim());
                woreda1.Id = id;
                db.Attach(woreda1);
                db.Entry(woreda1).State = EntityState.Modified;
                return Ok( await db.SaveChangesAsync() > 0);
            }
            catch (Exception ex)
            {
                return NoContent();
            }
        }

        

        [HttpGet("{id}")]
        public async Task<ActionResult<Woreda1>> GetById(int id)
        {
            try
            {
                var model = await db.Woreda1s.FirstOrDefaultAsync(m=>m.Id==id);

                if (model == null)
                {
                    return NotFound();
                }
                return model;
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError); ;
            }

        }


        [Authorize(Roles = "Main Admin")]
        [HttpPost]
        [UserActivityFilter]
        public async Task<ActionResult> Save(Woreda1 model)
        {
            try
            {
                model.NameFile = Encoding.UTF8.GetBytes(model.Name.Trim());
                await db.AddAsync(model);
                return Ok(db.SaveChanges()>0);
                
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return CreatedAtAction("GetById", new { id = model.Id }, model);
        }

        [HttpPost("CheckName")]
        public ActionResult<Woreda1> CheckName(BasicModelVm woreda1)
        {
            try
            {

                var existWoreda = db.Woreda1s.FirstOrDefault(m => m.NameFile == Encoding.UTF8.GetBytes(woreda1.Name.Trim()));
                return existWoreda;
            }
            catch (Exception)
            {

                return StatusCode(500, "Internal Server Error");
            }

        }


        [Authorize(Roles = "Main Admin")]
        [HttpDelete("{id}")]
        [UserActivityFilter]
        public async Task<ActionResult<Woreda1>> Delete(int id)
        {
            var model = await db.Woreda1s.FirstOrDefaultAsync(m => m.Id == id);
            if (model == null)
            {
                return NotFound();
            }
            db.Woreda1s.Remove(model);
            db.SaveChanges();
            db.Remove(model);

            return model;
        }
    }
}