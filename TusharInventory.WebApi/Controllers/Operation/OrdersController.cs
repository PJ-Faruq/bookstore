﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using TusharInventory.Entity.Models.Basic;
using TusharInventory.Entity.Models.inventory;
using TusharInventory.Entity.Models.Operation;
using TusharInventory.Entity.Models.Settings;
using TusharInventory.Entity.ViewModels;
using TusharInventory.Reporitory.Dbcontext;
using TusharInventory.Reporitory.Service.IServices.Operation;
using TusharInventory.WebApi.Extensions;

namespace TusharInventory.WebApi.Controllers.Operation
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class OrdersController : ControllerBase
    {
        private readonly IOrderService _service;
        private readonly TusharDbContext _db;

        public OrdersController(IOrderService service, TusharDbContext db)
        {
            _service = service;
            _db = db;
        }

        // GET: api/Orders
        [HttpGet]
        
        public async Task<IEnumerable<Order>> GetAll()
        {
            return await _service.GetManager();
        }


        // GET: api/Orders
        [HttpGet("GetOrderCount")]
        public async Task<IActionResult> GetOrderCount()
        {
            DateTime todayDate = DateTime.Now;
            return  Ok(_db.Orders.Where(m => m.OrderDate.Year == todayDate.Year && m.OrderDate.Month == todayDate.Month && m.OrderDate.Day == todayDate.Day).Count());

        }
        

        [HttpGet("{id}/Pending")]
        public async Task<IEnumerable<Order>> GetPendingAll()
        {
            return await _service.GetPendingAll();
        }
        // GET: api/Orders/5

        [HttpGet("{id}")]
        public async Task<ActionResult<Order>> GetById(int id)
        {
            try
            {
                var model = await _service.GetByIdAsync(id);

                if (model == null)
                {
                    return NotFound();
                }
                return model;
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }

        }

        [Authorize(Roles = "School Employee,Bookstore Employee")]
        [HttpPut("{id}")]
        [UserActivityFilter]
        public async Task<IActionResult> Update(int id, Order model)
        {
            if (id != model.Id)
            {
                return BadRequest();
            }
            try
            {
                //if (model.File != null)
                //{
                //    model.Status = 5;
                //}
                bool isUpdate = _service.Update(model);
                return Ok(isUpdate);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            
        }

        [Authorize(Roles = "School Employee,Bookstore Employee,Bookstore Manager,Main Admin, Subcity Manager")]
        [HttpPost("Search")]
        public async Task<IEnumerable<Order>> Search(OrderSearchVm vm)
        {

            
            if (vm.CurrentUserId != null)
            {
                var orderList = _db.Orders.Include(m => m.School).AsQueryable();
                #region Filtering By Different User

                     User user = _db.Users.Include(x => x.Role).FirstOrDefault(m => m.Id == vm.CurrentUserId);

                    //Get Order List for Particular School Employee
                    if (user.Role.Name == "School Employee")
                    {
                        orderList = orderList.Where(m => m.SchoolEmployeeId == user.Id).AsQueryable();
                    }


                    //Get Order List for Particular Bookstore Employee
                    if (user.Role.Name == "Bookstore Employee")
                    {
                        orderList = orderList.Where(m => m.BookstoreEmployeeId == user.Id).AsQueryable();
                    }


                    //Get Order List for Particular Bookstore Manager
                    if (user.Role.Name == "Bookstore Manager")
                    {
                        orderList = orderList.Where(m => m.BookstoreManagerId == user.Id).AsQueryable();
                    }

                    //Get Order List for Particular Subcity Manager
                    if (user.Role.Name == "Subcity Manager")
                    {
                        orderList = orderList.Where(m => m.SubcityId == user.SubcityId).AsQueryable();
                    }


                //For Main Admin No filtering GetAll
                #endregion

                #region Filtering For Different Dashboard 

                    #region Filter For Main Admin Dashboard

                if (vm.AdminStatus != null)
                        {
                            if (vm.AdminStatus == "pending")
                            {
                                orderList = orderList.Where(m => m.Status == 1 || m.Status == 2 || m.Status == 3);
                            }

                            if (vm.AdminStatus == "approved")
                            {
                                orderList = orderList.Where(m => m.Status == 4 || m.Status == 7);
                            }

                            if (vm.AdminStatus == "rejected")
                            {
                                orderList = orderList.Where(m => m.Status == 5 || m.Status == 6);
                            }

                        }

                #endregion

                    #region FIlter For Bookstore Employee Dashboard

                        if(vm.EmpStatus != null)
                        {
                            if (vm.EmpStatus == "pending")
                            {
                                orderList = orderList.Where(m => m.BookstoreEmployeeStatus == 1);
                            }

                            if (vm.EmpStatus == "approved")
                            {
                                orderList = orderList.Where(m => m.BookstoreEmployeeStatus == 5);
                            }

                            if (vm.EmpStatus == "completed")
                            {
                                orderList = orderList.Where(m => m.BookstoreEmployeeStatus == 7);
                            }

                    if (vm.EmpStatus == "rejected")
                            {
                                orderList = orderList.Where(m => m.BookstoreEmployeeStatus == 6);
                            }

                            if (vm.EmpStatus == "under-review")
                            {
                                orderList = orderList.Where(m => m.BookstoreEmployeeStatus == 2);
                            }

                            if (vm.EmpStatus == "outOfStock")
                            {
                                orderList = orderList.Where(m => m.BookstoreEmployeeStatus == 8);
                            }

                    if (vm.EmpStatus == "pendingForBankSlip")
                            {
                                orderList = orderList.Where(m => m.BookstoreEmployeeStatus == 3 || m.BookstoreEmployeeStatus == 4);
                            }
                        }

                #endregion

                    #region FIlter For School Dashboard
                        if (vm.SchoolStatus != null)
                        {
                            if (vm.SchoolStatus == "pending")
                            {
                                orderList = orderList.Where(m => m.Status == 1 || m.Status == 2 || m.Status == 3 || m.Status == 6);
                            }

                            if (vm.SchoolStatus == "approved")
                            {
                                orderList = orderList.Where(m => m.Status == 4 || m.Status==8);
                            }

                            if (vm.SchoolStatus == "completed")
                            {
                                orderList = orderList.Where(m => m.Status == 7);
                            }

                    if (vm.SchoolStatus == "rejected")
                            {
                                orderList = orderList.Where(m => m.Status == 5);
                            }
                        }
                #endregion

                    #region Filtering For Purchase Department
                        if (vm.ManagerStatus == "pending")
                        {
                            orderList = orderList.Where(m => m.BookstoreManagerStatus == 1);
                        }

                        if (vm.ManagerStatus == "approved")
                        {
                            orderList = orderList.Where(m => m.BookstoreManagerStatus == 2 || m.BookstoreManagerStatus == 4);
                        }

                        if (vm.ManagerStatus == "rejected")
                        {
                             orderList = orderList.Where(m => m.BookstoreManagerStatus == 3);
                        }

                #endregion

                    #region FIlter For Subcity Dashboard
                    if (vm.SubcityStatus != null)
                    {

                        if (vm.SubcityStatus == "approved")
                        {
                            orderList = orderList.Where(m => m.Status == 8);
                        }

                        if (vm.SubcityStatus == "completed")
                        {
                            orderList = orderList.Where(m => m.Status == 7);
                        }
                    }
                    #endregion

                #endregion

                return await orderList.ToListAsync();
            }

            if (vm.isNeedBankSlip)
            {
               return await _db.Orders.Include(m => m.School).Where(x=>x.File!="").ToListAsync();
            }

            
            else
            {
                return null;
            }


        }

        [Authorize(Roles = "Main Admin,Bookstore Manager,School Employee")]
        [HttpPost("BookReportSearch")]
        public async Task<IActionResult> BookReportSearch(BookReportVm vm)
        {
            var orderList = _db.Orders
                .Include(m => m.School)
                .Include(m=>m.BookstoreEmployee)
                .Include(m => m.BookstoreManager)
                .Include(m => m.BookstoreEmployee)
                .Include(m => m.SchoolEmployee)
                .AsQueryable();

            if (vm.SchoolId != null)
            {
                orderList = orderList.Where(m => m.SchoolId == vm.SchoolId).AsQueryable();
            }

            if (vm.TrackingNo != null && vm.TrackingNo != "")
            {
                orderList = orderList.Where(m => m.TrackingNo == vm.TrackingNo).AsQueryable();
            }

            if (vm.Status != null)
            {
                // Approved = 1
                if (vm.Status == 1)
                {
                    orderList = orderList.Where(m => m.Status == 4).AsQueryable();
                }

                // Completed = 2
                if (vm.Status == 2)
                {
                    orderList = orderList.Where(m => m.Status == 7).AsQueryable();
                }

                //Approved & Completed = 3
                if (vm.Status == 3)
                {
                    orderList = orderList.Where(m => m.Status == 4 || m.Status==7).AsQueryable();
                }

                //Rejected = 4
                if (vm.Status == 4)
                {
                    orderList = orderList.Where(m => m.Status == 5).AsQueryable();
                }

                //Pending = 5
                if (vm.Status == 5)
                {
                    orderList = orderList.Where(m => m.Status == 1 || m.Status == 2 || m.Status == 3 || m.Status == 6).AsQueryable();
                }

            }

            if (vm.FromDate != null )
            {
                string[] dateValues= vm.FromDate.ToString().Split(' ');
                DateTime fromDate = Convert.ToDateTime(dateValues[0]).Date;

                orderList = orderList.Where(m => m.OrderDate.Date >= fromDate.Date).AsQueryable();
            }

            if (vm.ToDate != null)
            {
                string[] dateValues = vm.ToDate.ToString().Split(' ');
                DateTime toDate = Convert.ToDateTime(dateValues[0]).Date;
                orderList = orderList.Where(m => m.OrderDate.Date <= toDate.Date).AsQueryable();
            }

            //List<BookReportResultVm> list = new List<BookReportResultVm>();

            //foreach (var item in orderList)
            //{
            //    BookReportResultVm resultVm = new BookReportResultVm();
            //    resultVm.Amount = item.Amount;
            //    resultVm.ApprovedBy = item.BookstoreManager.FullName;
            //    resultVm.BankSlip = item.File!="" ? "Uploaded" : "No";
            //    resultVm.Note = item.Note;
            //    resultVm.OrderDate = item.OrderDate.ToShortDateString();
            //    resultVm.OrderId = item.OrderId;
            //    resultVm.ReasonForReject = item.ReasonForReject;
            //    resultVm.RequestedBy = item.SchoolEmployee.FullName;
            //    resultVm.RequestedTo = item.BookstoreEmployee.FullName;
            //    resultVm.SchoolName = item.School.Name;

            //    if (item.Status == 4)
            //    {
            //        resultVm.Status = "Approved";
            //    }
            //    if (item.Status == 7)
            //    {
            //        resultVm.Status = "Completed";
            //    }
            //    if (item.Status == 5)
            //    {
            //        resultVm.Status = "Rejected";
            //    }

            //    resultVm.TrackingNo = item.TrackingNo;

            //    //list.Add(resultVm);

            //}

            return Ok(await orderList.ToListAsync());


        }

        
        [HttpPost("CheckSubcityStock")]
        public async Task<IActionResult> CheckSubcityStock(Order order)
        {

            try
            {
                bool isAvailable = true;
                int? subcityId = 0;

                //Check weather the books are available in the Subcity stock
                foreach (var item in order.OrderLines)
                {
                    SubcityStock stock = _db.SubcityStocks.FirstOrDefault(m => m.BookGradeId == item.BookGradeLavelId && m.BookLanguageId == item.BookLanguageId && m.BookSubjectId == item.BookSubjectId && m.SubcityId == order.School.SchoolSubcityId);
                    School school = _db.Schools.FirstOrDefault(m => m.Id == order.SchoolId);
                    Console.WriteLine(stock);

                    if (school != null)
                    {
                        subcityId = school.SchoolSubcityId;
                    }

                    if (stock != null)
                    {
                        if (stock.AvailableQty < item.Quantity)
                        {
                            isAvailable = false;
                            break;
                        }
                    }

                    if (stock == null)
                    {
                        isAvailable = false;
                        break;
                    }


                }

                if (isAvailable)
                {
                    // Update Allocated Subcity Store 
                    if (subcityId != null)
                    {
                        foreach (var item in order.OrderLines)
                        {
                            // Increase Allocated stock in Subcity Store
                            SubcityStock stock = _db.SubcityStocks.FirstOrDefault(m => m.BookGradeId == item.BookGradeLavelId && m.BookLanguageId == item.BookLanguageId && m.BookSubjectId == item.BookSubjectId && m.SubcityId == order.School.SchoolSubcityId);
                            stock.AllocatedQty = stock.AllocatedQty + item.Quantity;

                            _db.SubcityStocks.Attach(stock);
                            _db.Entry(stock).State = EntityState.Modified;
                            _db.SaveChanges();



                            // Decrease Quantity from Allocated Stock
                            var stockItem = _db.HoStocks.FirstOrDefault(m => m.HoId == -1 && m.BookSubjectId == item.BookSubjectId && m.BookLanguageId == item.BookLanguageId && m.BookGradeId == item.BookGradeLavelId);

                            if (stockItem != null)
                            {
                                stockItem.AllocatedQty = stockItem.AllocatedQty - item.Quantity;

                                _db.HoStocks.Attach(stockItem);
                                _db.Entry(stockItem).State = EntityState.Modified;
                                _db.SaveChanges();
                            }
                        }


                    }

                    return Ok(subcityId);
                }

                return Ok(null);
            }
            catch (Exception)
            {

                return StatusCode(500,"Internal Server Error");
            }

            
        }


        [HttpPut("UpdateAllocatedStock")]
        public IActionResult UpdateAllocatedStock(Order order)
        {
            try
            {

                //Update or Add Allocated Qty in the stock
                foreach (var item in order.OrderLines)
                {

                    var stockItem = _db.HoStocks.FirstOrDefault(m => m.HoId == -1 && m.BookSubjectId == item.BookSubjectId && m.BookLanguageId == item.BookLanguageId && m.BookGradeId == item.BookGradeLavelId);

                    if (stockItem != null)
                    {
                        //OnReject decrease the Allocated stock
                        if (order.Status == 5)
                        {
                            stockItem.AllocatedQty = stockItem.AllocatedQty - item.Quantity;
                        }
                        else
                        {
                            stockItem.AllocatedQty = stockItem.AllocatedQty + item.Quantity;
                        }
                        
                        _db.HoStocks.Attach(stockItem);
                        _db.Entry(stockItem).State = EntityState.Modified;
                        _db.SaveChanges();
                    }

                    else
                    {
                        stockItem = new HoStock();
                        stockItem.HoId = -1;
                        stockItem.BookSubjectId = item.BookSubjectId;
                        stockItem.BookLanguageId = item.BookLanguageId;
                        stockItem.BookGradeId = item.BookGradeLavelId;
                        stockItem.AllocatedQty = item.Quantity;

                        _db.HoStocks.Add(stockItem);
                        _db.SaveChanges();
                    }

                }

                return Ok();
            }
            catch (Exception ex)
            {
                return StatusCode(500, "Some Error Occured");
            }
        }

        [HttpPut("UpdateMainStock")]
        [UserActivityFilter]
        public IActionResult UpdateMainStock(Order order)
        {
            try
            {
                

                
                foreach (var item in order.OrderLines)
                {

                    var stockItem = _db.HoStocks.FirstOrDefault(m => m.HoId == -1 && m.BookSubjectId == item.BookSubjectId && m.BookLanguageId == item.BookLanguageId && m.BookGradeId == item.BookGradeLavelId);

                    if (stockItem != null)
                    {
                        
                       // Decrease Quantity from both Allocated and Main Stock

                        stockItem.AllocatedQty = stockItem.AllocatedQty - item.Quantity;
                        stockItem.AvailableQty = stockItem.AvailableQty - item.Quantity;

                        _db.HoStocks.Attach(stockItem);
                        _db.Entry(stockItem).State = EntityState.Modified;
                        _db.SaveChanges();
                    }

                }

                UpdteSchoolStock(order);

                order.OrderLines = null;
                _db.Entry(order).State = EntityState.Modified;
                 bool isUpdated= _db.SaveChanges() > 0;

                return Ok(isUpdated);
            }
            catch (Exception ex)
            {
                return StatusCode(500, "Some Error Occured");
            }
        }

        [HttpPut("UpdateSubcityStock")]
        [UserActivityFilter]
        public IActionResult UpdateSubcityStock(Order order)
        {
            try
            {
                foreach (var item in order.OrderLines)
                {

                    var stockItem = _db.SubcityStocks.FirstOrDefault(m => m.BookSubjectId == item.BookSubjectId && m.BookLanguageId == item.BookLanguageId && m.BookGradeId == item.BookGradeLavelId && m.SubcityId==order.SubcityId);

                    if (stockItem != null)
                    {

                        // Decrease Quantity from both Allocated and Main Stock

                        stockItem.AllocatedQty = stockItem.AllocatedQty - item.Quantity;
                        stockItem.AvailableQty = stockItem.AvailableQty - item.Quantity;

                        _db.SubcityStocks.Attach(stockItem);
                        _db.Entry(stockItem).State = EntityState.Modified;
                        _db.SaveChanges();
                    }

                }

                UpdteSchoolStock(order);

                order.OrderLines = null;
                _db.Entry(order).State = EntityState.Modified;
                bool isUpdated = _db.SaveChanges() > 0;

                return Ok(isUpdated);
            }
            catch (Exception ex)
            {
                return StatusCode(500, "Some Error Occured");
            }
        }

        private void UpdteSchoolStock(Order order)
        {
            foreach (var item in order.OrderLines)
            {

                var stockItem = _db.SchoolStocks.FirstOrDefault(m => m.SchoolId == order.SchoolId && m.BookSubjectId == item.BookSubjectId && m.BookLanguageId == item.BookLanguageId && m.BookGradeId == item.BookGradeLavelId);

                if (stockItem != null)
                {
                    stockItem.Quantity = stockItem.Quantity + item.Quantity;
                    _db.SchoolStocks.Attach(stockItem);
                    _db.Entry(stockItem).State = EntityState.Modified;
                    _db.SaveChanges();
                }

                else
                {
                    stockItem = new SchoolStock();
                    stockItem.SchoolId = order.SchoolId;
                    stockItem.BookSubjectId = item.BookSubjectId;
                    stockItem.BookLanguageId = item.BookLanguageId;
                    stockItem.BookGradeId = item.BookGradeLavelId;
                    stockItem.Quantity = item.Quantity;

                    _db.SchoolStocks.Add(stockItem);
                    _db.SaveChanges();
                }


            }
        }

        [HttpPut("UpdateStatus/{id}")]
        [UserActivityFilter]
        public async Task<IActionResult> UpdateStatus(int id,Order model)
        {
            try
            {
                //1="new",
                //2="approved",
                //3="rejected",
                //4="accepted",
                //5=bank slip uploaded,

               bool isUpdate= _service.UpdateStatus(model);

                if (isUpdate)
                {
                    return Ok(isUpdate);
                }

                return Ok(false);
            }
            catch (Exception ex)
            {
                return StatusCode(500, "Some Error Occured");
            }
        }


        [HttpPut("UpdateFile")]
        [UserActivityFilter]
        public async Task<IActionResult> UpdateFile()
        {
            try
            {

                int id = Convert.ToInt32(Request.Form["Id"].ToString());
                string file = SaveAndGetImagePath(Request.Form.Files["File"]);

                Order order = _service.GetById(id);
                if (order == null)
                {
                    return NotFound();
                }

                order.File = file;
                order.BookstoreEmployeeStatus = 3;
                order.Status = 3;


                //UpdateStatus Method also works for Update file
                bool isUpdate = _service.UpdateStatus(order);

                if (isUpdate)
                {
                    return Ok(isUpdate);
                }

                return Ok(false);
            }
            catch (Exception ex)
            {
                return StatusCode(500, "Some Error Occured");
            }
        }


        [HttpPut("{id}/ChangeStatus")]
        [UserActivityFilter]
        public async Task<IActionResult> ChangeStatus(int id, Order model)
        {
            try
            {
                //1="new",
                //2="approved",
                //3="rejected",
                //4="accepted",
                //5=bank slip uploaded,


                model.Status = id;
                _service.ChangeStatus(model);

                return NoContent();
            }
            catch (Exception ex)
            {
                return StatusCode(500, "Some Error Occured");
            }
        }

        [Authorize(Roles = "School Employee,Bookstore Employee")]
        [HttpPost]
        [UserActivityFilter]
        public async Task<IActionResult> Save(Order order)
        {
            try
            {
                //order.IsSchool = Convert.ToBoolean(Request.Form["IsSchool"].ToString());

                // order.SchoolEmployeeId = Convert.ToInt32(Request.Form["SchoolEmployeeId"].ToString());
                //order.BookstoreEmployeeId = Convert.ToInt32(Request.Form["BookstoreEmployeeId"].ToString());
                //order.SchoolId = Convert.ToInt32(Request.Form["SchoolId"].ToString());

                //order.Amount = Convert.ToDouble(Request.Form["Amount"].ToString());
                //order.Note = Request.Form["Note"].ToString();
                //order.ReasonForReject = Request.Form["ReasonForReject"].ToString();
                //order.Status = Convert.ToInt32(Request.Form["Status"].ToString());

                //string OrderLines = Request.Form["OrderLine"];

                // Convert Javascript Array to .net List
                //int[] arr = new int[10000];
                //order.OrderLines = (List<OrderLine>)JsonConvert.DeserializeObject(OrderLines,typeof(List<OrderLine>));


                order.OrderDate = DateTime.Now;
                order.BookstoreManagerStatus = 0;
                order.OrderId = GenerateCode();
                order.TrackingNo= GenerateCodeForTracking();
                order.File = "";
                order.HoId = -1;

                bool isAdded=await _service.AddAsync(order);
                if (isAdded)
                {   
                    return Ok(isAdded);
                }

                return Ok(false);

            }
            catch (Exception e)
            {

                return StatusCode(500, "Some Error Occured");
            }
            

        }

        private string GenerateCode()
        {
            string code = "";
            if (_db.Orders.Count() <= 0)
            {
                code = "1";
            }
            else
            {
                code = (_db.Orders.Max(m => m.Id) + 1).ToString();
            }

            string generatedCode = code.ToString();

            if (generatedCode.Length == 1)
            {
                generatedCode = "Ord-" + "0" + "0" + code;
            }
            if (code.Length == 2)
            {
                generatedCode = "Ord-" + "0" + code;
            }
            if (code.Length >= 3)
            {
                generatedCode = "Ord-" + code;
            }

            return generatedCode;

        }

        private string GenerateCodeForTracking()
        {
            string code = "";
            if (_db.Orders.Count() <= 0)
            {
                code = "1";
            }
            else
            {
                code = (_db.Orders.Max(m => m.Id) + 11).ToString();
            }

            string generatedCode = code.ToString();

            if (generatedCode.Length == 1)
            {
                generatedCode = "TR" + "0" + "0" + code;
            }
            if (code.Length == 2)
            {
                generatedCode = "TR" + "0" + code;
            }
            if (code.Length >= 3)
            {
                generatedCode = "TR" + code;
            }

            return generatedCode;

        }
        private string SaveAndGetImagePath(IFormFile image)
        {
            var file = image;
            var folderName = Path.Combine("Resources", "Files");
            var pathToSave = Path.Combine(Directory.GetCurrentDirectory(), folderName);

            if (file != null)
            {
                var fileName = Guid.NewGuid().ToString() + "_" + file.FileName;
                //var fileName = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');
                var fullPath = Path.Combine(pathToSave, fileName);
                var dbPath = Path.Combine(folderName, fileName);

                using (var stream = new FileStream(fullPath, FileMode.Create))
                {
                    file.CopyTo(stream);
                }

                return fileName;
            }
            else
            {
                return "";
            }
        }

        [Authorize(Roles = "School Employee")]
        // DELETE: api/Orders/5
        [HttpDelete("{id}")]
        [UserActivityFilter]
        public async Task<ActionResult<Order>> Delete(int id,Order order)
        {
            try
            {
                var item = order.Amount;
                var model = await _service.GetByIdAsync(id);
                if (model == null)
                {
                    return NotFound();
                }
                _service.Remove(model);

                return model;
            }
            catch (Exception e)
            {

                throw;
            }
            
        }

        [HttpPost("GetBookPrice")]
        public IActionResult GetBookPrice(BookSearchVm searchVm)
        {
            Book book = _db.Books.FirstOrDefault(m => m.BookSubjectId == searchVm.BookSubjectId && m.BookGradeLavelId == searchVm.BookGradeLavelId && m.BookLanguageId == searchVm.BookLanguageId);
            if (book == null)
            {
                return Ok(null);
            }
            return Ok(book.SalePrice);
        }


    }
}