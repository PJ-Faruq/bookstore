﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TusharInventory.Entity.Models.Operation;
using TusharInventory.Reporitory.Service.IServices.Operation;

namespace TusharInventory.WebApi.Controllers.Operation
{
    [Route("api/[controller]")]
    [ApiController]
    public class SalesController : ControllerBase
    {
        private readonly ISaleService _service;

        public SalesController(ISaleService service)
        {
            _service = service;
        }

        // GET: api/Sales
        [HttpGet]
        public async Task<IEnumerable<Sale>> GetAll()
        {
            return await _service.GetManager();
        }

        // GET: api/Sales/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Sale>> GetById(int id)
        {
            try
            {
                var model = await _service.GetByIdAsync(id);

                if (model == null)
                {
                    return NotFound();
                }
                return model;
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError); ;
            }

        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Update(int id, Sale model)
        {
            if (id != model.Id)
            {
                return BadRequest();
            }
            try
            {
                _service.Update(model);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return NoContent();
        }


        [HttpPost]
        public async Task<ActionResult<Sale>> Save(Sale model)
        {
            try
            {
                await _service.AddAsync(model);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return CreatedAtAction("GetById", new { id = model.Id }, model);
        }

        // DELETE: api/Sales/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Sale>> Delete(int id)
        {
            var model = await _service.GetByIdAsync(id);
            if (model == null)
            {
                return NotFound();
            }
            _service.Remove(model);

            return model;
        }
    }
}