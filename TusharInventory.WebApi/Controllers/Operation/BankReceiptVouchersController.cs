﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TusharInventory.Entity.Models.Operation;
using TusharInventory.Entity.ViewModels;
using TusharInventory.Reporitory.Dbcontext;
using TusharInventory.WebApi.Extensions;

namespace TusharInventory.WebApi.Controllers.Operation
{
    [Route("api/[controller]")]
    [ApiController]
    public class BankReceiptVouchersController : ControllerBase
    {
        private readonly TusharDbContext db;

        public BankReceiptVouchersController(TusharDbContext _db)
        {
            db = _db;
        }

        [HttpGet]
        public async Task<IEnumerable<BankReceiptVoucher>> GetAll()
        {
            return await db.BankReceiptVouchers.ToListAsync();
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<BankReceiptVoucher>> GetById(int id)
        {
            try
            {
                var model = db.BankReceiptVouchers
                    .Include(m=>m.School)
                    .Include(m => m.Subcity)
                    .Include(m => m.BookstoreEmployee)
                    .FirstOrDefault(m=>m.OrderId==id);

                if (model == null)
                {
                    return NotFound();
                }
                return model;
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }

        }

        [Authorize(Roles = "Bookstore Employee")]
        [HttpGet("GetReceiptAndSlip")]
        public async Task<IEnumerable<BankReceiptSearchVm>> GetReceiptAndSlip()
        {
            List<BankReceiptSearchVm> receiptList = new List<BankReceiptSearchVm>();
            var model = db.Orders.Where(m=>m.File !="" && m.Status==7).Include(x => x.School).ToList();

            foreach (var item in model)
            {
                BankReceiptSearchVm vm = new BankReceiptSearchVm();
                vm.OrderId = item.Id;
                vm.OrderNo = item.OrderId;
                vm.SchoolName = item.School.Name;
                vm.TotalAmount = item.Amount;
                vm.File = item.File;

                var bankReceipt = db.BankReceiptVouchers.FirstOrDefault(m => m.OrderId == item.Id);
                if (bankReceipt != null)
                {
                    vm.BankReceiptId = bankReceipt.Id;
                }

                receiptList.Add(vm);
            }
            

            return receiptList;
        }

        [HttpPost("Create")]
        public async Task<IActionResult> Create(BankReceiptVoucher model)
        {
            try
            {
                db.BankReceiptVouchers.Add(model);
                bool isAdded = await db.SaveChangesAsync() > 0;
                return Ok(isAdded);
            }
            catch (Exception ex)
            {
                return StatusCode(500);
            }

        }


        [HttpDelete("{id}")]
        [UserActivityFilter]
        public async Task<ActionResult> Delete(int id)
        {
            try
            {
                var model = db.BankReceiptVouchers.FirstOrDefault(m => m.Id == id);
                if (model == null)
                {
                    return NotFound();
                }
                db.Remove(model);
                bool isRemove = await db.SaveChangesAsync() > 0;
                return Ok(isRemove);
            }
            catch (Exception)
            {

                throw;
            }



        }
    }
}