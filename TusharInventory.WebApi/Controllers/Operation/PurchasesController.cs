﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.EntityFrameworkCore;
using TusharInventory.Entity.Models.inventory;
using TusharInventory.Entity.Models.Operation;
using TusharInventory.Entity.Models.Settings;
using TusharInventory.Reporitory.Dbcontext;
using TusharInventory.Reporitory.Service.IServices.Operation;
using TusharInventory.WebApi.Extensions;

namespace TusharInventory.WebApi.Controllers.Operation
{
    [Route("api/[controller]")]
    [ApiController]
    public class PurchasesController : ControllerBase
    {
        private readonly IPurchaseService _service;

        private readonly TusharDbContext db;

        public PurchasesController(IPurchaseService service,TusharDbContext _db)
        {
            _service = service;
            db = _db;
        }


        public async Task<IEnumerable<Purchase>> GetAll()
        {
            return await _service.GetManager();
        }

        [HttpGet("{id}")]
        public async Task<ActionResult> GetById(int id)
        {
            try
            {
                var model = await _service.GetByIdAsync(id);

                if (model == null)
                {
                    return NotFound();
                }
                return Ok(model);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError); ;
            }

        }

        [HttpPost]
        [UserActivityFilter]
        public async Task<IActionResult> Add(Purchase purchase)
        {
            try
            {
                double amount = 0;
                foreach (var item in purchase.PurchaseLines)
                {
                    amount = amount + (item.BookPrice*item.PurchaseQuantity);
                }

                purchase.Amount = amount;

                purchase.Date = DateTime.Now;
                bool isAdded =await _service.AddAsync(purchase);
                if (isAdded)
                {
                    return Ok(isAdded);
                }

                return Ok(false);
            }
            catch (Exception e)
            {

                throw;
            }

           
        }

        [HttpGet("GetBookstoreManager")]
        public async Task<IEnumerable<User>> GetBookstoreManager()
        {
            return await _service.GetBookstoreManager();
        }

        [HttpGet("GetPurchaseEmployee")]
        public async Task<IEnumerable<User>> GetPurchaseEmployee()
        {
            return await _service.GetPurchaseEmployee();
        }


        [HttpPut("ChangeStatus")]
        [UserActivityFilter]
        public async Task<IActionResult> ChangeStatus(Purchase purchase)
        {
            try
            {
                db.Entry(purchase).State = EntityState.Modified;
                return Ok(await db.SaveChangesAsync() > 0);
          
            }
            catch (Exception e)
            {

                return StatusCode(StatusCodes.Status500InternalServerError);
            }

            
        }

        [HttpPost("AddBook")]
        [UserActivityFilter]
        public async Task<IActionResult> AddBook(PurchaseBook purchaseBook)
        {
            try
            {
                db.Add(purchaseBook);
                bool isAdded = db.SaveChanges() > 0;

                if (isAdded)
                {
                   

                    foreach (var item in purchaseBook.PurchaseBookLines)
                    {

                        var stockItem = db.HoStocks.FirstOrDefault(m => m.HoId == -1 && m.BookSubjectId == item.BookSubjectId && m.BookLanguageId == item.BookLanguageId && m.BookGradeId == item.BookGradeLavelId);

                        if (stockItem != null)
                        {
                            stockItem.AvailableQty = stockItem.AvailableQty + item.Quantity;
                            db.HoStocks.Attach(stockItem);
                            db.Entry(stockItem).State = EntityState.Modified;
                            db.SaveChanges();
                        }

                        else
                        {
                            stockItem = new HoStock();
                            stockItem.HoId = -1;
                            stockItem.BookSubjectId = item.BookSubjectId;
                            stockItem.BookLanguageId = item.BookLanguageId;
                            stockItem.BookGradeId = item.BookGradeLavelId;
                            stockItem.AvailableQty = item.Quantity;

                            db.HoStocks.Add(stockItem);
                            db.SaveChanges();
                        }

                        
                    }


                }
                return Ok(isAdded);

            }
            catch (Exception e)
            {

                return StatusCode(StatusCodes.Status500InternalServerError);
            }


        }



    }
}