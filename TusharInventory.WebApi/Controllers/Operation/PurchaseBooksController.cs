﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TusharInventory.Entity.Models.Basic;
using TusharInventory.Entity.Models.inventory;
using TusharInventory.Entity.Models.Operation;
using TusharInventory.Entity.ViewModels;
using TusharInventory.Reporitory.Dbcontext;
using TusharInventory.WebApi.Extensions;

namespace TusharInventory.WebApi.Controllers.Operation
{
    [Route("api/[controller]")]
    [ApiController]
    public class PurchaseBooksController : ControllerBase
    {
        private readonly TusharDbContext db;

        public PurchaseBooksController(TusharDbContext _db)
        {
            db = _db;
        }

        [Authorize(Roles = "Bookstore Employee,Bookstore Manager,Purchase Department")]
        [HttpGet]
        public async Task<IEnumerable<PurchaseBook>> GetAll()
        {
            return await db.PurchaseBooks
                .Include(m=>m.Supplier)
                .Include(m => m.PurcahseDeptEmployee)
                .Include(m=>m.BudgetType)
                .ToListAsync();
        }

        [HttpPost("GetBySearch")]
        public async Task<IEnumerable<PurchaseBook>> GetBySearch( PurchaseBookSearchVm vm)
        {
            var list = db.PurchaseBooks
                .Include(m => m.Supplier)
                .Include(m => m.PurcahseDeptEmployee)
                .Include(m => m.BudgetType)
                .Include(m => m.BookstoreEmployee)
                .Include(m => m.BookstoreManager)
                .AsQueryable();

            if (vm.BudgetTypeId != null)
            {
                list = list.Where(m => m.BudgetTypeId == vm.BudgetTypeId).AsQueryable();
            }

            if (vm.PurchaseId != null)
            {
                list = list.Where(m => m.PurchaseId.ToLower() == vm.PurchaseId.Trim().ToLower()).AsQueryable();
            }

            if (vm.SupplierId != null)
            {
                list = list.Where(m => m.SupplierId == vm.SupplierId).AsQueryable();
            }

            if (vm.FromDate != null)
            {
                string[] dateValues = vm.FromDate.ToString().Split(' ');
                DateTime fromDate = Convert.ToDateTime(dateValues[0]).Date;

                list = list.Where(m => m.PurchaseDate.Date >= fromDate.Date).AsQueryable();
            }

            if (vm.ToDate != null)
            {
                string[] dateValues = vm.ToDate.ToString().Split(' ');
                DateTime toDate = Convert.ToDateTime(dateValues[0]).Date;
                list = list.Where(m => m.PurchaseDate.Date <= toDate.Date).AsQueryable();
            }

            return await list.ToListAsync();
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<PurchaseBook>> GetById(int id)
        {
            try
            {
                var model = await db.PurchaseBooks
                    .Include(m => m.Supplier)
                    .Include(m => m.BudgetType)
                    .Include(m => m.BookstoreEmployee)
                    .Include(m => m.BookstoreManager)
                    .Include(m => m.PurcahseDeptEmployee)
                    .Include(m => m.Supplier)
                    .Include(m => m.PurchaseBookLines).ThenInclude(m => m.BookSubject)
                    .Include(m => m.PurchaseBookLines).ThenInclude(m => m.BookGradeLavel)
                    .Include(m => m.PurchaseBookLines).ThenInclude(m => m.BookLanguage)
                    .FirstOrDefaultAsync(m=>m.Id==id);

                if (model == null)
                {
                    return NotFound();
                }
                return model;
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }

        }

        [HttpGet("GetByPurchaseRequestId/{id}")]
        public async Task<ActionResult<PurchaseBook>> GetByPurchaseRequestId(int id)
        {
            try
            {
                var model = await db.PurchaseBooks
                    .Include(m => m.Supplier)
                    .Include(m => m.BudgetType)
                    .Include(m=>m.BookstoreEmployee)
                    .Include(m => m.BookstoreManager)
                    .Include(m => m.PurcahseDeptEmployee)
                    .Include(m => m.Supplier)
                    .Include(m => m.PurchaseBookLines).ThenInclude(m => m.BookSubject)
                    .Include(m => m.PurchaseBookLines).ThenInclude(m => m.BookGradeLavel)
                    .Include(m => m.PurchaseBookLines).ThenInclude(m => m.BookLanguage)
                    .FirstOrDefaultAsync(m => m.PurchaseRequestId == id);

                if (model == null)
                {
                    return model;
                }
                return model;
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }

        }

        [HttpPost]
        [UserActivityFilter]
        public async Task<ActionResult> Add(PurchaseBook book)
        {
            try
            {

                //Add PurchaseBook to Db
                book.PurchaseDate = DateTime.Now;
                book.PurchaseId = GenerateCode();
                await db.PurchaseBooks.AddAsync(book);
                bool isAdded = db.SaveChanges() > 0;

                //Deduct amount from the budget
                Budget budget = db.Budgets.FirstOrDefault(m => m.BudgetTypeId == book.BudgetTypeId && m.BudgetDate.Year == DateTime.Now.Year);
                budget.Amount = budget.Amount - book.Amount;
                db.Budgets.Attach(budget);
                db.Entry(budget).State = EntityState.Modified;
                db.SaveChanges();


                //Update the Stock
                foreach (var item in book.PurchaseBookLines)
                {

                    var stockItem = db.HoStocks.FirstOrDefault(m => m.HoId == -1 && m.BookSubjectId == item.BookSubjectId && m.BookLanguageId == item.BookLanguageId && m.BookGradeId == item.BookGradeLavelId);

                    if (stockItem != null)
                    {
                        stockItem.AvailableQty = stockItem.AvailableQty + item.Quantity;
                        db.HoStocks.Attach(stockItem);
                        db.Entry(stockItem).State = EntityState.Modified;
                        db.SaveChanges();
                    }

                    else
                    {
                        stockItem = new HoStock();
                        stockItem.HoId = -1;
                        stockItem.BookSubjectId = item.BookSubjectId;
                        stockItem.BookLanguageId = item.BookLanguageId;
                        stockItem.BookGradeId = item.BookGradeLavelId;
                        stockItem.AvailableQty = item.Quantity;

                        db.HoStocks.Add(stockItem);
                        db.SaveChanges();
                    }


                }        
                return Ok(isAdded);

            }
            catch (Exception ex)
            {
                return StatusCode(500, "Internal Server Error");
            }

            
        }

        private string GenerateCode()
        {
            string code = "";
            if (db.PurchaseBooks.Count() <= 0)
            {
                code = "1";
            }
            else
            {
                code = (db.PurchaseBooks.Max(m => m.Id) + 1).ToString();
            }

            string generatedCode = code.ToString();

            if (generatedCode.Length == 1)
            {
                generatedCode = "PN-" + "0" + "0" + code;
            }
            if (code.Length == 2)
            {
                generatedCode = "PN-" + "0" + code;
            }
            if (code.Length >= 3)
            {
                generatedCode = "PN-" + code;
            }

            return generatedCode;

        }

        [HttpGet("GetBudget/{id}")]
        public ActionResult GetBudget(int id)
        {
            try
            {
                int year = DateTime.Now.Year;
                double amount = db.Budgets.Where(m => m.BudgetTypeId == id && m.BudgetDate.Year == year).Sum(m => m.Amount);

                return Ok(amount);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }

        }


        [HttpPost("GetBookPurchasePrice")]
        public IActionResult GetBookPurchasePrice(BookSearchVm searchVm)
        {
            try
            {
                Book book = db.Books.FirstOrDefault(m => m.BookSubjectId == searchVm.BookSubjectId && m.BookGradeLavelId == searchVm.BookGradeLavelId && m.BookLanguageId == searchVm.BookLanguageId);
                if (book == null)
                {
                    return Ok(null);
                }
                return Ok(book.PurchasePrice);
            }
            catch (Exception)
            {

                return StatusCode(500, "Internal Server Error");
            }

           
        }

        [HttpGet("GetPurchaseBookByPurchaseId/{purchaseId}")]
        public IActionResult GetPurchaseBookByPurchaseId(int purchaseId)
        {
            try
            {
               var book= db.PurchaseBooks.FirstOrDefault(m => m.PurchaseRequestId == purchaseId);
                if(book != null)
                {
                    return Ok(book.Id);
                }

                return Ok(null);
            }
            catch (Exception)
            {

                return StatusCode(500, "Internal Server Error");
            }


        }

        

    }
}