﻿using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using TusharInventory.Entitiey.ViewModels;
using TusharInventory.Entity.Models.Settings;
using TusharInventory.Reporitory.Dbcontext;

namespace TusharInventory.WebApi.Extensions
{
    public class UserActivityFilter : ActionFilterAttribute
    {
        


        public override void OnActionExecuted(ActionExecutedContext context)
        {
            //throw new NotImplementedException();
        }

        public override void OnActionExecuting(ActionExecutingContext context)
        {
            try
            {
                string data = "";

                
                var routeData = context.RouteData;
                var controller = routeData.Values["controller"];
                var action = routeData.Values["action"];

                var url = $"{controller}/{action}";

                // if value comes in QueryString
                if (!string.IsNullOrEmpty(context.HttpContext.Request.QueryString.Value))
                {
                    data = context.HttpContext.Request.QueryString.Value;
                }

                //if value comes in FormData
                if (context.HttpContext.Request.HasFormContentType)
                {

                    var Params = context.HttpContext.Request.Form;
                    var dictionary = Params.ToDictionary(pair => pair.Key, pair => pair.Value);
                    var convertedValue = JsonConvert.SerializeObject(dictionary);
                    data = convertedValue;
                }
                else
                {
                    //if value comes in Model
                    var arguments = context.ActionArguments;

                    // For Update Operation . Two parameter comes one is Id and another is Model
                    if (arguments.Count > 1)
                    {
                        var val = arguments.Skip(1).FirstOrDefault().Value;
                        data = JsonConvert.SerializeObject(val);
                        
                    }
                    else
                    {
                        var value = arguments.FirstOrDefault().Value;
                        var convertedValue = JsonConvert.SerializeObject(value);
                        data = convertedValue;

                        
                    }

                }

                // var user = context.HttpContext.User.Identity.Name;
                // var ipAddress = context.HttpContext.Connection.RemoteIpAddress.ToString();

                UserActivity activity = new UserActivity();
               
                var db = context.HttpContext.RequestServices.GetRequiredService<TusharDbContext>();

                if (action.ToString() != "Authenticate")
                {
                    var userList = context.HttpContext.User.Claims.ToList();
                    activity.Url = url;
                    activity.Data = data;

                    if (userList.Count > 0)
                    {
                        activity.UserName = userList[0].Value;
                    }
                    
                    var user = db.Users.FirstOrDefault(m => m.Email == activity.UserName);
                    if(user != null)
                    {
                        activity.UserId = db.Users.FirstOrDefault(m => m.Email == activity.UserName).Id;
                    }
                    
                    db.UserActivities.Add(activity);
                    db.SaveChanges();
                }

                else
                {
                    // If Incorrect username or password then not save in audit table
                    UserVm value = (UserVm)context.ActionArguments.FirstOrDefault().Value;
                    var user = db.Users.FirstOrDefault(m => m.Email == value.Email && m.Password == value.Password);
                    if (user!=null)
                    {
                        activity.UserName = value.Email;
                        activity.Url = "Users/Login";
                        activity.Data = data;
                        activity.UserId = user.Id;
                        db.UserActivities.Add(activity);
                        db.SaveChanges();
                    }
                }

                

                
            }
            catch (Exception e)
            {

                throw;
            }

            
        }
    }
}
