using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using TusharInventory.Reporitory.Dbcontext;
using TusharInventory.Reporitory.Helpers;
using TusharInventory.Reporitory.Service.IServices.Basic;
using TusharInventory.Reporitory.Service.IServices.Inventory;
using TusharInventory.Reporitory.Service.IServices.Operation;
using TusharInventory.Reporitory.Service.IServices.Settings;
using TusharInventory.Reporitory.Service.Services.Basic;
using TusharInventory.Reporitory.Service.Services.Inventory;
using TusharInventory.Reporitory.Service.Services.Operation;
using TusharInventory.Reporitory.Service.Services.Settings;
using TusharInventory.WebApi.Extensions;

namespace TusharInventory.WebApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.ConfigureCors();
            services.ConfigureIISIntegration();
            services.AddControllers().AddNewtonsoftJson(options =>
            {
                options.UseMemberCasing();
            });

            services.AddControllers().AddNewtonsoftJson(x => x.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore);

            services.Configure<FormOptions>(o =>
            {
                o.ValueLengthLimit = int.MaxValue;
                o.MultipartBodyLengthLimit = int.MaxValue;
                o.MemoryBufferThreshold = int.MaxValue;
            });


            services.AddDbContext<TusharDbContext>(options =>
             options.UseSqlServer(Configuration.GetConnectionString("TusharInventoryDbConnection")));


            // configure strongly typed settings objects
            var appSettingsSection = Configuration.GetSection("AppSettings");
            services.Configure<AppSettings>(appSettingsSection);

            // configure jwt authentication
            var appSettings = appSettingsSection.Get<AppSettings>();
            var key = Encoding.ASCII.GetBytes(appSettings.Secret);
            services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(x =>
            {
                x.RequireHttpsMetadata = false;
                x.SaveToken = true;
                x.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateIssuer = false,
                    ValidateAudience = false
                };
            });



            // configure DI for application services

            services.AddScoped<IBookService, BookService>();
            services.AddScoped<IHeadOfficeService, HeadOfficeService>();
            services.AddScoped<IGradeLavelService, GradeLavelService>();
            services.AddScoped<ILanguageService, LanguageService>();
            services.AddScoped<IOwnershipService, OwnershipService>();
            services.AddScoped<ISchoolService, SchoolService>();
            services.AddScoped<ISchoolLavelService, SchoolLavelService>();
            services.AddScoped<ISubcityService, SubcityService>();
            services.AddScoped<ISubjectService, SubjectService>();
            services.AddScoped<ISupplierService, SupplierService>();
            services.AddScoped<IWoredaService, WoredaService>();

            services.AddScoped<IHoStockService, HoStockService>();
            services.AddScoped<ISchoolStockService, SchoolStockService>();

            services.AddScoped<IOrderService, OrderService>();
            services.AddScoped<IPurchaseService, PurchaseService>();
            services.AddScoped<ISaleService, SaleService>();
            services.AddScoped<IReceiveService, ReceiveService>();

            services.AddScoped<IRoleService, RoleService>();
            services.AddScoped<IUserService, UserService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.Use(async (context, next) =>
                {
                    await next();
                    if (context.Response.StatusCode == 404 && !Path.HasExtension(context.Request.Path.Value))
                    {
                        context.Request.Path = "/index.html";
                        await next();
                    }
                });
                app.UseHsts();
            }



            app.UseAuthentication();
            app.UseCors("CorsPolicy");
            app.UseRouting();

            //To Make "Resources" Folder Serverable
            app.UseStaticFiles(new StaticFileOptions()
            {
                FileProvider = new PhysicalFileProvider(Path.Combine(Directory.GetCurrentDirectory(), @"Resources")),
                RequestPath = new PathString("/Resources")
            });

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
            app.UseStaticFiles();
            
        }
    }
}
